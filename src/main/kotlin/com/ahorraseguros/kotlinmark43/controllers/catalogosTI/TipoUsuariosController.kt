package com.ahorraseguros.kotlinmark43.controllers.catalogosTI


import com.ahorraseguros.kotlinmark43.models.catalogosTI.TipoUsuarioModels
import com.ahorraseguros.kotlinmark43.repositories.TipoUsuarioRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tipo-usuarios")
class TipoUsuariosController {

    @Autowired
    lateinit var tipoUsuarioRepository: TipoUsuarioRepository

    @GetMapping
    fun findAll(): MutableList<TipoUsuarioModels> = tipoUsuarioRepository.findAll()

    @GetMapping("{id}")
    fun finById(@PathVariable("id") id: Long): ResponseEntity<TipoUsuarioModels> = tipoUsuarioRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Long,
               @Valid @RequestBody tipoUsuarioModels: TipoUsuarioModels): TipoUsuarioModels {
        val tipoUsuarioUpdate = tipoUsuarioRepository.findById(id)
        tipoUsuarioModels.id = tipoUsuarioUpdate.get().id
        return tipoUsuarioRepository.save(tipoUsuarioModels)
    }

    @PostMapping
    fun create(@Valid @RequestBody tipoUsuarioModels: TipoUsuarioModels): TipoUsuarioModels = tipoUsuarioRepository.save(tipoUsuarioModels)
    

}