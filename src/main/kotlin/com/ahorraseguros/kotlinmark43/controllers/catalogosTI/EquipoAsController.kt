package com.ahorraseguros.kotlinmark43.controllers.catalogosTI

import com.ahorraseguros.kotlinmark43.models.catalogosTI.EquiposAsModel

import com.ahorraseguros.kotlinmark43.repositories.catalogosTI.EquipoAsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/equipos")
class EquipoAsController {

    @Autowired
    lateinit var equipoAsRepository: EquipoAsRepository

    @GetMapping
    fun findAll(): MutableList<EquiposAsModel> = equipoAsRepository.findAll()

    @GetMapping("{id}")
    fun finById(@PathVariable("id") id: Long): ResponseEntity<EquiposAsModel> = equipoAsRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody tipoUsuarioModels: EquiposAsModel): EquiposAsModel = equipoAsRepository.save(tipoUsuarioModels)


}