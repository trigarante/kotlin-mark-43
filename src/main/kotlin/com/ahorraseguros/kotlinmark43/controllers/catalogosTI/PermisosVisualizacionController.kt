package com.ahorraseguros.kotlinmark43.controllers.catalogosTI

import com.ahorraseguros.kotlinmark43.models.catalogosTI.PermisosVisualizacionModels
import com.ahorraseguros.kotlinmark43.repositories.catalogosTI.PermisosVisualizacionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/permisos-visualizacion")
class PermisosVisualizacionController {
    @Autowired
    lateinit var permisosVisualizacionRepository: PermisosVisualizacionRepository

    @GetMapping
    fun findAll(): MutableList<PermisosVisualizacionModels> = permisosVisualizacionRepository.findAll()

    @GetMapping("{id}")
    fun finById(@PathVariable("id") id: Long): ResponseEntity<PermisosVisualizacionModels> = permisosVisualizacionRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody tipoUsuarioModels: PermisosVisualizacionModels): PermisosVisualizacionModels = permisosVisualizacionRepository.save(tipoUsuarioModels)
}