package com.ahorraseguros.kotlinmark43.controllers.landing

import com.ahorraseguros.kotlinmark43.models.landing.TacubaNivelesModels
import com.ahorraseguros.kotlinmark43.models.landing.views.TacubaNivelesViewModels
import com.ahorraseguros.kotlinmark43.repositories.landing.TacubaNivelesRepository
import com.ahorraseguros.kotlinmark43.repositories.landing.views.TacubaNivelesViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/landing-tacuba-niveles")
class TacubaNivelesController {
    @Autowired
    lateinit var tacubaNivelesRepository: TacubaNivelesRepository
    @Autowired
    lateinit var tacubaNivelesViewRepository: TacubaNivelesViewRepository

    @GetMapping
    fun getAll() : MutableList<TacubaNivelesViewModels> = tacubaNivelesViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<TacubaNivelesViewModels> = tacubaNivelesViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody tacubaNivelesModel: TacubaNivelesModels): TacubaNivelesModels = tacubaNivelesRepository.save(tacubaNivelesModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Int,
               @Valid @RequestBody tacubaNivelesModel: TacubaNivelesModels): TacubaNivelesModels {
        val tacubaNivelesUdate = tacubaNivelesRepository.findById(id)
        tacubaNivelesModel.id = tacubaNivelesUdate.get().id
        return tacubaNivelesRepository.save(tacubaNivelesModel)
    }


}