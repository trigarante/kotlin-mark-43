package com.ahorraseguros.kotlinmark43.controllers.landing

import com.ahorraseguros.kotlinmark43.models.landing.PisosModels
import com.ahorraseguros.kotlinmark43.repositories.landing.PisosRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/landing-pisos")
class PisosController {
    @Autowired
    lateinit var pisosRepository: PisosRepository

    @GetMapping
    fun getAll() : MutableList<PisosModels> = pisosRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<PisosModels> = pisosRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody pisosModel: PisosModels): PisosModels = pisosRepository.save(pisosModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Int,
               @Valid @RequestBody pisosModel: PisosModels): PisosModels {
        val pisosUdate = pisosRepository.findById(id)
        pisosModel.id = pisosUdate.get().id
        return pisosRepository.save(pisosModel)
    }
}