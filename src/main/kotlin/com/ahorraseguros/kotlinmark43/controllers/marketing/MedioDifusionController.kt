package com.ahorraseguros.kotlinmark43.controllers.marketing

import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.Marketing.MedioDifusionModelsLogs
import com.ahorraseguros.kotlinmark43.models.marketing.MedioDifusionModel
import com.ahorraseguros.kotlinmark43.models.marketing.view.MediosDifusionViewsModel
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import com.ahorraseguros.kotlinmark43.repositories.marketing.MedioDifusionRepository
import com.ahorraseguros.kotlinmark43.repositories.marketing.view.MediosDifusionViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/medio-difusion")
class MedioDifusionController: LogsOperations {
    @Autowired
    lateinit var medioDifusionRepository: MedioDifusionRepository

    @Autowired
    lateinit var medioDifusionViewRepository: MediosDifusionViewRepository

    @GetMapping
    fun findAll(): MutableList<MediosDifusionViewsModel> = medioDifusionViewRepository.findAll()
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "medioDifusion"

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<MedioDifusionModel> = medioDifusionRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody proveedoresLeadsModel: MedioDifusionModel): MedioDifusionModel = medioDifusionRepository.save(proveedoresLeadsModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody medioDifusionModel: MedioDifusionModel,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long ): HttpStatus {
        val medioDifusionUpdate = medioDifusionRepository.findById(id)

        return if (medioDifusionUpdate.isPresent) {
            val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

            callServerLogs(MedioDifusionModelsLogs(0, medioDifusionUpdate.get().idSubarea, medioDifusionUpdate.get().idProveedor,
                    medioDifusionUpdate.get().idExterno, medioDifusionUpdate.get().idPagina, medioDifusionUpdate.get().idPresupuesto,
                    medioDifusionUpdate.get().descripcion, medioDifusionUpdate.get().configuracion, medioDifusionUpdate.get().activo, idLog))

            medioDifusionModel.id = medioDifusionUpdate.get().id
            medioDifusionRepository.save(medioDifusionModel)
            HttpStatus.OK
        } else{
            HttpStatus.BAD_REQUEST
        }
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/medioDifusion", a, String::class.java)
}