package com.ahorraseguros.kotlinmark43.controllers.marketing

import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.Marketing.CampanaSubareaModelsLogs
import com.ahorraseguros.kotlinmark43.models.marketing.CampanaSubareaModel
import com.ahorraseguros.kotlinmark43.models.marketing.view.CampanaSubareaViewModel
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import com.ahorraseguros.kotlinmark43.repositories.marketing.CampanaSubareaRepository
import com.ahorraseguros.kotlinmark43.repositories.marketing.view.CampanaSubAreaViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid
@CrossOrigin
@RestController
@RequestMapping ("/v1/campana-subarea")
class CampanaSubareaController: LogsOperations {
    @Autowired
    lateinit var campanaSubAreaRepository: CampanaSubareaRepository
    @Autowired
    lateinit var campanaSubAreaviewRepository: CampanaSubAreaViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository
    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "campanaSubarea"

    @GetMapping
    fun getAll() = campanaSubAreaviewRepository.findAll();

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<CampanaSubareaViewModel> = campanaSubAreaviewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping()
    fun create(@Valid @RequestBody campanaSubareaModel: CampanaSubareaModel): CampanaSubareaModel = campanaSubAreaRepository.save(campanaSubareaModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody campanaSubareaModel: CampanaSubareaModel,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): HttpStatus {
        var registroUpdate = campanaSubAreaRepository.findById(id)

        return if (registroUpdate.isPresent) {
            val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

            callServerLogs(CampanaSubareaModelsLogs(0, registroUpdate.get().idCampana, registroUpdate.get().idSubarea,
                    registroUpdate.get().descripcionn, registroUpdate.get().activo, idLog))

            campanaSubareaModel.id = registroUpdate.get().id
            campanaSubAreaRepository.save(campanaSubareaModel)
            HttpStatus.OK
        } else{
            HttpStatus.BAD_REQUEST
        }
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/campanaSubarea", a, String::class.java)
}