package com.ahorraseguros.kotlinmark43.controllers.marketing

import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.Marketing.CampanaCategoriaModelsLogs
import com.ahorraseguros.kotlinmark43.models.marketing.CampanaCategoriaModel
import com.ahorraseguros.kotlinmark43.models.marketing.view.CamapanaCategoriaViewModel
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import com.ahorraseguros.kotlinmark43.repositories.marketing.CampanaCategoriaRepository
import com.ahorraseguros.kotlinmark43.repositories.marketing.view.CampanaCategoriaViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/campana-categorias")
class CampanaCategoriaController : LogsOperations {
    @Autowired
    lateinit var campanaCategoriaRepository: CampanaCategoriaRepository
    @Autowired
    lateinit var campanaCategoriaViewRepository: CampanaCategoriaViewRepository

    @GetMapping
    fun findAll(): MutableList<CamapanaCategoriaViewModel> = campanaCategoriaViewRepository.findAll()
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "campanaCategoria"

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<CamapanaCategoriaViewModel> = campanaCategoriaViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody campanaCategoriaModel: CampanaCategoriaModel): CampanaCategoriaModel = campanaCategoriaRepository.save(campanaCategoriaModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody campanaCategoriaModel: CampanaCategoriaModel,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long  ): HttpStatus{
        val categoriaUpdate = campanaCategoriaRepository.findById(id)


        return if (categoriaUpdate.isPresent) {

            val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

            callServerLogs(CampanaCategoriaModelsLogs(0, categoriaUpdate.get().idCampana, categoriaUpdate.get().idSubRamo,
                            categoriaUpdate.get().activo, categoriaUpdate.get().comentarios, idLog))
            campanaCategoriaModel.id = categoriaUpdate.get().id
            campanaCategoriaRepository.save(campanaCategoriaModel)
            HttpStatus.OK
        } else{
            HttpStatus.BAD_REQUEST
        }
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/campanaCategoria", a, String::class.java)
}