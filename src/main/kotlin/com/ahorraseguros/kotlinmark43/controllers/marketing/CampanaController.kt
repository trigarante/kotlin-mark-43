package com.ahorraseguros.kotlinmark43.controllers.marketing

import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.Marketing.CampanaModelsLogs
import com.ahorraseguros.kotlinmark43.models.marketing.CampanaModel
import com.ahorraseguros.kotlinmark43.models.marketing.view.CampanaViewModel
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import com.ahorraseguros.kotlinmark43.repositories.marketing.CampanaRepository
import com.ahorraseguros.kotlinmark43.repositories.marketing.view.CampanaViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/campanas-marketing")
class CampanaController: LogsOperations {
    @Autowired
    lateinit var campanaRepository: CampanaRepository

    @Autowired
    lateinit var campanaViewRepository: CampanaViewRepository

    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "ramo"

    @GetMapping
    fun findAll(): MutableList<CampanaViewModel> = campanaViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<CampanaModel> = campanaRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody campanaModel: CampanaModel): CampanaModel = campanaRepository.save(campanaModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody campanaModel: CampanaModel,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): HttpStatus {
        val campanaUpdate = campanaRepository.findById(id)

        return if (campanaUpdate.isPresent) {

            val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

            callServerLogs(CampanaModelsLogs(0, campanaUpdate.get().idSubRamo,
                    campanaUpdate.get().nombre,
                    campanaUpdate.get().descripcion, campanaUpdate.get().activo, campanaUpdate.get().idEstadoCampana, idLog))

            campanaModel.id = campanaUpdate.get().id
            campanaRepository.save(campanaModel)
            HttpStatus.OK
        } else{
            HttpStatus.BAD_REQUEST
        }
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/campana", a, String::class.java)
}