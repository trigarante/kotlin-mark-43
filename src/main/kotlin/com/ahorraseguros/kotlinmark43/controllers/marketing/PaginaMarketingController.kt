package com.ahorraseguros.kotlinmark43.controllers.marketing

import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.Marketing.PaginaModelsLogs
import com.ahorraseguros.kotlinmark43.models.marketing.PaginaModel
import com.ahorraseguros.kotlinmark43.models.marketing.view.PaginaViewModel
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import com.ahorraseguros.kotlinmark43.repositories.marketing.PaginaMarketingRepository
import com.ahorraseguros.kotlinmark43.repositories.marketing.view.PaginaMarketingViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/pagina")
class PaginaMarketingController: LogsOperations {
    @Autowired
    lateinit var paginaRepository: PaginaMarketingRepository
    @Autowired
    lateinit var paginaViewRepository: PaginaMarketingViewRepository

    @GetMapping
    fun findAll(): MutableList<PaginaViewModel> = paginaViewRepository.findAll()
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "Pagina"

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<PaginaViewModel> = paginaViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping("{fechaRegistro}/{fechaActualizacion}")
    fun create(@Valid @RequestBody paginaModel: PaginaModel, @PathVariable("fechaRegistro") fechaRegistro: Long,
               @PathVariable("fechaActualizacion") fechaActualizacion: Long): HttpStatus{
        return try{
            paginaModel.fechaActualizacion = Timestamp(fechaRegistro)
            paginaModel.fechaRegistro = Timestamp(fechaActualizacion)
            paginaRepository.save(paginaModel)
            HttpStatus.OK
        } catch (e: Exception) {
            HttpStatus.BAD_REQUEST
        }
    }

    @PutMapping("{id}/{fechaRegistro}/{fechaActualizacion}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @PathVariable("fechaRegistro") fechaRegistro: Long,
               @PathVariable("fechaActualizacion") fechaActualizacion: Long,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long,
               @Valid @RequestBody paginaModel: PaginaModel): HttpStatus {
        val paginaUpdate = paginaRepository.findById(id)

        return if (paginaUpdate.isPresent) {

            val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

            callServerLogs(PaginaModelsLogs(0, paginaUpdate.get().idCampana, paginaUpdate.get().idTipoPagina, paginaUpdate.get().url,
                    paginaUpdate.get().descripcion,  paginaUpdate.get().fechaRegistro, paginaUpdate.get().fechaActualizacion,
                    paginaUpdate.get().configuracion, idLog))

            paginaModel.id = paginaUpdate.get().id
            paginaModel.fechaActualizacion = Timestamp(fechaActualizacion)
            paginaModel.fechaRegistro = Timestamp(fechaRegistro)
            paginaRepository.save(paginaModel)

            HttpStatus.OK
        } else{
            HttpStatus.BAD_REQUEST
        }
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/pagina", a, String::class.java)
}