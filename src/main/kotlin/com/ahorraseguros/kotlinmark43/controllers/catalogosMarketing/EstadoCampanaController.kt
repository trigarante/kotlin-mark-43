package com.ahorraseguros.kotlinmark43.controllers.catalogosMarketing

import com.ahorraseguros.kotlinmark43.models.catalogosMarketing.EstadoCampanaModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.Marketing.CampanaModelsLogs
import com.ahorraseguros.kotlinmark43.models.logs.Marketing.catalogos.EstadoCampanaModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.catalogosComercial.EstadoCampanaRespository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping ("/v1/estado-campana")

class EstadoCampanaController: LogsOperations {

    @Autowired
    lateinit var estadoCampanaRepository: EstadoCampanaRespository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "estadoCampana"

    @GetMapping
    fun getAll() = estadoCampanaRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<EstadoCampanaModel> = estadoCampanaRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping()
    fun create(@Valid @RequestBody estadoCampanaModel: EstadoCampanaModel): EstadoCampanaModel = estadoCampanaRepository.save(estadoCampanaModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long,
               @Valid @RequestBody estadoCampanaModel: EstadoCampanaModel,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): HttpStatus {

        val estadoCampanaUpdate = estadoCampanaRepository.findById(id)

        return if (estadoCampanaUpdate.isPresent) {

            val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

            callServerLogs(EstadoCampanaModelsLogs(0, estadoCampanaUpdate.get().estado,
                    estadoCampanaUpdate.get().activo, idLog))

            estadoCampanaModel.id = estadoCampanaUpdate.get().id
            estadoCampanaRepository.save(estadoCampanaModel)
            HttpStatus.OK
        } else{
            HttpStatus.BAD_REQUEST
        }


//        var registroUpdate = estadoCampanaRepository.findById(id)
//        estadoCampanaModel.id = registroUpdate.get().id
//        return estadoCampanaRepository.save(estadoCampanaModel)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/estadoCampana", a, String::class.java)

}