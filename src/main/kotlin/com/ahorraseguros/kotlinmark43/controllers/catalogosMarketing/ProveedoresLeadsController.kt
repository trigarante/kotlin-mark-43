package com.ahorraseguros.kotlinmark43.controllers.catalogosMarketing

import com.ahorraseguros.kotlinmark43.models.catalogosMarketing.ProveedoresLeadsModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.Marketing.catalogos.ProveedoresLeadsModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.catalogosMarketing.ProveedoresLeadsRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/proveedores-lead")
class ProveedoresLeadsController : LogsOperations {
    @Autowired
    lateinit var proveedoresLeadsRepository: ProveedoresLeadsRepository

    @GetMapping
    fun findAll(): MutableList<ProveedoresLeadsModel> = proveedoresLeadsRepository.findAll()
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "proveedoresLeads"

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<ProveedoresLeadsModel> = proveedoresLeadsRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody proveedoresLeadsModel: ProveedoresLeadsModel): ProveedoresLeadsModel = proveedoresLeadsRepository.save(proveedoresLeadsModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody proveedoresLeadsModel: ProveedoresLeadsModel,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): HttpStatus {
        val proveedoresUpdate = proveedoresLeadsRepository.findById(id)

        return if (proveedoresUpdate.isPresent) {

            val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

            callServerLogs(ProveedoresLeadsModelsLogs(0, proveedoresUpdate.get().nombre, proveedoresUpdate.get().activo, idLog))
            proveedoresLeadsModel.id = proveedoresUpdate.get().id
            proveedoresLeadsRepository.save(proveedoresLeadsModel)
            HttpStatus.OK
        } else{
            HttpStatus.BAD_REQUEST
        }
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/proveedoresLeads", a, String::class.java)

}