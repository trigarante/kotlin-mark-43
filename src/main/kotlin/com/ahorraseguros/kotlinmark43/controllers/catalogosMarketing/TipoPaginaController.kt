package com.ahorraseguros.kotlinmark43.controllers.catalogosMarketing

import com.ahorraseguros.kotlinmark43.models.catalogosMarketing.TipoPaginaModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.Marketing.catalogos.TipoPaginaModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.catalogosMarketing.TipoPaginaRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tipo-pagina")
class TipoPaginaController: LogsOperations {
    @Autowired
    lateinit var tipoPaginaRepository: TipoPaginaRepository

    @GetMapping
    fun findAll(): MutableList<TipoPaginaModel> = tipoPaginaRepository.findAll()
    @Autowired
    lateinit var logsRepository: LogsRepository
    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "tipoPagina"

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<TipoPaginaModel> = tipoPaginaRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody tipoPaginaModel: TipoPaginaModel): TipoPaginaModel = tipoPaginaRepository.save(tipoPaginaModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody tipoPaginaModel: TipoPaginaModel,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): HttpStatus {
        val tipoPaginaUpdate = tipoPaginaRepository.findById(id)

        return if (tipoPaginaUpdate.isPresent) {
            val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

            callServerLogs(TipoPaginaModelsLogs(0, tipoPaginaUpdate.get().tipo, tipoPaginaUpdate.get().objetivo,
                    tipoPaginaUpdate.get().activo, idLog))

            tipoPaginaModel.id = tipoPaginaUpdate.get().id
            tipoPaginaRepository.save(tipoPaginaModel)
            HttpStatus.OK
        } else{
            HttpStatus.BAD_REQUEST
        }
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/tipoPagina", a, String::class.java)
}