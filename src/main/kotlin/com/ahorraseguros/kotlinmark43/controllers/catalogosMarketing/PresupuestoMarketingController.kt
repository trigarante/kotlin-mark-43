package com.ahorraseguros.kotlinmark43.controllers.catalogosMarketing

import com.ahorraseguros.kotlinmark43.models.catalogosMarketing.PresuspuestoMarketingModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.Marketing.catalogos.PresupuestoMarketingModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.catalogosMarketing.PresupuestoMarketingRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping ("/v1/presupuesto-marketing")

class PresupuestoMarketingController: LogsOperations {

    @Autowired
    lateinit var presupuestoMarketingRepository: PresupuestoMarketingRepository
    @Autowired
    lateinit var logsRepository: LogsRepository
    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "presuspuestoMarketing"

    @GetMapping
    fun getAll() = presupuestoMarketingRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<PresuspuestoMarketingModel> = presupuestoMarketingRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping()
    fun create(@Valid @RequestBody presuspuestoMarketingModel: PresuspuestoMarketingModel): PresuspuestoMarketingModel = presupuestoMarketingRepository.save(presuspuestoMarketingModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long,
               @Valid @RequestBody presuspuestoMarketingModel: PresuspuestoMarketingModel,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): HttpStatus {

        val presupuestoMarketingUpdate = presupuestoMarketingRepository.findById(id)

        return if (presupuestoMarketingUpdate.isPresent) {
            val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

            callServerLogs(PresupuestoMarketingModelsLogs(0, presupuestoMarketingUpdate.get().candidad, presupuestoMarketingUpdate.get().fechaPresupuesto,
                    presupuestoMarketingUpdate.get().activo, idLog))

            presuspuestoMarketingModel.id = presupuestoMarketingUpdate.get().id
            presupuestoMarketingRepository.save(presuspuestoMarketingModel)
            HttpStatus.OK
        } else{
            HttpStatus.BAD_REQUEST
        }
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/presuspuestoMarketing", a, String::class.java)

}