package com.ahorraseguros.kotlinmark43.controllers.ventaNueva

import com.ahorraseguros.kotlinmark43.models.ventaNueva.EstadoReciboModel
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.EstadoReciboRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@CrossOrigin
@RequestMapping("/v1/estado-recibo")

class EstadoReciboController {

    @Autowired
    lateinit var estadoReciboRepository: EstadoReciboRepository

    @GetMapping
    fun getAll() : MutableList<EstadoReciboModel> = estadoReciboRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<EstadoReciboModel> = estadoReciboRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping()
    fun create(@Valid @RequestBody estadoReciboModel: EstadoReciboModel): EstadoReciboModel = estadoReciboRepository.save(estadoReciboModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Int,
               @Valid @RequestBody estadoReciboModel: EstadoReciboModel): EstadoReciboModel {
        var estadoReciboUpdate = estadoReciboRepository.findById(id)
        estadoReciboModel.id = estadoReciboUpdate.get().id
        return estadoReciboRepository.save(estadoReciboModel)
    }
}