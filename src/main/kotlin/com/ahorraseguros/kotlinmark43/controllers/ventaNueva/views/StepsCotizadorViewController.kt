package com.ahorraseguros.kotlinmark43.controllers.ventaNueva.views

import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.StepsCotizadorViewModel
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view.StepsCotizadorViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin
@RequestMapping("v1/step-cotizador")
class StepsCotizadorViewController {
    @Autowired
    lateinit var stepsCotizadorViewRepository: StepsCotizadorViewRepository

    @GetMapping
    fun finAll(): MutableList<StepsCotizadorViewModel> = stepsCotizadorViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<StepsCotizadorViewModel> =
            stepsCotizadorViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }.orElse(ResponseEntity.notFound().build())
}