package com.ahorraseguros.kotlinmark43.controllers.ventaNueva

import com.ahorraseguros.kotlinmark43.models.ventaNueva.ConductorHabitualModel
import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.ConductorHabitualViewModel
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.ConductorHabitualRespository
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view.ConductorHabitualViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@CrossOrigin
@RequestMapping("/v1/conductor-habitual")

class ConductorHabitualController {

    @Autowired
    lateinit var conductorHabitualRespository: ConductorHabitualRespository

    @Autowired
    lateinit var conductorHabitualViewRepository: ConductorHabitualViewRepository

    @GetMapping
    fun getAll() : MutableList<ConductorHabitualViewModel> = conductorHabitualViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<ConductorHabitualViewModel> = conductorHabitualViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping()
    fun create(@Valid @RequestBody conductorHabitualModel: ConductorHabitualModel): ConductorHabitualModel = conductorHabitualRespository.save(conductorHabitualModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Long,
               @Valid @RequestBody conductorHabitualModel: ConductorHabitualModel): ConductorHabitualModel {
        var conductorHabituaUpdate = conductorHabitualRespository.findById(id)
        conductorHabitualModel.id = conductorHabituaUpdate.get().id
        return conductorHabitualRespository.save(conductorHabitualModel)
    }

}