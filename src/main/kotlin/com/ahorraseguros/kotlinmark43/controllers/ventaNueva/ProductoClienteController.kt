package com.ahorraseguros.kotlinmark43.controllers.ventaNueva

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.view.ProductoClienteViewModel
import com.ahorraseguros.kotlinmark43.models.ventaNueva.ProductoClienteModel
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.ProductoClienteRepository
import com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva.view.ProductoClienteViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@CrossOrigin
@RequestMapping ("v1/producto-cliente")

class ProductoClienteController {

    @Autowired
    lateinit var productoClienteRepository: ProductoClienteRepository

    @Autowired
    lateinit var productoClienteViewRepository: ProductoClienteViewRepository

    @GetMapping
    fun finAll(): MutableList<ProductoClienteViewModel> = productoClienteViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<ProductoClienteViewModel> =
            productoClienteViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }.orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody productoClienteModel: ProductoClienteModel): ProductoClienteModel =
            productoClienteRepository.save(productoClienteModel)

    @PutMapping("{id}")
    fun put(@PathVariable("id") id: Long,
            @Valid @RequestBody productoClienteModel: ProductoClienteModel): ProductoClienteModel {
        val productoClienteUpdate = productoClienteRepository.findById(id)
        productoClienteModel.id = productoClienteUpdate.get().id
        return productoClienteRepository.save(productoClienteModel)
    }
}