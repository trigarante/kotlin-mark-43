package com.ahorraseguros.kotlinmark43.controllers.ventaNueva

import com.ahorraseguros.kotlinmark43.models.ventaNueva.ClienteModel
import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.ClienteViewModel
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.ClienteRpository
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view.ClienteViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@CrossOrigin
@RequestMapping("v1/cliente")

class ClienteController {
    @Autowired
    lateinit var clienteRepository: ClienteRpository

    @Autowired
    lateinit var clienteViewRepository: ClienteViewRepository

    @GetMapping
    fun findAll() : MutableList<ClienteViewModel> = clienteViewRepository.findAll()

    @GetMapping ("{id}")
    fun findById (@PathVariable("id")  id: Long) : ResponseEntity<ClienteViewModel> = clienteViewRepository.findById(id).map{ s -> ResponseEntity.ok(s)}
            .orElse(ResponseEntity.notFound().build())

//    @GetMapping("curp/{curp}")
//    fun findByCurp(@PathVariable("curp") curp: String): ResponseEntity<Boolean> = clienteRepository.findByCurp(curp).map { s -> ResponseEntity.ok(true) }
//            .orElse(ResponseEntity.notFound().build())
    @GetMapping ("curp/{curp}")
    fun findByCurp(@PathVariable("curp") curp: String): ResponseEntity<ClienteModel?> {
        return try {
            ResponseEntity(clienteRepository.findByCurp(curp) , HttpStatus.OK)
        } catch (error: Exception) {
            ResponseEntity(HttpStatus.BAD_REQUEST)
        }
    }

    @GetMapping ("rfc/{rfc}")
    fun findByRFC(@PathVariable("rfc") rfc: String): ResponseEntity<ClienteModel?> {
        return try {
            ResponseEntity(clienteRepository.findByrfc(rfc) , HttpStatus.OK)
        } catch (error: Exception) {
            ResponseEntity(HttpStatus.BAD_REQUEST)
        }
    }

    @PostMapping
    fun create(@Valid @RequestBody clienteModel: ClienteModel) : ClienteModel = clienteRepository.save(clienteModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody clienteModel: ClienteModel): ClienteModel {
        val cotizacionesAliUpdate = clienteRepository.findById(id)
        clienteModel.id = cotizacionesAliUpdate.get().id
        return clienteRepository.save(clienteModel)
    }

    @PutMapping("update-archivo/{id}/{idFolder}")
    fun updateArchivo(@PathVariable("idFolder") idFolder: String, @PathVariable("id") id: Long): HttpStatus {
        val cliente = clienteRepository.findById(id).get()
        cliente.archivo = idFolder
        clienteRepository.save(cliente)
        return HttpStatus.OK
    }

}