package com.ahorraseguros.kotlinmark43.controllers.ventaNueva.views


import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.ProductividadViewModel
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view.ProductividadViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@CrossOrigin
@RestController
@RequestMapping("/v1/productividadView")
class ProductividadViewController {
    @Autowired
    lateinit var productividadViewRepository: ProductividadViewRepository

    @GetMapping
    fun findAll():List<ProductividadViewModel> = productividadViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<ProductividadViewModel> = productividadViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())
}