package com.ahorraseguros.kotlinmark43.controllers.ventaNueva

import com.ahorraseguros.kotlinmark43.models.TI.views.UsuarioViewModel
import com.ahorraseguros.kotlinmark43.models.rh.views.EmpleadoViewModels
import com.ahorraseguros.kotlinmark43.models.ventaNueva.SolicitudesModel
import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.SolicitudesViewModel
import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.SolicitudesViewModelSinJSON
import com.ahorraseguros.kotlinmark43.repositories.ti.views.UsuarioViewRepository
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.SolicitudesRepository
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view.SolicitudesViewRepository
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view.SolicitudesViewRepositorySinJSON
import com.ahorraseguros.kotlinmark43.repositories.views.EmpleadoViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping ("/v1/solicitudes-vn")

class SolicitudesController {
    @Autowired
    lateinit var solicitudRepository: SolicitudesRepository
    @Autowired
    lateinit var solicitudesViewRepository: SolicitudesViewRepository
    @Autowired
    lateinit var solicitudesViewRepositorySinJSON: SolicitudesViewRepositorySinJSON
    @Autowired
    lateinit var usuarioViewRepository: UsuarioViewRepository
    @Autowired
    lateinit var empleadoViewRepository: EmpleadoViewRepository

//    @GetMapping("{idUsuario}")
//    fun findAll (@PathVariable("idUsuario") idUsuario: Long) : MutableList<SolicitudesViewModel> {
//        val usuario: UsuarioViewModel = usuarioViewRepository.findById(idUsuario).get()
//        return if (usuario.idGrupo == 1) {
//            solicitudesViewRepository.findAll()
//        } else {
//            solicitudesViewRepository.findAllByIdEmpleado(usuario.idEmpleado)
//        }
//    }

    @GetMapping
    fun getAll() : MutableList<SolicitudesViewModel> = solicitudesViewRepository.findAll()

    @GetMapping("tipo/{tipo-solicitud}/{idUsuario}")
    fun getConFiltro (@PathVariable("tipo-solicitud") tipoSolicitud: String,
                      @PathVariable("idUsuario") idUsuario: Long): MutableList<SolicitudesViewModelSinJSON> {
        val usuario: UsuarioViewModel = usuarioViewRepository.findById(idUsuario).get()

        return if (usuario.idPermisosVisualizacion == 1) {
            val empleado = empleadoViewRepository.findById(usuario.idEmpleado).get()

            if (empleado.idPuesto == 7L) {
                when (tipoSolicitud) {
                    "no-contactado" ->  solicitudesViewRepositorySinJSON.findAllByIdEstadoSolicitudAndIdTipoContactoAndIdSubArea(1,1, usuario.idSubarea)
                    "contactado" -> solicitudesViewRepositorySinJSON.findAllByIdEstadoSolicitudAndIdTipoContactoAndIdSubArea(2,1, usuario.idSubarea)
                    "inbound" -> solicitudesViewRepositorySinJSON.findAllByIdTipoContactoAndIdEstadoSolicitudNotAndIdSubArea(2, usuario.idSubarea)
                    "interno" -> solicitudesViewRepositorySinJSON.findAllByIdTipoContactoAndIdEstadoSolicitudNotAndIdSubArea(3, usuario.idSubarea)
                    else ->  arrayListOf()
                }
            } else {
                when (tipoSolicitud) {
                    "no-contactado" ->  solicitudesViewRepositorySinJSON.findAllByIdEstadoSolicitudAndIdTipoContactoAndIdSubArea(1,1, usuario.idSubarea)
                    "contactado" -> solicitudesViewRepositorySinJSON.findAllByIdEstadoSolicitudAndIdTipoContactoAndIdSubArea(2,1, usuario.idSubarea)
                    "inbound" -> solicitudesViewRepositorySinJSON.findAllByIdTipoContactoAndIdEstadoSolicitud(2)
                    "interno" -> solicitudesViewRepositorySinJSON.findAllByIdTipoContactoAndIdEstadoSolicitud(3)
                    else ->  arrayListOf()
                }
            }
        }  else {
            when (tipoSolicitud) {
                "no-contactado" ->  solicitudesViewRepositorySinJSON.findAllByIdEstadoSolicitudAndIdTipoContactoAndIdEmpleado(1,1, usuario.idEmpleado)
                "contactado" -> solicitudesViewRepositorySinJSON.findAllByIdEstadoSolicitudAndIdTipoContactoAndIdEmpleado(2,1, usuario.idEmpleado)
                "inbound" -> solicitudesViewRepositorySinJSON.findAllByIdTipoContactoAndIdEstadoSolicitudNotAndIdEmpleado(2, usuario.idEmpleado)
                "interno" -> solicitudesViewRepositorySinJSON.findAllByIdTipoContactoAndIdEstadoSolicitudNotAndIdEmpleado(3, usuario.idEmpleado)
                else ->  arrayListOf()
            }
        }
    }

    @GetMapping("{idUsuario}")
    fun findAll (@PathVariable("idUsuario") idUsuario: Long) : MutableList<SolicitudesViewModel> {
        val usuario: UsuarioViewModel = usuarioViewRepository.findById(idUsuario).get()
        return if (usuario.idPermisosVisualizacion == 1) {
            solicitudesViewRepository.findAll()
        } else {
            solicitudesViewRepository.findAllByIdEmpleado(usuario.idEmpleado)
        }
    }


    @GetMapping ("get-by-id/{id}")
    fun findById (@PathVariable("id")  id: Long) : ResponseEntity<SolicitudesViewModel> = solicitudesViewRepository.findById(id).map{ s -> ResponseEntity.ok(s)}
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody solicitudesModel: SolicitudesModel) : SolicitudesModel = solicitudRepository.save(solicitudesModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Long,
               @Valid @RequestBody solicitudesModel: SolicitudesModel): SolicitudesModel {
        val solicitudesUdate = solicitudRepository.findById(id)
        solicitudesModel.id = solicitudesUdate.get().id
        return solicitudRepository.save(solicitudesModel)
    }

    @PutMapping("update-estado/{id}/{estado}")
    fun updateEstadoSolicitud (@PathVariable ("id") id: Long, @PathVariable ("estado") estado: Int): HttpStatus {
        val solicitudUpdate = solicitudRepository.findById(id).get()
        solicitudUpdate.idEstadoSolicitud = estado
        solicitudRepository.save(solicitudUpdate)
        return HttpStatus.OK
    }
}