package com.ahorraseguros.kotlinmark43.controllers.ventaNueva.views



import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.PanelVentaViewModel
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view.PanelVentaViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@CrossOrigin
@RestController
@RequestMapping("/v1/panelVentaView")
class PanelVentaViewController {
    @Autowired
    lateinit var panelVentaViewRepository: PanelVentaViewRepository
    @GetMapping
    fun findAll():List<PanelVentaViewModel> = panelVentaViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<PanelVentaViewModel> = panelVentaViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())
}