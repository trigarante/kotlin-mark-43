package com.ahorraseguros.kotlinmark43.controllers.ventaNueva

import com.ahorraseguros.kotlinmark43.models.ventaNueva.ProspectoModel
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.ProspectoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.sql.Timestamp
//import java.security.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/prospectovn")

class ProspectoController {
    @Autowired
    lateinit var prospectoRepository: ProspectoRepository

    @GetMapping
    fun getAll () = prospectoRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<ProspectoModel> = prospectoRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping()
    fun create(@Valid @RequestBody prospectoModel: ProspectoModel): ProspectoModel = prospectoRepository.save(prospectoModel)
//    fun create(@PathVariable("fechaCreacion") fechaCreacion: Long, @Valid @RequestBody  prospectoModel: ProspectoModel): ResponseEntity<ProspectoModel> {
//        return try{
//            val prospecto: ProspectoModel = prospectoModel
//            prospecto.fechaCreacion = Timestamp(fechaCreacion)
//            ResponseEntity(prospectoRepository.save(prospecto), HttpStatus.OK)
//        }catch(e: Exception){
//            ResponseEntity(HttpStatus.BAD_REQUEST)
//        }
//    }
////            ProspectoModel = prospectoRepository.save(prospectoModel) {}

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Long,
               @Valid @RequestBody prospectoModel: ProspectoModel): ProspectoModel {
        var prospectoUpdated = prospectoRepository.findById(id)
        //createLogs(1,1,vacantesUpdated.get().id)
        //callServerLogs(VacantesModelsLogs(0,vacantesUpdated.get().nombre,vacantesUpdated.get().idSubarea,vacantesUpdated.get().idPuesto,vacantesUpdated.get().idTipoPuesto,vacantesUpdated.get().idTipoUsuario,vacantesUpdated.get().id))
        prospectoModel.id = prospectoUpdated.get().id

        return prospectoRepository.save(prospectoModel)
    }

    @GetMapping("correo/{correo}")
    fun findByCorreo(@PathVariable("correo") correo: String): ResponseEntity<ProspectoModel?> {
        return try {
            ResponseEntity(prospectoRepository.findByCorreo(correo).get(), HttpStatus.OK)
        } catch (error: Exception) {
            ResponseEntity(HttpStatus.OK)
        }
    }
}