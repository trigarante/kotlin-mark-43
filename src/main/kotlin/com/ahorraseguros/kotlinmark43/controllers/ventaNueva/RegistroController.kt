package com.ahorraseguros.kotlinmark43.controllers.ventaNueva

import com.ahorraseguros.kotlinmark43.models.ventaNueva.RegistroModel
import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.RegistroViewModel
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.RegistroRepository
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view.RegistroViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/registro-vn")

class RegistroController {
    @Autowired
    lateinit var registroRepository: RegistroRepository

    @Autowired
    lateinit var registroViewRepository: RegistroViewRepository

    @GetMapping
    fun getAll() = registroViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<RegistroViewModel> = registroViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @GetMapping("buscar-no-serie/{noSerie}")
    fun findPlacas(@PathVariable ("noSerie") noSerie: String): String? {
        return registroViewRepository.findNoSerie(noSerie)
    }

    @PostMapping()
    fun create(@Valid @RequestBody registroModel: RegistroModel): RegistroModel = registroRepository.save(registroModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Long,
               @Valid @RequestBody registroModel: RegistroModel): RegistroModel {
        var registroUpdate = registroRepository.findById(id)
        registroModel.id = registroUpdate.get().id
        return registroRepository.save(registroModel)
    }

    @PutMapping("carpeta-drive/{id}/{idCarpeta}")
    fun updateDriveFolder (@PathVariable("id") id: Long, @PathVariable ("idCarpeta") idCarpeta: String): HttpStatus {
        val registroUpdate: RegistroModel = registroRepository.findById(id).get()
        registroUpdate.archivo = idCarpeta
        registroRepository.save(registroUpdate)
        return HttpStatus.OK
    }

    @PutMapping("update-estado/{id}/{estado}")
    fun updateEstadoRegistro (@PathVariable ("id") id: Long, @PathVariable ("estado") estado: Int): HttpStatus {
        val registroUpdate = registroRepository.findById(id).get()
        registroUpdate.idEstadoPoliza = estado
        registroRepository.save(registroUpdate)
        return HttpStatus.OK
    }
}