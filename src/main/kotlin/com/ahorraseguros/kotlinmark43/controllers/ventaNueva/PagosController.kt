package com.ahorraseguros.kotlinmark43.controllers.ventaNueva

import com.ahorraseguros.kotlinmark43.models.ventaNueva.PagosModel
import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.PagosViewModel
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.PagosRepository
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view.PagoViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@CrossOrigin
@RequestMapping("v1/pagos")

class PagosController {
    @Autowired
    lateinit var pagosRepository: PagosRepository

    @Autowired
    lateinit var pagoViewRepository: PagoViewRepository

    @GetMapping
    fun findAll() : MutableList<PagosViewModel> = pagoViewRepository.findAll()

    @GetMapping("{id}")
    fun findById (@PathVariable("id") id: Long): ResponseEntity<PagosViewModel> = pagoViewRepository.findById(id).map { s -> ResponseEntity.ok(s)}
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody pagosModel: PagosModel) : PagosModel = pagosRepository.save(pagosModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id")id: Int,
               @Valid @RequestBody pagosModel: PagosModel): PagosModel {
        val pagosUpdate = pagosRepository.findById(id)
        pagosModel.id = pagosUpdate.get().id
        return pagosRepository.save(pagosModel)
    }

}
