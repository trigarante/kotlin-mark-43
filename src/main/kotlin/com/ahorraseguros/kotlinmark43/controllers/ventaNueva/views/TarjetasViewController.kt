package com.ahorraseguros.kotlinmark43.controllers.ventaNueva.views

import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.TarjetasViewModel
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view.TarjetasViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
@CrossOrigin
@RequestMapping("v1/tarjetasvn")

class TarjetasViewController {

    @Autowired
    lateinit var tarjetasViewRepository: TarjetasViewRepository

    @GetMapping
    fun getAll() : MutableList<TarjetasViewModel> = tarjetasViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<TarjetasViewModel> = tarjetasViewRepository.findById(id).map {s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())
}
