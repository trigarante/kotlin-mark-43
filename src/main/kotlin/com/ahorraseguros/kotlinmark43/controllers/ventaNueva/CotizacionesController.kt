package com.ahorraseguros.kotlinmark43.controllers.ventaNueva

import com.ahorraseguros.kotlinmark43.models.ventaNueva.CotizacionesModel
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.CotizacionesRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping ("/v1/cotizaciones")

class CotizacionesController {

    @Autowired
    lateinit var cotizacionesRepository: CotizacionesRepository

    @GetMapping
    fun findAll (): MutableList<CotizacionesModel> = cotizacionesRepository.findAll()

    @GetMapping ("{id}")
    fun findById (@PathVariable ("id") id : Long) : ResponseEntity<CotizacionesModel> =
            cotizacionesRepository.findById(id).map{ s -> ResponseEntity.ok(s)}.orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody cotizacionesModel: CotizacionesModel) : CotizacionesModel =
            cotizacionesRepository.save(cotizacionesModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Long,
               @Valid @RequestBody cotizacionesModel: CotizacionesModel): CotizacionesModel {
        val cotizacionesUpdate = cotizacionesRepository.findById(id)
        cotizacionesModel.id = cotizacionesUpdate.get().id
        return cotizacionesRepository.save(cotizacionesModel)
    }
}