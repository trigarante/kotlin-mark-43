package com.ahorraseguros.kotlinmark43.controllers.ventaNueva.views

import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.DatosProductoClienteViewModel
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view.DatosProductoClienteViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin
@RequestMapping("v1/producto-cliente")

class DatosProductoClienteViewController {

    @Autowired
    lateinit var datosProductoClienteViewRepository: DatosProductoClienteViewRepository

    @GetMapping("/datos")
    fun findAllDatos(): MutableList<DatosProductoClienteViewModel> = datosProductoClienteViewRepository.findAll()

    @GetMapping("/datos/{id}")
    fun findByIdDatos(@PathVariable("id") id: Long): ResponseEntity<DatosProductoClienteViewModel> = datosProductoClienteViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())
}