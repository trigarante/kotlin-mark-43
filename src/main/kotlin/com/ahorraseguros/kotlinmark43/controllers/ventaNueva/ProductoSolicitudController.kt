package com.ahorraseguros.kotlinmark43.controllers.ventaNueva

import com.ahorraseguros.kotlinmark43.models.ventaNueva.ProductoSolicitudModel
import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.DatosProductoSolicitudViewModel
import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.ProductoSolicitudViewModel
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.ProductoSolicitudRepository
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view.DatosProductoSolicitudViewRepository
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view.ProductoSolicitudViewRepository
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view.SolicitudesViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@CrossOrigin
@RequestMapping("/v1/producto-solicitudvn")

class   ProductoSolicitudController {

    @Autowired
    lateinit var productoSolicitudRepository: ProductoSolicitudRepository
    @Autowired
    lateinit var productoSolicitudViewRepository: ProductoSolicitudViewRepository
    @Autowired
    lateinit var datosProductoSolicitudViewRepository: DatosProductoSolicitudViewRepository

    @GetMapping
    fun findAll(): MutableList<ProductoSolicitudViewModel> = productoSolicitudViewRepository.findAll()

    @GetMapping ("/datos")
    fun findAllDatos(): MutableList<DatosProductoSolicitudViewModel> = datosProductoSolicitudViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<ProductoSolicitudViewModel> = productoSolicitudViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @GetMapping("/datos/{id}")
    fun findByIdDatos(@PathVariable("id") id: Long): ResponseEntity<DatosProductoSolicitudViewModel> = datosProductoSolicitudViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody productoSolicitudModel: ProductoSolicitudModel): ProductoSolicitudModel = productoSolicitudRepository.save(productoSolicitudModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Long,
               @Valid @RequestBody productoSolicitudModel: ProductoSolicitudModel): ProductoSolicitudModel {
        val productosolicitudUpdate = productoSolicitudRepository.findById(id)
        productoSolicitudModel.id = productosolicitudUpdate.get().id
        return productoSolicitudRepository.save(productoSolicitudModel)
    }

}