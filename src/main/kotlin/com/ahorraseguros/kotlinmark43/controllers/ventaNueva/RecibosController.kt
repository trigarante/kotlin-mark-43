package com.ahorraseguros.kotlinmark43.controllers.ventaNueva

import com.ahorraseguros.kotlinmark43.models.ventaNueva.RecibosModel
import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.RecibosViewModel
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.RecibosRepository
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view.ReciboViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.lang.Exception
import javax.validation.Valid

@RestController
@CrossOrigin
@RequestMapping("/v1/recibovn")

class RecibosController {

    @Autowired
    lateinit var recibosRepository: RecibosRepository

    @Autowired
    lateinit var reciboViewRepository: ReciboViewRepository

    @GetMapping
    fun getAll() : MutableList<RecibosViewModel> = reciboViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<RecibosViewModel> = reciboViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @GetMapping("registro/{idRegistro}")
    fun findByIdRegistro(@PathVariable("idRegistro") idRegistro: Long) : MutableList<RecibosViewModel> =
            reciboViewRepository.findAllByIdRegistro(idRegistro)

    @PostMapping()
    fun create(@Valid @RequestBody recibosModel: MutableList<RecibosModel>): ResponseEntity<String> {
        return try {
            recibosRepository.saveAll(recibosModel)
            ResponseEntity("Datos ingresados correctamente", HttpStatus.CREATED)
        } catch (e: Exception){
            ResponseEntity("Ha ocurrido un error", HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Long,
               @Valid @RequestBody recibosModel: RecibosModel): RecibosModel {
        var reciboUpdate = recibosRepository.findById(id)
        recibosModel.id = reciboUpdate.get().id
        return recibosRepository.save(recibosModel)
    }
}