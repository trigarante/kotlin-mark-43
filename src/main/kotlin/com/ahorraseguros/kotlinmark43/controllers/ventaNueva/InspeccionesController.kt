package com.ahorraseguros.kotlinmark43.controllers.ventaNueva

import com.ahorraseguros.kotlinmark43.models.ventaNueva.InspeccionesModel
import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.InspeccionesViewModel
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.InspeccionesRepository
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view.InspeccionesViewRespository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@CrossOrigin
@RequestMapping("/v1/inspecciones")

class InspeccionesController {

    @Autowired
    lateinit var inspeccionesRepository: InspeccionesRepository

    @Autowired
    lateinit var inspeccionesViewRespository: InspeccionesViewRespository

    @GetMapping
    fun getAll() : MutableList<InspeccionesViewModel> = inspeccionesViewRespository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<InspeccionesViewModel> = inspeccionesViewRespository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping()
    fun create(@Valid @RequestBody inspeccionesModel: InspeccionesModel): InspeccionesModel = inspeccionesRepository.save(inspeccionesModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Long,
               @Valid @RequestBody inspeccionesModel: InspeccionesModel): InspeccionesModel {
        var inspeccionesUpdate = inspeccionesRepository.findById(id)
        inspeccionesModel.id = inspeccionesUpdate.get().id
        return inspeccionesRepository.save(inspeccionesModel)
    }
}