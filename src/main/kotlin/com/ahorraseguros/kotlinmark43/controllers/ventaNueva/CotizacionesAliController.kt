package com.ahorraseguros.kotlinmark43.controllers.ventaNueva

import com.ahorraseguros.kotlinmark43.models.ventaNueva.CotizacionesAliModel
import com.ahorraseguros.kotlinmark43.repositories.ventaNueva.CotizacionesAliRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping ("/v1/cotizacionesAli")

class CotizacionesAliController {

    @Autowired
    lateinit var cotizacionesAliRepository: CotizacionesAliRepository

    @GetMapping
    fun findAll() : MutableList<CotizacionesAliModel> = cotizacionesAliRepository.findAll()

    @GetMapping ("{id}")
    fun findById (@PathVariable("id")  id: Long) : ResponseEntity<CotizacionesAliModel> = cotizacionesAliRepository.findById(id).map{ s -> ResponseEntity.ok(s)}
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody cotizacionesAliModel: CotizacionesAliModel) : CotizacionesAliModel = cotizacionesAliRepository.save(cotizacionesAliModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Long,
               @Valid @RequestBody cotizacionesAliModel: CotizacionesAliModel): CotizacionesAliModel {
        val cotizacionesAliUpdate = cotizacionesAliRepository.findById(id)
        cotizacionesAliModel.id = cotizacionesAliUpdate.get().id
        return cotizacionesAliRepository.save(cotizacionesAliModel)
    }
}