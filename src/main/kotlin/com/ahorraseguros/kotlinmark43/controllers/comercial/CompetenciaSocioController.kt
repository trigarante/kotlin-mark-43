package com.ahorraseguros.kotlinmark43.controllers.comercial

import com.ahorraseguros.kotlinmark43.models.comercial.CompetenciaSocioModels
import com.ahorraseguros.kotlinmark43.models.comercial.views.CompetenciaSocioViewModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.comercial.CompetenciaSocioModelsLogs
import com.ahorraseguros.kotlinmark43.models.logs.comercial.RamoModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.comerciales.CompetenciaSocioRepository
import com.ahorraseguros.kotlinmark43.repositories.comerciales.views.CompetenciaSocioViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid


@CrossOrigin
@RestController
@RequestMapping("/v1/competencia-socio")
class CompetenciaSocioController : LogsOperations {
    @Autowired
    lateinit var competenciaSocioRepository: CompetenciaSocioRepository

    @Autowired
    lateinit var competenciaSocioViewRepository: CompetenciaSocioViewRepository

    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "CompetenciaSocio"

    @GetMapping
    fun findAll(): MutableList<CompetenciaSocioViewModels> = competenciaSocioViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<CompetenciaSocioViewModels> = competenciaSocioViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody competenciaModels: CompetenciaSocioModels): CompetenciaSocioModels = competenciaSocioRepository.save(competenciaModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody competenciaModels: CompetenciaSocioModels,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long ): CompetenciaSocioModels {
        val competenciaUpdate = competenciaSocioRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(CompetenciaSocioModelsLogs(0, competenciaUpdate.get().idProducto, competenciaUpdate.get().idTipoCompetencia,
                competenciaUpdate.get().competencia, competenciaUpdate.get().activo, idLog))
        competenciaModels.id = competenciaUpdate.get().id
        return competenciaSocioRepository.save(competenciaModels)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/competenciasSocio", a, String::class.java)

}