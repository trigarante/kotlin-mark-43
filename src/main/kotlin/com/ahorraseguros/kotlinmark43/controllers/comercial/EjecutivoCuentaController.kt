package com.ahorraseguros.kotlinmark43.controllers.comercial

import com.ahorraseguros.kotlinmark43.models.comercial.EjecutivoCuentaModels
import com.ahorraseguros.kotlinmark43.models.comercial.views.EjecutivoCuentaViewModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.comercial.EjecutivoCuentaModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.comerciales.EjecutivoCuentaRepository
import com.ahorraseguros.kotlinmark43.repositories.comerciales.views.EjecutivoCuentaViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid


@CrossOrigin
@RestController
@RequestMapping("/v1/ejecutivos-cuentas")
class EjecutivoCuentaController: LogsOperations {
    @Autowired
    lateinit var ejecutivoCuentaRepository: EjecutivoCuentaRepository

    @Autowired
    lateinit var ejecutivoCuentaViewRepository: EjecutivoCuentaViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "ejecutivoCuenta"

    @GetMapping
    fun findAll(): MutableList<EjecutivoCuentaViewModels> = ejecutivoCuentaViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<EjecutivoCuentaViewModels> = ejecutivoCuentaViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody ejecutivoCuentaModels: EjecutivoCuentaModels): EjecutivoCuentaModels = ejecutivoCuentaRepository.save(ejecutivoCuentaModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody ejecutivoCuentaModels: EjecutivoCuentaModels,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long ): EjecutivoCuentaModels {
        val ejecutivoCuentaUpdate = ejecutivoCuentaRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(EjecutivoCuentaModelsLogs(0,ejecutivoCuentaUpdate.get().idSocio, ejecutivoCuentaUpdate.get().nombre,
                       ejecutivoCuentaUpdate.get().apellidoPaterno, ejecutivoCuentaUpdate.get().apellidoMaterno,
                       ejecutivoCuentaUpdate.get().telefono, ejecutivoCuentaUpdate.get().ext, ejecutivoCuentaUpdate.get().celular,
                       ejecutivoCuentaUpdate.get().activo, ejecutivoCuentaUpdate.get().favorito, ejecutivoCuentaUpdate.get().email,
                       ejecutivoCuentaUpdate.get().puesto, ejecutivoCuentaUpdate.get().direccion ,idLog))


        ejecutivoCuentaModels.id = ejecutivoCuentaUpdate.get().id
        return ejecutivoCuentaRepository.save(ejecutivoCuentaModels)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any{
        return restTemplate.postForEntity(serverLogs+"/ejecutivoCuenta", a, String::class.java)
    }
}