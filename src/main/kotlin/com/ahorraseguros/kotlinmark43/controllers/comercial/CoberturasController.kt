package com.ahorraseguros.kotlinmark43.controllers.comercial

import com.ahorraseguros.kotlinmark43.models.comercial.CoberturasModels
import com.ahorraseguros.kotlinmark43.models.comercial.views.CoberturasViewModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.comercial.CoberturasModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.comerciales.CoberturasRepository
import com.ahorraseguros.kotlinmark43.repositories.comerciales.views.CoberturasViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid


@CrossOrigin
@RestController
@RequestMapping("/v1/coberturas")
class CoberturasController : LogsOperations {
    @Autowired
    lateinit var coberturasRepository: CoberturasRepository

    @Autowired
    lateinit var coberturasViewRepository: CoberturasViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "coberturas"

    @GetMapping
    fun findAll(): MutableList<CoberturasViewModels> = coberturasViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<CoberturasViewModels> = coberturasViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody coberturasModels: CoberturasModels): CoberturasModels = coberturasRepository.save(coberturasModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody coberturasModel: CoberturasModels,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long ): CoberturasModels {
        val coberturaUpdate = coberturasRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(CoberturasModelsLogs(0, coberturaUpdate.get().idProducto, coberturaUpdate.get().idTipoCobertura,
                coberturaUpdate.get().detalle, coberturaUpdate.get().activo , idLog))
        coberturasModel.id = coberturaUpdate.get().id
        return coberturasRepository.save(coberturasModel)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/coberturas", a, String::class.java)
}