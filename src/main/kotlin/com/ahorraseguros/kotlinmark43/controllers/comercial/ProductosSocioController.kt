package com.ahorraseguros.kotlinmark43.controllers.comercial

import com.ahorraseguros.kotlinmark43.models.comercial.ProductosSocioModels
import com.ahorraseguros.kotlinmark43.models.comercial.views.ProductoSocioViewModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.comercial.ProductosSocioModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.comerciales.ProductosSocioRepository
import com.ahorraseguros.kotlinmark43.repositories.comerciales.views.ProductosSocioViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid


@CrossOrigin
@RestController
@RequestMapping("/v1/productos-socio")
class ProductosSocioController : LogsOperations {
    @Autowired
    lateinit var productosSocioRepository: ProductosSocioRepository
    @Autowired
    lateinit var productosSocioViewRepository: ProductosSocioViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "productoSocio"

    @GetMapping
    fun findAll(): MutableList<ProductoSocioViewModels> = productosSocioViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<ProductoSocioViewModels> = productosSocioViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @GetMapping("id-subramo/{idSubramo}")
    fun findByIdSubramo(@PathVariable("idSubramo") idSubramo: Long): MutableList<ProductoSocioViewModels> =
            productosSocioViewRepository.findAllByIdSubRamoAndActivo(idSubramo)

    @PostMapping
    fun create(@Valid @RequestBody productosSocioModels: ProductosSocioModels): ProductosSocioModels = productosSocioRepository.save(productosSocioModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody productosSocioModels: ProductosSocioModels,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): ProductosSocioModels {
        val productosSocioUpdate = productosSocioRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(ProductosSocioModelsLogs(0,productosSocioUpdate.get().idTipoProducto, productosSocioUpdate.get().idSubRamo,
                productosSocioUpdate.get().prioridad, productosSocioUpdate.get().nombre, productosSocioUpdate.get().activo
                , idLog))
        productosSocioModels.id = productosSocioUpdate.get().id
        return productosSocioRepository.save(productosSocioModels)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/productosSocio", a, String::class.java)
}