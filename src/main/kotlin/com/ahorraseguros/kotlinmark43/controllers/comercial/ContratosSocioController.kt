package com.ahorraseguros.kotlinmark43.controllers.comercial

import com.ahorraseguros.kotlinmark43.models.comercial.ContratosSociosModels
import com.ahorraseguros.kotlinmark43.models.comercial.views.ContratosSociosViewModals
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.comercial.ContratosSociosModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.comerciales.ContratosSocioRepository
import com.ahorraseguros.kotlinmark43.repositories.comerciales.views.ContratosSocioViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpEntity
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid


@CrossOrigin
@RestController
@RequestMapping("/v1/contratos-socio")
class ContratosSocioController : LogsOperations {
    @Autowired
    lateinit var contratosSocioRepository: ContratosSocioRepository

    @Autowired
    lateinit var contratosSocioViewRepository: ContratosSocioViewRepository

    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "contratosSocios"

    @GetMapping
    fun findAll(): MutableList<ContratosSociosViewModals> = contratosSocioViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<ContratosSociosViewModals> = contratosSocioViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping("{fechaInicio}/{fechaFin}")
    fun create(@PathVariable ("fechaInicio") fechaInicio: Long, @PathVariable ("fechaFin") fechaFin: Long,
               @Valid @RequestBody contratosModels: ContratosSociosModels): ResponseEntity<String?> {
        contratosModels.fechaInicio = Timestamp(fechaInicio)
        contratosModels.fechaFin = Timestamp(fechaFin)
        try{
            contratosSocioRepository.save(contratosModels)
            return ResponseEntity(HttpStatus.OK)
        }catch (e: Exception){
           return ResponseEntity(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody contratosModels: ContratosSociosModels,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long ): ContratosSociosModels {
        val contratosUpdate = contratosSocioRepository.findById(id)


        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(ContratosSociosModelsLogs(0, contratosUpdate.get().idSocio, contratosUpdate.get().fechaInicio, contratosUpdate.get().fechaFin,
                contratosUpdate.get().referencia, contratosUpdate.get().activo, idLog))

        contratosModels.id = contratosUpdate.get().id
        return contratosSocioRepository.save(contratosModels)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/contratosSocios", a, String::class.java)
}