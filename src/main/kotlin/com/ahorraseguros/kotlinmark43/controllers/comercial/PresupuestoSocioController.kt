package com.ahorraseguros.kotlinmark43.controllers.comercial

import com.ahorraseguros.kotlinmark43.models.comercial.PresupuestoSocioModels
import com.ahorraseguros.kotlinmark43.models.comercial.views.PresupuestoSocioViewModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.comercial.PresupuestoSocioModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.comerciales.PresupuestoSocioRepository
import com.ahorraseguros.kotlinmark43.repositories.comerciales.views.PresupuestoSocioViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid


@CrossOrigin
@RestController
@RequestMapping("/v1/presupuesto-socio")
class PresupuestoSocioController: LogsOperations {
    @Autowired
    lateinit var presupuestoSocioRepository: PresupuestoSocioRepository

    @Autowired
    lateinit var presupuestoSocioViewRepository: PresupuestoSocioViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "presupuestoSocio"


    @GetMapping
    fun findAll(): MutableList<PresupuestoSocioViewModels> = presupuestoSocioViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<PresupuestoSocioViewModels> = presupuestoSocioViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody presupuestoSocioModels: PresupuestoSocioModels): PresupuestoSocioModels = presupuestoSocioRepository.save(presupuestoSocioModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody presupuestoSocioModels: PresupuestoSocioModels,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): PresupuestoSocioModels {
        val presupuestoSocioUpdate = presupuestoSocioRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(PresupuestoSocioModelsLogs(0, presupuestoSocioUpdate.get().idSocio, presupuestoSocioUpdate.get().presupuesto,
                presupuestoSocioUpdate.get().activo, presupuestoSocioUpdate.get().anio, idLog))
        presupuestoSocioModels.id = presupuestoSocioUpdate.get().id
        return presupuestoSocioRepository.save(presupuestoSocioModels)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/presupuestoSocio", a, String::class.java)
}