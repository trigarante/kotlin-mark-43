package com.ahorraseguros.kotlinmark43.controllers.comercial

import com.ahorraseguros.kotlinmark43.models.comercial.RamoModels
import com.ahorraseguros.kotlinmark43.models.comercial.views.RamoViewModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.comercial.RamoModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.comerciales.RamoRepository
import com.ahorraseguros.kotlinmark43.repositories.comerciales.views.RamoViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid


@CrossOrigin
@RestController
@RequestMapping("/v1/ramo")
class RamoController : LogsOperations {
    @Autowired
    lateinit var ramoRepository: RamoRepository

    @Autowired
    lateinit var ramoViewRepository: RamoViewRepository

    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "ramo"
    @GetMapping
    fun findAll(): MutableList<RamoViewModels> = ramoViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<RamoViewModels> = ramoViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @GetMapping("id-socio/{idSocio}")
    fun findByIdSocio(@PathVariable("idSocio") idSocio: Long): MutableList<RamoViewModels> =
            ramoViewRepository.findAllByIdSocioAndActivo(idSocio)

    @PostMapping
    fun create(@Valid @RequestBody ramoModels: RamoModels): RamoModels = ramoRepository.save(ramoModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody ramoModels: RamoModels,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long ): RamoModels {
        val giroUpdate = ramoRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(RamoModelsLogs(0, giroUpdate.get().idSocio,  giroUpdate.get().idTipoRamo,
                giroUpdate.get().descripcion, giroUpdate.get().prioridad, giroUpdate.get().activo
                , idLog))
        ramoModels.id = giroUpdate.get().id
        return ramoRepository.save(ramoModels)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/ramo", a, String::class.java)
}