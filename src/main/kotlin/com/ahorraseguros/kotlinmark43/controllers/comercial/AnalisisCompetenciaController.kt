package com.ahorraseguros.kotlinmark43.controllers.comercial

import com.ahorraseguros.kotlinmark43.models.comercial.AnalisisCompetenciaModels
import com.ahorraseguros.kotlinmark43.models.comercial.views.AnalisisCompetenciaViewModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.comercial.AnalisisCompetenciaModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.comerciales.AnalisisCompetenciaRepository
import com.ahorraseguros.kotlinmark43.repositories.comerciales.views.AnalisisCompetenciaViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid


@CrossOrigin
@RestController
@RequestMapping("/v1/analisis-competencias")
class AnalisisCompetenciaController : LogsOperations {
    @Autowired
    lateinit var analisisCompetenciaRepository: AnalisisCompetenciaRepository

    @Autowired
    lateinit var analisisCompetenciaViewRepository: AnalisisCompetenciaViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "analisisCompetencia"

    @GetMapping
    fun findAll(): MutableList<AnalisisCompetenciaViewModels> = analisisCompetenciaViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<AnalisisCompetenciaViewModels> = analisisCompetenciaViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody categoriasModels: AnalisisCompetenciaModels): AnalisisCompetenciaModels = analisisCompetenciaRepository.save(categoriasModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody analisisCompetenciaModel: AnalisisCompetenciaModels,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): AnalisisCompetenciaModels {
        val analisisCompetenciaUpdate = analisisCompetenciaRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(AnalisisCompetenciaModelsLogs(0, analisisCompetenciaUpdate.get().idCompetencia, analisisCompetenciaUpdate.get().precios,
                analisisCompetenciaUpdate.get().participacion, analisisCompetenciaUpdate.get().activo, idLog))

        analisisCompetenciaModel.id = analisisCompetenciaUpdate.get().id
        return analisisCompetenciaRepository.save(analisisCompetenciaModel)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/analisisCompetencia", a, String::class.java)

}