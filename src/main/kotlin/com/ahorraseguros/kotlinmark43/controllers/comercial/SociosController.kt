package com.ahorraseguros.kotlinmark43.controllers.comercial

import com.ahorraseguros.kotlinmark43.models.comercial.SociosModels
import com.ahorraseguros.kotlinmark43.models.comercial.views.SociosViewModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.comercial.sociosModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.comerciales.SociosRepository
import com.ahorraseguros.kotlinmark43.repositories.comerciales.views.SociosViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid


@CrossOrigin
@RestController
@RequestMapping("/v1/socios")
class SociosController: LogsOperations {
    @Autowired
    lateinit var sociosRepository: SociosRepository

    @Autowired
    lateinit var sociosViewRepository: SociosViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "socios"

    @GetMapping
    fun findAll(): MutableList<SociosViewModels> = sociosViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<SociosViewModels> = sociosViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @GetMapping("/alias/{alias}")
    fun findAlias(@PathVariable("alias") alias: String) = sociosViewRepository.findByAlias(alias)

    @PostMapping
    fun create(@Valid @RequestBody sociosModels: SociosModels): SociosModels = sociosRepository.save(sociosModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody sociosModels: SociosModels,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long ): SociosModels {
        val sociosUpdate = sociosRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(sociosModelsLogs(0, sociosUpdate.get().prioridad, sociosUpdate.get().nombreComercial,
                 sociosUpdate.get().rfc, sociosUpdate.get().razonSocial, sociosUpdate.get().alias,
                 sociosUpdate.get().idEstadoSocio, sociosUpdate.get().activo ,idLog))
        sociosModels.id = sociosUpdate.get().id
        return sociosRepository.save(sociosModels)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/socios", a, String::class.java)
}