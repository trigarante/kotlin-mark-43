package com.ahorraseguros.kotlinmark43.controllers.comercial

import com.ahorraseguros.kotlinmark43.models.comercial.TipoDivisaModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.comercial.TipoDivisaModalsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.comerciales.TipoDivisaRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tipoDivisa")


class TipoDivisaController: LogsOperations {

    @Autowired
    lateinit var tipoDivisaRepository: TipoDivisaRepository

    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "tipoDivisa"
    @GetMapping
    fun findAll(): MutableList<TipoDivisaModel> = tipoDivisaRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<TipoDivisaModel> = tipoDivisaRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody tipoDivisaModel: TipoDivisaModel): TipoDivisaModel = tipoDivisaRepository.save(tipoDivisaModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody tipoDivisaModel: TipoDivisaModel,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long): TipoDivisaModel {
        val tipoDivisaUpdate = tipoDivisaRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(TipoDivisaModalsLogs(0, tipoDivisaUpdate.get().divisa, tipoDivisaUpdate.get().descripcion,
                  tipoDivisaUpdate.get().activo, idLog))
        tipoDivisaModel.id = tipoDivisaUpdate.get().id
        return tipoDivisaRepository.save(tipoDivisaModel)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/tipoDivisa", a, String::class.java)

}