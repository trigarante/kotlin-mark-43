package com.ahorraseguros.kotlinmark43.controllers.comercial

import com.ahorraseguros.kotlinmark43.models.comercial.ConveniosModels
import com.ahorraseguros.kotlinmark43.models.comercial.views.ConveniosViewModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.comercial.ConveniosModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.comerciales.views.ConvenioViewRepository
import com.ahorraseguros.kotlinmark43.repositories.comerciales.ConvenioRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/convenios")
class ConvenioController : LogsOperations {
    @Autowired
    lateinit var convenioRepository: ConvenioRepository

    @Autowired
    lateinit var convenioViewRepository: ConvenioViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "convenio"

    @GetMapping
    fun findAll(): MutableList<ConveniosViewModels> = convenioViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<ConveniosViewModels> = convenioViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody conveniosModels: ConveniosModels): ConveniosModels = convenioRepository.save(conveniosModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody conveniosModels: ConveniosModels,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): ConveniosModels {
        val conveniosUpdate = convenioRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(ConveniosModelsLogs(0, conveniosUpdate.get().idSubRamo, conveniosUpdate.get().idSubCategoria,
                conveniosUpdate.get().idTipoDivisa, conveniosUpdate.get().cantidad, conveniosUpdate.get().activo,
                 idLog))

        conveniosModels.id = conveniosUpdate.get().id
        return convenioRepository.save(conveniosModels)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/convenios", a, String::class.java)
}