package com.ahorraseguros.kotlinmark43.controllers.comercial

import com.ahorraseguros.kotlinmark43.models.comercial.UrlSociosModels
import com.ahorraseguros.kotlinmark43.models.comercial.views.UrlSociosViewModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.comercial.UrlSociosModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.comerciales.UrlSociosRepository
import com.ahorraseguros.kotlinmark43.repositories.comerciales.views.UrlSociosViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid


@CrossOrigin
@RestController
@RequestMapping("/v1/url-socios")
class UrlSociosController : LogsOperations {
    @Autowired
    lateinit var urlSociosRepository: UrlSociosRepository

    @Autowired
    lateinit var urlSociosViewRepository: UrlSociosViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "urlSocios"


    @GetMapping
    fun findAll(): MutableList<UrlSociosViewModels> = urlSociosViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<UrlSociosViewModels> = urlSociosViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody urlSociosModels: UrlSociosModels): UrlSociosModels = urlSociosRepository.save(urlSociosModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody urlSociosModels: UrlSociosModels,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): UrlSociosModels {
        val urlSociosUpdate = urlSociosRepository.findById(id)
        urlSociosModels.id = urlSociosUpdate.get().id

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(UrlSociosModelsLogs(0, urlSociosUpdate.get().idSocio, urlSociosUpdate.get().url,
                     urlSociosUpdate.get().descripcion, urlSociosUpdate.get().activo, idLog))

        return urlSociosRepository.save(urlSociosModels)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/urlSocios", a, String::class.java)
}