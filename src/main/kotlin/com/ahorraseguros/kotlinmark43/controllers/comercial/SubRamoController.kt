package com.ahorraseguros.kotlinmark43.controllers.comercial

import com.ahorraseguros.kotlinmark43.models.comercial.SubRamoModels
import com.ahorraseguros.kotlinmark43.models.comercial.views.SubRamoViewModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.comercial.SubRamoModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.comerciales.SubRamoRepository
import com.ahorraseguros.kotlinmark43.repositories.comerciales.views.SubRamoViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import java.util.*
import javax.validation.Valid


@CrossOrigin
@RestController
@RequestMapping("/v1/subramo")
class SubRamoController: LogsOperations {
    @Autowired
    lateinit var subRamoRepository: SubRamoRepository

    @Autowired
    lateinit var subRamoViewRepository: SubRamoViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "subramo"

    @GetMapping
    fun findAll(): MutableList<SubRamoViewModels> = subRamoViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<SubRamoViewModels> =
            subRamoViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }.orElse(ResponseEntity.notFound().build())

    @GetMapping("tipo/{idTipoSubRamo}/{idEstadoSocio}")
    fun findByTipo(@PathVariable("idTipoSubRamo") idTipoSubRamo: Int, @PathVariable("idEstadoSocio") idEstadoSocio: Int):
            MutableList<SubRamoViewModels> = subRamoViewRepository.findByIdTipoSubRamoAndIdEstadoSocio(idTipoSubRamo,idEstadoSocio)
    @GetMapping("id-ramo/{id}")
    fun findByIdRamo(@PathVariable("id") idRamo: Long): MutableList<SubRamoViewModels> =
            subRamoViewRepository.findAllByIdRamoAndActivo(idRamo)

    @GetMapping("alias/{idTipoSubRamo}/{Alias}")
    fun findByTipoAndAlias(@PathVariable("idTipoSubRamo") idTipoSubRamo: Int, @PathVariable("Alias") Alias: String):
            Optional<SubRamoViewModels> = subRamoViewRepository.findByIdTipoSubRamoAndAlias(idTipoSubRamo,Alias)

    @PostMapping
    fun create(@Valid @RequestBody subRamoModels: SubRamoModels): SubRamoModels = subRamoRepository.save(subRamoModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody categoriaModel: SubRamoModels,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): SubRamoModels {
        val subramoUpdate = subRamoRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(SubRamoModelsLogs(0, subramoUpdate.get().prioridad, subramoUpdate.get().idRamo,
                subramoUpdate.get().idTipoSubRamo, subramoUpdate.get().descripcion, subramoUpdate.get().activo, idLog))
        categoriaModel.id = subramoUpdate.get().id
        return subRamoRepository.save(categoriaModel)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/subramo", a, String::class.java)
}