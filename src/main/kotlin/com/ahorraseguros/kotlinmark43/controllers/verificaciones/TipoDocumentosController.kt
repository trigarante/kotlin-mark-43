package com.ahorraseguros.kotlinmark43.controllers.verificaciones

import com.ahorraseguros.kotlinmark43.models.verificaciones.TipoDocumentosModel
import com.ahorraseguros.kotlinmark43.repositories.verificaciones.TipoDocumentosRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@CrossOrigin
@RestController
@RequestMapping("/v1/tipo-documentos")
class TipoDocumentosController {
    @Autowired
    lateinit var tipoDocumentosRepository: TipoDocumentosRepository

    @GetMapping("{idTabla}")
    fun getDocumentos(@PathVariable idTabla: Int): MutableList<TipoDocumentosModel>
            = tipoDocumentosRepository.findAllByIdTablaAndActivo(idTabla)
}