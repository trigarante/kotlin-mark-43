package com.ahorraseguros.kotlinmark43.controllers.verificaciones

import com.ahorraseguros.kotlinmark43.models.verificaciones.ErroresAnexosAModel
import com.ahorraseguros.kotlinmark43.repositories.verificaciones.ErroresAnexosARepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.lang.Exception
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/errores-anexos")
class ErroresAnexosAController {
    @Autowired
    lateinit var erroresAnexosARepository: ErroresAnexosARepository

    @GetMapping
    fun getAll():ResponseEntity<MutableList<ErroresAnexosAModel>> =
            ResponseEntity(erroresAnexosARepository.findAll(), HttpStatus.OK)

    @PostMapping
    fun create(@Valid @RequestBody erroresAnexosAModel: MutableList<ErroresAnexosAModel>): ResponseEntity<String> {
        return try {
            erroresAnexosARepository.saveAll(erroresAnexosAModel)
            ResponseEntity("Datos ingresados correctamente", HttpStatus.CREATED)
        } catch (e: Exception){
            ResponseEntity("Ha ocurrido un error", HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @PutMapping("{idErroresAnexos}")
    fun update(@Valid @RequestBody erroresAnexosAModel: ErroresAnexosAModel,
               @PathVariable("idErroresAnexos") idErroresAnexos:Long): HttpStatus {
        return try {
            val erroresUpdate = erroresAnexosARepository.findById(idErroresAnexos)
            erroresAnexosAModel.id = erroresUpdate.get().id
            erroresAnexosARepository.save(erroresAnexosAModel)
            HttpStatus.OK
        } catch (e: Exception){
            HttpStatus.BAD_REQUEST
        }
    }
}