package com.ahorraseguros.kotlinmark43.controllers.verificaciones

import com.ahorraseguros.kotlinmark43.models.verificaciones.ErroresAutorizacionModel
import com.ahorraseguros.kotlinmark43.repositories.verificaciones.ErroresAutorizacionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.lang.Exception
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/errores-autorizacion")
class ErroresAutorizacionController {
    @Autowired
    lateinit var erroresAutorizacionRepository: ErroresAutorizacionRepository

    @GetMapping
    fun getAll(): ResponseEntity<MutableList<ErroresAutorizacionModel>> =
            ResponseEntity(erroresAutorizacionRepository.findAll(), HttpStatus.OK)

    @PostMapping
    fun create(@Valid @RequestBody erroresAutorizacionModel: MutableList<ErroresAutorizacionModel>): ResponseEntity<String> {
        return try {
            erroresAutorizacionRepository.saveAll(erroresAutorizacionModel)
            ResponseEntity("Datos ingresados correctamente", HttpStatus.CREATED)
        } catch (e: Exception){
            ResponseEntity("Ha ocurrido un error", HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
}