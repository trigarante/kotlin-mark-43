package com.ahorraseguros.kotlinmark43.controllers.verificaciones

import com.ahorraseguros.kotlinmark43.models.verificaciones.CampoCorregirModel
import com.ahorraseguros.kotlinmark43.repositories.verificaciones.CampoCorregirRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@CrossOrigin
@RestController
@RequestMapping("/v1/campo-corregir")
class CampoCorregirController {
    @Autowired
    lateinit var campoCorregirRepository: CampoCorregirRepository

    @GetMapping("{idTabla}")
    fun getTableFields(@PathVariable ("idTabla") idTabla: Int):ResponseEntity<MutableList<CampoCorregirModel>>{
        return ResponseEntity(campoCorregirRepository.findAllByIdTablaAndActivo(idTabla), HttpStatus.OK)
    }

}