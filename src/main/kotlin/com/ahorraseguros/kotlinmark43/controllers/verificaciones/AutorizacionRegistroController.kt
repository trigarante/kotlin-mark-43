package com.ahorraseguros.kotlinmark43.controllers.verificaciones

import com.ahorraseguros.kotlinmark43.models.verificaciones.AutorizacionRegistroModel
import com.ahorraseguros.kotlinmark43.models.verificaciones.view.AutorizacionRegistroViewModel
import com.ahorraseguros.kotlinmark43.repositories.verificaciones.AutorizacionRegistroRepository
import com.ahorraseguros.kotlinmark43.repositories.verificaciones.views.AutorizacionRegistroViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@CrossOrigin
@RestController
@RequestMapping("/v1/autorizacion-registro")
class AutorizacionRegistroController {
    @Autowired
    lateinit var autorizacionRegistroRepository: AutorizacionRegistroRepository

    @Autowired
    lateinit var autorizacionRegistroViewRepository: AutorizacionRegistroViewRepository

    @GetMapping
    fun getAll(): MutableList<AutorizacionRegistroViewModel> = autorizacionRegistroViewRepository.findAll()

    @GetMapping("{id}")
    fun getById(@PathVariable id: Long ): Optional<AutorizacionRegistroViewModel> = autorizacionRegistroViewRepository.findById(id)

    @PostMapping("{idRegistro}")
    fun create(@PathVariable idRegistro: Long): AutorizacionRegistroModel =
            autorizacionRegistroRepository.save(AutorizacionRegistroModel(idRegistro = idRegistro))

    @PutMapping("{documentoVerificado}/{idAutorizacionRegistro}")
    fun activateVerificado(@PathVariable idDocumentoVerificado: Int,
                           @PathVariable idAutorizacionRegistro: Long): ResponseEntity<String?> {
        return try {
            val autorizacion = autorizacionRegistroRepository.findById(idAutorizacionRegistro).get()

            when (idDocumentoVerificado) {
                1 -> autorizacion.verificadoCliente = true
                2 -> autorizacion.verificadoPago = true
            }
            autorizacionRegistroRepository.save(autorizacion)
            ResponseEntity(HttpStatus.NO_CONTENT)
        } catch (e: Exception) {
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
}