package com.ahorraseguros.kotlinmark43.controllers.examen

import com.ahorraseguros.kotlinmark43.models.examen.ProductosExamenModel
import com.ahorraseguros.kotlinmark43.repositories.examen.ProductosExamenRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@CrossOrigin
@RequestMapping("/v1/productos-examen")

class ProductosExamenController {

    @Autowired
    lateinit var examenRepository: ProductosExamenRepository

    @GetMapping
    fun getAll() : MutableList<ProductosExamenModel> = examenRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<ProductosExamenModel> = examenRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping()
    fun create(@Valid @RequestBody examenModel: ProductosExamenModel): ProductosExamenModel = examenRepository.save(examenModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Long,
               @Valid @RequestBody examenModel: ProductosExamenModel): ProductosExamenModel {
        var reciboUpdate = examenRepository.findById(id)
        examenModel.id = reciboUpdate.get().id
        return examenRepository.save(examenModel)
    }
}