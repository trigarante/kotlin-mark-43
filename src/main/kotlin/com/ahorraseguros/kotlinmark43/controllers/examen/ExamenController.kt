package com.ahorraseguros.kotlinmark43.controllers.examen

import com.ahorraseguros.kotlinmark43.models.examen.ExamenModel
import com.ahorraseguros.kotlinmark43.repositories.examen.ExamenRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@CrossOrigin
@RequestMapping("/v1/examen")

class ExamenController {

    @Autowired
    lateinit var examenRepository: ExamenRepository

    @GetMapping
    fun getAll() : MutableList<ExamenModel> = examenRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<ExamenModel> = examenRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping()
    fun create(@Valid @RequestBody examenModel: ExamenModel): ExamenModel = examenRepository.save(examenModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Int,
               @Valid @RequestBody examenModel: ExamenModel): ExamenModel {
        var reciboUpdate = examenRepository.findById(id)
        examenModel.id = reciboUpdate.get().id
        return examenRepository.save(examenModel)
    }


}