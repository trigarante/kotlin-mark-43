package com.ahorraseguros.kotlinmark43.controllers.catalogosComercial

import com.ahorraseguros.kotlinmark43.models.catalogosComercial.TipoCompetenciaModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.catalogosComercial.TipoCompetenciaModelsLogs
import com.ahorraseguros.kotlinmark43.models.logs.comercial.RamoModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.catalogosComercial.TipoCompetenciaRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tipo-competencia")
class TipoCompetenciaController : LogsOperations {
    @Autowired
    lateinit var tipoCompetenciaRepository: TipoCompetenciaRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @GetMapping
    fun findAll(): MutableList<TipoCompetenciaModels> = tipoCompetenciaRepository.findAll()

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "tipoCompetencia"

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<TipoCompetenciaModels> = tipoCompetenciaRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody tipoCompetenciaModels: TipoCompetenciaModels): TipoCompetenciaModels = tipoCompetenciaRepository.save(tipoCompetenciaModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody tipoCompetenciaModels: TipoCompetenciaModels,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long ): TipoCompetenciaModels {
        val tipoCompetenciaUpdate = tipoCompetenciaRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(TipoCompetenciaModelsLogs(0, tipoCompetenciaUpdate.get().tipo, tipoCompetenciaUpdate.get().activo
                , idLog))
        tipoCompetenciaModels.id = tipoCompetenciaUpdate.get().id
        return tipoCompetenciaRepository.save(tipoCompetenciaModels)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/tipoCompetencia", a, String::class.java)
}