package com.ahorraseguros.kotlinmark43.controllers.catalogosComercial

import com.ahorraseguros.kotlinmark43.models.rh.PaginaModels
import com.ahorraseguros.kotlinmark43.repositories.PaginaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid


@CrossOrigin
@RestController
@RequestMapping("v1/paginas")
class PaginaController {

    @Autowired
    lateinit var paginaRepository: PaginaRepository

    @GetMapping
    fun findAll(): MutableList<PaginaModels> = paginaRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody paginaModels: PaginaModels): PaginaModels = paginaRepository.save(paginaModels)










}