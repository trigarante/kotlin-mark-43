package com.ahorraseguros.kotlinmark43.controllers.catalogosComercial

import com.ahorraseguros.kotlinmark43.models.catalogosComercial.TipoRamoModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.catalogosComercial.TipoRamoModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.catalogosComercial.TipoRamoRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tipo-ramo")
class TipoRamoController: LogsOperations {
    @Autowired
    lateinit var tipoRamoRepository: TipoRamoRepository

    @GetMapping
    fun findAll(): MutableList<TipoRamoModels> = tipoRamoRepository.findAll()

    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "tipoRamo"

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<TipoRamoModels> = tipoRamoRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody tipoRamoModels: TipoRamoModels): TipoRamoModels = tipoRamoRepository.save(tipoRamoModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody tipoRamoModels: TipoRamoModels,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): TipoRamoModels {
        val tipoGiroUpdate = tipoRamoRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(TipoRamoModelsLogs(0, tipoGiroUpdate.get().tipo, tipoGiroUpdate.get().activo
                , idLog))
        tipoRamoModels.id = tipoGiroUpdate.get().id
        return tipoRamoRepository.save(tipoRamoModels)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/tipoRamo", a, String::class.java)
}