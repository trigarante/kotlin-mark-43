package com.ahorraseguros.kotlinmark43.controllers.catalogosComercial

import com.ahorraseguros.kotlinmark43.models.catalogosComercial.TipoProductoModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.catalogosComercial.TipoProductoModelsLogs
import com.ahorraseguros.kotlinmark43.models.logs.comercial.RamoModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.catalogosComercial.TipoProductoRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tipo-producto")
class TipoProductoController : LogsOperations {
    @Autowired
    lateinit var tipoProductoRepository: TipoProductoRepository
    @Autowired
    lateinit var logsRepository: LogsRepository
    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "tipoProducto"

    @GetMapping
    fun findAll(): MutableList<TipoProductoModels> = tipoProductoRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<TipoProductoModels> = tipoProductoRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody tipoProductoModels: TipoProductoModels): TipoProductoModels = tipoProductoRepository.save(tipoProductoModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody tipoProductoModels: TipoProductoModels,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): TipoProductoModels {
        val tipoProductoUpdate = tipoProductoRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(TipoProductoModelsLogs(0, tipoProductoUpdate.get().tipo, tipoProductoUpdate.get().activo
                , idLog))
        tipoProductoModels.id = tipoProductoUpdate.get().id
        return tipoProductoRepository.save(tipoProductoModels)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/tipoProducto", a, String::class.java)
}