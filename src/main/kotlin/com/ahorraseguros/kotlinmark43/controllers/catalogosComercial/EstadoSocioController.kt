package com.ahorraseguros.kotlinmark43.controllers.catalogosComercial

import com.ahorraseguros.kotlinmark43.models.catalogosComercial.EstadoSocioModel
import com.ahorraseguros.kotlinmark43.models.catalogosComercial.view.EstadoSociosViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.catalogosComercial.EstadoSocioModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.catalogosComercial.EstadoSocioRepository
import com.ahorraseguros.kotlinmark43.repositories.catalogosComercial.view.EstadoSociosViewlRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/estado-socio")
class EstadoSocioController : LogsOperations {
    @Autowired
    lateinit var estadoSocioRepository: EstadoSocioRepository
    @Autowired
    lateinit var estadoSociosViewRepository: EstadoSociosViewlRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "EstadoSocio"
    @GetMapping
    fun findAll(): MutableList<EstadoSociosViewModel> = estadoSociosViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<EstadoSociosViewModel> = estadoSociosViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody tipoCategoriaModels: EstadoSocioModel): EstadoSocioModel = estadoSocioRepository.save(tipoCategoriaModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody tipoCategoriaModels: EstadoSocioModel,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long ): EstadoSocioModel {
        val tipoCategoriaUpdate = estadoSocioRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(EstadoSocioModelsLogs(0, tipoCategoriaUpdate.get().idEstado, tipoCategoriaUpdate.get().estado,
                tipoCategoriaUpdate.get().activo, idLog))

        tipoCategoriaModels.id = tipoCategoriaUpdate.get().id
        return estadoSocioRepository.save(tipoCategoriaModels)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/estadoSocio", a, String::class.java)
}