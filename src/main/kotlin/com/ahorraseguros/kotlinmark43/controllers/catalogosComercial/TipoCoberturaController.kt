package com.ahorraseguros.kotlinmark43.controllers.catalogosComercial

import com.ahorraseguros.kotlinmark43.models.catalogosComercial.TipoCoberturaModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte.ApsModelsLogs
import com.ahorraseguros.kotlinmark43.models.logs.catalogosComercial.TipoCoberturaModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.catalogosComercial.TipoCoberturaRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tipo-cobertura")
class TipoCoberturaController : LogsOperations {
    @Autowired
    lateinit var tipoCoberturaRepository: TipoCoberturaRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "aps"

    @GetMapping
    fun findAll(): MutableList<TipoCoberturaModels> = tipoCoberturaRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<TipoCoberturaModels> = tipoCoberturaRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody tipoCoberturaModels: TipoCoberturaModels): TipoCoberturaModels = tipoCoberturaRepository.save(tipoCoberturaModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody tipoCoberturaModels: TipoCoberturaModels,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long ): TipoCoberturaModels {
        val tipoCoberturaUpdate = tipoCoberturaRepository.findById(id)
        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(TipoCoberturaModelsLogs(0, tipoCoberturaUpdate.get().cobertura, tipoCoberturaUpdate.get().activo , idLog))

        tipoCoberturaModels.id = tipoCoberturaUpdate.get().id
        return tipoCoberturaRepository.save(tipoCoberturaModels)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/tipoCobertura", a, String::class.java)

}