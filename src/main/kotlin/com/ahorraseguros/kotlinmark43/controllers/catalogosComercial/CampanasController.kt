package com.ahorraseguros.kotlinmark43.controllers.catalogosComercial

import com.ahorraseguros.kotlinmark43.models.catalogosSolicitudes.CampanaModels
import com.ahorraseguros.kotlinmark43.repositories.CampanasRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@CrossOrigin
@RestController
@RequestMapping("/v1/campanas")
class CampanasController {
    @Autowired
    lateinit var campanasRepository: CampanasRepository

    @GetMapping
    fun findAll(): MutableList<CampanaModels> = campanasRepository.findAll()
}