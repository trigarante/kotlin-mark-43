package com.ahorraseguros.kotlinmark43.controllers.catalogosComercial

import com.ahorraseguros.kotlinmark43.models.catalogosComercial.SubCategoriaModels
import com.ahorraseguros.kotlinmark43.models.catalogosComercial.view.SubCategoriaViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.catalogosComercial.SubCategoriaModalsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.catalogosComercial.SubCategoriaRepository
import com.ahorraseguros.kotlinmark43.repositories.catalogosComercial.view.SubCategoriaViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/sub-categoria")
class SubCategoriaController : LogsOperations {
    @Autowired
    lateinit var subCategoriaRepository: SubCategoriaRepository

    @Autowired
    lateinit var subCategoriaViewRepository: SubCategoriaViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository
    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "subCategoria"

    @GetMapping
    fun findAll(): MutableList<SubCategoriaViewModel> = subCategoriaViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<SubCategoriaModels> = subCategoriaRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody subCategoriaModels: SubCategoriaModels): SubCategoriaModels = subCategoriaRepository.save(subCategoriaModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody subCategoriaModels: SubCategoriaModels,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long): SubCategoriaModels {
        val tipoConvenioUpdate = subCategoriaRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(SubCategoriaModalsLogs(0, tipoConvenioUpdate.get().detalle, tipoConvenioUpdate.get().regla,
                tipoConvenioUpdate.get().activo, tipoConvenioUpdate.get().idCategoria, tipoConvenioUpdate.get().idDivisas, idLog))

        subCategoriaModels.id = tipoConvenioUpdate.get().id
        return subCategoriaRepository.save(subCategoriaModels)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/subCategoria", a, String::class.java)
}