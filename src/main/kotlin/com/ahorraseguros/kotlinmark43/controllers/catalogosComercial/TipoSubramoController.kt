package com.ahorraseguros.kotlinmark43.controllers.catalogosComercial

import com.ahorraseguros.kotlinmark43.models.catalogosComercial.TipoSubRamoModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.catalogosComercial.TipoSubRamoModalsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.catalogosComercial.TipoSubramoRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tipo-subramo")
class TipoSubramoController : LogsOperations {
    @Autowired
    lateinit var tipoSubramoRepository: TipoSubramoRepository

    @GetMapping
    fun findAll(): MutableList<TipoSubRamoModels> = tipoSubramoRepository.findAll()
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "tipoSubRamo"

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<TipoSubRamoModels> = tipoSubramoRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody tipoSubRamoModels: TipoSubRamoModels): TipoSubRamoModels = tipoSubramoRepository.save(tipoSubRamoModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody tipoSubRamoModels: TipoSubRamoModels,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long ): TipoSubRamoModels {
        val tipoCategoriaUpdate = tipoSubramoRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(TipoSubRamoModalsLogs(0, tipoCategoriaUpdate.get().tipo, tipoCategoriaUpdate.get().activo, idLog))

        tipoSubRamoModels.id = tipoCategoriaUpdate.get().id
        return tipoSubramoRepository.save(tipoSubRamoModels)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/tipoSubRamo", a, String::class.java)
}