package com.ahorraseguros.kotlinmark43.controllers.finanzas

import com.ahorraseguros.kotlinmark43.models.finanzas.GestionIngresosModel
import com.ahorraseguros.kotlinmark43.models.finanzas.views.GestionIngresosViewModel
import com.ahorraseguros.kotlinmark43.repositories.finanzas.GestionIngresosRepository
import com.ahorraseguros.kotlinmark43.repositories.finanzas.views.GestionIngresosViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@CrossOrigin
@RequestMapping ("/v1/gestion-ingreso")

class GestionIngresosController {

    @Autowired
    lateinit var gestionIngresosRepository: GestionIngresosRepository
    @Autowired
    lateinit var gestionIngresosViewRepository: GestionIngresosViewRepository

    @GetMapping
    fun getAll() = gestionIngresosViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<GestionIngresosViewModel> = gestionIngresosViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping()
    fun create(@Valid @RequestBody gestionIngresosModel: GestionIngresosModel): GestionIngresosModel = gestionIngresosRepository.save(gestionIngresosModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Long,
               @Valid @RequestBody gestionIngresosModel: GestionIngresosModel): GestionIngresosModel {
        var registroUpdate = gestionIngresosRepository.findById(id)
        gestionIngresosModel.id = registroUpdate.get().id
        return gestionIngresosRepository.save(gestionIngresosModel)
    }


}