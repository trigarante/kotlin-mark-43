package com.ahorraseguros.kotlinmark43.controllers.finanzas


import com.ahorraseguros.kotlinmark43.models.finanzas.views.AplicacionesViewModel
import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.RegistroViewModel
import com.ahorraseguros.kotlinmark43.repositories.finanzas.views.AplicacionesViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin
@RequestMapping("/v1/aplicaciones-view")
class AplicacionesViewController {

    @Autowired
    lateinit var aplicacionesViewRepository: AplicacionesViewRepository

    @GetMapping
    fun getAll() = aplicacionesViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<AplicacionesViewModel> = aplicacionesViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())


    @GetMapping("registro-validacion/{idEmpleado}")
    fun registrosValidacion(@PathVariable("idEmpleado") idEmpleado: Long): ResponseEntity<MutableList<AplicacionesViewModel?>> {
        return try {
            return ResponseEntity.ok(aplicacionesViewRepository.registrosSinAutorizacionSinFiltroEmpleado())
        } catch (e: Exception) {
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
}