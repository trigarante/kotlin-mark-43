package com.ahorraseguros.kotlinmark43.controllers.catalgoAtencionClientes

import com.ahorraseguros.kotlinmark43.models.catalogosAtencionClientes.EstadoSolicitudEndosoModel
import com.ahorraseguros.kotlinmark43.repositories.catalogoAtencionClientes.EstadoSolicitudEndososRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/estado-solicitud-endoso")

class EstadoSolicitudEndosoController {

    @Autowired
    lateinit var estadoSolicitudEndososRepository: EstadoSolicitudEndososRepository

    @GetMapping
    fun getAll() : MutableList<EstadoSolicitudEndosoModel> = estadoSolicitudEndososRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<EstadoSolicitudEndosoModel> = estadoSolicitudEndososRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping()
    fun create(@Valid @RequestBody estadoSolicitudEndosoModel: EstadoSolicitudEndosoModel): EstadoSolicitudEndosoModel = estadoSolicitudEndososRepository.save(estadoSolicitudEndosoModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Int,
               @Valid @RequestBody estadoSolicitudEndosoModel: EstadoSolicitudEndosoModel): EstadoSolicitudEndosoModel {
        var reciboUpdate = estadoSolicitudEndososRepository.findById(id)
        estadoSolicitudEndosoModel.id = reciboUpdate.get().id
        return estadoSolicitudEndososRepository.save(estadoSolicitudEndosoModel)
    }
}