package com.ahorraseguros.kotlinmark43.controllers.catalgoAtencionClientes

import com.ahorraseguros.kotlinmark43.models.catalogosAtencionClientes.EstadoEndosoModel
import com.ahorraseguros.kotlinmark43.repositories.catalogoAtencionClientes.EstadoEndosoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/estado-endoso")

class EstadoEndosoController {
    @Autowired
    lateinit var estadoEndosoRepository: EstadoEndosoRepository

    @GetMapping
    fun getAll() : MutableList<EstadoEndosoModel> = estadoEndosoRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<EstadoEndosoModel> = estadoEndosoRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping()
    fun create(@Valid @RequestBody estadoEndosoModel: EstadoEndosoModel): EstadoEndosoModel = estadoEndosoRepository.save(estadoEndosoModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Int,
               @Valid @RequestBody estadoEndosoModel: EstadoEndosoModel): EstadoEndosoModel {
        var reciboUpdate = estadoEndosoRepository.findById(id)
        estadoEndosoModel.id = reciboUpdate.get().id
        return estadoEndosoRepository.save(estadoEndosoModel)
    }
}