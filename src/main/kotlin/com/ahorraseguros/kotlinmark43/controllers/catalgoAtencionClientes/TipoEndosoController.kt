package com.ahorraseguros.kotlinmark43.controllers.catalgoAtencionClientes

import com.ahorraseguros.kotlinmark43.models.catalogosAtencionClientes.TipoEndosoModel
import com.ahorraseguros.kotlinmark43.repositories.catalogoAtencionClientes.TipoEndosoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tipo-endoso")

class TipoEndosoController {

    @Autowired
    lateinit var tipoEndosoRepository: TipoEndosoRepository

    @GetMapping
    fun getAll() : MutableList<TipoEndosoModel> = tipoEndosoRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<TipoEndosoModel> = tipoEndosoRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping()
    fun create(@Valid @RequestBody tipoEndosoModel: TipoEndosoModel): TipoEndosoModel = tipoEndosoRepository.save(tipoEndosoModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Int,
               @Valid @RequestBody tipoEndosoModel: TipoEndosoModel): TipoEndosoModel {
        var reciboUpdate = tipoEndosoRepository.findById(id)
        tipoEndosoModel.id = reciboUpdate.get().id
        return tipoEndosoRepository.save(tipoEndosoModel)
    }
}