package com.ahorraseguros.kotlinmark43.controllers.catalogoFinanzas

import com.ahorraseguros.kotlinmark43.models.catalogosFinanzas.TipoIngresoModel
import com.ahorraseguros.kotlinmark43.repositories.catalogoFinanzas.TipoIngresoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping ("/v1/tipo-ingreso")

class TipoIngresoController {

    @Autowired
    lateinit var tipoIngresoRepository: TipoIngresoRepository

    @GetMapping
    fun getAll() = tipoIngresoRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<TipoIngresoModel> = tipoIngresoRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping()
    fun create(@Valid @RequestBody tipoIngresoModel: TipoIngresoModel): TipoIngresoModel = tipoIngresoRepository.save(tipoIngresoModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Int,
               @Valid @RequestBody tipoIngresoModel: TipoIngresoModel): TipoIngresoModel {
        var registroUpdate = tipoIngresoRepository.findById(id)
        tipoIngresoModel.id = registroUpdate.get().id
        return tipoIngresoRepository.save(tipoIngresoModel)
    }


}