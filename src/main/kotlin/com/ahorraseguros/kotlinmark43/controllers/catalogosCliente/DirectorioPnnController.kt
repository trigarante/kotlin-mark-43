package com.ahorraseguros.kotlinmark43.controllers.catalogosCliente

import com.ahorraseguros.kotlinmark43.models.catalogosClientes.DirectorioPnnModels
import com.ahorraseguros.kotlinmark43.repositories.DirectorioPnnRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*


@CrossOrigin
@RestController
@RequestMapping("/v1/pnn")
class DirectorioPnnController {
    @Autowired
    lateinit var directorioPnnRepository: DirectorioPnnRepository

    @PostMapping("numero/{numero}")
    fun validarPnn( @PathVariable numero: String ): String? {
        return directorioPnnRepository.validacionPNN(numero)
    }
}