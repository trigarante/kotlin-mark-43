package com.ahorraseguros.kotlinmark43.controllers.catalogosCliente

import com.ahorraseguros.kotlinmark43.models.catalogosClientes.CpSepomexModels
import com.ahorraseguros.kotlinmark43.repositories.SepomexRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*


@CrossOrigin
@RestController
@RequestMapping("/v1/sepomex")
class SepomexController {
    @Autowired
    lateinit var sepomexRepository: SepomexRepository

    @GetMapping("{cp}")
    fun findColonias(@PathVariable("cp") cp:Int): MutableList<CpSepomexModels> = sepomexRepository.findAllByCp(cp)

    @GetMapping("colonia/{id}")
    fun findColonias(@PathVariable("id") id:Long): Optional<CpSepomexModels> = sepomexRepository.findById(id)


}