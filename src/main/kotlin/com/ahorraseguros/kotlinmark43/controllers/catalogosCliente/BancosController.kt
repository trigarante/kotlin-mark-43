package com.ahorraseguros.kotlinmark43.controllers.catalogosCliente

import com.ahorraseguros.kotlinmark43.models.catalogosClientes.BancosModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH.BancosModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.BancosRepository
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/bancos")
class BancosController: LogsOperations {
    @Autowired
    lateinit var bancosRepository: BancosRepository

    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "bancos"

    @GetMapping
    fun findAll(): MutableList<BancosModels> = bancosRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<BancosModels> = bancosRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody bancosModel: BancosModels): BancosModels = bancosRepository.save(bancosModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody bancosModel: BancosModels,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long): BancosModels {
        var bancoUpdate = bancosRepository.findById(id)
        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(BancosModelsLogs(0,bancoUpdate.get().nombre,bancoUpdate.get().activo,idLog))
        bancosModel.id = bancoUpdate.get().id
        return bancosRepository.save(bancosModel)
    }

    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
        logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/bancos", a, String::class.java)
}