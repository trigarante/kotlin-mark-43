package com.ahorraseguros.kotlinmark43.controllers.catalogosVentaNueva

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.TipoPolizaModel
import com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva.TipoPolizaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping ("v1/tipo-poliza")

class TipoPolizaController {

    @Autowired
    lateinit var tipoPolizaRepository: TipoPolizaRepository

    @GetMapping
    fun finAll(): MutableList<TipoPolizaModel> = tipoPolizaRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<TipoPolizaModel> = tipoPolizaRepository.findById(id).map { s -> ResponseEntity.ok(s) }.orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody tipoPolizaModel: TipoPolizaModel): TipoPolizaModel = tipoPolizaRepository.save(tipoPolizaModel)

    @PutMapping("{id}")
    fun put(@PathVariable("id") id: Long,
            @Valid @RequestBody tipoPolizaModel: TipoPolizaModel): TipoPolizaModel {
        val tipoPolizaUpdate = tipoPolizaRepository.findById(id)
        tipoPolizaModel.id = tipoPolizaUpdate.get().id
        return tipoPolizaRepository.save(tipoPolizaModel)
    }
}