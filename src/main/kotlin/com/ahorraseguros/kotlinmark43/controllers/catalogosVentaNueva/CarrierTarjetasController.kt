package com.ahorraseguros.kotlinmark43.controllers.catalogosVentaNueva

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.CarrierTarjetasModel
import com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva.CarrrierTarjetasRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("v1/carrier")

class CarrierTarjetasController {
    @Autowired
    lateinit var carrrierTarjetasRepository: CarrrierTarjetasRepository

    @GetMapping
    fun finAll(): MutableList<CarrierTarjetasModel> = carrrierTarjetasRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<CarrierTarjetasModel> = carrrierTarjetasRepository.findById(id).map { s -> ResponseEntity.ok(s) }.orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody carrierTarjetasModel: CarrierTarjetasModel): CarrierTarjetasModel = carrrierTarjetasRepository.save(carrierTarjetasModel)

    @PutMapping("{id}")
    fun put(@PathVariable("id") id: Int,
            @Valid @RequestBody carrierTarjetasModel: CarrierTarjetasModel): CarrierTarjetasModel {
        val tipoPagoModelUpdate = carrrierTarjetasRepository.findById(id)
        carrierTarjetasModel.id = tipoPagoModelUpdate.get().id
        return carrrierTarjetasRepository.save(carrierTarjetasModel)
    }
}