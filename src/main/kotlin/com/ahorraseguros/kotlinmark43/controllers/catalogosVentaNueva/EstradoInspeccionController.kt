package com.ahorraseguros.kotlinmark43.controllers.catalogosVentaNueva

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.EstadoInspeccionModel
import com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva.EstadoInspeccionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@CrossOrigin
@RequestMapping("/v1/estado-inspeccion")

class EstradoInspeccionController {

    @Autowired
    lateinit var estadoInspeccionRepository: EstadoInspeccionRepository

    @GetMapping
    fun getAll() : MutableList<EstadoInspeccionModel> = estadoInspeccionRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<EstadoInspeccionModel> = estadoInspeccionRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping()
    fun create(@Valid @RequestBody estadoInspeccionModel: EstadoInspeccionModel): EstadoInspeccionModel = estadoInspeccionRepository.save(estadoInspeccionModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Int,
               @Valid @RequestBody estadoInspeccionModel: EstadoInspeccionModel): EstadoInspeccionModel {
        var estadoInspeccionUpdate = estadoInspeccionRepository.findById(id)
        estadoInspeccionModel.id = estadoInspeccionUpdate.get().id
        return estadoInspeccionRepository.save(estadoInspeccionModel)
    }
}