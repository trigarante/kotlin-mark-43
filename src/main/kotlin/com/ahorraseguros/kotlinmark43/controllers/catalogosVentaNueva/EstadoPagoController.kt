package com.ahorraseguros.kotlinmark43.controllers.catalogosVentaNueva

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.EstadoPagoModel
import com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva.EstadoPagoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("v1/estado-pago")
class EstadoPagoController {
    @Autowired
    lateinit var estadoPagoRepository: EstadoPagoRepository

    @GetMapping
    fun finAll(): MutableList<EstadoPagoModel> = estadoPagoRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<EstadoPagoModel> = estadoPagoRepository.findById(id).map { s-> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody estadoPagoModel: EstadoPagoModel): EstadoPagoModel = estadoPagoRepository.save(estadoPagoModel)

    @PutMapping("{id}")
    fun put(@PathVariable("id") id: Int,
            @Valid @RequestBody estadoPagoModel: EstadoPagoModel): EstadoPagoModel {
        val estadoPagoModelUpdate = estadoPagoRepository.findById(id)
        estadoPagoModel.id = estadoPagoModelUpdate.get().id
        return estadoPagoRepository.save(estadoPagoModel)
    }


}
