package com.ahorraseguros.kotlinmark43.controllers.catalogosVentaNueva

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.FormaPagoModel
import com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva.FormaPagoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid


@CrossOrigin
@RestController
@RequestMapping("v1/forma-pago")
class FormaPagoController {

    @Autowired
    lateinit var formaPagoRepository: FormaPagoRepository

    @GetMapping
    fun finAll(): MutableList<FormaPagoModel> = formaPagoRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<FormaPagoModel> = formaPagoRepository.findById(id).map { s -> ResponseEntity.ok(s) }.orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody formaPagoModel: FormaPagoModel): FormaPagoModel = formaPagoRepository.save(formaPagoModel)

    @PutMapping("{id}")
    fun put(@PathVariable("id") id: Long,
            @Valid @RequestBody formaPagoModel: FormaPagoModel): FormaPagoModel {
        val formaPagoModelUpdate = formaPagoRepository.findById(id)
        formaPagoModel.id = formaPagoModelUpdate.get().id
        return formaPagoRepository.save(formaPagoModel)
    }
}
