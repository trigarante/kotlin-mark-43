package com.ahorraseguros.kotlinmark43.controllers.catalogosVentaNueva

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.TarjetasBancoModel
import com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva.TarjestasBancoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("v1/tarjetas")

class TarjetasBancoController {
    @Autowired
    lateinit var tarjestasBancoRepository: TarjestasBancoRepository

    @GetMapping
    fun finAll(): MutableList<TarjetasBancoModel> = tarjestasBancoRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<TarjetasBancoModel> = tarjestasBancoRepository.findById(id).map { s -> ResponseEntity.ok(s) }.orElse(ResponseEntity.notFound().build())

    @GetMapping("{idBanco}/{idCarrier}")
    fun findTarjetas(@PathVariable("idBanco")idBanco: Int, @PathVariable ("idCarrier") idCarrier: Int): MutableList<TarjetasBancoModel>
        = tarjestasBancoRepository.findAllByIdBancoAndIdCarrier(idBanco,idCarrier)
    @PostMapping
    fun create(@Valid @RequestBody tarjetasBancoModel: TarjetasBancoModel): TarjetasBancoModel = tarjestasBancoRepository.save(tarjetasBancoModel)

    @PutMapping("{id}")
    fun put(@PathVariable("id") id: Int,
            @Valid @RequestBody tarjetasBancoModel: TarjetasBancoModel): TarjetasBancoModel {
        val tipoPagoModelUpdate = tarjestasBancoRepository.findById(id)
        tarjetasBancoModel.id = tipoPagoModelUpdate.get().id
        return tarjestasBancoRepository.save(tarjetasBancoModel)
    }
}