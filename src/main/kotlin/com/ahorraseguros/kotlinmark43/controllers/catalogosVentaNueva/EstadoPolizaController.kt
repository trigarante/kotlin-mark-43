package com.ahorraseguros.kotlinmark43.controllers.catalogosVentaNueva

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.EstadoPolizaModel
import com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva.EstadoPolizaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping ("v1/estado-poliza")

class EstadoPolizaController {

    @Autowired
    lateinit var estadoPolizaRepository: EstadoPolizaRepository

    @GetMapping
    fun finAll(): MutableList<EstadoPolizaModel> = estadoPolizaRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<EstadoPolizaModel> = estadoPolizaRepository.findById(id).map { s -> ResponseEntity.ok(s) }.orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody estadoPolizaModel: EstadoPolizaModel): EstadoPolizaModel = estadoPolizaRepository.save(estadoPolizaModel)

    @PutMapping("{id}")
    fun put(@PathVariable("id") id: Int,
            @Valid @RequestBody estadoPolizaModel: EstadoPolizaModel): EstadoPolizaModel {
        val estadoPolizaUpdate = estadoPolizaRepository.findById(id)
        estadoPolizaModel.id = estadoPolizaUpdate.get().id
        return estadoPolizaRepository.save(estadoPolizaModel)
    }
}