package com.ahorraseguros.kotlinmark43.controllers.catalogosVentaNueva

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.EstadoSolicitudModel
import com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva.EstadoSolicitudRepository
import com.google.api.services.drive.Drive
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/estado-solicitud")
class EstadoSolicitudController {
    @Autowired
    lateinit var estadoSolicitudRepository: EstadoSolicitudRepository

    @GetMapping
    fun finAll(): MutableList<EstadoSolicitudModel> = estadoSolicitudRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<EstadoSolicitudModel> = estadoSolicitudRepository.findById(id).map { s -> ResponseEntity.ok(s) }.orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody estadoSolicitudModel: EstadoSolicitudModel): EstadoSolicitudModel = estadoSolicitudRepository.save(estadoSolicitudModel)

    @PutMapping("{id}")
    fun put(@PathVariable("id") id: Int,
            @Valid @RequestBody estadoSolicitudModel: EstadoSolicitudModel): EstadoSolicitudModel {
        val estadoSolicitudUpdate = estadoSolicitudRepository.findById(id)
        estadoSolicitudModel.id = estadoSolicitudUpdate.get().id
        return estadoSolicitudRepository.save(estadoSolicitudModel)
    }
}