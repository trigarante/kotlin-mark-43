package com.ahorraseguros.kotlinmark43.controllers.catalogosVentaNueva

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.TipoPagoModel
import com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva.TipoPagoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("v1/tipo-pago")

class TipoPagoController {
    @Autowired
    lateinit var tipoPagoRepository: TipoPagoRepository

    @GetMapping
    fun finAll(): MutableList<TipoPagoModel> = tipoPagoRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<TipoPagoModel> = tipoPagoRepository.findById(id).map { s -> ResponseEntity.ok(s) }.orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody tipoPagoModel: TipoPagoModel): TipoPagoModel = tipoPagoRepository.save(tipoPagoModel)

    @PutMapping("{id}")
    fun put(@PathVariable("id") id: Int,
            @Valid @RequestBody tipoPagoModel: TipoPagoModel): TipoPagoModel {
        val tipoPagoModelUpdate = tipoPagoRepository.findById(id)
        tipoPagoModel.id = tipoPagoModelUpdate.get().id
        return tipoPagoRepository.save(tipoPagoModel)
    }
}