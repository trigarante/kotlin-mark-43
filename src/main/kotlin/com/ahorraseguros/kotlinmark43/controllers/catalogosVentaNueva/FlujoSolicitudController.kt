package com.ahorraseguros.kotlinmark43.controllers.catalogosVentaNueva

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.FlujoSolicitudModel
import com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva.FlujoSolicitudRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/flujo-solicitud")

class FlujoSolicitudController {

    @Autowired
    lateinit var flujoSolicitudRepository: FlujoSolicitudRepository

    @GetMapping
    fun getAll() : MutableList<FlujoSolicitudModel> = flujoSolicitudRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<FlujoSolicitudModel> = flujoSolicitudRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping()
    fun create(@Valid @RequestBody flujoSolicitudModel: FlujoSolicitudModel): FlujoSolicitudModel = flujoSolicitudRepository.save(flujoSolicitudModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Long,
               @Valid @RequestBody flujoSolicitudModel: FlujoSolicitudModel): FlujoSolicitudModel {
        var reciboUpdate = flujoSolicitudRepository.findById(id)
        flujoSolicitudModel.id = reciboUpdate.get().id
        return flujoSolicitudRepository.save(flujoSolicitudModel)
    }

}