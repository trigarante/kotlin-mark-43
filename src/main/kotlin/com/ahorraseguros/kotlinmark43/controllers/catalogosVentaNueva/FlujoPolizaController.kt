package com.ahorraseguros.kotlinmark43.controllers.catalogosVentaNueva

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.FlujoPolizaModel
import com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva.FlujoPolizaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("v1/flujo-poliza")

class FlujoPolizaController {

    @Autowired
    lateinit var flujoPolizaRepository: FlujoPolizaRepository

    @GetMapping
    fun finAll(): MutableList<FlujoPolizaModel> = flujoPolizaRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<FlujoPolizaModel> = flujoPolizaRepository.findById(id).map { s -> ResponseEntity.ok(s) }.orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody flujoPolizaModel: FlujoPolizaModel): FlujoPolizaModel = flujoPolizaRepository.save(flujoPolizaModel)

    @PutMapping("{id}")
    fun put(@PathVariable("id") id: Int,
            @Valid @RequestBody flujoPolizaModel: FlujoPolizaModel): FlujoPolizaModel {
        val flujoPolizaUpdate = flujoPolizaRepository.findById(id)
        flujoPolizaModel.id = flujoPolizaUpdate.get().id
        return flujoPolizaRepository.save(flujoPolizaModel)
    }
}