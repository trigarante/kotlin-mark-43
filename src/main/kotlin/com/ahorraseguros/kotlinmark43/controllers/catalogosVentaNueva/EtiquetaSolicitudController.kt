package com.ahorraseguros.kotlinmark43.controllers.catalogosVentaNueva

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.EtiquetaSolicitudModel
import com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva.EtiquetaSolicitudRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/etiqueta-solicitud")

class EtiquetaSolicitudController {

    @Autowired
    lateinit var etiquetaSolicitudRepository: EtiquetaSolicitudRepository

    @GetMapping
    fun getAll() : MutableList<EtiquetaSolicitudModel> = etiquetaSolicitudRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<EtiquetaSolicitudModel> = etiquetaSolicitudRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping()
    fun create(@Valid @RequestBody etiquetaSolicitudModel: EtiquetaSolicitudModel): EtiquetaSolicitudModel = etiquetaSolicitudRepository.save(etiquetaSolicitudModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Long,
               @Valid @RequestBody etiquetaSolicitudModel: EtiquetaSolicitudModel): EtiquetaSolicitudModel {
        var reciboUpdate = etiquetaSolicitudRepository.findById(id)
        etiquetaSolicitudModel.id = reciboUpdate.get().id
        return etiquetaSolicitudRepository.save(etiquetaSolicitudModel)
    }


}