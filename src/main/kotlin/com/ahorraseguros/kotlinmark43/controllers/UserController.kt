package com.ahorraseguros.kotlinmark43.controllers

import com.ahorraseguros.kotlinmark43.models.TI.views.UsuarioViewModel
import com.ahorraseguros.kotlinmark43.repositories.ti.views.UsuarioViewRepository
import com.ahorraseguros.kotlinmark43.security.CurrentUser
import com.ahorraseguros.kotlinmark43.security.UserPrincipal
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

import java.util.Optional


@RestController
class UserController {

    @Autowired
    private val usuarioViewRepository: UsuarioViewRepository? = null

    @GetMapping("/user/me")
    @PreAuthorize("hasRole('USER')")
    fun getCurrentUser(@CurrentUser userPrincipal: UserPrincipal): Optional<UsuarioViewModel> {
        return usuarioViewRepository!!.findById(userPrincipal.id!!)
    }
}
