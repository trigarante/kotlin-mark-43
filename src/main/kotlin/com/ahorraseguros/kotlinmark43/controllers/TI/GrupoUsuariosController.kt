package com.ahorraseguros.kotlinmark43.controllers.TI

import com.ahorraseguros.kotlinmark43.models.TI.GrupoUsuariosModels
import com.ahorraseguros.kotlinmark43.models.TI.views.GrupoUsuarioViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.GrupoUsuariosModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.GrupoUsuariosRepository
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.GrupoUsuariosViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/grupos")
class GrupoUsuariosController: LogsOperations {
    @Autowired
    lateinit var grupoUsuariosRepository: GrupoUsuariosRepository

    @Autowired
    lateinit var grupoUsuariosViewRepository: GrupoUsuariosViewRepository

    @Autowired
    lateinit var logsRepository: LogsRepository

    var restTemplate = RestTemplate()

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val tabla = "grupoUsuarios"

    @GetMapping
    fun findAll():List<GrupoUsuarioViewModel> = grupoUsuariosViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<GrupoUsuarioViewModel> = grupoUsuariosViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @GetMapping("nombres/{nombre}")
    fun findByName(@PathVariable("nombre") nombre: String): GrupoUsuariosModels = grupoUsuariosRepository.findByNombre(nombre)

    @PostMapping
    fun create(@Valid @RequestBody grupo: GrupoUsuariosModels) : GrupoUsuariosModels = grupoUsuariosRepository.save(grupo)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long,
               @Valid @RequestBody grupoModels: GrupoUsuariosModels): GrupoUsuariosModels {
        val grupoUpdate = grupoUsuariosRepository.findById(id)
        val idLog: Long = createLogs(idEmpleadoModificante,1,grupoUpdate.get().id).id

        callServerLogs(GrupoUsuariosModelsLogs(0,grupoUpdate.get().idSubarea,grupoUpdate.get().idPuesto,
                grupoUpdate.get().idPermisosVisualizacion, grupoUpdate.get().nombre, grupoUpdate.get().descripcion,
                grupoUpdate.get().permisos,grupoUpdate.get().activo, idLog))

        grupoModels.id = grupoUpdate.get().id
        return grupoUsuariosRepository.save(grupoModels)
    }

    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/grupos", a, String::class.java)

}