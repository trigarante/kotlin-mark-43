package com.ahorraseguros.kotlinmark43.controllers.TI.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.PatchPanelModel
import com.ahorraseguros.kotlinmark43.models.TI.views.PatchPanelViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte.PatchPanelModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.Soporte.PathPanelRespository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.PatchPanelViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping ("/v1/patchPanel")

class PatchPanelController: LogsOperations {
    @Autowired
    lateinit var patchPathPanelRespository: PathPanelRespository

    @Autowired
    lateinit var patchPanelViewRepository: PatchPanelViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository
    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "patchPanel"

    @GetMapping
    fun findAll () : MutableList<PatchPanelViewModel> = patchPanelViewRepository.findAll()

    @GetMapping ("{id}")
    fun findById (@PathVariable("id")  id: Int) : ResponseEntity<PatchPanelViewModel> = patchPanelViewRepository.findById(id).map{ s -> ResponseEntity.ok(s)}
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody patchPanelModel: PatchPanelModel) : PatchPanelModel = patchPathPanelRespository.save(patchPanelModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody patchPanelModel: PatchPanelModel,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long): PatchPanelModel {
        val patchUdate = patchPathPanelRespository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(PatchPanelModelsLogs(0, patchUdate.get().idEmpleado, patchUdate.get().idObservacionesInventario,
                patchUdate.get().idEstadoInventario, patchUdate.get().Folio, patchUdate.get().idMarca,
                patchUdate.get().idTipoSede, patchUdate.get().origenRecurso, patchUdate.get().activo,
                patchUdate.get().fechaRecepcion, patchUdate.get().fechaSalida, idLog))

        patchPanelModel.id = patchUdate.get().id
        return patchPathPanelRespository.save(patchPanelModel)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/patchPanel", a, String::class.java)
}