package com.ahorraseguros.kotlinmark43.controllers.TI

import com.ahorraseguros.kotlinmark43.models.TI.ProcesadoresSoporteModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.CatalogoSoporte.ProcesadoresSoporteModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.ProcesadorSoporteRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@RestController
@CrossOrigin
@RequestMapping ("/v1/procesador")
class ProcesadorSoporteController : LogsOperations {
    @Autowired
    lateinit var procesadorSoporteRepository: ProcesadorSoporteRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "procesadores"

    @GetMapping
    fun getProcesador() = procesadorSoporteRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody procesadorModels:ProcesadoresSoporteModel ): ProcesadoresSoporteModel= procesadorSoporteRepository.save(procesadorModels)

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<ProcesadoresSoporteModel> = procesadorSoporteRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody procesadorModels: ProcesadoresSoporteModel,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long  ): ProcesadoresSoporteModel {
        var procesadorUpdate = procesadorSoporteRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(ProcesadoresSoporteModelsLogs(0, procesadorUpdate.get().nombreProcesador, procesadorUpdate.get().activo, idLog))

        procesadorModels.id = procesadorUpdate.get().id
        return procesadorSoporteRepository.save(procesadorModels)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/procesadores", a, String::class.java)
}