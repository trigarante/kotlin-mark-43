package com.ahorraseguros.kotlinmark43.controllers.TI

import com.ahorraseguros.kotlinmark43.models.TI.AudioListModel
import com.google.api.client.auth.oauth2.Credential
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.http.FileContent
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.client.util.store.FileDataStoreFactory
import com.google.api.services.drive.Drive
import com.google.api.services.drive.DriveScopes
import com.google.api.services.drive.model.FileList
import com.google.api.services.drive.model.File
import kotlinx.coroutines.*
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.io.*
import java.io.InputStreamReader
import java.util.*

@CrossOrigin
@RestController
@RequestMapping("/v1/api-drive")
class DriveAPI {
    private val APPLICATION_NAME = "Google Drive API"
    private val JSON_FACTORY = JacksonFactory.getDefaultInstance()
    // Estos permisos son de la cuenta admin@ahorraseguros.mx
    private val TOKENS_PATH_LLAMADAS = "/opt/tokens/Drive/llamadas"
    // Estos permisos son de la cuenta usuarios@ahorraseguros.mx
    private val TOKENS_PATH_ARCHIVOS = "/opt/tokens/Drive/archivosEjecutivos"
    private val CREDENTIALS_FILE_PATH = "/opt/resources/DriveCredentials.json"
    private val HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport()
    private lateinit var service: Drive
    private lateinit var audioList: MutableList<AudioListModel>
    //El primer parámetro es el ID de la carpeta Manual y el otro es el de la capeta ACD para las llamadas
    private val foldersId : Array<String> = arrayOf ("1T1a2wlQWAU0qG0JMMkeSJDANqSXZwBR_","1zKXsFSRRDXIVmTJ7K9xYyMO5v1UoZzOF")
    // Carpeta general para subir archivos de pólizas
    val idFolderGeneral = "1Tbs3CfS-S9qxN4FIflG_TFqq6icYQ--j"

    @PostMapping("upload-files/{idFolder}")
    fun uploadFileToSpecificFolder (@PathVariable ("idFolder") idFolder: String,
                              @RequestParam ("file") file: MultipartFile): ResponseEntity<String?> {
        val googleMetadata = File()
        val javaFile = java.io.File.createTempFile("temp", null)
        var service = Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY,
                getCredentials(HTTP_TRANSPORT,TOKENS_PATH_ARCHIVOS, listOf(DriveScopes.DRIVE)))
                .setApplicationName(APPLICATION_NAME)
                .build()

        file.transferTo(javaFile)
        googleMetadata.name = file.originalFilename
        googleMetadata.mimeType = file.contentType
        googleMetadata.parents = Collections.singletonList(idFolder)

        //En caso de que haya algún error se regresará una respuesta
        return try {
            val id = service.Files().create(googleMetadata, FileContent(file.contentType, javaFile)).execute().id
            javaFile.delete()
            ResponseEntity(id,HttpStatus.OK)
        } catch (e: Exception) {
            javaFile.delete()
            ResponseEntity("El archivo: '" + googleMetadata.name + "' no se subió correctamente", HttpStatus.EXPECTATION_FAILED)
        }

    }

    @PostMapping("upload-files/{idCliente}/{idRegistro}")
    fun createANewFolder (@PathVariable("idCliente") idCliente: Long,
                    @PathVariable("idRegistro") idRegistro: Long,
                    @RequestParam ("file") file: MultipartFile): ResponseEntity<String> {
        var idFolderArchivo: String
        var idArchivo: String = idCliente.toString()
        var service = Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY,
                    getCredentials(HTTP_TRANSPORT,TOKENS_PATH_ARCHIVOS, listOf(DriveScopes.DRIVE)))
                    .setApplicationName(APPLICATION_NAME)
                    .build()

        if (file.isEmpty) {
            return ResponseEntity("¡Debes adjuntar archivos a la petición!", HttpStatus.BAD_REQUEST)
        } else {
            try {
                /*Se busca una carpeta que tenga como nombre el ID de cliente y
                en caso de que no exista se crea y una vez creada se guarda su id*/

                idFolderArchivo = createAndSearchFolder(this.idFolderGeneral,idCliente.toString())

                /* En caso de que la solicitud sea para almacenar archivos del producto cliente, se buscará y/o se creará
                   su carpeta correspondiente */
                if (idRegistro != 0L) {
                    idArchivo = idRegistro.toString()
                    idFolderArchivo = createAndSearchFolder(idFolderArchivo,idRegistro.toString())
                }
            } catch (e: Exception) {
               return ResponseEntity("Error al subir los archivos", HttpStatus.EXPECTATION_FAILED)
            }

            // En esta parte del código se suben los archivos a la carpeta que se buscó en el bloque anterior de código
            val googleMetadata = File()
            val javaFile = java.io.File.createTempFile("temp", null)

            file.transferTo(javaFile)
            googleMetadata.name = idArchivo + "-" + file.originalFilename
            googleMetadata.mimeType = file.contentType
            googleMetadata.parents = Collections.singletonList(idFolderArchivo)

            //En caso de que haya algún error se regresará una respuesta
            return try {
                service.Files().create(googleMetadata, FileContent(file.contentType, javaFile)).execute()
                javaFile.delete()
                ResponseEntity(idFolderArchivo, HttpStatus.OK)
            } catch (e: Exception) {
                javaFile.delete()
                ResponseEntity("El archivo: '" + googleMetadata.name + "' no se subió correctamente", HttpStatus.EXPECTATION_FAILED)
            }
        }
    }

    @PostMapping("subir-imagen")
    fun subirImagen (@RequestParam ("file") file: MultipartFile): ResponseEntity<String> {
        var service = Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY,
                getCredentials(HTTP_TRANSPORT,TOKENS_PATH_ARCHIVOS, listOf(DriveScopes.DRIVE)))
                .setApplicationName(APPLICATION_NAME)
                .build()
        val googleMetadata = File()
        val javaFile = java.io.File.createTempFile("temp", null)

        file.transferTo(javaFile)
        googleMetadata.name = file.originalFilename
        googleMetadata.mimeType = file.contentType
        googleMetadata.parents = Collections.singletonList("1bvYwJ3J9nV1Fh6cq2yrnJhRWdH8qbHck")
        return try {
            service.Files().create(googleMetadata, FileContent(file.contentType, javaFile)).execute()
            javaFile.delete()
            ResponseEntity("Archivos subidos correctamente!", HttpStatus.OK)
        } catch (e: Exception) {
            javaFile.delete()
            ResponseEntity("Error al subir los archivos", HttpStatus.EXPECTATION_FAILED)
        }
    }

    @GetMapping("obtener-documentos/{tipo-documentos}/{parametroComplementario}")
    fun getFiles (@PathVariable("tipo-documentos") tipoDocumento: Int,
                  @PathVariable ("parametroComplementario") parametroComplementario: String): ResponseEntity<MutableList<ByteArray>> {
        /* Debido a que no es una misma búsqueda para todos los archivos, la variable "parametroComplementario"
            puede ser el ID del cliente, de la carpeta de registro o del archivo de pago*/
        val arregloElementos = mutableListOf<ByteArray>()
        var folderId: String?
        val service = Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY,
                getCredentials(HTTP_TRANSPORT,TOKENS_PATH_ARCHIVOS, listOf(DriveScopes.DRIVE)))
                .setApplicationName(APPLICATION_NAME)
                .build()

        when (tipoDocumento) {
            1 -> {
                folderId = createAndSearchFolder(this.idFolderGeneral,parametroComplementario)
                val fileId = getIdOfMostRecentFile ("cliente", folderId.toString())
                if (fileId != null) {
                    arregloElementos.add(service.files().get(fileId).executeMediaAsInputStream().readBytes())
                }
                return ResponseEntity(arregloElementos, HttpStatus.OK)
            }
            2 -> {
                val fileId = getIdOfMostRecentFile ("poliza", parametroComplementario)
                if (fileId != null) {
                    arregloElementos.add(service.files().get(fileId).executeMediaAsInputStream().readBytes())
                }
                return ResponseEntity(arregloElementos, HttpStatus.OK)
            }
            4 -> {
                arregloElementos.add(service.files().get(parametroComplementario).executeMediaAsInputStream().readBytes())
                return ResponseEntity(arregloElementos, HttpStatus.OK)
            }
            else -> return ResponseEntity(HttpStatus.BAD_REQUEST)
        }
    }

     /*Método que busca si existe alguna carpeta que tenga de nombre el ID que se le especificó y
        en caso de que no exista, la crea*/
    fun createAndSearchFolder (idParentFolder: String ,folderName: String): String {
        val service = Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY,
                getCredentials(HTTP_TRANSPORT,TOKENS_PATH_ARCHIVOS, listOf(DriveScopes.DRIVE)))
                .setApplicationName(APPLICATION_NAME)
                .build()

        val idFolder =  service.files().list().setQ(
                "'" + idParentFolder + "' in parents and name = '" + folderName
                        + "' and trashed=false and mimeType= 'application/vnd.google-apps.folder'"
        ).setFields("files(id)").execute().files

        return if (idFolder.isNullOrEmpty()) {
            val newFolder = File()

            newFolder.parents = Collections.singletonList(idParentFolder)
            newFolder.name = folderName
            newFolder.mimeType = "application/vnd.google-apps.folder"
            service.Files().create(newFolder).setFields("id").execute().id
        } else {
            idFolder.get(0).id
        }

    }

    fun getIdOfMostRecentFile (nombreArchivo: String, folderId: String): String? {
        val service = Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY,
                getCredentials(HTTP_TRANSPORT,TOKENS_PATH_ARCHIVOS, listOf(DriveScopes.DRIVE)))
                .setApplicationName(APPLICATION_NAME)
                .build()
        val listRequest = service.files().list()

        listRequest.q = "'" + folderId + "' in parents and name contains '" + nombreArchivo + "' and trashed=false"
        listRequest.pageSize = 1
        listRequest.fields = "files(id)"

        val idFile = listRequest.execute().files

        return if (idFile.isNullOrEmpty()) {
            null
        } else {
            idFile.get(0).id
        }
    }

/************************* El código que está abajo es para el módulo de control de llamadas **************************/

    @GetMapping("{firstDate}/{lastDate}/{ACD}/{manual}/{numeroTelefonico}")
    fun getAvailableFiles(@PathVariable ("firstDate") firstDate: Long, @PathVariable("lastDate")lastDate: Long,
                          @PathVariable("ACD") ACD: Boolean, @PathVariable ("manual") manual: Boolean,
                          @PathVariable("numeroTelefonico") numeroTelefonico: String): MutableList<AudioListModel> {
        val fechaInicio: Calendar =  Calendar.getInstance()
        audioList = mutableListOf()

        service = Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY,
                getCredentials(HTTP_TRANSPORT,TOKENS_PATH_LLAMADAS, listOf(DriveScopes.DRIVE_READONLY)))
                .setApplicationName(APPLICATION_NAME)
                .build()

        fechaInicio.timeInMillis = firstDate

        runBlocking {
            if (ACD && manual) {
                if (lastDate != 0.toLong()) {
                    var fechaAux = fechaInicio
                    val fechaFin: Calendar = Calendar.getInstance()
                    fechaFin.timeInMillis = lastDate

                    while (fechaAux <= fechaFin) {
                        val fechaCorrutina: Calendar = Calendar.getInstance()
                        fechaCorrutina.timeInMillis = fechaAux.timeInMillis
                        async { searchOnFolders(foldersId[0], fechaCorrutina.get(Calendar.YEAR).toString(), 1, fechaCorrutina, numeroTelefonico) }
                        async { searchOnFolders(foldersId[1], fechaCorrutina.get(Calendar.YEAR).toString(), 1, fechaCorrutina, numeroTelefonico) }
                        fechaAux.add(Calendar.DAY_OF_MONTH, 1)
                    }
                } else {
                    async { searchOnFolders(foldersId[0], fechaInicio.get(Calendar.YEAR).toString(), 1, fechaInicio, numeroTelefonico) }
                    async { searchOnFolders(foldersId[1], fechaInicio.get(Calendar.YEAR).toString(), 1, fechaInicio, numeroTelefonico) }
                }
            } else {
                val idTipo: Int

                if (manual) {
                    idTipo = 0
                } else {
                    idTipo = 1
                }

                if (lastDate != 0.toLong()) {
                    var fechaAux = fechaInicio
                    val fechaFin: Calendar = Calendar.getInstance()

                    fechaFin.timeInMillis = lastDate
                    while (fechaAux <= fechaFin) {
                        val fechaCorrutina: Calendar = Calendar.getInstance()
                        fechaCorrutina.timeInMillis = fechaAux.timeInMillis
                        async { searchOnFolders(foldersId[idTipo], fechaCorrutina.get(Calendar.YEAR).toString(), 1, fechaCorrutina, numeroTelefonico) }
                        fechaAux.add(Calendar.DAY_OF_MONTH, 1)
                    }
                } else {
                    searchOnFolders(foldersId[idTipo], fechaInicio.get(Calendar.YEAR).toString(), 1, fechaInicio, numeroTelefonico)
                }
            }
        }

        return audioList
    }

    private fun searchOnFolders(folderId: String, folderName: String, level: Int, date: Calendar, numeroTelefonico: String) {
        val folders: FileList = service.files().list().setQ(
                "'" + folderId + "' in parents and name contains '" + folderName
                        + "' and trashed=false and mimeType= 'application/vnd.google-apps.folder'"
        ).setFields("files(id,name)").execute()

        try{
            val id = folders.files[0].id

            when(level){
                1 -> {
                    val monthsName: Array <String> = arrayOf(
                            "Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre", "Octubre",
                            "Noviembre","Diciembre")
                    searchOnFolders(id, monthsName[date.get(Calendar.MONTH)],2, date, numeroTelefonico)
                }
                2 -> {
                    var fecha: String
                    fecha = date.get(Calendar.YEAR).toString() + "-"

                    if (date.get(Calendar.MONTH) < 9) {
                        fecha = fecha + "0" + (date.get(Calendar.MONTH) + 1).toString() + "-"
                    } else { fecha = fecha  + (date.get(Calendar.MONTH) + 1).toString() + "-" }

                    if (date.get(Calendar.DAY_OF_MONTH) < 10) {
                        fecha = fecha + "0" + date.get(Calendar.DAY_OF_MONTH).toString()
                    } else { fecha = fecha + date.get(Calendar.DAY_OF_MONTH).toString() }

                    searchOnFolders(id, fecha, 3, date, numeroTelefonico) }
                3 -> { runBlocking { getFilesInFolder(id, date, numeroTelefonico) } }
            }
        }catch(e: Exception){}
    }

    private suspend fun getFilesInFolder(folderId: String, date: Calendar, numeroTelefonico: String) {
        var filesOnFolder = FileList()
        var previousToken = String()
        val listRequest: Drive.Files.List = service.files().list()

        while(true){
            listRequest.q = "'" + folderId + "' in parents and trashed=false and name contains '" + numeroTelefonico + "'"
            listRequest.pageSize = 1000
            listRequest.fields = "nextPageToken,files(name,webViewLink)"

            if(!filesOnFolder.isNullOrEmpty()) { listRequest.pageToken = previousToken }

            filesOnFolder = listRequest.execute()

            for (f: File in filesOnFolder.files) {
                val tipo: String
                val fecha: Date = date.time

                if (f.name[0] == 'M') {
                    tipo = "Manual"
                } else {
                    tipo = "ACD"
                }

                audioList.add(AudioListModel(tipo, fecha, numeroTelefonico, f.webViewLink))
            }

            if(filesOnFolder.nextPageToken.isNullOrBlank() == false){
                previousToken = filesOnFolder.nextPageToken
            } else { break }
        }
    }

    //Obtener credenciales
    @Throws(IOException::class)
    private fun getCredentials(HTTP_TRANSPORT: NetHttpTransport, path: String, SCOPE: List<String>): Credential {
        // Load client secrets.
        val `in`: java.io.File = File(CREDENTIALS_FILE_PATH)
        val clientSecrets: GoogleClientSecrets = GoogleClientSecrets.load(JSON_FACTORY, InputStreamReader(`in`.inputStream()))

        // Build flow and trigger user authorization request.
        val flow: GoogleAuthorizationCodeFlow = GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPE)
                .setDataStoreFactory(FileDataStoreFactory(java.io.File(path)))
                .setAccessType("offline")
                .build()
        val receiver: LocalServerReceiver = LocalServerReceiver.Builder().setPort(8888).build()
        return AuthorizationCodeInstalledApp(flow, receiver).authorize("user")
    }

}