package com.ahorraseguros.kotlinmark43.controllers.TI

import com.ahorraseguros.kotlinmark43.models.TI.EstadoInventarioModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.CatalogoSoporte.EstadoInventarioModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.EstadoInventarioRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/estado-inventario")
class EstadoInventarioController : LogsOperations {
    @Autowired
    lateinit var estadoInventarioRepository: EstadoInventarioRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "estadoInventario"
    @GetMapping
    fun findAll(): MutableList<EstadoInventarioModels>? {
        try {
            return estadoInventarioRepository.findAll()
        }catch (e:Exception){
            e.printStackTrace()
            return null
        }
    }

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<EstadoInventarioModels> = estadoInventarioRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody estadoInventarioModels: EstadoInventarioModels): EstadoInventarioModels = estadoInventarioRepository.save(estadoInventarioModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody estadoInventarioModels: EstadoInventarioModels,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long ): EstadoInventarioModels {
        var inventarioUpdated = estadoInventarioRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(EstadoInventarioModelsLogs(0, inventarioUpdated.get().estado, inventarioUpdated.get().idEstado,
                           inventarioUpdated.get().activo, idLog))

        estadoInventarioModels.id = inventarioUpdated.get().id
        return estadoInventarioRepository.save(estadoInventarioModels)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/estadoInventario", a, String::class.java)
}