package com.ahorraseguros.kotlinmark43.controllers.TI.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.CharolasRackModel
import com.ahorraseguros.kotlinmark43.models.TI.views.CharolasRackViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte.CharolasRackModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.Soporte.CharolasRackRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.CharolasRackViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@RestController
@CrossOrigin
@RequestMapping ("/v1/charolasRack")

class CharolasRackController: LogsOperations {
    @Autowired
    lateinit var charolasRackRepository: CharolasRackRepository

    @Autowired
    lateinit var charolasRacksViewRepository: CharolasRackViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "charolasRack"
    @GetMapping
    fun findAll () : MutableList<CharolasRackViewModel> = charolasRacksViewRepository.findAll()

    @GetMapping ("{id}")
    fun findById (@PathVariable("id")  id: Int) : ResponseEntity<CharolasRackViewModel> = charolasRacksViewRepository.findById(id).map{ s -> ResponseEntity.ok(s)}
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody charolasRackModel: CharolasRackModel) : CharolasRackModel = charolasRackRepository.save(charolasRackModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody charolasRackModel: CharolasRackModel,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long): CharolasRackModel {
        val charolasUdate = charolasRackRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(CharolasRackModelsLogs(0, charolasUdate.get().idEmpleado, charolasUdate.get().idObservacionesInventario,
                charolasUdate.get().idEstadoInventario, charolasUdate.get().Folio, charolasUdate.get().idMarca,
                charolasUdate.get().idTipoSede, charolasUdate.get().origenRecurso, charolasUdate.get().activo,
                charolasUdate.get().fechaRecepcion, charolasUdate.get().fechaSalida, idLog))

        charolasRackModel.id = charolasUdate.get().id
        return charolasRackRepository.save(charolasRackModel)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/charolasRack", a, String::class.java)
}