package com.ahorraseguros.kotlinmark43.controllers.TI.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.CamarasModel
import com.ahorraseguros.kotlinmark43.models.TI.views.CamarasViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte.CamarasModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.Soporte.CamarasRespository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.CamarasViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping ("/v1/camaras")

class CamarasController: LogsOperations {
    @Autowired
    lateinit var camarasRespository: CamarasRespository

    @Autowired
    lateinit var camarasViewRepository: CamarasViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "camaras"

    @GetMapping
    fun findAll () : MutableList<CamarasViewModel> = camarasViewRepository.findAll()

    @GetMapping ("{id}")
    fun findById (@PathVariable("id")  id: Int) : ResponseEntity<CamarasViewModel> = camarasViewRepository.findById(id).map{ s -> ResponseEntity.ok(s)}
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody camarasModel: CamarasModel) : CamarasModel = camarasRespository.save(camarasModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody camarasModel: CamarasModel,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long ): CamarasModel {
        val camarasUdate = camarasRespository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(CamarasModelsLogs(0, camarasUdate.get().idEmpleado, camarasUdate.get ().idObservacionesInventario,
                camarasUdate.get().idEstadoInventario, camarasUdate.get().Folio, camarasUdate.get().idMarca,
                camarasUdate.get().idTipoSede, camarasUdate.get().origenRecurso, camarasUdate.get().activo,
                camarasUdate.get().fechaRecepcion, camarasUdate.get().fechaSalida, idLog))

        camarasModel.id = camarasUdate.get().id
        return camarasRespository.save(camarasModel)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/camaras", a, String::class.java)
}