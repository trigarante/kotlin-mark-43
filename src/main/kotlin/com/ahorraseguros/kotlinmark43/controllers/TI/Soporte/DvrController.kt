package com.ahorraseguros.kotlinmark43.controllers.TI.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.DvrModel
import com.ahorraseguros.kotlinmark43.models.TI.views.DvrViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte.DvrModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.Soporte.DvrRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.DvrViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/Dvr")

class DvrController: LogsOperations {
    @Autowired
    lateinit var dvrRepository: DvrRepository

    @Autowired
    lateinit var dvrViewRepository: DvrViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "dvr"

    @GetMapping
    fun findAll () : MutableList<DvrViewModel> = dvrViewRepository.findAll()

    @GetMapping ("{id}")
    fun findById (@PathVariable("id")  id: Int) : ResponseEntity<DvrViewModel> = dvrViewRepository.findById(id).map{ s -> ResponseEntity.ok(s)}
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody dvrModel: DvrModel) : DvrModel = dvrRepository.save(dvrModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody dvrModel: DvrModel,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long ): DvrModel {
        val dvrUdate = dvrRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(DvrModelsLogs(0, dvrUdate.get().idEmpleado, dvrUdate.get ().idObservacionesInventario,
                dvrUdate.get().idEstadoInventario, dvrUdate.get().Folio, dvrUdate.get().idMarca,
                dvrUdate.get().idTipoSede, dvrUdate.get().origenRecurso, dvrUdate.get().activo,
                dvrUdate.get().fechaRecepcion, dvrUdate.get().fechaSalida, idLog))
        dvrModel.id = dvrUdate.get().id
        return dvrRepository.save(dvrModel)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/dvr", a, String::class.java)
}