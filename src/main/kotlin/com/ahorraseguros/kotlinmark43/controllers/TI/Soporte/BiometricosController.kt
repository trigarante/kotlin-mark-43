package com.ahorraseguros.kotlinmark43.controllers.TI.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.BiometricosSoporteModel
import com.ahorraseguros.kotlinmark43.models.TI.views.BiometricosSoporteViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte.BiometricosSoporteModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.Soporte.BiometricosRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.BiometricosViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping ("/v1/biometricos")

class BiometricosController: LogsOperations {
    @Autowired
    lateinit var biometricosRepository: BiometricosRepository

    @Autowired
    lateinit var biometricosViewRepository: BiometricosViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "BiometricosSoporte"
    @GetMapping
    fun findAll () : MutableList<BiometricosSoporteViewModel> = biometricosViewRepository.findAll()

    @GetMapping ("{id}")
    fun findById (@PathVariable("id")  id: Int) : ResponseEntity<BiometricosSoporteViewModel> = biometricosViewRepository.findById(id).map{ s -> ResponseEntity.ok(s)}
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody biometricosModel: BiometricosSoporteModel) : BiometricosSoporteModel = biometricosRepository.save(biometricosModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody biometricosModel: BiometricosSoporteModel,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long): BiometricosSoporteModel {
        val biometricosUdate = biometricosRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(BiometricosSoporteModelsLogs(0, biometricosUdate.get().idEmpleado, biometricosUdate.get ().idObservacionesInventario,
                biometricosUdate.get().idEstadoInventario, biometricosUdate.get().folio, biometricosUdate.get().idMarca,
                biometricosUdate.get().idTipoSede, biometricosUdate.get().origenRecurso, biometricosUdate.get().activo,
                biometricosUdate.get().fechaRecepcion, biometricosUdate.get().fechaSalida, idLog))
        biometricosModel.id = biometricosUdate.get().id
        return biometricosRepository.save(biometricosModel)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/biometricosSoporte", a, String::class.java)
}