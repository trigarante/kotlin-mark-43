package com.ahorraseguros.kotlinmark43.controllers.TI.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.DiscoDuroExternoModel
import com.ahorraseguros.kotlinmark43.models.TI.views.DiscoDuroExternoViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte.DiscoDuroExternoModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.Soporte.DiscoDuroExternoRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.DiscoDuroExternoViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping ("/v1/discodExterno")

class DiscoDuroExternoController: LogsOperations {

    @Autowired
    lateinit var discoDuroExternoRepository: DiscoDuroExternoRepository

    @Autowired
    lateinit var discoDuroExternoViewRepository: DiscoDuroExternoViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "discoDuroExterno"
    @GetMapping
    fun findAll () : MutableList<DiscoDuroExternoViewModel> = discoDuroExternoViewRepository.findAll()

    @GetMapping ("{id}")
    fun findById (@PathVariable("id")  id: Int) : ResponseEntity<DiscoDuroExternoViewModel> = discoDuroExternoViewRepository.findById(id).map{ s -> ResponseEntity.ok(s)}
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody discoDuroExternoModel: DiscoDuroExternoModel) : DiscoDuroExternoModel = discoDuroExternoRepository.save(discoDuroExternoModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody discoDuroExternoModel: DiscoDuroExternoModel,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long): DiscoDuroExternoModel {
        val ddExternoUdate = discoDuroExternoRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(DiscoDuroExternoModelsLogs(0, ddExternoUdate.get().idEmpleado, ddExternoUdate.get().idObservacionesInventario,
                                  ddExternoUdate.get().idEstadoInventario, ddExternoUdate.get().Folio, ddExternoUdate.get().idMarca,
                                  ddExternoUdate.get().idTipoSede, ddExternoUdate.get().origenRecurso, ddExternoUdate.get().activo,
                                  ddExternoUdate.get().fechaRecepcion, ddExternoUdate.get().fechaSalida, idLog))

        discoDuroExternoModel.id = ddExternoUdate.get().id
        return discoDuroExternoRepository.save(discoDuroExternoModel)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/discoDuroExterno", a, String::class.java)

}