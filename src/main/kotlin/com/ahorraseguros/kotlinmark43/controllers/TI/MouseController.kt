package com.ahorraseguros.kotlinmark43.controllers.TI

import com.ahorraseguros.kotlinmark43.models.TI.MouseModels
import com.ahorraseguros.kotlinmark43.models.TI.views.MouseViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte.MouseModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.MouseRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.MouseViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/mouse")
class MouseController: LogsOperations {
    @Autowired
    lateinit var mouseRepository: MouseRepository
    @Autowired
    lateinit var mouseViewRepository: MouseViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "mouse"

    @GetMapping
    fun findAll(): MutableList<MouseViewModel> = mouseViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<MouseViewModel> = mouseViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody mouseModels: MouseModels): MouseModels = mouseRepository.save(mouseModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody mouseModels: MouseModels,
               @PathVariable( "idEmpleadoModificante") idEmpleadoModificante: Long): MouseModels {
        var mouseUpdate = mouseRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1, id).id

        callServerLogs(MouseModelsLogs(0, mouseUpdate.get().idEmpleado, mouseUpdate.get().idMarca,
                mouseUpdate.get().idEstadoInventario, mouseUpdate.get().fechaRecepcion, mouseUpdate.get().fechaSalida,
                mouseUpdate.get().idObservacionesInventario, mouseUpdate.get().comentarios, mouseUpdate.get().activo,
                mouseUpdate.get().idTipoSede, mouseUpdate.get().origenRecurso, idLog))

        mouseModels.id = mouseUpdate.get().id
        return mouseRepository.save(mouseModels)
    }

    override fun createLogs(idEmpleado:Long, idTipoLog: Int, idTabla:Long) : LogsModels =
            logsRepository.save(LogsModels(0, idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                           Timestamp(System.currentTimeMillis())))
    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/mouse", a, String::class.java)
    }