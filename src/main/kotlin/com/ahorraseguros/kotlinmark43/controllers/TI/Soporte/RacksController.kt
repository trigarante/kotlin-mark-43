package com.ahorraseguros.kotlinmark43.controllers.TI.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.RacksModel
import com.ahorraseguros.kotlinmark43.models.TI.views.RacksViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte.RacksModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.Soporte.RacksRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.RacksViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping ("/v1/racks")

class RacksController: LogsOperations {

    @Autowired
    lateinit var racksRepository: RacksRepository

    @Autowired
    lateinit var racksViewsRepository: RacksViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "racks"

    @GetMapping
    fun findAll () : MutableList<RacksViewModel> = racksViewsRepository.findAll()

    @GetMapping ("{id}")
    fun findById (@PathVariable("id")  id: Int) : ResponseEntity<RacksViewModel> = racksViewsRepository.findById(id).map{ s -> ResponseEntity.ok(s)}
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody racksModel: RacksModel) : RacksModel = racksRepository.save(racksModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody racksModel: RacksModel,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long): RacksModel {
        val rackwsUdate = racksRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(RacksModelsLogs(0, rackwsUdate.get().idEmpleado, rackwsUdate.get().idObservacionesInventario,
                rackwsUdate.get().idEstadoInventario, rackwsUdate.get().Folio, rackwsUdate.get().idMarca,
                rackwsUdate.get().idTipoSede, rackwsUdate.get().origenRecurso, rackwsUdate.get().activo,
                rackwsUdate.get().fechaRecepcion, rackwsUdate.get().fechaSalida, idLog))


        racksModel.id = rackwsUdate.get().id
        return racksRepository.save(racksModel)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/racks", a, String::class.java)
}