package com.ahorraseguros.kotlinmark43.controllers.TI

import com.ahorraseguros.kotlinmark43.models.TI.TabletModels

import com.ahorraseguros.kotlinmark43.models.TI.views.TabletsViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte.TabletsModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.TabletRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.TabletsViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tablet")
class TabletController: LogsOperations {
    @Autowired
    lateinit var tabletRepository: TabletRepository

    @Autowired
    lateinit var tabletsViewRepository: TabletsViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "tablets"
    @GetMapping
    fun findAll(): MutableList<TabletsViewModel> = tabletsViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<TabletsViewModel> = tabletsViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody tabletModels: TabletModels): TabletModels = tabletRepository.save(tabletModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody tabletModels: TabletModels,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long  ): TabletModels {
        var tabletUpdate = tabletRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(TabletsModelsLogs(0,tabletUpdate.get().idEmpleado, tabletUpdate.get().numFolio, tabletUpdate.get().idProcesador
                                              ,tabletUpdate.get().capacidad, tabletUpdate.get().ram, tabletUpdate.get().idMarca
                                              ,tabletUpdate.get().idEstadoInventario, tabletUpdate.get().telefono
                                              ,tabletUpdate.get().fechaRecepcion, tabletUpdate.get().fechaSalida, tabletUpdate.get().idObservacionesInventario
                                              ,tabletUpdate.get().comentarios, tabletUpdate.get().activo, tabletUpdate.get().idTipoSede
                                              ,tabletUpdate.get().origenRecurso,  idLog))

        tabletModels.id = tabletUpdate.get().id
        return tabletRepository.save(tabletModels)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/tablets", a, String::class.java)

    //fun prueba()= tabletRepository1.findAllByTelefono()

}