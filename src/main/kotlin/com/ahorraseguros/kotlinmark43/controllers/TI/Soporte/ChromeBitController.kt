package com.ahorraseguros.kotlinmark43.controllers.TI.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.ChromeBitModel
import com.ahorraseguros.kotlinmark43.models.TI.views.ChromeBitViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte.ChromeBitModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.Soporte.ChromeBitRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.ChromeBitViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping ("/v1/chromeBit")

class ChromeBitController: LogsOperations {

    @Autowired
    lateinit var chromeBitRepository: ChromeBitRepository

    @Autowired
    lateinit var chromeBitViewRepository: ChromeBitViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "chromeBit"

    @GetMapping
    fun findAll () : MutableList<ChromeBitViewModel> = chromeBitViewRepository.findAll()

    @GetMapping ("{id}")
    fun findById (@PathVariable("id")  id: Int) : ResponseEntity<ChromeBitViewModel> = chromeBitViewRepository.findById(id).map{ s -> ResponseEntity.ok(s)}
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody chromeBitModel: ChromeBitModel) : ChromeBitModel = chromeBitRepository.save(chromeBitModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody chromeBitModel: ChromeBitModel,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long): ChromeBitModel {
        val chromeUdate = chromeBitRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(ChromeBitModelsLogs(0, chromeUdate.get().idEmpleado, chromeUdate.get ().idObservacionesInventario,
                chromeUdate.get().idEstadoInventario, chromeUdate.get().Folio, chromeUdate.get().idMarca,
                chromeUdate.get().idTipoSede, chromeUdate.get().origenRecurso, chromeUdate.get().activo,
                chromeUdate.get().fechaRecepcion, chromeUdate.get().fechaSalida, idLog))

        chromeBitModel.id = chromeUdate.get().id
        return chromeBitRepository.save(chromeBitModel)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/chromeBit", a, String::class.java)
}