package com.ahorraseguros.kotlinmark43.controllers.TI.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.ServidoresModel
import com.ahorraseguros.kotlinmark43.models.TI.views.ServidoresViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte.ServidoresModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.Soporte.ServidoresRpository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.ServidoresViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping ("/v1/servidores")

class ServidoresController: LogsOperations {

    @Autowired
    lateinit var servidoresRpository: ServidoresRpository

    @Autowired
    lateinit var servidoresViewRepository: ServidoresViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "servidores"

    @GetMapping
    fun findAll () : MutableList<ServidoresViewModel> = servidoresViewRepository.findAll()

    @GetMapping ("{id}")
    fun findById (@PathVariable("id")  id: Int) : ResponseEntity<ServidoresViewModel> = servidoresViewRepository.findById(id).map{ s -> ResponseEntity.ok(s)}
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody servidoresModel: ServidoresModel) : ServidoresModel = servidoresRpository.save(servidoresModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody servidoresModel: ServidoresModel,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long ): ServidoresModel {
        val servidoresUdate = servidoresRpository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(ServidoresModelsLogs(0, servidoresUdate.get().idEmpleado, servidoresUdate.get().idObservacionesInventario,
                          servidoresUdate.get().idEstadoInventario, servidoresUdate.get().folio, servidoresUdate.get().idMarca,
                          servidoresUdate.get().idTipoSede, servidoresUdate.get().origenRecurso, servidoresUdate.get().activo,
                          servidoresUdate.get().fechaRecepcion, servidoresUdate.get().fechaSalida, idLog))

        servidoresModel.id = servidoresUdate.get().id

        return servidoresRpository.save(servidoresModel)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/servidores", a, String::class.java)
}