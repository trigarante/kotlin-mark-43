package com.ahorraseguros.kotlinmark43.controllers.TI.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.TelefonoIpModel
import com.ahorraseguros.kotlinmark43.models.TI.views.TelefonolpViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte.TelefonoIpModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.Soporte.TelefonoIpRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.TelefonoIpViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping ("/v1/telefonoIp")

class TelefonoIpController: LogsOperations {

    @Autowired
    lateinit var telefonoIpRepository: TelefonoIpRepository

    @Autowired
    lateinit var telefonoIpViewRepository: TelefonoIpViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository
    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "telefonoIp"

    @GetMapping
    fun findAll () : MutableList<TelefonolpViewModel> = telefonoIpViewRepository.findAll()

    @GetMapping ("{id}")
    fun findById (@PathVariable("id")  id: Int) : ResponseEntity<TelefonolpViewModel> = telefonoIpViewRepository.findById(id).map{ s -> ResponseEntity.ok(s)}
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody telefonoIpModel: TelefonoIpModel) : TelefonoIpModel = telefonoIpRepository.save(telefonoIpModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody telefonoIpModel: TelefonoIpModel,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long): TelefonoIpModel {
        val telefonoIpUdate = telefonoIpRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(TelefonoIpModelsLogs(0, telefonoIpUdate.get().idEmpleado, telefonoIpUdate.get ().idObservacionesInventario,
                telefonoIpUdate.get().idEstadoInventario, telefonoIpUdate.get().Folio, telefonoIpUdate.get().idMarca,
                telefonoIpUdate.get().idTipoSede, telefonoIpUdate.get().origenRecurso, telefonoIpUdate.get().activo,
                telefonoIpUdate.get().fechaRecepcion, telefonoIpUdate.get().fechaSalida, idLog))

        telefonoIpModel.id = telefonoIpUdate.get().id
        return telefonoIpRepository.save(telefonoIpModel)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/telefonoIp", a, String::class.java)

}