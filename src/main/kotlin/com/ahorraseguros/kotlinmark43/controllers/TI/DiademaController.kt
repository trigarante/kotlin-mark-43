package com.ahorraseguros.kotlinmark43.controllers.TI

import com.ahorraseguros.kotlinmark43.models.TI.DiademaModels
import com.ahorraseguros.kotlinmark43.models.TI.views.DiademaViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte.DiademaModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.DiademaRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.DiademaViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/diadema")
class DiademaController: LogsOperations {
    @Autowired
    lateinit var diademaRepository: DiademaRepository
    @Autowired
    lateinit var diademaViewRepository: DiademaViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository
    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "diadema"

    @GetMapping
    fun findAll(): MutableList<DiademaViewModel> = diademaViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<DiademaViewModel> = diademaViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())


    @PostMapping
    fun create(@Valid @RequestBody diademaModels: DiademaModels): DiademaModels = diademaRepository.save(diademaModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody diademaModels: DiademaModels,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long): DiademaModels {
        var diademaUpdate = diademaRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(DiademaModelsLogs(0, diademaUpdate.get().idEmpleado, diademaUpdate.get().numFolio,
                       diademaUpdate.get().idMarca, diademaUpdate.get().idEstadoInventario, diademaUpdate.get().fechaRecepcion,
                       diademaUpdate.get().fechaSalida, diademaUpdate.get().idObservacionesInventario,
                       diademaUpdate.get().comentarios, diademaUpdate.get().activo, diademaUpdate.get().idTipoSede,
                       diademaUpdate.get().origenRecurso ,idLog))

        diademaModels.id = diademaUpdate.get().id
        return diademaRepository.save(diademaModels)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/diadema", a, String::class.java)


}