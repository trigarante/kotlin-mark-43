package com.ahorraseguros.kotlinmark43.controllers.TI

import com.ahorraseguros.kotlinmark43.models.TI.TecladoModels
import com.ahorraseguros.kotlinmark43.models.TI.views.TecladoViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte.TecladoModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.TecladoRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.TecladoViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/teclado")
class TecladoController: LogsOperations {
    @Autowired
    lateinit var tecladoRepository: TecladoRepository
    @Autowired
    lateinit var tecladoViewRepository: TecladoViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "teclado"

    @GetMapping
    fun findAll(): MutableList<TecladoViewModel> = tecladoViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<TecladoViewModel> = tecladoViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody tecladoModels: TecladoModels): TecladoModels = tecladoRepository.save(tecladoModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody tecladoModels: TecladoModels,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long): TecladoModels {
        var tecladoUpdate = tecladoRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(TecladoModelsLogs(0,tecladoUpdate.get().idEmpleado, tecladoUpdate.get().idMarca,
                    tecladoUpdate.get().idEstadoInventario, tecladoUpdate.get().fechaRecepcion, tecladoUpdate.get().fechaSalida,
                    tecladoUpdate.get().idObservacionesInventario, tecladoUpdate.get().comentarios, tecladoUpdate.get().activo,
                    tecladoUpdate.get().idTipoSede, tecladoUpdate.get().origenRecursos, idLog))

        tecladoModels.id = tecladoUpdate.get().id
        return tecladoRepository.save(tecladoModels)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/teclado", a, String::class.java)
}