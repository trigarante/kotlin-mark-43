package com.ahorraseguros.kotlinmark43.controllers.TI.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.TipoSedeModel
import com.ahorraseguros.kotlinmark43.repositories.ti.Soporte.TipoSedeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("v1/tipo-sede")

class TipoSedeController {

    @Autowired
    lateinit var tipoSedeRepository: TipoSedeRepository

    @GetMapping
    fun findAll(): MutableList<TipoSedeModel> = tipoSedeRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<TipoSedeModel> = tipoSedeRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping()
    fun create(@Valid @RequestBody tipoSedeModel: TipoSedeModel): TipoSedeModel = tipoSedeRepository.save(tipoSedeModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Int,
               @Valid @RequestBody tipoSedeModel: TipoSedeModel): TipoSedeModel {
        var tipoSedeUpdate = tipoSedeRepository.findById(id)
        tipoSedeModel.id = tipoSedeUpdate.get().id
        return tipoSedeRepository.save(tipoSedeModel)
    }

}