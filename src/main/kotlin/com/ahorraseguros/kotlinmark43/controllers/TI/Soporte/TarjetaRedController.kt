package com.ahorraseguros.kotlinmark43.controllers.TI.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.TarjetaRedModel
import com.ahorraseguros.kotlinmark43.models.TI.views.TarjetaRedViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte.TarjetaRedModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.Soporte.TarjetaRedRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.TarjetaRedViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping ("/v1/tarjetaRed")

class TarjetaRedController: LogsOperations {

    @Autowired
    lateinit var tarjetaRedRepository: TarjetaRedRepository

    @Autowired
    lateinit var tarjetaRedViewRepository: TarjetaRedViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "tarjetaRed"
    @GetMapping
    fun findAll () : MutableList<TarjetaRedViewModel> = tarjetaRedViewRepository.findAll()

    @GetMapping ("{id}")
    fun findById (@PathVariable("id")  id: Int) : ResponseEntity<TarjetaRedViewModel> = tarjetaRedViewRepository.findById(id).map{ s -> ResponseEntity.ok(s)}
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody tarjetaRedModel: TarjetaRedModel) : TarjetaRedModel = tarjetaRedRepository.save(tarjetaRedModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody tarjetaRedModel: TarjetaRedModel,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long  ): TarjetaRedModel {
        val tRedUdate = tarjetaRedRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(TarjetaRedModelsLogs(0, tRedUdate.get().idEmpleado, tRedUdate.get ().idObservacionesInventario,
                           tRedUdate.get().idEstadoInventario, tRedUdate.get().Folio, tRedUdate.get().idMarca,
                           tRedUdate.get().idTipoSede, tRedUdate.get().origenRecurso, tRedUdate.get().activo,
                           tRedUdate.get().fechaRecepcion, tRedUdate.get().fechaSalida, idLog))
        tarjetaRedModel.id = tRedUdate.get().id
        return tarjetaRedRepository.save(tarjetaRedModel)
    }

    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/tarjetaRed", a, String::class.java)
}