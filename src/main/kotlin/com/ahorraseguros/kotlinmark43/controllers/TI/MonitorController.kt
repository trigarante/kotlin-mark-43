package com.ahorraseguros.kotlinmark43.controllers.TI

import com.ahorraseguros.kotlinmark43.models.TI.MonitorModels
import com.ahorraseguros.kotlinmark43.models.TI.views.MonitorViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte.MonitorModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.MonitorRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.MonitorViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/monitor")
class MonitorController: LogsOperations {
    @Autowired
    lateinit var monitorRepository: MonitorRepository
    @Autowired
    lateinit var monitorViewRepository: MonitorViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "monitor"

    @GetMapping
    fun findAll(): MutableList<MonitorViewModel> = monitorViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<MonitorViewModel> = monitorViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())
    @PostMapping
    fun create(@Valid @RequestBody monitorModels: MonitorModels): MonitorModels = monitorRepository.save(monitorModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody monitorModels: MonitorModels,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long   ): MonitorModels {
        var monitorUpdate = monitorRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(MonitorModelsLogs(0,monitorUpdate.get().idEmpleado, monitorUpdate.get().numSerie, monitorUpdate.get().idMarca,
                                           monitorUpdate.get().dimensiones, monitorUpdate.get().idEstadoInventario, monitorUpdate.get().fechaRecepcion,
                                           monitorUpdate.get().fechaSalida, monitorUpdate.get().idObservacionesInventario,
                                           monitorUpdate.get().comentarios, monitorUpdate.get().activo, monitorUpdate.get().idTipoSede,
                                           monitorUpdate.get().origenRecurso, idLog))

        monitorModels.id = monitorUpdate.get().id
        return monitorRepository.save(monitorModels)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/monitor", a, String::class.java)
}