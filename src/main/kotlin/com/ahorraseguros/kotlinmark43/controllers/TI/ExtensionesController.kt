package com.ahorraseguros.kotlinmark43.controllers.TI

import com.ahorraseguros.kotlinmark43.models.TI.ExtensionesModels
import com.ahorraseguros.kotlinmark43.models.TI.views.ExtensionesViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.ExtensionesModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.ExtensionesRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.ExtensionesViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid


@CrossOrigin
@RestController
@RequestMapping("/v1/extensiones")
class ExtensionesController: LogsOperations {
    @Autowired
    lateinit var logsRepository: LogsRepository

    var restTemplate = RestTemplate()

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val tabla = "extenciones"

    @Autowired
    lateinit var extensionesRepository: ExtensionesRepository

    @Autowired
    lateinit var extensionesViewRepository: ExtensionesViewRepository

    @GetMapping
    fun findAll(): MutableList<ExtensionesViewModel> = extensionesViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ExtensionesViewModel = extensionesViewRepository.findById(id).get()

    @GetMapping("idSubarea/{idSubarea}/idEstado/{idEstado}")
    fun findByEstadoAndSubarea(@PathVariable idSubarea: Int, @PathVariable idEstado: Int): MutableList<ExtensionesViewModel>
            = extensionesViewRepository.findByIdSubareaAndIdEstado(idSubarea, idEstado)

    @PostMapping
    fun create(@Valid @RequestBody extencionesModels: ExtensionesModels): ExtensionesModels = extensionesRepository.save(extencionesModels)

    @PostMapping("rango/{min}/{max}")
    fun createRange(@PathVariable min: Int ,@PathVariable max: Int ,@RequestBody extencionesModels: ExtensionesModels): Boolean {
        try{
            extensionesRepository.extencionesAuto(min, max, extencionesModels.idSubarea, extencionesModels.idEstado, extencionesModels.descrip)
            return true
        } catch(e: Exception) {
            e.printStackTrace()
            return false
        }
    }

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long,
               @Valid @RequestBody extencionesModels: ExtensionesModels): ExtensionesModels {
        val extensionUpdate = extensionesRepository.findById(id.toInt())
        val idLog: Long = createLogs(idEmpleadoModificante,1,extensionUpdate.get().id.toLong()).id

        callServerLogs(ExtensionesModelsLogs(0,extensionUpdate.get().idSubarea,extensionUpdate.get().idEstado,
                extensionUpdate.get().descrip, idLog))
        extencionesModels.id = extensionUpdate.get().id
        return extensionesRepository.save(extencionesModels)
    }

    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/extensiones", a, String::class.java)
}