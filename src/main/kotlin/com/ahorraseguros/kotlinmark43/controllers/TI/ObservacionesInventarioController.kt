package com.ahorraseguros.kotlinmark43.controllers.TI

import com.ahorraseguros.kotlinmark43.models.TI.ObservacionesInventarioModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.CatalogoSoporte.ObservacionesInventarioModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.ObservacionesInventarioRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/observaciones-inventario")
class ObservacionesInventarioController : LogsOperations {
    @Autowired
    lateinit var observacionesInventarioRepository: ObservacionesInventarioRepository

    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "observacionesInventario"

    @GetMapping
    fun findAll(): MutableList<ObservacionesInventarioModels> = observacionesInventarioRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<ObservacionesInventarioModels> = observacionesInventarioRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody observacionesInventarioModels: ObservacionesInventarioModels): ObservacionesInventarioModels = observacionesInventarioRepository.save(observacionesInventarioModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody observacionesInventarioModels: ObservacionesInventarioModels,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long  ): ObservacionesInventarioModels {
        var observacionesInventarioUpdate = observacionesInventarioRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(ObservacionesInventarioModelsLogs(0, observacionesInventarioUpdate.get().observaciones,
                observacionesInventarioUpdate.get().activo, idLog))

        observacionesInventarioModels.id = observacionesInventarioUpdate.get().id
        return observacionesInventarioRepository.save(observacionesInventarioModels)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/observacionesInventario", a, String::class.java)
}