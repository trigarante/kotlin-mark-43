package com.ahorraseguros.kotlinmark43.controllers.TI

import com.ahorraseguros.kotlinmark43.models.TI.CpuModel
import com.ahorraseguros.kotlinmark43.models.TI.views.CpuViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte.CpuModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.CpuRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.CpuViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/cpu")
class CpuController: LogsOperations{
    @Autowired
    lateinit var cpuRepository: CpuRepository
    @Autowired
    lateinit var cpuViewRepository: CpuViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "cpu"

    @GetMapping
    fun findAll(): MutableList<CpuViewModel> = cpuViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<CpuViewModel> = cpuViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody cpuModel: CpuModel): CpuModel = cpuRepository.save(cpuModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody cpuModel: CpuModel,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long): CpuModel {
        var cpuUpdate = cpuRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(CpuModelsLogs(0, cpuUpdate.get().idEmpleado, cpuUpdate.get().idObservacionesInventario,
                cpuUpdate.get().numeroSerie, cpuUpdate.get().idProcesador, cpuUpdate.get().hdd,
                cpuUpdate.get().ram, cpuUpdate.get().idMarca, cpuUpdate.get().modelo, cpuUpdate.get().fechaRecepcion,
                cpuUpdate.get().fechaSalida, cpuUpdate.get().comentarios, cpuUpdate.get().activo, cpuUpdate.get().idTipoSede,
                cpuUpdate.get().origenRecurso, idLog))

        cpuModel.id = cpuUpdate.get().id
        return cpuRepository.save(cpuModel)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/cpu", a, String::class.java)

}