package com.ahorraseguros.kotlinmark43.controllers.TI

import com.ahorraseguros.kotlinmark43.models.TI.UsuariosModels
import com.ahorraseguros.kotlinmark43.models.TI.views.UsuarioViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.UsuarioModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.EmpleadoRepository
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.UsuarioRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.UsuarioViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import com.ahorraseguros.mx.logskotlinmark43.models.EmpleadoModelsLogs
import org.springframework.http.HttpStatus
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import java.util.*
import javax.validation.Valid


@CrossOrigin
@RestController
@RequestMapping("/v1/usuarios")
class UsuariosController: LogsOperations {

    @Autowired
    lateinit var usuarioRepository: UsuarioRepository

    @Autowired
    lateinit var usuarioViewRepository: UsuarioViewRepository

    @Autowired
    lateinit var empleadoRepository: EmpleadoRepository

    @Autowired
    lateinit var logsRepository: LogsRepository

    var restTemplate = RestTemplate()

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    var tabla = "usuarios"

    @GetMapping
    fun findAll(): MutableList<UsuarioViewModel> = usuarioViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<UsuarioViewModel> = usuarioViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @GetMapping("getUser/{usuario}")
    fun findByUser(@PathVariable("usuario") usuario: String): Optional<UsuarioViewModel> = usuarioViewRepository.findByUsuario(usuario)

    @GetMapping("idSubarea/{idSubarea}/asignado/{asignado}")
    fun findByIdsubarea(@PathVariable("idSubarea") idSubarea: Int, @PathVariable("asignado") asignado: Int): MutableList<UsuarioViewModel> =
            usuarioViewRepository.findAllByIdSubareaAndAndAsignado(idSubarea, asignado)

    @GetMapping("asignado/{asignado}")
    fun findByIdsubarea(@PathVariable("asignado") asignado: Int): MutableList<UsuarioViewModel> =
            usuarioViewRepository.findAllByAsignado(asignado)

    @GetMapping("findByRango/idEstado/{idEstado}/{menor}/{mayor}")
    fun findByRango(@PathVariable("idEstado") idEstado: Int, @PathVariable("menor") menor: Int, @PathVariable("mayor") mayor: Int): MutableList<UsuarioViewModel> =
            usuarioViewRepository.findAllByEstadoAndIdSubareaBetween(idEstado, menor, mayor)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long,
               @Valid @RequestBody usuariosModels: UsuariosModels): UsuariosModels {
        val usuarioUpdate = usuarioRepository.findById(id)
        val idLog: Long = createLogs(idEmpleadoModificante,1,usuarioUpdate.get().id).id

        callServerLogs(UsuarioModelsLogs(0,usuarioUpdate.get().idCorreo,usuarioUpdate.get().idGrupo,
                usuarioUpdate.get().idTipo, usuarioUpdate.get().idSubarea, usuarioUpdate.get().usuario,
                usuarioUpdate.get().password,usuarioUpdate.get().fechaCreacion,
                usuarioUpdate.get().datosUsuario,usuarioUpdate.get().estado,usuarioUpdate.get().nombre, idLog))
        usuariosModels.id = usuarioUpdate.get().id
        return usuarioRepository.save(usuariosModels)
    }

    @PutMapping("idEmpleado/{idEmpleado}/{idEmpleadoModificante}")
    fun updateEmpleado(@PathVariable("idEmpleado") idEmpleado: Long, @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long,
                       @Valid @RequestBody usuarioNuevo: UsuariosModels): HttpStatus {
        return try {
            var usuarioViejo = usuarioRepository.findById(usuarioNuevo.id)
            val empleadoViejo = empleadoRepository.findById(idEmpleado)

            if (usuarioViejo.isPresent && empleadoViejo.isPresent) {
                var idLog: Long = createLogs(idEmpleadoModificante,1,usuarioViejo.get().id).id

                callServerLogs(UsuarioModelsLogs(0,usuarioViejo.get().idCorreo,usuarioViejo.get().idGrupo,
                        usuarioViejo.get().idTipo, usuarioViejo.get().idSubarea, usuarioViejo.get().usuario,
                        usuarioViejo.get().password,usuarioViejo.get().fechaCreacion,
                        usuarioViejo.get().datosUsuario,usuarioViejo.get().estado,usuarioViejo.get().nombre, idLog))

                tabla = "empleado"
                idLog = createLogs(idEmpleadoModificante,1,empleadoViejo.get().id).id

                callServerLogs(EmpleadoModelsLogs(0, idBanco = empleadoViejo.get().idBanco, idCandidato = empleadoViejo.get().idCandidato,
                        idPuesto = empleadoViejo.get().idPuesto,idTipoPuesto = empleadoViejo.get().idTipoPuesto, idUsuario = empleadoViejo.get().idUsuario,
                        puestoDetalle = empleadoViejo.get().puestoDetalle,fechaAltaImss = empleadoViejo.get().fechaAltaImss,
                        documentosPersonales = empleadoViejo.get().documentosPersonales, documentosAdministrativos = empleadoViejo.get().documentosAdministrativos,
                        fechaIngreso = empleadoViejo.get().fechaIngreso,sueldoDiario = empleadoViejo.get().sueldoDiario,sueldoMensual = empleadoViejo.get().sueldoMensual,
                        kpi = empleadoViejo.get().kpi,kpiMensual = empleadoViejo.get().kpiMensual,kpiTrimestral = empleadoViejo.get().kpiTrimestral,
                        kpiSemestral = empleadoViejo.get().kpiSemestral,fechaCambioSueldo = empleadoViejo.get().fechaCambioSueldo, ctaClabe = empleadoViejo.get().ctaClabe,
                        fechaAsignacion = empleadoViejo.get().fechaAsignacion, comentarios = empleadoViejo.get().comentarios, imss = empleadoViejo.get().imss,
                        razonSocial = empleadoViejo.get().razonSocial,rfc = empleadoViejo.get().rfc, fechaFiniquito = empleadoViejo.get().fechaFiniquito,
                        idEstadoFiquito = empleadoViejo.get().idEstadoFiquito,montoFiniquito = empleadoViejo.get().montoFiniquito, idLog = idLog))

                usuarioRepository.save(usuarioNuevo)
                empleadoViejo.get().idUsuario = usuarioNuevo.id
                empleadoRepository.save(empleadoViejo.get())
            }
            HttpStatus.OK
        } catch (e: Exception) {
            print("El error está aquí: ")
            e.printStackTrace()
            HttpStatus.INTERNAL_SERVER_ERROR
        }

    }

    @PostMapping("idEmpleado/{idEmpleado}")
    fun create(@PathVariable("idEmpleado") idEmpleado: Long, @Valid @RequestBody usuariosModels: UsuariosModels): UsuariosModels {
        val u = usuarioRepository.save(usuariosModels)
        try {
            var e = empleadoRepository.findById(idEmpleado)
            e.get().idUsuario = u.id
            empleadoRepository.save(e.get())

            return u
        } catch (e: Exception) {
            println("Error al asignar usuario a empleado $e")
        }
        return u
    }

    @PutMapping("baja-grupo/{idGrupo}/{idEmpleadoModificante}")
    fun bajaGrupo(@PathVariable("idGrupo") idGrupo: Int, @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): String{
        val usuariosViejos: MutableList<UsuariosModels> = usuarioRepository.findByIdGrupo(idGrupo)

        if(!usuariosViejos.isEmpty()){
            var idLog: Long
            for(usuario in usuariosViejos){
                idLog = createLogs(idEmpleadoModificante,1,usuario.id).id
                callServerLogs(UsuarioModelsLogs(0,usuario.idCorreo,usuario.idGrupo, usuario.idTipo,
                        usuario.idSubarea, usuario.usuario, usuario.password,usuario.fechaCreacion,
                        usuario.datosUsuario,usuario.estado,usuario.nombre, idLog))
            }
        }
        return "Se han modificado "+ usuarioRepository.updateGrupo(1,idGrupo).toString()+" campos"
    }

    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any {
        var URL = "$serverLogs/"

        if (tabla == "empleado") {
            URL = URL + "empleados"
        } else if (tabla == "usuarios"){
            URL = URL + "usuarios"
        }

        return restTemplate.postForEntity(URL, a, String::class.java)
    }
}