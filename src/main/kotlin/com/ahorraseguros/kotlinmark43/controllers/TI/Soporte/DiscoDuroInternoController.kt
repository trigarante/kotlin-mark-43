package com.ahorraseguros.kotlinmark43.controllers.TI.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.DiscoDuroInternoModel
import com.ahorraseguros.kotlinmark43.models.TI.views.DiscoDuroInternoViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte.DiscoDuroInternoModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.Soporte.DiscoDuroInternoRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.DiscoDuroInternoViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@RestController
@CrossOrigin
@RequestMapping ("/v1/discodInterno")

class DiscoDuroInternoController: LogsOperations {

    @Autowired
    lateinit var discoDuroInternoRepository: DiscoDuroInternoRepository

    @Autowired
    lateinit var discoDuroInternoViewRepository: DiscoDuroInternoViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "discoDuroInterno"

    @GetMapping
    fun findAll () : MutableList<DiscoDuroInternoViewModel> = discoDuroInternoViewRepository.findAll()

    @GetMapping ("{id}")
    fun findById (@PathVariable("id")  id: Int) : ResponseEntity<DiscoDuroInternoViewModel> = discoDuroInternoViewRepository.findById(id).map{ s -> ResponseEntity.ok(s)}
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody discoDuroInternoModel: DiscoDuroInternoModel) : DiscoDuroInternoModel = discoDuroInternoRepository.save(discoDuroInternoModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody discoDuroInternoModel: DiscoDuroInternoModel,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long): DiscoDuroInternoModel {
        val ddInternoUdate = discoDuroInternoRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(DiscoDuroInternoModelsLogs(0, ddInternoUdate.get().idEmpleado, ddInternoUdate.get ().idObservacionesInventario,
                ddInternoUdate.get().idEstadoInventario, ddInternoUdate.get().Folio, ddInternoUdate.get().idMarca,
                ddInternoUdate.get().idTipoSede, ddInternoUdate.get().origenRecurso, ddInternoUdate.get().activo,
                ddInternoUdate .get().fechaRecepcion, ddInternoUdate.get().fechaSalida, idLog))

        discoDuroInternoModel.id = ddInternoUdate.get().id
        return discoDuroInternoRepository.save(discoDuroInternoModel)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/discoDuroInterno", a, String::class.java)
}