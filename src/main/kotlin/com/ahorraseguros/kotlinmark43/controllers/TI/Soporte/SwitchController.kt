package com.ahorraseguros.kotlinmark43.controllers.TI.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.SwitchsModel
import com.ahorraseguros.kotlinmark43.models.TI.views.SwitchViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte.SwitchModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.Soporte.SwitchRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.SwitchViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping ("/v1/switch")

class SwitchController: LogsOperations {


    @Autowired
    lateinit var switchRepository: SwitchRepository

    @Autowired
    lateinit var switchViewRepository: SwitchViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "switch"

    @GetMapping
    fun findAll () : MutableList<SwitchViewModel> = switchViewRepository.findAll()

    @GetMapping ("{id}")
    fun findById (@PathVariable("id")  id: Int) : ResponseEntity<SwitchViewModel> = switchViewRepository.findById(id).map{ s -> ResponseEntity.ok(s)}
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody switchsModel: SwitchsModel) : SwitchsModel = switchRepository.save(switchsModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody switchsModel: SwitchsModel,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long ): SwitchsModel {
        val switchsUdate = switchRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1, id).id

        callServerLogs(SwitchModelsLogs(0, switchsUdate.get().idEmpleado, switchsUdate.get().idObservacionesInventario,
                                               switchsUdate.get().idEstadoInventario, switchsUdate.get().folio,
                                               switchsUdate.get().idMarca, switchsUdate.get().idTipoSede,
                                               switchsUdate.get().origenRecurso, switchsUdate.get().activo,
                                               switchsUdate.get().fechaRecepcion, switchsUdate.get().fechaRecepcion, idLog))
        switchsModel.id = switchsUdate.get().id
        return switchRepository.save(switchsModel)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/switch", a, String::class.java)
}