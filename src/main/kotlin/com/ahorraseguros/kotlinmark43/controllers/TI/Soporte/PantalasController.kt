package com.ahorraseguros.kotlinmark43.controllers.TI.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.PantallasModel
import com.ahorraseguros.kotlinmark43.models.TI.views.PantallasViewModel
import com.ahorraseguros.kotlinmark43.repositories.ti.Soporte.PantallaRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.PantallasViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping ("/v1/pantallas")

class PantalasController {

    @Autowired
    lateinit var pantallaRepository: PantallaRepository

    @Autowired
    lateinit var pantallasViewRepository: PantallasViewRepository

    @GetMapping
    fun findAll () : MutableList<PantallasViewModel> = pantallasViewRepository.findAll()

    @GetMapping ("{id}")
    fun findById (@PathVariable("id")  id: Int) : ResponseEntity<PantallasViewModel> = pantallasViewRepository.findById(id).map{ s -> ResponseEntity.ok(s)}
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody pantallasModel: PantallasModel) : PantallasModel = pantallaRepository.save(pantallasModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Int,
               @Valid @RequestBody pantallasModel: PantallasModel): PantallasModel {
        val pantallasUdate = pantallaRepository.findById(id)
        pantallasModel.id = pantallasUdate.get().id
        return pantallaRepository.save(pantallasModel)
    }
}