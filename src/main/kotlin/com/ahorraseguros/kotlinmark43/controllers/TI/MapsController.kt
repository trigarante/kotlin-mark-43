package com.ahorraseguros.kotlinmark43.controllers.TI

import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate


@CrossOrigin
@RestController
@RequestMapping("/v1/maps")
class MapsController {

    var restTemplate = RestTemplate()

    @GetMapping("{latitud}/{longitud}/{lugar}")
    fun createURL(@PathVariable("latitud") latitud: String, @PathVariable("longitud") longitud: String,
                  @PathVariable("lugar") lugar: String):String  =
            restTemplate.getForObject(
                    "https://maps.googleapis.com/maps/api/place/textsearch/json?location=$latitud,$longitud&radius=1000&sensor=true&query=$lugar&key=AIzaSyCt40qebdmO_J-_LDw4OLF8b7NnxjeVID4",
                    String::class.java,
                    String::class.java)!!
}