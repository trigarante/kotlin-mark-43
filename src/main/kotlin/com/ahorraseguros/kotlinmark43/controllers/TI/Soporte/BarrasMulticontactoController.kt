package com.ahorraseguros.kotlinmark43.controllers.TI.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.BarrasMulticontactoModel
import com.ahorraseguros.kotlinmark43.models.TI.views.BarrasMulticontactoViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte.BarrasMulticontactoModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.Soporte.BarrasMulticontactoRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.BarrasMulticontactoViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@RestController
@CrossOrigin
@RequestMapping ("/v1/barrasMulticontacto")

class BarrasMulticontactoController : LogsOperations {
    @Autowired
    lateinit var barrasMulticontactoRepository: BarrasMulticontactoRepository

    @Autowired
    lateinit var barrasMulticontactoViewRepository: BarrasMulticontactoViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "barrasMulticontacto"
    @GetMapping
    fun findAll () : MutableList<BarrasMulticontactoViewModel> = barrasMulticontactoViewRepository.findAll()

    @GetMapping ("{id}")
    fun findById (@PathVariable("id")  id: Int) : ResponseEntity<BarrasMulticontactoViewModel> = barrasMulticontactoViewRepository.findById(id).map{ s -> ResponseEntity.ok(s)}
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody barrasMulticontactoModel: BarrasMulticontactoModel) : BarrasMulticontactoModel = barrasMulticontactoRepository.save(barrasMulticontactoModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody barrasMulticontactoModel: BarrasMulticontactoModel,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long  ): BarrasMulticontactoModel {
        val barrasUdate = barrasMulticontactoRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(BarrasMulticontactoModelsLogs(0, barrasUdate.get().idEmpleado, barrasUdate.get ().idObservacionesInventario,
                barrasUdate.get().idEstadoInventario, barrasUdate.get().Folio, barrasUdate.get().idMarca,
                barrasUdate.get().idTipoSede, barrasUdate.get().origenRecurso, barrasUdate.get().activo,
                barrasUdate.get().fechaRecepcion, barrasUdate.get().fechaSalida, idLog))
        barrasMulticontactoModel.id = barrasUdate.get().id
        return barrasMulticontactoRepository.save(barrasMulticontactoModel)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/barrasMulticontacto", a, String::class.java)
}