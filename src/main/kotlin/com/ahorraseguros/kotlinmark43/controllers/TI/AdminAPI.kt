package com.ahorraseguros.kotlinmark43.controllers.TI

import com.ahorraseguros.kotlinmark43.models.TI.GoogleUser
import com.ahorraseguros.kotlinmark43.models.TI.UserDataModel
import com.google.api.client.auth.oauth2.Credential
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.client.util.store.FileDataStoreFactory
import com.google.api.services.admin.directory.Directory
import com.google.api.services.admin.directory.DirectoryScopes
import com.google.api.services.admin.directory.model.*
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.io.File
import java.io.IOException
import java.io.InputStreamReader
import java.security.GeneralSecurityException
import javax.validation.Valid


@CrossOrigin
@RestController
@RequestMapping("/v1/api-admin")
class AdminAPI {
    private val APPLICATION_NAME = "Admin API"
    private val JSON_FACTORY = JacksonFactory.getDefaultInstance()
    //Credenciales administrativo (La cuenta para proporcionarlos es admin@trigarante.com)
    private val TOKENS_USERS_PATH_ADMINISTRATIVO = "/opt/tokens/AdminAPI/administrativo/users"
    private val TOKENS_DOMAINS_PATH_ADMINISTRATIVO = "/opt/tokens/AdminAPI/administrativo/domains"
    private val TOKENS_ORG_UNIT_PATH_ADMINISTRATIVO = "/opt/tokens/AdminAPI/administrativo/orgUnit"
    //Credenciales ejecutivo (La cuenta para proporcionarlos es admin@ahorraseguros.com)
    private val TOKENS_USERS_PATH_EJECUTIVO = "/opt/tokens/AdminAPI/ejecutivo/users"
    private val TOKENS_DOMAINS_PATH_EJECUTIVO = "/opt/tokens/AdminAPI/ejecutivo/domains"
    private val TOKENS_ORG_UNIT_PATH_EJECUTIVO = "/opt/tokens/AdminAPI/ejecutivo/orgUnit"
    //Credenciales asistencia (La cuenta para proporcionarlos es admin@ahorraasistencia.com)
    private val TOKENS_USERS_PATH_ASISTENCIA = "/opt/tokens/AdminAPI/asistencia/users"
    private val TOKENS_DOMAINS_PATH_ASISTENCIA = "/opt/tokens/AdminAPI/asistencia/domains"
    private val TOKENS_ORG_UNIT_PATH_ASISTENCIA = "/opt/tokens/AdminAPI/asistencia/orgUnit"

    private val CREDENTIALS_FILE_PATH = "/opt/resources/credentials.json"

    val HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport()

    //Métodos para dominios
    @Throws(IOException::class)
    @GetMapping( "get-available-domains/{tipo}")//ejecutivo o administrativo
    fun getAvailableDomains(@PathVariable ("tipo") tipo: String): MutableList<Domains>? {
        val credential: String

        when(tipo){
            "administrativo" -> credential = TOKENS_DOMAINS_PATH_ADMINISTRATIVO
            "ejecutivo" -> credential = TOKENS_DOMAINS_PATH_EJECUTIVO
            "asistencia" -> credential = TOKENS_DOMAINS_PATH_ASISTENCIA
            else -> {return null}
        }

        var service = com.google.api.services.admin.directory.Directory.Builder(
                HTTP_TRANSPORT, JSON_FACTORY,
                getCredentials(HTTP_TRANSPORT, credential, listOf(DirectoryScopes.ADMIN_DIRECTORY_DOMAIN_READONLY))
        ).setApplicationName(APPLICATION_NAME).build()

        return service.domains().list("my_customer").setFields("domains(domainName)").execute().domains
    }

    //Métodos para usuarios
    @Throws(IOException::class, GeneralSecurityException::class)
    @GetMapping("get-users/{tipo}")
    fun getUsers(@PathVariable ("tipo") tipo: String): MutableList<UserDataModel>? {
        val list: MutableList<UserDataModel> = mutableListOf()
        var result = Users()
        var previousToken = String()
        val credential: String

        when (tipo){
            "administrativo" -> credential = TOKENS_USERS_PATH_ADMINISTRATIVO
            "ejecutivo" -> credential = TOKENS_USERS_PATH_EJECUTIVO
            "asistencia" -> credential = TOKENS_USERS_PATH_ASISTENCIA
            else -> { return null }
        }

        var service = com.google.api.services.admin.directory.Directory.Builder(
                HTTP_TRANSPORT, JSON_FACTORY,
                getCredentials(HTTP_TRANSPORT, credential, listOf(DirectoryScopes.ADMIN_DIRECTORY_USER))
        ).setApplicationName(APPLICATION_NAME).build()

        while(true){
            val funcion= service.users().list()
                    .setCustomer("my_customer")
                    .setOrderBy("givenName")
                    .setMaxResults(500)

            if(!result.isNullOrEmpty()){
                result = funcion.setPageToken(previousToken).execute()
            }else{
                result = funcion.execute()
            }

            for (u in result.users) {
                list.add(UserDataModel(u.id, u.primaryEmail, u.name.fullName ,u.lastLoginTime.toString(), u.orgUnitPath.toString()))
            }

            if(!result.nextPageToken.isNullOrBlank()){
                previousToken = result.nextPageToken
            }else { break }
        }

        return list
    }

    @Throws(IOException::class, GeneralSecurityException::class)
    @GetMapping("get-user/{tipo}/{id}")
    fun getUserById(@PathVariable ("tipo") tipo: String, @PathVariable ("id") id: String): ResponseEntity<User?> {
        val credential: String

        when (tipo){
            "administrativo" -> credential = TOKENS_USERS_PATH_ADMINISTRATIVO
            "ejecutivo" -> credential = TOKENS_USERS_PATH_EJECUTIVO
            "asistencia" -> credential = TOKENS_USERS_PATH_ASISTENCIA
            else -> { return ResponseEntity(HttpStatus.BAD_REQUEST) }
        }

        var service = com.google.api.services.admin.directory.Directory.Builder(
                HTTP_TRANSPORT, JSON_FACTORY,
                getCredentials(HTTP_TRANSPORT, credential, listOf(DirectoryScopes.ADMIN_DIRECTORY_USER))
        ).setApplicationName(APPLICATION_NAME).build()

        return ResponseEntity(service.users().get(id).setFields("name(fullName),primaryEmail,emails(address), orgUnitPath").execute(), HttpStatus.OK)
    }

    @Throws(IOException::class)
    @PostMapping("create-user/{tipo}")
    fun createUser(@PathVariable ("tipo") tipo: String,@Valid @RequestBody userCreate: GoogleUser): String? {
        val user = User()
        val name = UserName()
        val credential: String

        when (tipo){
            "administrativo" -> credential = TOKENS_USERS_PATH_ADMINISTRATIVO
            "ejecutivo" -> credential = TOKENS_USERS_PATH_EJECUTIVO
            "asistencia" -> credential = TOKENS_USERS_PATH_ASISTENCIA
            else -> { return null }
        }

        var service = Directory.Builder(
                HTTP_TRANSPORT, JSON_FACTORY,
                getCredentials(HTTP_TRANSPORT, credential, listOf(DirectoryScopes.ADMIN_DIRECTORY_USER))
        ).setApplicationName(APPLICATION_NAME).build()

        name.familyName = userCreate.familyName
        name.givenName = userCreate.givenName
        user.name = name
        user.password = "Seguridad01"
        user.primaryEmail = userCreate.email
        user.changePasswordAtNextLogin = true

        return try{
            service.users().insert(user).execute().id
        }catch(e: Exception){ null }
    }

    //Métodos para los alias
    @Throws(IOException::class)
    @GetMapping( "get-user-aliases/{tipo}/{id}")
    fun getAliases(@PathVariable("id")id: String, @PathVariable("tipo")tipo: String): MutableCollection<Any>? {
        val credential: String

        when (tipo){
            "administrativo" -> credential = TOKENS_USERS_PATH_ADMINISTRATIVO
            "ejecutivo" -> credential = TOKENS_USERS_PATH_EJECUTIVO
            "asistencia" -> credential = TOKENS_USERS_PATH_ASISTENCIA
            else -> { return null }
        }

        var service = Directory.Builder(
                HTTP_TRANSPORT, JSON_FACTORY,
                getCredentials(HTTP_TRANSPORT, credential, listOf(DirectoryScopes.ADMIN_DIRECTORY_USER_ALIAS))
        ).setApplicationName(APPLICATION_NAME).build()

        return service.Users().Aliases().list(id).setFields("aliases(alias)").execute().aliases
    }

    @Throws(IOException::class, GeneralSecurityException::class)
    @PutMapping( "add-primary-alias/{tipo}")
    fun addPrimaryAlias(@PathVariable ("tipo") tipo: String, @Valid @RequestBody userCreate: GoogleUser) : HttpStatus {
        val credential: String

        when(tipo){
            "administrativo" -> credential = TOKENS_USERS_PATH_ADMINISTRATIVO
            "ejecutivo" -> credential = TOKENS_USERS_PATH_EJECUTIVO
            "asistencia" -> credential = TOKENS_USERS_PATH_ASISTENCIA
            else -> { return HttpStatus.EXPECTATION_FAILED }
        }

        var service = Directory.Builder(
                HTTP_TRANSPORT, JSON_FACTORY,
                getCredentials(HTTP_TRANSPORT, credential, listOf(DirectoryScopes.ADMIN_DIRECTORY_USER))
        ).setApplicationName(APPLICATION_NAME).build()
        var user = User()
        val name = UserName()

        if(userCreate.familyName != null && userCreate.givenName != null){
            name.familyName = userCreate.familyName
            name.givenName = userCreate.givenName
            user.name = name
        }

        if(userCreate.email != null){
            user.primaryEmail = userCreate.email
            user.password="Seguridad01"
        }

       if(userCreate.orgUnitPath != null){
            user.orgUnitPath = userCreate.orgUnitPath
        }

        try{
            service.users().update(userCreate.id, user).execute()
            return HttpStatus.OK
        }catch (e: Exception){
            return HttpStatus.EXPECTATION_FAILED
        }
    }

    @Throws(IOException::class, GeneralSecurityException::class)
    @PutMapping( "add-alias/{tipo}")
    fun addAlias(@PathVariable ("tipo") tipo: String, @Valid @RequestBody userCreate: GoogleUser) : HttpStatus {
        val credential: String

        when(tipo){
            "administrativo" -> credential = TOKENS_USERS_PATH_ADMINISTRATIVO
            "ejecutivo" -> credential = TOKENS_USERS_PATH_EJECUTIVO
            "asistencia" -> credential = TOKENS_USERS_PATH_ASISTENCIA
            else -> { return HttpStatus.EXPECTATION_FAILED }
        }

        var service = Directory.Builder(
                HTTP_TRANSPORT, JSON_FACTORY,
                getCredentials(HTTP_TRANSPORT, credential, listOf(DirectoryScopes.ADMIN_DIRECTORY_USER))
        ).setApplicationName(APPLICATION_NAME).build()

        try{
            val alias = Alias()
            alias.alias = userCreate.email
            service.Users().Aliases().insert(userCreate.id, alias).execute()
            return HttpStatus.OK
        }catch (e: Exception){
            return HttpStatus.EXPECTATION_FAILED
        }
    }

    @Throws(IOException::class, GeneralSecurityException::class)
    @DeleteMapping( "delete-alias/{tipo}/{id}/{alias}")
    fun deleteAlias(@PathVariable ("tipo") tipo: String, @PathVariable ("id")id:String, @PathVariable("alias") alias:String): HttpStatus {
        val credential: String

        when(tipo){
            "administrativo" -> credential = TOKENS_USERS_PATH_ADMINISTRATIVO
            "ejecutivo" -> credential = TOKENS_USERS_PATH_EJECUTIVO
            "asistencia" -> credential = TOKENS_USERS_PATH_ASISTENCIA
            else -> { return HttpStatus.EXPECTATION_FAILED }
        }

        var service = Directory.Builder(
                HTTP_TRANSPORT, JSON_FACTORY,
                getCredentials(HTTP_TRANSPORT, credential, listOf(DirectoryScopes.ADMIN_DIRECTORY_USER))
        ).setApplicationName(APPLICATION_NAME).build()

        try{
            service.Users().aliases().delete(id,alias).execute()
            return HttpStatus.OK
        }catch (e: Exception){
            return HttpStatus.EXPECTATION_FAILED
        }
    }

    @Throws(IOException::class, GeneralSecurityException::class)
    @GetMapping( "find-alias/{tipo}/{alias}")
    fun findAlias(@PathVariable("alias") alias: String,@PathVariable("tipo") tipo: String): Boolean {
        val credential: String

        when (tipo){
            "administrativo" -> credential = TOKENS_USERS_PATH_ADMINISTRATIVO
            "ejecutivo" -> credential = TOKENS_USERS_PATH_EJECUTIVO
            "asistencia" -> credential = TOKENS_USERS_PATH_ASISTENCIA
            else -> { return false }
        }

        var service = Directory.Builder(
                HTTP_TRANSPORT, JSON_FACTORY,
                getCredentials(HTTP_TRANSPORT, credential, listOf(DirectoryScopes.ADMIN_DIRECTORY_USER))
        ).setApplicationName(APPLICATION_NAME).build()
        val result: Users = service.users().list()
                .setCustomer("my_customer")
                .setQuery("email='"+alias+"'")
                .execute()

        try{
            result.users.size
            return true
        }catch (e: Exception){
            return false
        }
    }

    //Métodos para unidades organizativas
    @GetMapping("unidades-organizativas/{tipo}")
    fun getOrgUnits(@PathVariable("tipo") tipo: String): ResponseEntity<MutableList<OrgUnit>> {
        val credential: String

        when (tipo){
            "administrativo" -> credential = TOKENS_ORG_UNIT_PATH_ADMINISTRATIVO
            "ejecutivo" -> credential = TOKENS_ORG_UNIT_PATH_EJECUTIVO
            "asistencia" -> credential = TOKENS_ORG_UNIT_PATH_ASISTENCIA
            else -> { return ResponseEntity(HttpStatus.BAD_REQUEST) }
        }

        var service = Directory.Builder(
                HTTP_TRANSPORT, JSON_FACTORY,
                getCredentials(HTTP_TRANSPORT, credential, listOf(DirectoryScopes.ADMIN_DIRECTORY_ORGUNIT))
        ).setApplicationName(APPLICATION_NAME).build()

        val result: OrgUnits = service.Orgunits().list("my_customer")
                .setType("all")
                .setFields("organizationUnits(name,orgUnitPath)")
                .execute()
        return ResponseEntity(result.organizationUnits, HttpStatus.OK)
    }

    //Método para las credenciales
    @Throws(IOException::class)
    private fun getCredentials(HTTP_TRANSPORT: NetHttpTransport, path: String, SCOPE: List<String>): Credential {
        // Load client secrets.
        val `in` = File(CREDENTIALS_FILE_PATH)
        val clientSecrets: GoogleClientSecrets = GoogleClientSecrets.load(JSON_FACTORY, InputStreamReader(`in`.inputStream()))

        // Build flow and trigger user authorization request.
        val flow: GoogleAuthorizationCodeFlow = GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPE)
                .setDataStoreFactory(FileDataStoreFactory(File(path)))
                .setAccessType("offline")
                .build()
        val receiver: LocalServerReceiver = LocalServerReceiver.Builder().setPort(8888).build()

        return AuthorizationCodeInstalledApp(flow, receiver).authorize("user")
    }
}