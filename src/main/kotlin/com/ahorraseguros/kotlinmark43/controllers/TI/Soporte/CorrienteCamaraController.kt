package com.ahorraseguros.kotlinmark43.controllers.TI.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.CorrienteCamaraModel
import com.ahorraseguros.kotlinmark43.models.TI.views.CorrienteCamaraViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte.CorrienteCamaraModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.Soporte.CorrienteCamaraRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.CorrienteCamarasViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping ("/v1/corrienteCamara")

class CorrienteCamaraController: LogsOperations {
    @Autowired
    lateinit var corrienteCamaraRepository: CorrienteCamaraRepository

    @Autowired
    lateinit var corrienteCamarasViewRepository: CorrienteCamarasViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "corrienteCamara"

    @GetMapping
    fun findAll () : MutableList<CorrienteCamaraViewModel> = corrienteCamarasViewRepository.findAll()

    @GetMapping ("{id}")
    fun findById (@PathVariable("id")  id: Int) : ResponseEntity<CorrienteCamaraViewModel> = corrienteCamarasViewRepository.findById(id).map{ s -> ResponseEntity.ok(s)}
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody corrienteCamaraModel: CorrienteCamaraModel) : CorrienteCamaraModel = corrienteCamaraRepository.save(corrienteCamaraModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody corrienteCamaraModel: CorrienteCamaraModel,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): CorrienteCamaraModel {
        val corrienteUdate = corrienteCamaraRepository.findById(id)
        corrienteCamaraModel.id = corrienteUdate.get().id

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(CorrienteCamaraModelsLogs(0, corrienteUdate.get().idEmpleado, corrienteUdate.get ().idObservacionesInventario,
                corrienteUdate.get().idEstadoInventario, corrienteUdate.get().Folio, corrienteUdate.get().idMarca,
                corrienteUdate.get().idTipoSede, corrienteUdate.get().origenRecurso, corrienteUdate.get().activo,
                corrienteUdate.get().fechaRecepcion, corrienteUdate.get().fechaSalida, idLog))

        return corrienteCamaraRepository.save(corrienteCamaraModel)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/corrienteCamara", a, String::class.java)
}