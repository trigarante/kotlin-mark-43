package com.ahorraseguros.kotlinmark43.controllers.TI.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.ApsModel
import com.ahorraseguros.kotlinmark43.models.TI.views.ApsViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte.ApsModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.Soporte.ApsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.ApsViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping ("/v1/aps")

class ApsController : LogsOperations {
    @Autowired
    lateinit var apsRepository: ApsRepository

    @Autowired
    lateinit var apsViewRepository: ApsViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository
    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "aps"

    @GetMapping
    fun findAll () : MutableList<ApsViewModel> = apsViewRepository.findAll()

    @GetMapping ("{id}")
    fun findById (@PathVariable("id")  id: Int) : ResponseEntity<ApsViewModel> = apsViewRepository.findById(id).map{ s -> ResponseEntity.ok(s)}
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody apsModel: ApsModel) : ApsModel = apsRepository.save(apsModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody apsModel: ApsModel,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long): ApsModel {
        val apsUdate = apsRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(ApsModelsLogs(0, apsUdate.get().idEmpleado, apsUdate.get ().idObservacionesInventario,
                apsUdate.get().idEstadoInventario, apsUdate.get().Folio, apsUdate.get().idMarca,
                apsUdate.get().idTipoSede, apsUdate.get().origenRecurso, apsUdate.get().activo,
                apsUdate.get().fechaRecepcion, apsUdate.get().fechaSalida, idLog))

        apsModel.id = apsUdate.get().id
        return apsRepository.save(apsModel)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/aps", a, String::class.java)
}