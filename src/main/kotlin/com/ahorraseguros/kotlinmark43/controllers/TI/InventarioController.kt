package com.ahorraseguros.kotlinmark43.controllers.TI

import com.ahorraseguros.kotlinmark43.models.TI.InventarioModels
import com.ahorraseguros.kotlinmark43.repositories.ti.InventarioRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/inventario")
class InventarioController {
    @Autowired
    lateinit var inventarioRepository: InventarioRepository

    @GetMapping
    fun findAll(): MutableList<InventarioModels> = inventarioRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody inventarioModels: InventarioModels): InventarioModels = inventarioRepository.save(inventarioModels)
}