package com.ahorraseguros.kotlinmark43.controllers.TI

import com.ahorraseguros.kotlinmark43.models.TI.MarcaSoporteModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.CatalogoSoporte.MarcaSoporteModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.MarcaSoporteRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@RestController
@CrossOrigin
@RequestMapping ("/v1/marca")
class MarcaSoporteController : LogsOperations {

    @Autowired
    lateinit var  marcaSoporteRepository: MarcaSoporteRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "marcaSoporte"

    @GetMapping
    fun getMarca() = marcaSoporteRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody marcaModels:MarcaSoporteModel ): MarcaSoporteModel= marcaSoporteRepository.save(marcaModels)

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<MarcaSoporteModel> = marcaSoporteRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody marcaModels: MarcaSoporteModel,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): MarcaSoporteModel {
        var marcaUpdate = marcaSoporteRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(MarcaSoporteModelsLogs(0, marcaUpdate.get().nombreMarca, marcaUpdate.get().activo, idLog))
        marcaModels.id = marcaUpdate.get().id
        return marcaSoporteRepository.save(marcaModels)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/marcaSoporte", a, String::class.java)
}