package com.ahorraseguros.kotlinmark43.controllers.TI

import com.ahorraseguros.kotlinmark43.models.TI.LaptopsModels
import com.ahorraseguros.kotlinmark43.models.TI.views.LaptopsViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte.LaptopsModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.LaptopRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.LaptopsViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/laptop")
class LaptopController: LogsOperations {
    @Autowired
    lateinit var laptopRepository: LaptopRepository
    @Autowired
    lateinit var laptopsViewRepository: LaptopsViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "laptops"

    @GetMapping
    fun findAll(): MutableList<LaptopsViewModel> = laptopsViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<LaptopsViewModel> = laptopsViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody laptopsModels: LaptopsModels): LaptopsModels = laptopRepository.save(laptopsModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody laptopsModels: LaptopsModels,
               @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long): LaptopsModels {
        var laptopsUpdate = laptopRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id

        callServerLogs(LaptopsModelsLogs(0,laptopsUpdate.get().idEmpleado, laptopsUpdate.get().idEstadoInventario,
                         laptopsUpdate.get().idObservacionesInventario, laptopsUpdate.get().numeroSerie,
                         laptopsUpdate.get().idProcesador, laptopsUpdate.get().hdd, laptopsUpdate.get().ram,
                         laptopsUpdate.get().idMarca, laptopsUpdate.get().modelo, laptopsUpdate.get().fechaRecepcion,
                         laptopsUpdate.get().fechaSalida, laptopsUpdate.get().comentarios, laptopsUpdate.get().medidaPantalla,
                         laptopsUpdate.get().activo, laptopsUpdate.get().idTipoSede, laptopsUpdate.get().origenRecurso, idLog))

        laptopsModels.id = laptopsUpdate.get().id
        return laptopRepository.save(laptopsModels)
    }
    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/laptops", a, String::class.java)
}