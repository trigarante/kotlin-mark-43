package com.ahorraseguros.kotlinmark43.controllers.TI

import com.ahorraseguros.kotlinmark43.models.TI.LicenciasDisponiblesModel
import com.ahorraseguros.kotlinmark43.repositories.ti.LicenciasDisponiblesRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@CrossOrigin
@RestController
@RequestMapping("/v1/licencias-disponibles")

class LicenciasController {
    @Autowired
    lateinit var licenciasRepository: LicenciasDisponiblesRepository

    @GetMapping
    fun licenciasDiponibles(): List<LicenciasDisponiblesModel> = licenciasRepository.findAll()

    @GetMapping("{id}")
    fun licenciasDiponibles(@PathVariable("id") id: Int): ResponseEntity<LicenciasDisponiblesModel> =
            licenciasRepository.findById(id).map { s: LicenciasDisponiblesModel -> ResponseEntity.ok(s) }
                    .orElse(ResponseEntity.notFound().build())

    @PutMapping("{id}/{cantidad}")
    fun updateLicencias(@PathVariable("id") id: Int, @PathVariable("cantidad") cantidad: Int ): HttpStatus{
        return try{
            val licenciaVieja = licenciasRepository.findById(id)

            if(licenciaVieja.isPresent){
                licenciaVieja.get().cantidad = cantidad

                licenciasRepository.save(licenciaVieja.get())

                HttpStatus.OK
            }else{
                HttpStatus.NOT_FOUND
            }
        }catch(e: Exception){
           HttpStatus.BAD_REQUEST
        }
    }

}