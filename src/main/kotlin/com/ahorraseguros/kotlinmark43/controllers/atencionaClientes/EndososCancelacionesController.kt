package com.ahorraseguros.kotlinmark43.controllers.atencionaClientes

import com.ahorraseguros.kotlinmark43.models.atencionaClientes.EndososCancelacionesModel
import com.ahorraseguros.kotlinmark43.models.atencionaClientes.views.EndososCancelacionesViewModel
import com.ahorraseguros.kotlinmark43.repositories.atencionaClientes.EnsosyCancelacionesRepository
import com.ahorraseguros.kotlinmark43.repositories.atencionaClientes.views.EndososCancelacionesViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/endosos-cancelaciones")

class EndososCancelacionesController {

    @Autowired
    lateinit var endososCancelacionesRepository: EnsosyCancelacionesRepository

    @Autowired
    lateinit var endososCancelacionesViewRepository: EndososCancelacionesViewRepository

    @GetMapping
    fun getAll() : MutableList<EndososCancelacionesViewModel> = endososCancelacionesViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<EndososCancelacionesViewModel> = endososCancelacionesViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping()
    fun create(@Valid @RequestBody endososCancelacionesModel: EndososCancelacionesModel): EndososCancelacionesModel = endososCancelacionesRepository.save(endososCancelacionesModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Long,
               @Valid @RequestBody endososCancelacionesModel: EndososCancelacionesModel): EndososCancelacionesModel {
        var reciboUpdate = endososCancelacionesRepository.findById(id)
        endososCancelacionesModel.id = reciboUpdate.get().id
        return endososCancelacionesRepository.save(endososCancelacionesModel)
    }
}