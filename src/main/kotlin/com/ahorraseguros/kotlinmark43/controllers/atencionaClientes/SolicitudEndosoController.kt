package com.ahorraseguros.kotlinmark43.controllers.atencionaClientes

import com.ahorraseguros.kotlinmark43.models.atencionaClientes.SolicitudEndosoModel
import com.ahorraseguros.kotlinmark43.models.atencionaClientes.views.SolicitudEndosoViewModel
import com.ahorraseguros.kotlinmark43.repositories.atencionaClientes.SolicitudEndosoRepository
import com.ahorraseguros.kotlinmark43.repositories.atencionaClientes.views.SolicitudEndosoViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/solicitud-endoso")

class SolicitudEndosoController {

    @Autowired
    lateinit var solicitudEndosoRepository: SolicitudEndosoRepository

    @Autowired
    lateinit var solicitudEndosoViewRepository: SolicitudEndosoViewRepository

    @GetMapping
    fun getAll() : MutableList<SolicitudEndosoViewModel> = solicitudEndosoViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<SolicitudEndosoViewModel> = solicitudEndosoViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping()
    fun create(@Valid @RequestBody solicitudEndosoModel: SolicitudEndosoModel): SolicitudEndosoModel = solicitudEndosoRepository.save(solicitudEndosoModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Long,
               @Valid @RequestBody solicitudEndosoModel: SolicitudEndosoModel): SolicitudEndosoModel {
        var reciboUpdate = solicitudEndosoRepository.findById(id)
        solicitudEndosoModel.id = reciboUpdate.get().id
        return solicitudEndosoRepository.save(solicitudEndosoModel)
    }

}