package com.ahorraseguros.kotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.GrupoModels
import com.ahorraseguros.kotlinmark43.repositories.catalogosRRHH.GrupoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/grupo")
class GrupoController{
    @Autowired
    lateinit var grupoRepository: GrupoRepository

    @GetMapping
    fun findAll(): List<GrupoModels> = grupoRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<GrupoModels> =
            grupoRepository.findById(id).map { s: GrupoModels -> ResponseEntity.ok(s) }
                    .orElse(ResponseEntity.notFound().build())


    @PostMapping
    fun create(@Valid @RequestBody grupo: GrupoModels): GrupoModels = grupoRepository.save(grupo)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Int,
               @Valid @RequestBody grupo: GrupoModels): GrupoModels {
        var grupoUpdate = grupoRepository.findById(id)
        grupo.id = grupoUpdate.get().id
        return grupoRepository.save(grupo)
    }
}