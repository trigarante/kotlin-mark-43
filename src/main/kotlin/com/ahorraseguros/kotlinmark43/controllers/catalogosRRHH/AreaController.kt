package com.ahorraseguros.kotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.AreaModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH.AreaModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.AreasRepository
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/areas")
class AreaController: LogsOperations {
    @Autowired
    lateinit var areasRepository: AreasRepository

    @Autowired
    lateinit var logsRepository: LogsRepository
    var restTemplate = RestTemplate()

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    var tabla = "seguimiento"

    @GetMapping
    fun findAll(): MutableList<AreaModels> = areasRepository.findAll()

    @GetMapping("/active-areas/{id}")
    fun findAllAreasByIdSedeAndActivo(@PathVariable("id") id: Int): List<AreaModels> =
            areasRepository.findAllByIdSedeAndActivo(id,1)


    @PostMapping
    fun create(@Valid @RequestBody areaModel: AreaModels): AreaModels = areasRepository.save(areaModel)

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<AreaModels> = areasRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody areaModel: AreaModels,
               @PathVariable ("idEmpleadoModificante")idEmpleadoModificante: Long): AreaModels {

        var areaUpdate = areasRepository.findById(id)
        var idLog = createLogs(idEmpleadoModificante,1,id).id

        callServerLogs(AreaModelsLogs(0,areaUpdate.get().idSede,areaUpdate.get().nombre,areaUpdate.get().descripcion,
                areaUpdate.get().activo,areaUpdate.get().codigo, idLog))

        areaModel.id = areaUpdate.get().id
        return areasRepository.save(areaModel)
    }

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity("$serverLogs/areas", a, String::class.java)

    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0, idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName ,
                        Timestamp(System.currentTimeMillis())))
}