package com.ahorraseguros.kotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.EtapasModulosModels
import com.ahorraseguros.kotlinmark43.repositories.catalogosRRHH.EtapasModulosRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/etapas")
class EtapasModulosController{
    @Autowired
    lateinit var etapasModulosRepository: EtapasModulosRepository

    @GetMapping
    fun findAll(): List<EtapasModulosModels> = etapasModulosRepository.findAll()

    @GetMapping("{id}")
    fun finId(@PathVariable("id") id: Int): ResponseEntity<EtapasModulosModels>? =
            etapasModulosRepository.findById(id).map{ s -> ResponseEntity.ok(s) }
                    .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody grupo: EtapasModulosModels): EtapasModulosModels = etapasModulosRepository.save(grupo)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Int, @Valid @RequestBody etapa: EtapasModulosModels): HttpStatus{
        return try{
            val etapaVieja = etapasModulosRepository.findById(id)

            if(etapaVieja.isPresent){
                val actualizacion = etapaVieja.get()

                actualizacion.idArea = etapa.idArea
                actualizacion.etapa = etapa.etapa
                actualizacion.nivel = etapa.nivel

                etapasModulosRepository.save(actualizacion)
                HttpStatus.OK
            }else{
               HttpStatus.EXPECTATION_FAILED
            }
        }catch(e: Exception){
            HttpStatus.BAD_REQUEST
        }
    }
}