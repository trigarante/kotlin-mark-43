package com.ahorraseguros.kotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.MedioTransporteModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH.MedioTransporteModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.MedioTransporteRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/medio-transporte")
class MedioTransporteController: LogsOperations {
    @Autowired
    lateinit var medioTransporteRepository: MedioTransporteRepository

    @Autowired
    lateinit var logsRepository: LogsRepository

    var restTemplate = RestTemplate()

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val tabla = "medioTransporte"

    @GetMapping
    fun findAll(): MutableList<MedioTransporteModels> = medioTransporteRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<MedioTransporteModels> = medioTransporteRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody medioTransporteModels: MedioTransporteModels): MedioTransporteModels = medioTransporteRepository.save(medioTransporteModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long,
               @Valid @RequestBody medioTransporteModels: MedioTransporteModels): MedioTransporteModels {
        var medioTransporteUpdate = medioTransporteRepository.findById(id)
        val idLog: Long = createLogs(idEmpleadoModificante,1,medioTransporteUpdate.get().id).id

        callServerLogs(MedioTransporteModelsLogs(0,medioTransporteUpdate.get().medio, medioTransporteUpdate.get().activo, idLog))
        medioTransporteModels.id = medioTransporteUpdate.get().id
        return medioTransporteRepository.save(medioTransporteModels)
    }

    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/medio-transporte", a, String::class.java)
}