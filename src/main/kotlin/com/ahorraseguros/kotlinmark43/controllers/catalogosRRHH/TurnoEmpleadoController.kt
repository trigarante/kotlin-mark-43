package com.ahorraseguros.kotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.TurnoEmpleadoModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH.TurnoEmpleadoModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.catalogosRRHH.TurnoEmpleadoRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/turnos")

class TurnoEmpleadoController: LogsOperations {
    @Autowired
    lateinit var turnoEmpleadoRepository: TurnoEmpleadoRepository

    @Autowired
    lateinit var logsRepository: LogsRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val restTemplate = RestTemplate()
    val tabla = "turnoEmpleado"


    @GetMapping
    fun findAll(): List<TurnoEmpleadoModel> = turnoEmpleadoRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<TurnoEmpleadoModel> =
            turnoEmpleadoRepository.findById(id).map { s: TurnoEmpleadoModel -> ResponseEntity.ok(s) }
                    .orElse(ResponseEntity.notFound().build())


    @PostMapping
    fun create(@Valid @RequestBody turnoEmpleado: TurnoEmpleadoModel): TurnoEmpleadoModel = turnoEmpleadoRepository.save(turnoEmpleado)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody turnoEmpleado: TurnoEmpleadoModel,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): TurnoEmpleadoModel {
        var turnoEmpleadoUpdate = turnoEmpleadoRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante, 1,id).id
        callServerLogs(TurnoEmpleadoModelsLogs(0, turnoEmpleadoUpdate.get().turno, turnoEmpleadoUpdate.get().horario,
                turnoEmpleadoUpdate.get().activo, idLog))


        turnoEmpleado.id = turnoEmpleadoUpdate.get().id
        return turnoEmpleadoRepository.save(turnoEmpleado)
    }

    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/turnoEmpleado", a, String::class.java)

}