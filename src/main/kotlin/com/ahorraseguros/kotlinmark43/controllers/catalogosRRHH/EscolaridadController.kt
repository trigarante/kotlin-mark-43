package com.ahorraseguros.kotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.EscolaridadModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH.EscolaridadModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.EscolaridadRepository
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/escolaridades")
class EscolaridadController: LogsOperations {
    @Autowired
    lateinit var escolaridadRepository: EscolaridadRepository

    @Autowired
    lateinit var logsRepository: LogsRepository

    var restTemplate = RestTemplate()

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val tabla = "escolaridad"

    @GetMapping
    fun findAll(): MutableList<EscolaridadModels> = escolaridadRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<EscolaridadModels> = escolaridadRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody escolaridadModels: EscolaridadModels): EscolaridadModels = escolaridadRepository.save(escolaridadModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long,
               @Valid @RequestBody escolaridadModels: EscolaridadModels): EscolaridadModels {
        var escolaridadUpdate = escolaridadRepository.findById(id)
        val idLog: Long = createLogs(idEmpleadoModificante,1,escolaridadUpdate.get().id).id

        callServerLogs(EscolaridadModelsLogs(0,escolaridadUpdate.get().nivel, escolaridadUpdate.get().activo, idLog))
        escolaridadModels.id = escolaridadUpdate.get().id
        return escolaridadRepository.save(escolaridadModels)
    }

    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/escolaridades", a, String::class.java)
}