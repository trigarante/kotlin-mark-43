package com.ahorraseguros.kotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.CompetenciasModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH.CompetenciaModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.CompetenciaRepository
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/competencias")
class CompetenciasController: LogsOperations {
    @Autowired
    lateinit var competenciaRepository: CompetenciaRepository

    @Autowired
    lateinit var logsRepository: LogsRepository

    var restTemplate = RestTemplate()

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val tabla = "competencias"

    @GetMapping
    fun findAll(): MutableList<CompetenciasModels> = competenciaRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<CompetenciasModels> = competenciaRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody candidatoModel: CompetenciasModels): CompetenciasModels = competenciaRepository.save(candidatoModel)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long,
               @Valid @RequestBody competenciasModels: CompetenciasModels): CompetenciasModels {
        var competenciasUpdate = competenciaRepository.findById(id)
        val idLog: Long = createLogs(idEmpleadoModificante,1,competenciasUpdate.get().id).id

        callServerLogs(CompetenciaModelsLogs(0,competenciasUpdate.get().escala,competenciasUpdate.get().estado,
                competenciasUpdate.get().descripcion,competenciasUpdate.get().activo, idLog))
        competenciasModels.id = competenciasUpdate.get().id
        return competenciaRepository.save(competenciasModels)
    }

    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/competencias", a, String::class.java)
}