package com.ahorraseguros.kotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.SubareaModels
import com.ahorraseguros.kotlinmark43.repositories.SubareaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/subarea")
class SubareaController {
    @Autowired
    lateinit var subareaRepository: SubareaRepository

    @GetMapping
    fun findAll(): MutableList<SubareaModels> = subareaRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<SubareaModels> = subareaRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @GetMapping("find-by-id-area/{id}")
    fun findByIdArea(@PathVariable("id") id: Long): MutableList<SubareaModels> = subareaRepository.findAllByIdArea(id.toInt())


    @PostMapping
    fun create(@Valid @RequestBody subareaModels: SubareaModels): SubareaModels = subareaRepository.save(subareaModels)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Long,
               @Valid @RequestBody subareaModels: SubareaModels): SubareaModels {
        var subareaUpdate = subareaRepository.findById(id)
        subareaModels.id = subareaUpdate.get().id
        return subareaRepository.save(subareaModels)
    }

}