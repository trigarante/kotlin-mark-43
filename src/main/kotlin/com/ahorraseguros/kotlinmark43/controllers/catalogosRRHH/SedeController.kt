package com.ahorraseguros.kotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.SedeModel
import com.ahorraseguros.kotlinmark43.repositories.SedeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/sedes")

class SedeController {
    @Autowired
    lateinit var sedeRepository: SedeRepository

    @GetMapping
    fun sedesDisponibles(): List<SedeModel> =
            sedeRepository.findAll().map { s: SedeModel -> s }

    @GetMapping("{id}")
    fun sedesDiponibles(@PathVariable("id") id: Int): ResponseEntity<SedeModel> =
            sedeRepository.findById(id).map { s: SedeModel -> ResponseEntity.ok(s) }
                    .orElse(ResponseEntity.notFound().build())

    @GetMapping("get-by-name/{sede}")
    fun getSedeByName(@PathVariable("sede") sede: String): SedeModel =
            sedeRepository.findByNombre(sede.toUpperCase())

    @PostMapping
    fun nuevaSede(@Valid @RequestBody sedeNueva: SedeModel): ResponseEntity<SedeModel> {
        return try {
            ResponseEntity.ok(sedeRepository.save(sedeNueva))
        } catch (e: Exception) {
            ResponseEntity.notFound().build()
        }
    }

    @PutMapping("{id}")
    fun updateSedes(@PathVariable("id") id: Int, @Valid @RequestBody sedeActualizada: SedeModel): ResponseEntity<SedeModel> {
        return try {
            val sedeVieja = sedeRepository.findById(id)

            if (sedeVieja.isPresent) {


                sedeVieja.get().idEmpresa = sedeActualizada.idEmpresa
                sedeVieja.get().idPais = sedeActualizada.idPais
                sedeVieja.get().calle = sedeActualizada.calle
                sedeVieja.get().colonia = sedeActualizada.colonia
                sedeVieja.get().nombre = sedeActualizada.nombre
                sedeVieja.get().numero = sedeActualizada.numero
                sedeVieja.get().cp = sedeActualizada.cp

                sedeRepository.save(sedeVieja.get())

                ResponseEntity.ok(sedeVieja.get())
            } else {
                ResponseEntity.notFound().build()
            }
        } catch (e: Exception) {
            ResponseEntity.badRequest().build()
        }
    }

}