package com.ahorraseguros.kotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.PuestoTipoModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH.PuestoTipoModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.PuestoTipoRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tipo-puesto")
class TipoPuestoController: LogsOperations {

    @Autowired
    lateinit var puestoTipoRepository: PuestoTipoRepository

    @Autowired
    lateinit var logsRepository: LogsRepository

    var restTemplate = RestTemplate()

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val tabla = "puestoTipo"

    @GetMapping
    fun findAll(): MutableList<PuestoTipoModels> = puestoTipoRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<PuestoTipoModels> = puestoTipoRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody tipoPuestoModels: PuestoTipoModels): PuestoTipoModels = puestoTipoRepository.save(tipoPuestoModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long,
               @Valid @RequestBody tipoPuestoModels: PuestoTipoModels): PuestoTipoModels {
        var puestoTipoUpdate = puestoTipoRepository.findById(id)
        val idLog: Long = createLogs(idEmpleadoModificante,1,puestoTipoUpdate.get().id).id

        callServerLogs(PuestoTipoModelsLogs(0,puestoTipoUpdate.get().nombre,puestoTipoUpdate.get().activo, idLog))
        tipoPuestoModels.id = puestoTipoUpdate.get().id
        return puestoTipoRepository.save(tipoPuestoModels)
    }

    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/tipo-puesto", a, String::class.java)

}