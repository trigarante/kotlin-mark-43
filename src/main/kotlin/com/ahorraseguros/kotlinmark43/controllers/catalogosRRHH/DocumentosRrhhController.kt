package com.ahorraseguros.kotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.DocumentosRrhhModel
import com.ahorraseguros.kotlinmark43.repositories.DocumentosRrhhRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid


@CrossOrigin
@RestController
@RequestMapping("/v1/DocumentosRrhh")
class DocumentosRrhhController {
    @Autowired
    lateinit var documentosRrhhRepository: DocumentosRrhhRepository

    @GetMapping
    fun findAll(): MutableList<DocumentosRrhhModel> = documentosRrhhRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody documentosRrhhModel: DocumentosRrhhModel): DocumentosRrhhModel = documentosRrhhRepository.save(documentosRrhhModel)
}