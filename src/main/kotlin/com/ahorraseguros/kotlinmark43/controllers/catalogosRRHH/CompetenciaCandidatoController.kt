package com.ahorraseguros.kotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.CompetenciaCandidatoModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH.CompetenciaCandidatoModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.CompetenciaCandidatoRepository
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/competencias-candidatos")
class CompetenciaCandidatoController: LogsOperations {
    @Autowired
    lateinit var competenciaCandidatoRepository: CompetenciaCandidatoRepository

    @Autowired
    lateinit var logsRepository: LogsRepository

    var restTemplate = RestTemplate()

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val tabla = "competenciaCandidato"

    @GetMapping
    fun findAll(): MutableList<CompetenciaCandidatoModels> = competenciaCandidatoRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<CompetenciaCandidatoModels> =
            competenciaCandidatoRepository.findById(id).map { s -> ResponseEntity.ok(s) }
                    .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody competenciaCandidatoModels: CompetenciaCandidatoModels): CompetenciaCandidatoModels =
            competenciaCandidatoRepository.save(competenciaCandidatoModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @PathVariable ("idEmpleadoModificante") idEmpleadoModificante: Long,
               @Valid @RequestBody competenciaCandidatoModels: CompetenciaCandidatoModels): CompetenciaCandidatoModels {
        var competenciaCandidatoUpdate = competenciaCandidatoRepository.findById(id)
        val idLog: Long = createLogs(idEmpleadoModificante,1,competenciaCandidatoUpdate.get().id).id

        callServerLogs(CompetenciaCandidatoModelsLogs(0,competenciaCandidatoUpdate.get().descripcion,
                competenciaCandidatoUpdate.get().activo, idLog))
        competenciaCandidatoModels.id = competenciaCandidatoUpdate.get().id
        return competenciaCandidatoRepository.save(competenciaCandidatoModels)
    }

    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/competencias-candidatos", a, String::class.java)
}