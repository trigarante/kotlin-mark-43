package com.ahorraseguros.kotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.BolsaTrabajoModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH.BolsaTrabajoModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.BolsaTrabajoRepository
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/bolsa-trabajo")
class BolsaTrabajoController: LogsOperations {
    @Autowired
    lateinit var bolsaTrabajoRepository: BolsaTrabajoRepository

    @Autowired
    lateinit var logsRepository: LogsRepository

    var restTemplate = RestTemplate()

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val tabla = "bolsaTrabajo"

    @GetMapping
    fun findAll(): MutableList<BolsaTrabajoModels> = bolsaTrabajoRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<BolsaTrabajoModels> = bolsaTrabajoRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody bolsaTrabajoModels: BolsaTrabajoModels): BolsaTrabajoModels = bolsaTrabajoRepository.save(bolsaTrabajoModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long,
               @Valid @RequestBody bolsaTrabajoModels: BolsaTrabajoModels): BolsaTrabajoModels {
        var bolsaTrabajoUpdate = bolsaTrabajoRepository.findById(id)

        val idLog: Long = createLogs(idEmpleadoModificante,1,bolsaTrabajoUpdate.get().id).id

        callServerLogs(BolsaTrabajoModelsLogs(0,bolsaTrabajoUpdate.get().nombre,bolsaTrabajoUpdate.get().tipo,
                bolsaTrabajoUpdate.get().activo, idLog))
        bolsaTrabajoModels.id = bolsaTrabajoUpdate.get().id
        return bolsaTrabajoRepository.save(bolsaTrabajoModels)
    }

    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/bolsa-trabajo", a, String::class.java)
}