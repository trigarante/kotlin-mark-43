package com.ahorraseguros.kotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.EstadoEscolaridadModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH.EstadoEscolaridadModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.EstadoEscolaridadRepository
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/estado-escolar")
class EstadoEscolarController: LogsOperations {
    @Autowired
    lateinit var estadoEscolaridadRepository: EstadoEscolaridadRepository

    @Autowired
    lateinit var logsRepository: LogsRepository

    var restTemplate = RestTemplate()

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val tabla = "estadoEscolaridad"

    @GetMapping
    fun findAll(): MutableList<EstadoEscolaridadModels> = estadoEscolaridadRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<EstadoEscolaridadModels> = estadoEscolaridadRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody estadoEscolaridadModels: EstadoEscolaridadModels): EstadoEscolaridadModels = estadoEscolaridadRepository.save(estadoEscolaridadModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long,
               @Valid @RequestBody estadoEscolaridadModels: EstadoEscolaridadModels): EstadoEscolaridadModels {
        val estadoEscolaridadUpdate = estadoEscolaridadRepository.findById(id)
        val idLog: Long = createLogs(idEmpleadoModificante,1,estadoEscolaridadUpdate.get().id).id

        callServerLogs(EstadoEscolaridadModelsLogs(0,estadoEscolaridadUpdate.get().estado,estadoEscolaridadUpdate.get().activo, idLog))
        estadoEscolaridadModels.id = estadoEscolaridadUpdate.get().id
        return estadoEscolaridadRepository.save(estadoEscolaridadModels)
    }

    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/estado-escolar", a, String::class.java)
}