package com.ahorraseguros.kotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.MarcaEmpresarialModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.repositories.MarcaEmpresarialRepository
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/marca-empresarial")
class MarcaEmpresarialController: LogsOperations {
    @Autowired
    lateinit var marcaEmpresarialRepository: MarcaEmpresarialRepository

    @Autowired
    lateinit var logsRepository: LogsRepository
    var restTemplate = RestTemplate()

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    var tabla = "seguimiento"

    @GetMapping
    fun findAll(): MutableList<MarcaEmpresarialModels> = marcaEmpresarialRepository.findAll()

    @GetMapping("/active-marcas/{id}")
    fun findAllAreasByIdSedeAndActives(@PathVariable("id") id: Long): List<MarcaEmpresarialModels> =
            marcaEmpresarialRepository.findAllByIdSedeAndActivo(id,1)


    @PostMapping
    fun create(@Valid @RequestBody marcaEmpresarialModels: MarcaEmpresarialModels): MarcaEmpresarialModels = marcaEmpresarialRepository.save(marcaEmpresarialModels)

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<MarcaEmpresarialModels> = marcaEmpresarialRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody marcaEmpresarialModels: MarcaEmpresarialModels,
               @PathVariable ("idEmpleadoModificante")idEmpleadoModificante: Long): MarcaEmpresarialModels {

//        var idLog = createLogs(idEmpleadoModificante,1,id).id

//        callServerLogs(AreaModelsLogs(0,areaUpdate.get().idSede,areaUpdate.get().nombre,areaUpdate.get().descripcion,
//                areaUpdate.get().activo,areaUpdate.get().codigo, idLog))
//        areaModel.id = areaUpdate.get().id
        return marcaEmpresarialRepository.save(marcaEmpresarialModels)
    }

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity("$serverLogs/areas", a, String::class.java)

    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0, idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName ,
                        Timestamp(System.currentTimeMillis())))
}