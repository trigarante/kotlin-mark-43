package com.ahorraseguros.kotlinmark43.controllers.catalogosRRHH


import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.EstadoRrhhModel
import com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH.EstadoRrhhModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.EstadoRrhhRepository
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid


@CrossOrigin
@RestController
@RequestMapping("/v1/estadosRrhh")
class EstadoRrhhController: LogsOperations {
    @Autowired
    lateinit var estadoRrhhRepository: EstadoRrhhRepository

    @Autowired
    lateinit var logsRepository: LogsRepository

    var restTemplate = RestTemplate()

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val tabla = "estadoRRHH"

    @GetMapping
    fun findAll(): MutableList<EstadoRrhhModel> = estadoRrhhRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<EstadoRrhhModel> = estadoRrhhRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping()
    fun create(@Valid @RequestBody estadoNuevo: EstadoRrhhModel): ResponseEntity<EstadoRrhhModel>{
        try{
            val estado = estadoRrhhRepository.save(estadoNuevo)
            return ResponseEntity.ok(estado)
        }catch (e: Exception){
            return ResponseEntity.badRequest().build()
        }
    }

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody estadoNuevo: EstadoRrhhModel,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): ResponseEntity<EstadoRrhhModel>{
        return try{
            val estadoViejo = estadoRrhhRepository.findById(id)

            if(estadoViejo.isPresent){
                val idLog: Long = createLogs(idEmpleadoModificante,1,id).id
                callServerLogs(EstadoRrhhModelsLogs(0,estadoViejo.get().idEstadoRh,estadoViejo.get().idEtapa,
                        estadoViejo.get().estado,estadoViejo.get().activo,idLog))
                estadoNuevo.id = id
                estadoRrhhRepository.save(estadoNuevo)

                ResponseEntity.ok(estadoNuevo)
            }else{
                ResponseEntity.notFound().build()
            }

        }catch (e: Exception){
            ResponseEntity.badRequest().build()
        }
    }

    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels {
        return logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla,idTabla, javaClass.simpleName ,fechaLog = Timestamp(System.currentTimeMillis())))
    }

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/estadosRrhh", a, String::class.java)
}