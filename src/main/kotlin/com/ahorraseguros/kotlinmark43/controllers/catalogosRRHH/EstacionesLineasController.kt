package com.ahorraseguros.kotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.EstacionesLineasModels
import com.ahorraseguros.kotlinmark43.repositories.EstacionesLineasRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@CrossOrigin
@RestController
@RequestMapping("/v1/estaciones-lineas")
class EstacionesLineasController {
    @Autowired
    lateinit var estacionesLineasRepository: EstacionesLineasRepository

    @GetMapping("transporte/{idTransporte}")
    fun findByIdTransporte(@PathVariable("idTransporte") idTransporte: Int): MutableList<EstacionesLineasModels> = estacionesLineasRepository.findByIdTransporteOrderByLineas(idTransporte)

    @GetMapping("transporte/{idTransporte}/linea/{idLinea}")
    fun findByIdTransporte(@PathVariable("idTransporte") idTransporte: Int,
                           @PathVariable("idLinea") idLinea: String): MutableList<EstacionesLineasModels> = estacionesLineasRepository.findAllByIdTransporteAndLineasOrderByEstacionAsc(idTransporte,idLinea)

    @GetMapping("estacion/{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<EstacionesLineasModels> = estacionesLineasRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())


}