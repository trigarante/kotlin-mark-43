package com.ahorraseguros.kotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.EmpresaModels
import com.ahorraseguros.kotlinmark43.repositories.EmpresaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/empresa")
class EmpresaController {
    @Autowired
    lateinit var empresaRepository: EmpresaRepository

    @GetMapping
    fun findAll(): MutableList<EmpresaModels> = empresaRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<EmpresaModels> = empresaRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody empleadoModel: EmpresaModels): EmpresaModels = empresaRepository.save(empleadoModel)

    @PutMapping("{id}")
    fun update(@PathVariable("id") id: Long,
               @Valid @RequestBody empresaModels: EmpresaModels): EmpresaModels {
        var empresaUpdate = empresaRepository.findById(id)
        empresaModels.id = empresaUpdate.get().id
        return empresaRepository.save(empresaModels)
    }
}