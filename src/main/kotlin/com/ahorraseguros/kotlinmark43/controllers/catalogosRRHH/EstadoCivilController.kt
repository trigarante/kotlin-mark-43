package com.ahorraseguros.kotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.EstadoCivilModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH.EstadoCivilModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.EstadoCivilRepository
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/estado-civil")
class EstadoCivilController: LogsOperations {
    @Autowired
    lateinit var estadoCivilRepository: EstadoCivilRepository

    @Autowired
    lateinit var logsRepository: LogsRepository

    var restTemplate = RestTemplate()

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val tabla = "estadoCivil"

    @GetMapping
    fun findAll(): MutableList<EstadoCivilModels> = estadoCivilRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<EstadoCivilModels> = estadoCivilRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody estadoCivilModels: EstadoCivilModels): EstadoCivilModels = estadoCivilRepository.save(estadoCivilModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long,
               @Valid @RequestBody estadoCivilModels: EstadoCivilModels): EstadoCivilModels {
        var estadoCivilUpdate = estadoCivilRepository.findById(id)
        val idLog: Long = createLogs(idEmpleadoModificante,1,estadoCivilUpdate.get().id).id

        callServerLogs(EstadoCivilModelsLogs(0,estadoCivilUpdate.get().descripcion, estadoCivilUpdate.get().activo, idLog))
        estadoCivilModels.id = estadoCivilUpdate.get().id
        return estadoCivilRepository.save(estadoCivilModels)
    }

    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/estado-civil", a, String::class.java)
}