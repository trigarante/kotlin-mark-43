package com.ahorraseguros.kotlinmark43.controllers.RH

import com.ahorraseguros.kotlinmark43.models.TI.views.UsuarioViewModel
import com.ahorraseguros.kotlinmark43.models.rh.CandidatoModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.CandidatoModelsLogs
import com.ahorraseguros.kotlinmark43.models.rh.views.CandidatoViewModels
import com.ahorraseguros.kotlinmark43.repositories.CandidatoRepository
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.RH.PrecandidatoRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.UsuarioViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import com.ahorraseguros.kotlinmark43.repositories.views.CandidatoViewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp


@CrossOrigin
@RestController
@RequestMapping("/v1/candidato")
class CandidatoController: LogsOperations {

    @Autowired
    lateinit var candidatoRepository: CandidatoRepository
    @Autowired
    lateinit var usuarioViewRepository: UsuarioViewRepository
    @Autowired
    lateinit var logsRepository: LogsRepository

    var restTemplate = RestTemplate()

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val tabla: String = "candidato"

    @Autowired
    lateinit var candidatoViewRepository: CandidatoViewRepository

    @Autowired
    lateinit var precandidatoRepository: PrecandidatoRepository

    @GetMapping("{idUsuario}")
    fun findAll(@PathVariable("idUsuario") idUsuario: Long): MutableList<CandidatoViewModels> {
        val usuario: UsuarioViewModel = usuarioViewRepository.findById(idUsuario).get()

        return if (usuario.idPermisosVisualizacion == 1) {
            candidatoViewRepository.findAll()
        } else {
            candidatoViewRepository.findByIdEmpleado(usuario.idEmpleado)
        }
    }

    @GetMapping("get-by-id/{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<CandidatoViewModels> =
            candidatoViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
                    .orElse(ResponseEntity.notFound().build())

    @PostMapping("{idEtapa}")
    fun create(@PathVariable("idEtapa") idEtapa: Int,@Valid @RequestBody candidatoModel: CandidatoModels): CandidatoModels {
       val precandidatoUpdate =  precandidatoRepository.findById(candidatoModel.idPrecandidato)

        precandidatoUpdate.get().idEtapa = idEtapa

        precandidatoRepository.save(precandidatoUpdate.get())

       return candidatoRepository.save(candidatoModel)
    }

    @PutMapping("update/{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody candidatoModel: CandidatoModels,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): CandidatoModels {

        val candidatoModelsOld = candidatoRepository.findById(id)

        return candidatoModelsOld.map { exist ->
            val idLog: Long = createLogs(idEmpleadoModificante,1,id).id

            callServerLogs(CandidatoModelsLogs(0, candidatoModelsOld.get().idPrecandidato,
                    candidatoModelsOld.get().calificacionPsicometrico, candidatoModelsOld.get().calificacionExamen,
                    candidatoModelsOld.get().comentarios, candidatoModelsOld.get().fechaIngreso, idLog))

            val candidatoUpdated: CandidatoModels = exist.copy( calificacionPsicometrico = candidatoModel.calificacionPsicometrico,
                    calificacionExamen = candidatoModel.calificacionExamen, comentarios = candidatoModel.comentarios)
            candidatoRepository.save(candidatoUpdated)
        }.get()
    }

    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels = logsRepository.save(
            LogsModels(0, idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName ,Timestamp(System.currentTimeMillis()))
    )

    override fun callServerLogs(a: Any): Any =
            restTemplate.postForEntity(serverLogs+"/candidatos", a, String::class.java)

}