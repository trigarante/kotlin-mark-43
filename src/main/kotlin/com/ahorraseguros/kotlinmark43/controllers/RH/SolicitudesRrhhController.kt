package com.ahorraseguros.kotlinmark43.controllers.RH

import com.ahorraseguros.kotlinmark43.models.TI.views.UsuarioViewModel
import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.DocumentosSolicitudRrhhModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.rh.SolicitudesRrhhModels
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import com.ahorraseguros.kotlinmark43.models.rh.views.SolicitudesRrhhViewModels
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.SolicitudesRrhhRepository
import com.ahorraseguros.kotlinmark43.repositories.SolicitudesRrhhViewRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.UsuarioViewRepository
import com.ahorraseguros.mx.logskotlinmark43.models.SolicitudesRrhhModelsLogs
import org.json.JSONObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/solicitudes")
class SolicitudesRrhhController : LogsOperations {


    @Autowired
    lateinit var solicitudesRrhhRepository: SolicitudesRrhhRepository

    @Autowired
    lateinit var logsRepository: LogsRepository

    @Autowired
    lateinit var solicitudesRrhhViewRepository: SolicitudesRrhhViewRepository

    @Autowired
    lateinit var usuarioViewRepository: UsuarioViewRepository

    var restTemplate = RestTemplate()
    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val tabla = "solicitudesRRHH"

    @GetMapping("{idUsuario}")
    fun findAll(@PathVariable("idUsuario") idUsuario: Long): MutableList<SolicitudesRrhhViewModels> {
        val usuario: UsuarioViewModel = usuarioViewRepository.findById(idUsuario).get()

        return if (usuario.idPermisosVisualizacion == 1) {
            solicitudesRrhhViewRepository.findAll()
        } else {
            solicitudesRrhhViewRepository.findAllByIdReclutador(usuario.idEmpleado)
        }
    }

    @GetMapping("get-by-id/{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<SolicitudesRrhhViewModels> = solicitudesRrhhViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun updateAll(@PathVariable("id") id: Long, @Valid @RequestBody solicitudNueva: SolicitudesRrhhModels,
                  @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): HttpStatus{
        return try{
            val solicitudVieja = solicitudesRrhhRepository.findById(id)

            if(solicitudVieja.isPresent){
                val solicitud = solicitudVieja.get()
                val idLog: Long = createLogs(idEmpleadoModificante,1, id).id

                callServerLogs(SolicitudesRrhhModelsLogs(0, solicitudVieja.get().idBolsaTrabajo, solicitudVieja.get().idReclutador,
                        solicitudVieja.get().idVacante, solicitudVieja.get().nombre, solicitudVieja.get().apellidoPaterno,
                        solicitudVieja.get().apellidoMaterno,solicitudVieja.get().telefono,solicitudVieja.get().correo,
                        solicitudVieja.get().cp, solicitudVieja.get().edad,solicitudVieja.get().fecha, solicitudVieja.get().estado,
                        solicitudVieja.get().fechaCita,solicitudVieja.get().fechaDescartado,solicitudVieja.get().documentos,
                        solicitudVieja.get().comentarios, idLog))

                solicitud.nombre = solicitudNueva.nombre
                solicitud.apellidoPaterno = solicitudNueva.apellidoPaterno
                solicitud.apellidoMaterno = solicitudNueva.apellidoMaterno
                solicitud.telefono = solicitudNueva.telefono
                solicitud.correo = solicitudNueva.correo
                solicitud.cp = solicitudNueva.cp
                solicitud.edad = solicitudNueva.edad
                solicitud.idReclutador = solicitudNueva.idReclutador
                solicitud.idBolsaTrabajo = solicitudNueva.idBolsaTrabajo
                solicitud.idVacante = solicitudNueva.idVacante

                solicitudesRrhhRepository.save(solicitud)

                HttpStatus.OK
            }else{
                HttpStatus.EXPECTATION_FAILED
            }
        }catch(e: Exception){
            HttpStatus.BAD_REQUEST
        }
    }

    @PutMapping("baja-solicitud/{id}/{idEmpleadoModificante}")
    fun bajaSolicitud(@PathVariable("id") id: Long , @Valid @RequestBody datos: SolicitudesRrhhModels,
                      @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): SolicitudesRrhhModels{
        var solicitud=solicitudesRrhhRepository.findById(id)
        val idLog: Long = createLogs(idEmpleadoModificante,1, id).id

        callServerLogs(SolicitudesRrhhModelsLogs(0, solicitud.get().idBolsaTrabajo, solicitud.get().idReclutador,
                solicitud.get().idVacante, solicitud.get().nombre, solicitud.get().apellidoPaterno,
                solicitud.get().apellidoMaterno,solicitud.get().telefono,solicitud.get().correo,
                solicitud.get().cp, solicitud.get().edad,solicitud.get().fecha, solicitud.get().estado,
                solicitud.get().fechaCita,solicitud.get().fechaDescartado,solicitud.get().documentos,
                solicitud.get().comentarios, idLog))

        solicitud.get().estado=datos.estado
        solicitud.get().comentarios = datos.comentarios
        solicitud.get().fechaDescartado= Timestamp(System.currentTimeMillis())

        return solicitudesRrhhRepository.save(solicitud.get())
    }


    @GetMapping("documentos/{id}")
    fun findDocsById(@PathVariable("id") id: Long): ResponseEntity<String?> = solicitudesRrhhRepository.findById(id).map { s -> ResponseEntity.ok(s.documentos!!) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun create(@Valid @RequestBody solicitudesRrhhModel: SolicitudesRrhhModels): SolicitudesRrhhModels =
            solicitudesRrhhRepository.save(solicitudesRrhhModel)


    @PutMapping("{id}/{fecha}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @PathVariable("fecha") fecha: Long,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): HttpStatus {

        return try{
            val solicitudVieja = solicitudesRrhhRepository.findById(id)

            if(solicitudVieja.isPresent){
                val actualizacion = solicitudVieja.get()
                val idLog: Long = createLogs(idEmpleadoModificante,1, id).id

                actualizacion.fechaCita = Timestamp(fecha)
                callServerLogs(SolicitudesRrhhModelsLogs(0, solicitudVieja.get().idBolsaTrabajo, solicitudVieja.get().idReclutador,
                        solicitudVieja.get().idVacante, solicitudVieja.get().nombre, solicitudVieja.get().apellidoPaterno,
                        solicitudVieja.get().apellidoMaterno,solicitudVieja.get().telefono,solicitudVieja.get().correo,
                        solicitudVieja.get().cp, solicitudVieja.get().edad,solicitudVieja.get().fecha, solicitudVieja.get().estado,
                        solicitudVieja.get().fechaCita,solicitudVieja.get().fechaDescartado,solicitudVieja.get().documentos,
                        solicitudVieja.get().comentarios, idLog))

                solicitudesRrhhRepository.save(actualizacion)
                HttpStatus.OK
            }else{
                HttpStatus.EXPECTATION_FAILED
            }
        }catch(e: Exception){
            HttpStatus.BAD_REQUEST
        }
    }

    @PutMapping("documentos/{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long,
               @Valid @RequestBody documentosSolicitudRrhhModel: DocumentosSolicitudRrhhModels): SolicitudesRrhhModels {
        return solicitudesRrhhRepository.findById(id).map { exist ->
            val idLog: Long = createLogs(idEmpleadoModificante,1, id).id

            callServerLogs(SolicitudesRrhhModelsLogs(0, exist.idBolsaTrabajo, exist.idReclutador, exist.idVacante,
                    exist.nombre, exist.apellidoPaterno, exist.apellidoMaterno,exist.telefono,exist.correo,
                    exist.cp, exist.edad,exist.fecha, exist.estado, exist.fechaCita,exist.fechaDescartado,exist.documentos,
                    exist.comentarios, idLog))
            val solicitudUpdated: SolicitudesRrhhModels = exist.copy(documentos = JSONObject(documentosSolicitudRrhhModel).toString())
            solicitudesRrhhRepository.save(solicitudUpdated)
        }.get()
    }

    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels = logsRepository.save(
            LogsModels(0, idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,Timestamp(System.currentTimeMillis())))


    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity("$serverLogs/solicitudesRrhh", a, String::class.java)


}
