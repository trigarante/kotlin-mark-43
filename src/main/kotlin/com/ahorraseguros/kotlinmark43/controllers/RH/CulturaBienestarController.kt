package com.ahorraseguros.kotlinmark43.controllers.RH

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.AreaModels
import com.ahorraseguros.kotlinmark43.models.rh.*
import com.ahorraseguros.kotlinmark43.repositories.AreasRepository
import com.ahorraseguros.kotlinmark43.repositories.RH.AreasCulturaBienestarRepository
import com.ahorraseguros.kotlinmark43.repositories.RH.ContadorCulturaBienestarRepository
import com.ahorraseguros.kotlinmark43.repositories.RH.PreguntasCulturaBienestar
import com.ahorraseguros.kotlinmark43.repositories.RH.RespuestasCulturaBienestarRepository
import org.json.JSONArray
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.sql.Timestamp
import java.util.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/cultura-bienestar")
class CulturaBienestarController {

    @Autowired
    lateinit var areasRepository: AreasRepository
    @Autowired
    lateinit var preguntasCulturaBienestarRepository: PreguntasCulturaBienestar
    @Autowired
    lateinit var respuestasCulturaBienestarRepository: RespuestasCulturaBienestarRepository
    @Autowired
    lateinit var contadorCulturaBienestarRepository: ContadorCulturaBienestarRepository


    @GetMapping("areas")
    fun findAllAreas(): MutableList<AreaModels> = areasRepository.findAll()

    @GetMapping("areas-by-id/{id}")
    fun findAllAreas(@PathVariable("id") id: Long): Optional<AreaModels> = areasRepository.findById(id)

//    @GetMapping("contador")
//    fun findAllContador(): Int = contadorCulturaBienestarRepository.findAllByContador(1).get(0).contador

    @PostMapping("areas")
    fun createArea(@Valid @RequestBody areasModel: AreaModels): AreaModels = areasRepository.save(areasModel)

    @GetMapping("preguntas")
    fun findAllPreguntas(): MutableList<PreguntasCulturaBienestarModel> = preguntasCulturaBienestarRepository.findAll()

    @PostMapping("preguntas")
    fun createPreguntas(@Valid @RequestBody preguntasCulturaBienestar: PreguntasCulturaBienestarModel): PreguntasCulturaBienestarModel = preguntasCulturaBienestarRepository.save(preguntasCulturaBienestar)

    @GetMapping("preguntas-by-area/{id}")
    fun findPreguntasByAreas(@PathVariable("id") id: Int): MutableList<PreguntasCulturaBienestarModel> = preguntasCulturaBienestarRepository.findAllByIdArea(id)

    @GetMapping("preguntas-by-id/{id}")
    fun findPreguntasById(@PathVariable("id") id: Int): Optional<PreguntasCulturaBienestarModel> = preguntasCulturaBienestarRepository.findById(id)

    @GetMapping("preguntas-by-fecha/{id}/{fechaI}/{fechaF}")
    fun findPreguntasByAreasAndFechas(@PathVariable("id") id: Int, @PathVariable("fechaI") fechaI: String,
                                      @PathVariable("fechaF") fechaF: String): MutableList<PreguntasCulturaBienestarModel> {
        println(id)
        println(fechaI.toString())
        println(fechaF.toString())
        var x = preguntasCulturaBienestarRepository.aiiuda(id, fechaI, fechaF)
        println(x)
        return x
    }


    @GetMapping("respuestas-by-pregunta/{id}")
    fun findRespuestasByPregunta(@PathVariable("id") id: Int): MutableList<RespuestasCulturaBienestarModel> {
        return respuestasCulturaBienestarRepository.findAllByIdPregunta(id)
    }

    @GetMapping("get-contador-by-respuesta/{id}")
    fun findContadorByRespuesta(@PathVariable("id") id: Int): Int = contadorCulturaBienestarRepository.countAllByIdRespuesta(id)

    @GetMapping("respuestas")
    fun findAllRespuestas(): MutableList<RespuestasCulturaBienestarModel> = respuestasCulturaBienestarRepository.findAll()

    @PostMapping("respuestas")
    fun createRespuesta(@Valid @RequestBody respuestasCulturaBienestarModel: RespuestasCulturaBienestarModel): RespuestasCulturaBienestarModel = respuestasCulturaBienestarRepository.save(respuestasCulturaBienestarModel)

    @PutMapping("respuestas/{id}")
    fun agregarRespuesta(@PathVariable("id") id: Int, @Valid @RequestBody respuestasCulturaBienestarModel: RespuestasCulturaBienestarModel): RespuestasCulturaBienestarModel {

        val r = respuestasCulturaBienestarRepository.findByIdPregunta(id)
        if (r.isPresent) {
            r.get().respuestas = respuestasCulturaBienestarModel.respuestas
            respuestasCulturaBienestarRepository.save(r.get())
        }
        return respuestasCulturaBienestarModel
    }

    @PutMapping("respuestas/update-contador/{idRespuesta}")
    fun agregarRespuesta(@PathVariable("idRespuesta") idRespuesta: Int): Boolean {
        contadorCulturaBienestarRepository.save(ContadorCulturaBienestarModel(idRespuesta, Timestamp(System.currentTimeMillis()), 0))
        return true
    }
}