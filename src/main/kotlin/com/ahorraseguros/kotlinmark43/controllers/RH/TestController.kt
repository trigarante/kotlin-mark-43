package com.ahorraseguros.kotlinmark43.controllers.RH

import com.ahorraseguros.kotlinmark43.models.rh.TestModel
import com.ahorraseguros.kotlinmark43.repositories.RH.TestRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@CrossOrigin
@RestController
@RequestMapping("/v1/test")

class TestController {

    @Autowired
    lateinit var reposotoryTest: TestRepository

    @GetMapping
    fun getTest(): MutableList<TestModel> = reposotoryTest.findAll()

}