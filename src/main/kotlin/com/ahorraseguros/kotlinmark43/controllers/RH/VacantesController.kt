package com.ahorraseguros.kotlinmark43.controllers.RH

import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.rh.VacantesModels
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import com.ahorraseguros.kotlinmark43.models.rh.views.VacantesViewModels
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.VacantesRepository
import com.ahorraseguros.kotlinmark43.repositories.VacantesViewRepository
import com.ahorraseguros.mx.logskotlinmark43.models.VacantesModelsLogs

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/vacantes")
class VacantesController: LogsOperations {

    @Autowired
    lateinit var vacantesRepository: VacantesRepository
    @Autowired
    lateinit var logsRepository: LogsRepository
    var restTemplate = RestTemplate()
    @Autowired
    lateinit var vacantesViewRepository: VacantesViewRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val tabla = "vacantes"

    @GetMapping()
    fun findAll(): MutableList<VacantesViewModels> = vacantesViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<VacantesViewModels> = vacantesViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping()
    fun create(@Valid @RequestBody vacantesModels: VacantesModels): VacantesModels = vacantesRepository.save(vacantesModels)

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody vacantesModels: VacantesModels,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): VacantesModels {
        var vacantesUpdated = vacantesRepository.findById(id)
        val idLog: Long = createLogs(idEmpleadoModificante,1,vacantesUpdated.get().id).id

        callServerLogs(VacantesModelsLogs(0,vacantesUpdated.get().nombre,vacantesUpdated.get().idPuesto,
                vacantesUpdated.get().idSubarea,vacantesUpdated.get().idTipoPuesto, vacantesUpdated.get().activo,idLog))
        vacantesModels.id = vacantesUpdated.get().id

        return vacantesRepository.save(vacantesModels)
    }

    @PutMapping("baja-vacante/{id}/{idEmpleadoModificante}")
    fun bajaVacante(@PathVariable("id") id: Long,
                    @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): VacantesModels {
        var vacantesUpdated = vacantesRepository.findById(id)
        val idLog: Long = createLogs(idEmpleadoModificante,1,vacantesUpdated.get().id).id

        callServerLogs(VacantesModelsLogs(0,vacantesUpdated.get().nombre,vacantesUpdated.get().idPuesto,
                vacantesUpdated.get().idSubarea, vacantesUpdated.get().idTipoPuesto,vacantesUpdated.get().activo, idLog))

        return vacantesUpdated.map { exist ->
            var vacantesNuevo: VacantesModels = exist.copy(activo = 0)
            vacantesRepository.save(vacantesNuevo)
        }.get()
    }

    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/vacantes", a, String::class.java)

}