package com.ahorraseguros.kotlinmark43.controllers.RH

import com.ahorraseguros.kotlinmark43.models.TI.views.UsuarioViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.RH.PrecandidatoModelsLogs
import com.ahorraseguros.kotlinmark43.models.rh.PrecandidatoModels
import com.ahorraseguros.kotlinmark43.models.rh.SolicitudesRrhhModels
import com.ahorraseguros.kotlinmark43.models.rh.views.PrecandidatoViewModels
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.RH.PrecandidatoRepository
import com.ahorraseguros.kotlinmark43.repositories.SolicitudesRrhhRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.UsuarioViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import com.ahorraseguros.kotlinmark43.repositories.views.PrecandidatoViewRepository

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid


@CrossOrigin
@RestController
@RequestMapping("/v1/precandidatos")
class PrecandidatoController: LogsOperations{
    @Autowired
    lateinit var logsRepository: LogsRepository

    @Autowired
    lateinit var precandidatoRepository: PrecandidatoRepository

    @Autowired
    lateinit var solicitudesRrhhRepository: SolicitudesRrhhRepository

    @Autowired
    lateinit var precandidatoViewRepository: PrecandidatoViewRepository

    @Autowired
    lateinit var usuarioViewRepository: UsuarioViewRepository

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    var restTemplate = RestTemplate()

    val tabla: String = "precandidato"

    @GetMapping("get-all/{idUsuario}")
    fun findAll(@PathVariable("idUsuario") idUsuario: Long): MutableList<PrecandidatoViewModels> {
        val usuario: UsuarioViewModel = usuarioViewRepository.findById(idUsuario).get()

        return if (usuario.idPermisosVisualizacion == 1) {
            precandidatoViewRepository.findAll()
        } else {
            precandidatoViewRepository.findByIdEmpleado(usuario.idEmpleado)
        }
    }

    @GetMapping("idEtapa/{idEtapa}")
    fun findByIdEtapa(@PathVariable("idEtapa") idEtapa: Int) = precandidatoViewRepository.findByIdEtapaOrderByFechaCreacionDesc(idEtapa)

    @GetMapping("id/{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<PrecandidatoViewModels> = precandidatoViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping("{id}/{fechaNacimiento}")
    fun create(@PathVariable("id") id: Long, @PathVariable ("fechaNacimiento") fechaNacimiento: Long,
               @Valid @RequestBody precandidatoModel: PrecandidatoModels): PrecandidatoModels {
        solicitudesRrhhRepository.findById(id).map { exist ->
            val solicitudesUpdated: SolicitudesRrhhModels = exist.copy(estado = 1)
            solicitudesRrhhRepository.save(solicitudesUpdated)
        }.get()
        val precandidato: PrecandidatoModels = precandidatoModel
        precandidato.fechaNacimiento = Timestamp(fechaNacimiento)
        return precandidatoRepository.save(precandidatoModel)
    }

    @GetMapping("{curp}")
    fun findByCurp(@PathVariable("curp") curp: String): ResponseEntity<Boolean> =
            precandidatoViewRepository.findByCurp(curp).map {
                ResponseEntity.ok(true)
            }.orElse(ResponseEntity.notFound().build())

    @PutMapping("{id}/{fechaNacimiento}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @PathVariable ("fechaNacimiento") fechaNacimiento: Long,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long,
               @Valid @RequestBody precandidatoModel: PrecandidatoModels): PrecandidatoModels {
        val precandidatoUpdated = precandidatoRepository.findById(id)
        val idLog: Long = createLogs(idEmpleadoModificante,1,id).id

        callServerLogs(PrecandidatoModelsLogs(0,precandidatoUpdated.get().idEtapa,precandidatoUpdated.get().idEscolaridad,
            precandidatoUpdated.get().idEstacion,precandidatoUpdated.get().idEstadoRH,precandidatoUpdated.get().idEstadoCivil,
            precandidatoUpdated.get().idEstadoEscolaridad,precandidatoUpdated.get().idMedioTraslado,precandidatoUpdated.get().idPais,
            precandidatoUpdated.get().idSolicitudRRHH, precandidatoUpdated.get().nombre,precandidatoUpdated.get().apellidoPaterno,
            precandidatoUpdated.get().apellidoMaterno, precandidatoUpdated.get().fechaNacimiento,precandidatoUpdated.get().email,
            precandidatoUpdated.get().curp, precandidatoUpdated.get().genero, precandidatoUpdated.get().cp, precandidatoUpdated.get().idColonia,
            precandidatoUpdated.get().calle,precandidatoUpdated.get().numeroExterior, precandidatoUpdated.get().numeroInterior,
            precandidatoUpdated.get().telefonoFijo,precandidatoUpdated.get().telefonoMovil, precandidatoUpdated.get().fechaCreacion,
            precandidatoUpdated.get().fechaBaja, precandidatoUpdated.get().tiempoTraslado,precandidatoUpdated.get().recontratable,
            precandidatoUpdated.get().detalleBaja,precandidatoUpdated.get().fechaRegistroBaja,idLog))
        precandidatoModel.id = precandidatoUpdated.get().id
        precandidatoModel.fechaNacimiento = Timestamp(fechaNacimiento)

        return precandidatoRepository.save(precandidatoModel)
    }

    @PutMapping("reactivar/{id}/{fechaCreacion}/{idEmpleadoModificante}")
    fun updateEstadoRH(@PathVariable("id") id: Long, @PathVariable("fechaCreacion") fechaCreacion: Long,
                       @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): HttpStatus

            = precandidatoRepository.findById(id).map { precandidato ->
                val idLog: Long = createLogs(idEmpleadoModificante,1,id).id

                callServerLogs(PrecandidatoModelsLogs(0,precandidato.idEtapa,precandidato.idEscolaridad,
                        precandidato.idEstacion,precandidato.idEstadoRH,precandidato.idEstadoCivil,
                        precandidato.idEstadoEscolaridad,precandidato.idMedioTraslado,precandidato.idPais,
                        precandidato.idSolicitudRRHH, precandidato.nombre,precandidato.apellidoPaterno,
                        precandidato.apellidoMaterno, precandidato.fechaNacimiento,precandidato.email,
                        precandidato.curp, precandidato.genero, precandidato.cp, precandidato.idColonia,
                        precandidato.calle,precandidato.numeroExterior, precandidato.numeroInterior,
                        precandidato.telefonoFijo,precandidato.telefonoMovil, precandidato.fechaCreacion,
                        precandidato.fechaBaja, precandidato.tiempoTraslado,precandidato.recontratable,
                        precandidato.detalleBaja,precandidato.fechaRegistroBaja,idLog))
                precandidato.idEstadoRH = 16
                precandidato.fechaCreacion = Timestamp(fechaCreacion)
                precandidatoRepository.save(precandidato)
                HttpStatus.OK
            }.orElse(HttpStatus.BAD_REQUEST)

    @PutMapping("recontratar/{id}/{recontratable}/{idEmpleadoModificante}")
    fun updateRecontratable(@PathVariable("id") id: Long, @PathVariable("recontratable") recontratable: Boolean,
                            @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): HttpStatus {

        return precandidatoRepository.findById(id).map { precandidato ->
            val idLog: Long = createLogs(idEmpleadoModificante, 1, id).id

            callServerLogs(PrecandidatoModelsLogs(0, precandidato.idEtapa, precandidato.idEscolaridad,
                    precandidato.idEstacion, precandidato.idEstadoRH, precandidato.idEstadoCivil,
                    precandidato.idEstadoEscolaridad, precandidato.idMedioTraslado, precandidato.idPais,
                    precandidato.idSolicitudRRHH, precandidato.nombre, precandidato.apellidoPaterno,
                    precandidato.apellidoMaterno, precandidato.fechaNacimiento, precandidato.email,
                    precandidato.curp, precandidato.genero, precandidato.cp, precandidato.idColonia,
                    precandidato.calle, precandidato.numeroExterior, precandidato.numeroInterior,
                    precandidato.telefonoFijo, precandidato.telefonoMovil, precandidato.fechaCreacion,
                    precandidato.fechaBaja, precandidato.tiempoTraslado, precandidato.recontratable,
                    precandidato.detalleBaja, precandidato.fechaRegistroBaja, idLog))
            precandidato.recontratable = recontratable
            precandidatoRepository.save(precandidato)
            HttpStatus.OK
        }.orElse(HttpStatus.BAD_REQUEST)
    }

    @PutMapping("pre-empleado/{id}/{idEmpleadoModificante}")
    fun updatePreEmpleado(@PathVariable("id") id: Long,
                          @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): PrecandidatoModels {

        return precandidatoRepository.findById(id).map { precandidato ->
            val idLog: Long = createLogs(idEmpleadoModificante,1,id).id
            callServerLogs(PrecandidatoModelsLogs(0,precandidato.idEtapa,precandidato.idEscolaridad,
                    precandidato.idEstacion,precandidato.idEstadoRH,precandidato.idEstadoCivil,
                    precandidato.idEstadoEscolaridad,precandidato.idMedioTraslado,precandidato.idPais,
                    precandidato.idSolicitudRRHH, precandidato.nombre,precandidato.apellidoPaterno,
                    precandidato.apellidoMaterno, precandidato.fechaNacimiento,precandidato.email,
                    precandidato.curp, precandidato.genero, precandidato.cp, precandidato.idColonia,
                    precandidato.calle,precandidato.numeroExterior, precandidato.numeroInterior,
                    precandidato.telefonoFijo,precandidato.telefonoMovil, precandidato.fechaCreacion,
                    precandidato.fechaBaja, precandidato.tiempoTraslado,precandidato.recontratable,
                    precandidato.detalleBaja,precandidato.fechaRegistroBaja,idLog))
            precandidato.idEtapa = 7
            precandidatoRepository.save(precandidato)
        }.get()
    }

    @PutMapping("baja/{id}/{fechaBaja}/{idEmpleadoModificante}")
    fun baja(@PathVariable("id") id: Long, @PathVariable ("fechaBaja") fechaBaja: Long,
             @Valid @RequestBody precandidatoModel: PrecandidatoModels,
             @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long ): HttpStatus{

        return try{
            val precandidato = precandidatoRepository.findById(id)

            if(precandidato.isPresent){
                val idLog: Long = createLogs(idEmpleadoModificante,1,id).id
                callServerLogs(PrecandidatoModelsLogs(0,precandidato.get().idEtapa,precandidato.get().idEscolaridad,
                        precandidato.get().idEstacion,precandidato.get().idEstadoRH,precandidato.get().idEstadoCivil,
                        precandidato.get().idEstadoEscolaridad,precandidato.get().idMedioTraslado,precandidato.get().idPais,
                        precandidato.get().idSolicitudRRHH, precandidato.get().nombre,precandidato.get().apellidoPaterno,
                        precandidato.get().apellidoMaterno, precandidato.get().fechaNacimiento,precandidato.get().email,
                        precandidato.get().curp, precandidato.get().genero, precandidato.get().cp, precandidato.get().idColonia,
                        precandidato.get().calle,precandidato.get().numeroExterior, precandidato.get().numeroInterior,
                        precandidato.get().telefonoFijo,precandidato.get().telefonoMovil, precandidato.get().fechaCreacion,
                        precandidato.get().fechaBaja, precandidato.get().tiempoTraslado,precandidato.get().recontratable,
                        precandidato.get().detalleBaja,precandidato.get().fechaRegistroBaja,idLog))

                precandidato.get().fechaBaja = Timestamp(fechaBaja)
                precandidato.get().idEstadoRH = precandidatoModel.idEstadoRH
                precandidato.get().fechaRegistroBaja = Timestamp(System.currentTimeMillis())
                precandidato.get().recontratable = precandidatoModel.recontratable
                precandidato.get().detalleBaja = precandidatoModel.detalleBaja
                precandidatoRepository.save(precandidato.get())

                HttpStatus.OK
            }else{ HttpStatus.NOT_FOUND }
        }catch(e: Exception){
            HttpStatus.BAD_REQUEST
        }
    }

    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0, idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/precandidato", a, String::class.java)
}
