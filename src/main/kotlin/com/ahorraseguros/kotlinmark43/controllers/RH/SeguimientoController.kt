package com.ahorraseguros.kotlinmark43.controllers.RH

import com.ahorraseguros.kotlinmark43.models.TI.views.UsuarioViewModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.RH.PrecandidatoModelsLogs
import com.ahorraseguros.kotlinmark43.models.rh.PrecandidatoModels
import com.ahorraseguros.kotlinmark43.models.rh.SeguimientoModels
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import com.ahorraseguros.kotlinmark43.models.rh.views.SeguimientoViewModels
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.RH.PrecandidatoRepository
import com.ahorraseguros.kotlinmark43.repositories.SeguimientoRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.UsuarioViewRepository
import com.ahorraseguros.kotlinmark43.repositories.views.SeguimientoViewRepository
import com.ahorraseguros.mx.logskotlinmark43.models.SeguimientoModelsLogs
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/seguimiento")
class SeguimientoController: LogsOperations {


    @Autowired
    lateinit var seguimientoRepository: SeguimientoRepository
    @Autowired
    lateinit var seguimientoViewRepository: SeguimientoViewRepository
    @Autowired
    lateinit var precandidatoRepository: PrecandidatoRepository
    @Autowired
    lateinit var logsRepository: LogsRepository
    @Autowired
    lateinit var usuarioViewRepository: UsuarioViewRepository
    var restTemplate = RestTemplate()
    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    var tabla = "seguimiento"


    @GetMapping("{idUsuario}")
    fun findAll(@PathVariable ("idUsuario") idUsuario: Long): MutableList<SeguimientoViewModels> {
        val usuario: UsuarioViewModel = usuarioViewRepository.findById(idUsuario).get()

        return if (usuario.idPermisosVisualizacion == 1) {
            seguimientoViewRepository.findAll()
        } else {
            seguimientoViewRepository.findAllByIdCoach(usuario.idEmpleado)
        }
    }

    @GetMapping("get-by-id/{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<SeguimientoViewModels> = seguimientoViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping("{id}/{idEtapa}/{fechaIngreso}")
    fun create(@PathVariable("id") id: Long, @PathVariable("idEtapa") idEtapa: Int,
               @PathVariable("fechaIngreso") fechaIngreso: Long,
               @Valid @RequestBody seguimientoModel: SeguimientoModels): SeguimientoModels {
        precandidatoRepository.findById(id).map { exist ->
            val precandidatoUpdated: PrecandidatoModels = exist.copy(idEtapa = idEtapa)
            precandidatoRepository.save(precandidatoUpdated)
        }.get()
        val seguimiento: SeguimientoModels = seguimientoModel
        seguimiento.fechaIngreso = Timestamp(fechaIngreso)
        return seguimientoRepository.save(seguimiento)
    }

    @PutMapping("etapa-practica/{id}/{calificacionRollPlay}/{comentariosFaseDos}/{idEtapa}/{idPrecandidato}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Int, @PathVariable("calificacionRollPlay") calificacionRollPlay: Boolean,
               @PathVariable("comentariosFaseDos") comentariosFaseDos: String, @PathVariable("idEtapa") idEtapa: Int,
               @PathVariable("idPrecandidato") idPrecandidato: Long,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): SeguimientoModels {
        var idLog: Long
        precandidatoRepository.findById(idPrecandidato).map { exist ->
            tabla = "precandidato"
            idLog = createLogs(idEmpleadoModificante,1,idPrecandidato).id
            callServerLogs(PrecandidatoModelsLogs(0,exist.idEtapa,exist.idEscolaridad, exist.idEstacion,exist.idEstadoRH,
                    exist.idEstadoCivil,exist.idEstadoEscolaridad,exist.idMedioTraslado,exist.idPais, exist.idSolicitudRRHH,
                    exist.nombre,exist.apellidoPaterno, exist.apellidoMaterno, exist.fechaNacimiento,exist.email,
                    exist.curp, exist.genero, exist.cp, exist.idColonia, exist.calle,exist.numeroExterior, exist.numeroInterior,
                    exist.telefonoFijo,exist.telefonoMovil, exist.fechaCreacion, exist.fechaBaja, exist.tiempoTraslado,exist.recontratable,
                    exist.detalleBaja,exist.fechaRegistroBaja,idLog))
            val precandidatoUpdate: PrecandidatoModels = exist.copy(idEtapa = idEtapa)
            precandidatoRepository.save(precandidatoUpdate)
        }

        val seguimientoOld=seguimientoRepository.findById(id)
        return seguimientoOld.map { exist ->
            tabla = "seguimiento"
            idLog = createLogs(idEmpleadoModificante,1,seguimientoOld.get().id.toLong()).id
            val seguimientoUpdate: SeguimientoModels = exist.copy(calificacionRollPlay = calificacionRollPlay, comentariosFaseDos = comentariosFaseDos)
            callServerLogs(SeguimientoModelsLogs(0,seguimientoOld.get().idSubarea,seguimientoOld.get().idCapacitacion,
                    seguimientoOld.get().idCoach,seguimientoOld.get().fechaRegistro,seguimientoOld.get().fechaIngreso,
                    seguimientoOld.get().comentarios,seguimientoOld.get().asistencia,seguimientoOld.get().registro,
                    seguimientoOld.get().comentariosFaseDos,seguimientoOld.get().calificacionRollPlay,idLog))
            seguimientoRepository.save(seguimientoUpdate)
        }.get()
    }

    @PutMapping("{id}/{fechaIngreso}/{idEmpleadoModificante}")
    fun updateAll(@PathVariable("id") id: Int, @PathVariable ("fechaIngreso") fechaIngreso: Long,
                  @Valid @RequestBody seguimientoModel: SeguimientoModels,
                  @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): HttpStatus {

        return try{
            val seguimientoViejo = seguimientoRepository.findById(id)

            if(seguimientoViejo.isPresent){
                val seguimiento = seguimientoViejo.get()
                val idLog: Long = createLogs(idEmpleadoModificante,1,id.toLong()).id
                callServerLogs(SeguimientoModelsLogs(0,seguimiento.idSubarea,seguimiento.idCapacitacion,
                        seguimiento.idCoach,seguimiento.fechaRegistro,seguimiento.fechaIngreso,seguimiento.comentarios,
                        seguimiento.asistencia,seguimiento.registro,seguimiento.comentariosFaseDos,
                        seguimiento.calificacionRollPlay,idLog))
                seguimiento.calificacionRollPlay = seguimientoModel.calificacionRollPlay
                seguimiento.comentarios = seguimientoModel.comentarios
                seguimiento.fechaIngreso = Timestamp(fechaIngreso)
                seguimiento.idCapacitacion = seguimientoModel.idCapacitacion
                seguimiento.idCoach = seguimientoModel.idCoach
                seguimiento.idSubarea = seguimientoModel.idSubarea
                seguimiento.asistencia = seguimientoModel.asistencia
                seguimientoRepository.save(seguimiento)

                HttpStatus.OK
            }else{
                HttpStatus.EXPECTATION_FAILED
            }
        }catch(e: Exception){
            HttpStatus.BAD_REQUEST
        }
    }


    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels = logsRepository.save(
            LogsModels(0, idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName ,Timestamp(System.currentTimeMillis())))


    override fun callServerLogs(a: Any): Any {
        var URL = "$serverLogs/"
        if(tabla == "seguimiento"){
            URL = URL + "seguimientos"
        } else {
            URL = URL + "precandidato"
        }
        return restTemplate.postForEntity(URL, a, String::class.java)
    }
}