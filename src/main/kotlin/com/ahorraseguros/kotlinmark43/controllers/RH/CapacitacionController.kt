package com.ahorraseguros.kotlinmark43.controllers.RH

import com.ahorraseguros.kotlinmark43.models.TI.views.UsuarioViewModel
import com.ahorraseguros.kotlinmark43.models.rh.CapacitacionModels
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.rh.PrecandidatoModels
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import com.ahorraseguros.kotlinmark43.models.rh.views.CapacitacionViewModels
import com.ahorraseguros.kotlinmark43.repositories.CapacitacionRepository
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.RH.PrecandidatoRepository
import com.ahorraseguros.kotlinmark43.repositories.ti.views.UsuarioViewRepository
import com.ahorraseguros.kotlinmark43.repositories.views.CapacitacionViewRepository
import com.ahorraseguros.mx.logskotlinmark43.models.CapacitacionModelsLogs
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/capacitacion")


class CapacitacionController : LogsOperations {


    @Autowired
    lateinit var capacitacionRepository: CapacitacionRepository

    @Autowired
    lateinit var capacitacionViewRepository: CapacitacionViewRepository

    @Autowired
    lateinit var precandidatoRepository: PrecandidatoRepository

    @Autowired
    lateinit var usuarioViewRepository: UsuarioViewRepository

    @Autowired
    lateinit var logsRepository: LogsRepository
    var restTemplate = RestTemplate()

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val tabla: String = "capacitacion"

    @GetMapping("{idUsuario}")
    fun findAll(@PathVariable("idUsuario") idUsuario: Long): MutableList<CapacitacionViewModels> {
        val usuario: UsuarioViewModel = usuarioViewRepository.findById(idUsuario).get()

        return if (usuario.idPermisosVisualizacion == 1) {
            capacitacionViewRepository.findAll()
        } else {
            capacitacionViewRepository.findAllByIdCapacitador(usuario.idEmpleado)
        }
    }

    @GetMapping("get-by-id/{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<CapacitacionViewModels>  = capacitacionViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping("{id}/{idEtapa}/{fechaIngreso}")
    fun create(@PathVariable("id") id: Long, @PathVariable("idEtapa") idEtapa: Int,
               @PathVariable("fechaIngreso") fechaIngreso: Long,
               @Valid @RequestBody capacitacionModel: CapacitacionModels): CapacitacionModels {
        precandidatoRepository.findById(id).map { exist ->
            val precandidatoUpdated: PrecandidatoModels = exist.copy(idEtapa = idEtapa)
            precandidatoRepository.save(precandidatoUpdated)
        }.get()
        val capacitacion: CapacitacionModels = capacitacionModel
        capacitacion.fechaIngreso = Timestamp(fechaIngreso)
        return capacitacionRepository.save(capacitacion)
    }

    @PutMapping("{id}/{fechaIngreso}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @PathVariable ("fechaIngreso") fechaIngreso: Long,
               @Valid @RequestBody newCapacitacion: CapacitacionModels,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): CapacitacionModels {
        val capacitacionOld = capacitacionRepository.findById(id)

        return capacitacionOld.map { exist ->
            val capacitacionUpdated: CapacitacionModels = exist.copy(calificacion = newCapacitacion.calificacion,
                    idCompetenciaCandidato = newCapacitacion.idCompetenciaCandidato,
                    idCalificacionCompetencia = newCapacitacion.idCalificacionCompetencia,
                    comentarios = newCapacitacion.comentarios, asistencia = newCapacitacion.asistencia,
                    fechaIngreso = Timestamp(fechaIngreso), idCandidato = newCapacitacion.idCandidato,
                    idCapacitador = newCapacitacion.idCapacitador)
            val idLog: Long = createLogs(idEmpleadoModificante,1,capacitacionOld.get().id).id
            callServerLogs(CapacitacionModelsLogs(0,capacitacionOld.get().idCandidato,
                    capacitacionOld.get().calificacion,capacitacionOld.get().idCompetenciaCandidato,
                    capacitacionOld.get().idCalificacionCompetencia,capacitacionOld.get().comentarios,
                    capacitacionOld.get().idCapacitador,capacitacionOld.get().asistencia,
                    capacitacionOld.get().fechaIngreso,capacitacionOld.get().fechaRegistro, idLog))
            capacitacionRepository.save(capacitacionUpdated)
        }.get()
    }

    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels = logsRepository.save(
            LogsModels(0, idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName ,Timestamp(System.currentTimeMillis())))


    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity("$serverLogs/capacitaciones", a, String::class.java)

}