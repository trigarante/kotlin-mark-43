package com.ahorraseguros.kotlinmark43.controllers.RH

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.RazonSocialModel
import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH.RazonSocialModelsLogs
import com.ahorraseguros.kotlinmark43.repositories.LogsRepository
import com.ahorraseguros.kotlinmark43.repositories.RH.RazonSocialRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/razon-social")
class RazonSocialController: LogsOperations {
    @Autowired
    lateinit var razonSocialRepository: RazonSocialRepository

    @Autowired
    lateinit var logsRepository: LogsRepository

    var restTemplate = RestTemplate()

    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    val tabla = "razonSocial"

    @GetMapping
    fun findAll(): MutableList<RazonSocialModel> = razonSocialRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Int): ResponseEntity<RazonSocialModel> = razonSocialRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @PostMapping()
    fun create(@Valid @RequestBody razonNueva: RazonSocialModel): ResponseEntity<RazonSocialModel> {
        try{
            val estado = razonSocialRepository.save(razonNueva)
            return ResponseEntity.ok(estado)
        }catch (e: Exception){
            return ResponseEntity.badRequest().build()
        }
    }

    @PutMapping("{id}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Int, @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long,
               @Valid @RequestBody razonNueva: RazonSocialModel): HttpStatus {
        return try{
            val razonSocial = razonSocialRepository.findById(id)
            
            if (razonSocial.isPresent) {
                val razon = razonSocial.get()
                val idLog: Long = createLogs(idEmpleadoModificante,1,id.toLong()).id

                callServerLogs(RazonSocialModelsLogs(0,razon.nombreComercial,razon.alias, razon.activo, idLog))
                razon.activo = razonNueva.activo
                razon.alias = razonNueva.alias
                razon.nombreComercial = razonNueva.nombreComercial

                razonSocialRepository.save(razon)
                HttpStatus.OK
            }else{
                HttpStatus.NOT_FOUND
            }

        }catch (e: Exception){
            HttpStatus.BAD_REQUEST
        }
    }

    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0,idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))

    override fun callServerLogs(a: Any): Any = restTemplate.postForEntity(serverLogs+"/razon-social", a, String::class.java)
}