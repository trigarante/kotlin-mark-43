package com.ahorraseguros.kotlinmark43.controllers.RH

import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import com.ahorraseguros.kotlinmark43.models.logs.RH.PrecandidatoModelsLogs
import com.ahorraseguros.kotlinmark43.models.rh.EmpleadoModels
import com.ahorraseguros.kotlinmark43.models.rh.PrecandidatoModels
import com.ahorraseguros.kotlinmark43.models.rh.views.EmpleadoBajaViewModel
import com.ahorraseguros.kotlinmark43.models.rh.views.EmpleadoViewModels
import com.ahorraseguros.kotlinmark43.models.rh.views.PreEmpleadosViewModels
import com.ahorraseguros.kotlinmark43.repositories.*
import com.ahorraseguros.kotlinmark43.repositories.RH.*
import com.ahorraseguros.kotlinmark43.repositories.RH.views.EmpleadosBajaViewRepository
import com.ahorraseguros.kotlinmark43.repositories.logs.LogsOperations
import com.ahorraseguros.kotlinmark43.repositories.views.EmpleadoViewRepository
import com.ahorraseguros.kotlinmark43.repositories.views.PreEmpleadoViewRepository
import com.ahorraseguros.mx.logskotlinmark43.models.EmpleadoModelsLogs
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import org.springframework.web.multipart.MultipartFile
import java.io.IOException
import java.sql.Timestamp
import javax.validation.Valid
import java.sql.Blob
import javax.sql.rowset.serial.SerialBlob

@CrossOrigin
@RestController
@RequestMapping("/v1/empleados")
class EmpleadosControllers : LogsOperations {


    @Autowired
    lateinit var empleadoRepository: EmpleadoRepository
    @Autowired
    lateinit var empleadoViewRepository: EmpleadoViewRepository
    @Autowired
    lateinit var empleadoBajaViewRepository: EmpleadosBajaViewRepository
    @Autowired
    lateinit var precandidatoRepository: PrecandidatoRepository
    @Autowired
    lateinit var huellaUsuarioRepository: HuellaUsuarioRepository
    @Autowired
    lateinit var fileStorage: FileStorage
    @Autowired
    lateinit var logsRepository: LogsRepository
    @Autowired
    lateinit var preEmpleadoViewRepository: PreEmpleadoViewRepository


    var restTemplate = RestTemplate()
    @Value("\${logs.servidor}")
    val serverLogs: String = ""

    var tabla = "empleado"

    @GetMapping
    fun findAll(): MutableList<EmpleadoViewModels> = empleadoViewRepository.findAll()

    @GetMapping("bajaEmpleado")
    fun finEmpleadoBajaAll(): MutableList<EmpleadoBajaViewModel> = empleadoBajaViewRepository.findAll()

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<EmpleadoViewModels> = empleadoViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @GetMapping("pre-empleado")
    fun finPreEempleadosALL(): MutableList<PreEmpleadosViewModels> = preEmpleadoViewRepository.findAll()

    @GetMapping("pre-empleado/{id}")
    fun finPreEempleadosId(@PathVariable("id") id: Long): ResponseEntity<PreEmpleadosViewModels> = preEmpleadoViewRepository.findById(id).map { s -> ResponseEntity.ok(s) }
            .orElse(ResponseEntity.notFound().build())

    @GetMapping("idSubarea/{idSubarea}/idUsuario/{idUsuario}")
    fun findByIdSubareaAndIdUsuario(@PathVariable("idSubarea") idSubarea: Int, @PathVariable("idUsuario") idUsuario: Long): EmpleadoViewModels = empleadoViewRepository.findAllByIdSubareaAndIdUsuario(idSubarea, idUsuario)

    @GetMapping("get-foto/{id}")
    fun getFoto(@PathVariable("id") id: Long): ByteArray? {
        return try {
            val empleado = empleadoRepository.findById(id)

            if (empleado.isPresent) {
                val imagen: Blob? = empleado.get().imagenEmpleado

                if (imagen != null) {
                    val imageSize: Int = imagen.length().toInt()
                    imagen.getBytes(1, imageSize)
                } else
                    null
            } else {
                null
            }
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    @PostMapping("{idPrecandidato}/{idEtapa}")
    fun create(@PathVariable("idPrecandidato") idPrecandidato: Long,
               @PathVariable("idEtapa") idEtapa: Int, @Valid @RequestBody empleadoModel: EmpleadoModels): HttpStatus {

        return try {

            val empleado: EmpleadoModels = empleadoModel
//            empleado.fechaIngreso = Timestamp(fechaIngreso)
            empleadoRepository.save(empleado)

            precandidatoRepository.findById(idPrecandidato).map { exist ->
                val precandidatoUpdated: PrecandidatoModels = exist.copy(idEtapa = idEtapa)
                precandidatoRepository.save(precandidatoUpdated)
            }.get()
            HttpStatus.OK
        } catch (e: Exception) {
            print(e.toString())
            HttpStatus.BAD_REQUEST
        }

    }

    @PutMapping("{id}/{fechaIngreso}/{idEmpleadoModificante}")
    fun update(@PathVariable("id") id: Long, @PathVariable("fechaIngreso") fechaIngreso: Long,
               @Valid @RequestBody updateEmpleado: EmpleadoModels,
               @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): Boolean {
        try {
            empleadoRepository.findById(id).map { exist ->
                val idLog = createLogs(idEmpleadoModificante, 1, id).id
                callServerLogs(EmpleadoModelsLogs(0, idBanco = exist.idBanco, idCandidato = exist.idCandidato,
                        idPuesto = exist.idPuesto, idTipoPuesto = exist.idTipoPuesto, idUsuario = exist.idUsuario,
                        puestoDetalle = exist.puestoDetalle, fechaAltaImss = exist.fechaAltaImss,
                        documentosPersonales = exist.documentosPersonales, documentosAdministrativos = exist.documentosAdministrativos,
                        fechaIngreso = exist.fechaIngreso, sueldoDiario = exist.sueldoDiario, sueldoMensual = exist.sueldoMensual,
                        kpi = exist.kpi, kpiMensual = exist.kpiMensual, kpiTrimestral = exist.kpiTrimestral,
                        kpiSemestral = exist.kpiSemestral, fechaCambioSueldo = exist.fechaCambioSueldo, ctaClabe = exist.ctaClabe,
                        fechaAsignacion = exist.fechaAsignacion, comentarios = exist.comentarios, imss = exist.imss,
                        razonSocial = exist.razonSocial, rfc = exist.rfc, fechaFiniquito = exist.fechaFiniquito,
                        idEstadoFiquito = exist.idEstadoFiquito, montoFiniquito = exist.montoFiniquito, idTurnoEmpleado = exist.idTurnoEmpleado,
                        tarjetaSecundaria = exist.tarjetaSecundaria, idLog = idLog))

                val empleadoUpdate: EmpleadoModels = exist.copy(idBanco = updateEmpleado.idBanco, idPuesto = updateEmpleado.idPuesto,
                        idTipoPuesto = updateEmpleado.idTipoPuesto, puestoDetalle = updateEmpleado.puestoDetalle,
                        idSubarea = updateEmpleado.idSubarea,
                        fechaIngreso = Timestamp(fechaIngreso), ctaClabe = updateEmpleado.ctaClabe,
                        idUsuario = updateEmpleado.idUsuario, sueldoDiario = updateEmpleado.sueldoDiario,
                        sueldoMensual = updateEmpleado.sueldoMensual, kpiMensual = updateEmpleado.kpiMensual,
                        kpiSemestral = updateEmpleado.kpiSemestral, kpiTrimestral = updateEmpleado.kpiTrimestral,
                        rfc = updateEmpleado.rfc, imss = updateEmpleado.imss, idTurnoEmpleado = updateEmpleado.idTurnoEmpleado,
                        tarjetaSecundaria = updateEmpleado.tarjetaSecundaria, kpi = updateEmpleado.kpi, idSubarea = updateEmpleado.idSubarea
                )
                empleadoRepository.save(empleadoUpdate)
                if(exist.id != updateEmpleado.id){
                    empleadoRepository.updateEmpleado(exist.id , updateEmpleado.id);
                }
            }

            return true
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }
    }


//    @PostMapping("guardar-huella/{id}")
//    fun savePhoto(@PathVariable("id") id: Long,
//                  @Valid @RequestBody huellaUsuarioModel: HuellaUsuarioModel): EmpleadoModels {
//
//        huellaUsuarioRepository.save(huellaUsuarioModel)
//        return empleadoRepository.findById(id).map { exist ->
//            val empleadoUpdated: EmpleadoModels = exist.copy(idHuella = huellaUsuarioRepository.save(huellaUsuarioModel).id)
//            empleadoRepository.save(empleadoUpdated)
//        }.get()
//
//    }

    @PutMapping("baja-empleado/{idPrecandidato}/{idEmpleado}/{fechaBaja}/{idEmpleadoModificante}")
    fun bajaEmpleado(@PathVariable("idPrecandidato") idPrecandidato: Long, @PathVariable("idEmpleado") idEmpleado: Long,
                     @PathVariable("fechaBaja") fechaBaja: Long, @Valid @RequestBody precandidatoModels: PrecandidatoModels,
                     @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): HttpStatus {
        val precandidatoOld = precandidatoRepository.findById(idPrecandidato)
        val empleado = empleadoRepository.findById(idEmpleado)

        return try {
            var idLog: Long
            precandidatoOld.map { exist ->
                tabla = "precandidato"
                idLog = createLogs(idEmpleadoModificante, 1, idPrecandidato).id
                callServerLogs(PrecandidatoModelsLogs(0, precandidatoOld.get().idEtapa, precandidatoOld.get().idEscolaridad,
                        precandidatoOld.get().idEstacion, precandidatoOld.get().idEstadoRH, precandidatoOld.get().idEstadoCivil,
                        precandidatoOld.get().idEstadoEscolaridad, precandidatoOld.get().idMedioTraslado, precandidatoOld.get().idPais,
                        precandidatoOld.get().idSolicitudRRHH, precandidatoOld.get().nombre, precandidatoOld.get().apellidoPaterno,
                        precandidatoOld.get().apellidoMaterno, precandidatoOld.get().fechaNacimiento, precandidatoOld.get().email,
                        precandidatoOld.get().curp, precandidatoOld.get().genero, precandidatoOld.get().cp, precandidatoOld.get().idColonia,
                        precandidatoOld.get().calle, precandidatoOld.get().numeroExterior, precandidatoOld.get().numeroInterior,
                        precandidatoOld.get().telefonoFijo, precandidatoOld.get().telefonoMovil, precandidatoOld.get().fechaCreacion,
                        precandidatoOld.get().fechaBaja, precandidatoOld.get().tiempoTraslado, precandidatoOld.get().recontratable,
                        precandidatoOld.get().detalleBaja, precandidatoOld.get().fechaRegistroBaja, idLog))
                val precandidatoUpdated: PrecandidatoModels = exist.copy(fechaRegistroBaja = Timestamp(System.currentTimeMillis()),
                        idEstadoRH = precandidatoModels.idEstadoRH, detalleBaja = precandidatoModels.detalleBaja,
                        fechaBaja = Timestamp(fechaBaja), recontratable = precandidatoModels.recontratable)
                precandidatoRepository.save(precandidatoUpdated)
            }
            empleado.map { exist ->
                tabla = "empleado"
                idLog = createLogs(idEmpleadoModificante, 1, idEmpleado).id
                callServerLogs(EmpleadoModelsLogs(0, idBanco = exist.idBanco, idCandidato = exist.idCandidato,
                        idPuesto = exist.idPuesto, idTipoPuesto = exist.idTipoPuesto, idUsuario = exist.idUsuario,
                        puestoDetalle = exist.puestoDetalle, fechaAltaImss = exist.fechaAltaImss,
                        documentosPersonales = exist.documentosPersonales, documentosAdministrativos = exist.documentosAdministrativos,
                        fechaIngreso = exist.fechaIngreso, sueldoDiario = exist.sueldoDiario, sueldoMensual = exist.sueldoMensual,
                        kpi = exist.kpi, kpiMensual = exist.kpiMensual, kpiTrimestral = exist.kpiTrimestral,
                        kpiSemestral = exist.kpiSemestral, fechaCambioSueldo = exist.fechaCambioSueldo, ctaClabe = exist.ctaClabe,
                        fechaAsignacion = exist.fechaAsignacion, comentarios = exist.comentarios, imss = exist.imss,
                        razonSocial = exist.razonSocial, rfc = exist.rfc, fechaFiniquito = exist.fechaFiniquito,
                        idEstadoFiquito = exist.idEstadoFiquito, idTurnoEmpleado = exist.idTurnoEmpleado, montoFiniquito = exist.montoFiniquito,
                        tarjetaSecundaria = exist.tarjetaSecundaria, idLog = idLog))
                val empleadoUpdated: EmpleadoModels = exist.copy(idUsuario = null)
                empleadoRepository.save(empleadoUpdated)
            }
            HttpStatus.OK
        } catch (e: Exception) {
            HttpStatus.INTERNAL_SERVER_ERROR
        }
    }

    @PutMapping("alta-imss/{id}/{fechaIngreso}/{fechaAltaImss}/{idEmpleadoModificante}")
    fun altaImss(@PathVariable("id") id: Long, @PathVariable("fechaIngreso") fechaIngreso: Long,
                 @PathVariable("fechaAltaImss") fechaAltaImss: Long, @Valid @RequestBody empleadoModel: EmpleadoModels,
                 @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long) {
        val empleado = empleadoRepository.findById(id)

        empleado.map { exist ->
            val idLog = createLogs(idEmpleadoModificante, 1, id).id
            callServerLogs(EmpleadoModelsLogs(0, idBanco = exist.idBanco, idCandidato = exist.idCandidato,
                    idPuesto = exist.idPuesto, idTipoPuesto = exist.idTipoPuesto, idUsuario = exist.idUsuario,
                    puestoDetalle = exist.puestoDetalle, fechaAltaImss = exist.fechaAltaImss,
                    documentosPersonales = exist.documentosPersonales, documentosAdministrativos = exist.documentosAdministrativos,
                    fechaIngreso = exist.fechaIngreso, sueldoDiario = exist.sueldoDiario, sueldoMensual = exist.sueldoMensual,
                    kpi = exist.kpi, kpiMensual = exist.kpiMensual, kpiTrimestral = exist.kpiTrimestral,
                    kpiSemestral = exist.kpiSemestral, fechaCambioSueldo = exist.fechaCambioSueldo, ctaClabe = exist.ctaClabe,
                    fechaAsignacion = exist.fechaAsignacion, comentarios = exist.comentarios, imss = exist.imss,
                    razonSocial = exist.razonSocial, rfc = exist.rfc, fechaFiniquito = exist.fechaFiniquito,
                    idEstadoFiquito = exist.idEstadoFiquito, montoFiniquito = exist.montoFiniquito, idLog = idLog))
            val empleadoUpdated: EmpleadoModels = exist.copy(fechaAltaImss = Timestamp(fechaAltaImss),
                    comentarios = empleadoModel.comentarios, imss = empleadoModel.imss, razonSocial = empleadoModel.razonSocial,
                    tarjetaSecundaria = empleadoModel.tarjetaSecundaria,
                    idTurnoEmpleado = exist.idTurnoEmpleado, fechaIngreso = Timestamp(fechaIngreso))
            empleadoRepository.save(empleadoUpdated)
        }
    }

    @PutMapping("documento/{id}/{documentId}/{option}/{idEmpleadoModificante}")
    fun createFolderDrive(@PathVariable("id") id: Long, @PathVariable("documentId") documentId: String,
                          @PathVariable("option") option: Int, @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long) {
        val empleado = empleadoRepository.findById(id)

        empleado.map { exist ->
            val idLog = createLogs(idEmpleadoModificante, 1, id).id
            callServerLogs(EmpleadoModelsLogs(0, idBanco = exist.idBanco, idCandidato = exist.idCandidato,
                    idPuesto = exist.idPuesto, idTipoPuesto = exist.idTipoPuesto, idUsuario = exist.idUsuario,
                    puestoDetalle = exist.puestoDetalle, fechaAltaImss = exist.fechaAltaImss,
                    documentosPersonales = exist.documentosPersonales, documentosAdministrativos = exist.documentosAdministrativos,
                    fechaIngreso = exist.fechaIngreso, sueldoDiario = exist.sueldoDiario, sueldoMensual = exist.sueldoMensual,
                    kpi = exist.kpi, kpiMensual = exist.kpiMensual, kpiTrimestral = exist.kpiTrimestral,
                    kpiSemestral = exist.kpiSemestral, fechaCambioSueldo = exist.fechaCambioSueldo, ctaClabe = exist.ctaClabe,
                    fechaAsignacion = exist.fechaAsignacion, comentarios = exist.comentarios, imss = exist.imss,
                    razonSocial = exist.razonSocial, rfc = exist.rfc, fechaFiniquito = exist.fechaFiniquito, tarjetaSecundaria = exist.tarjetaSecundaria,
                    idEstadoFiquito = exist.idEstadoFiquito, idTurnoEmpleado = exist.idTurnoEmpleado, montoFiniquito = exist.montoFiniquito, idLog = idLog))
            if (option == 1) {
                val empleadoUpdated: EmpleadoModels = exist.copy(documentosAdministrativos = documentId)
                empleadoRepository.save(empleadoUpdated)
            } else {
                val empleadoUpdated: EmpleadoModels = exist.copy(documentosPersonales = documentId)
                empleadoRepository.save(empleadoUpdated)
            }
        }
    }

    @Throws(IOException::class)
    @PutMapping("actualizar-foto/{id}")
    fun actualizarFoto(@PathVariable("id") id: Long, @Valid @RequestPart foto: MultipartFile): HttpStatus {
        return try {
            val p: Blob = SerialBlob(foto.bytes)
            val empleadoViejo = empleadoRepository.findById(id)

            if (empleadoViejo.isPresent) {
                val update: EmpleadoModels = empleadoViejo.get()

                update.imagenEmpleado = p

                empleadoRepository.save(update)

                HttpStatus.OK
            } else {
                HttpStatus.BAD_REQUEST
            }
        } catch (e: Exception) {
            e.printStackTrace()
            HttpStatus.INTERNAL_SERVER_ERROR
        }
    }

    @GetMapping("get-by-id-subarea/{id}")
    fun findByidSubarea(@PathVariable("id") id: Long): MutableList<EmpleadoViewModels> =
            empleadoViewRepository.findAllByIdSubarea(id)

    @PutMapping("reactivar/{id}/{idPrecandidato}/{fecha}/{idEmpleadoModificante}")
    fun updateEstadoRH(@PathVariable("id") id: Long, @PathVariable("idPrecandidato") idPrecandidato: Long,
                       @PathVariable("fecha") fecha: Long, @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): HttpStatus {
        val empleado = empleadoRepository.findById(id).get()
        val precandidato = precandidatoRepository.findById(idPrecandidato).get()

        var idLog: Long
        tabla = "precandidato"
        idLog = createLogs(idEmpleadoModificante,1,idPrecandidato).id
        callServerLogs(PrecandidatoModelsLogs(0,precandidato.idEtapa,precandidato.idEscolaridad, precandidato.idEstacion,
                precandidato.idEstadoRH,precandidato.idEstadoCivil, precandidato.idEstadoEscolaridad,
                precandidato.idMedioTraslado,precandidato.idPais, precandidato.idSolicitudRRHH, precandidato.nombre,
                precandidato.apellidoPaterno, precandidato.apellidoMaterno, precandidato.fechaNacimiento,precandidato.email,
                precandidato.curp, precandidato.genero, precandidato.cp, precandidato.idColonia, precandidato.calle,
                precandidato.numeroExterior, precandidato.numeroInterior, precandidato.telefonoFijo,
                precandidato.telefonoMovil, precandidato.fechaCreacion, precandidato.fechaBaja, precandidato.tiempoTraslado,
                precandidato.recontratable, precandidato.detalleBaja,precandidato.fechaRegistroBaja,idLog))
        tabla = "empleado"
        idLog = createLogs(idEmpleadoModificante,1,id).id
        callServerLogs(EmpleadoModelsLogs(0, idBanco = empleado.idBanco, idCandidato = empleado.idCandidato,
                idPuesto = empleado.idPuesto,idTipoPuesto = empleado.idTipoPuesto, idUsuario = empleado.idUsuario,
                puestoDetalle = empleado.puestoDetalle,fechaAltaImss = empleado.fechaAltaImss,
                documentosPersonales = empleado.documentosPersonales, documentosAdministrativos = empleado.documentosAdministrativos,
                fechaIngreso = empleado.fechaIngreso,sueldoDiario = empleado.sueldoDiario,sueldoMensual = empleado.sueldoMensual,
                kpi = empleado.kpi,kpiMensual = empleado.kpiMensual,kpiTrimestral = empleado.kpiTrimestral,
                kpiSemestral = empleado.kpiSemestral,fechaCambioSueldo = empleado.fechaCambioSueldo, ctaClabe = empleado.ctaClabe,
                fechaAsignacion = empleado.fechaAsignacion, comentarios = empleado.comentarios, imss = empleado.imss,
                razonSocial = empleado.razonSocial,rfc = empleado.rfc, fechaFiniquito = empleado.fechaFiniquito,
                idEstadoFiquito = empleado.idEstadoFiquito,idTurnoEmpleado = empleado.idTurnoEmpleado,montoFiniquito = empleado.montoFiniquito, idLog = idLog))
        empleado.fechaIngreso = Timestamp(fecha)
            precandidato.idEstadoRH = 16
            empleadoRepository.save(empleado)
            precandidatoRepository.save(precandidato)

        return HttpStatus.OK
    }

    @PutMapping("finiquito/{idEmpleado}/{fecha}/{idEstadoFiniquito}/{montoFiniquito}/{idEmpleadoModificante}")
    fun finiquito(@PathVariable("idEmpleado") id: Long, @PathVariable("fecha") fecha: Long,
                  @PathVariable("idEstadoFiniquito") idEstadoFiniquito: Int,
                  @PathVariable("montoFiniquito") montoFiniquito: Float,
                  @PathVariable("idEmpleadoModificante") idEmpleadoModificante: Long): ResponseEntity<String?> {
        return try {
            val empleado = empleadoRepository.findById(id)

            empleado.map { exist ->
                val idLog = createLogs(idEmpleadoModificante, 1, id).id
                callServerLogs(EmpleadoModelsLogs(0, idBanco = exist.idBanco, idCandidato = exist.idCandidato,
                        idPuesto = exist.idPuesto, idTipoPuesto = exist.idTipoPuesto, idUsuario = exist.idUsuario,
                        puestoDetalle = exist.puestoDetalle, fechaAltaImss = exist.fechaAltaImss,
                        documentosPersonales = exist.documentosPersonales, documentosAdministrativos = exist.documentosAdministrativos,
                        fechaIngreso = exist.fechaIngreso, sueldoDiario = exist.sueldoDiario, sueldoMensual = exist.sueldoMensual,
                        kpi = exist.kpi, kpiMensual = exist.kpiMensual, kpiTrimestral = exist.kpiTrimestral,
                        kpiSemestral = exist.kpiSemestral, fechaCambioSueldo = exist.fechaCambioSueldo, ctaClabe = exist.ctaClabe,
                        fechaAsignacion = exist.fechaAsignacion, comentarios = exist.comentarios, imss = exist.imss,
                        razonSocial = exist.razonSocial, rfc = exist.rfc, fechaFiniquito = exist.fechaFiniquito, tarjetaSecundaria = exist.tarjetaSecundaria,
                        idEstadoFiquito = exist.idEstadoFiquito, idTurnoEmpleado = exist.idTurnoEmpleado, montoFiniquito = exist.montoFiniquito, idLog = idLog))
                val empleadoUpdate: EmpleadoModels = exist.copy(montoFiniquito = montoFiniquito,
                        idEstadoFiquito = idEstadoFiniquito, fechaFiniquito = Timestamp(fecha))
                empleadoRepository.save(empleadoUpdate)
            }
            ResponseEntity(HttpStatus.OK)
        } catch (e: Exception) {
            ResponseEntity(e.toString(), HttpStatus.BAD_REQUEST)
        }
    }

    override fun createLogs(idEmpleado: Long, idTipoLog: Int, idTabla: Long): LogsModels =
            logsRepository.save(LogsModels(0, idEmpleado, idTipoLog, tabla, idTabla, javaClass.simpleName,
                    Timestamp(System.currentTimeMillis())))


    override fun callServerLogs(a: Any): Any {
        var URL = "$serverLogs/"

        if (tabla == "empleado") {
            URL = URL + "empleados"
        } else {
            URL = URL + "precandidato"
        }

        return restTemplate.postForEntity(URL, a, String::class.java)
    }

}