package com.ahorraseguros.kotlinmark43.controllers.RH

import com.ahorraseguros.kotlinmark43.models.rh.DatosBiometricosModels
import com.ahorraseguros.kotlinmark43.repositories.DatosBiometricosRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid


@CrossOrigin
@RestController
@RequestMapping("/v1/datos-biometricos")
class DatosBiometricosController {

    @Autowired
    lateinit var datosBiometricosRepository: DatosBiometricosRepository

    @GetMapping
    fun findAll():MutableList<DatosBiometricosModels> = datosBiometricosRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody datosBiometricosModels: DatosBiometricosModels): DatosBiometricosModels =
            datosBiometricosRepository.save(datosBiometricosModels)
}