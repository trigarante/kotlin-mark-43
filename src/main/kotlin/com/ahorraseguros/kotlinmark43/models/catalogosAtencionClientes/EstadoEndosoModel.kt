package com.ahorraseguros.kotlinmark43.models.catalogosAtencionClientes

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "estadoEndoso")
class EstadoEndosoModel {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
}
