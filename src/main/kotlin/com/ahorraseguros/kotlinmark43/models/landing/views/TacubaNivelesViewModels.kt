package com.ahorraseguros.kotlinmark43.models.landing.views

import javax.persistence.*

@Entity
@Table(name = "tacubaNiveles")
class TacubaNivelesViewModels {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "nombreArea")
    var nombreArea: String? = null
    @get:Basic
    @get:Column(name = "idPiso")
    var idPiso: Int = 0
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
}
