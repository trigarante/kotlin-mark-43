package com.ahorraseguros.kotlinmark43.models;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "usuarios")

public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String nombre;

    @Email
    @Column(nullable = false)
    private String usuario;

    private String imagenUrl;

    @Column(nullable = false)
    private int idGrupo;

    @Column
    private String password;

    @NotNull
    private int idTipo;

    private String idCorreo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return nombre;
    }

    public void setName(String name) {
        this.nombre = name;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String email) {
        this.usuario = email;
    }

    public String getImagenUrl() {
        return imagenUrl;
    }

    public void setImagenUrl(String imageUrl) {
        this.imagenUrl = imageUrl;
    }

    public int getidGrupo() {
        return idGrupo;
    }


    public void setidGrupo(int emailVerified) {
        this.idGrupo = emailVerified;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getProvider() {
        return idTipo;
    }

    public void setTipo(int provider) {
        this.idTipo = provider;
    }

    public String getidCorreo() {
        return idCorreo;
    }

    public void setidCorreo(String providerId) {
        this.idCorreo = providerId;
    }
}
