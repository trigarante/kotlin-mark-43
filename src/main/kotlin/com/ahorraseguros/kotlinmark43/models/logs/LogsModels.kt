package com.ahorraseguros.kotlinmark43.models.logs

import javax.persistence.*
import java.sql.Timestamp
import java.util.Objects

@Entity
@Table(name = "logs")
data class LogsModels (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @get:Column(name = "id")
    var id: Long = 0,
    @get:Basic
    @get:Column(name = "idEmpleado")
    var idEmpleado: Long = 0,
    @get:Basic
    @get:Column(name = "idTipoLog")
    var idTipoLog: Int = 0,
    @get:Basic
    @get:Column(name = "tabla")
    var tabla: String? = null,
    @get:Basic
    @get:Column(name = "idTablaID")
    var idTablaId: Long = 0,
    @get:Basic
    @get:Column(name = "origenModificacion")
    var origenModificacion: String = "",
    @get:Basic
    @get:Column(name = "fechaLog")
    var fechaLog: Timestamp? = null

)
