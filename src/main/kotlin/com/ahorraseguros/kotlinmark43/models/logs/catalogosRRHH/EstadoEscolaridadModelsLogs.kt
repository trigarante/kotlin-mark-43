package com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH

data class EstadoEscolaridadModelsLogs (
        var id: Int = 0,
        var estado: String? = null,
        var activo: Int = 0,
        var idLog: Long = 0
)