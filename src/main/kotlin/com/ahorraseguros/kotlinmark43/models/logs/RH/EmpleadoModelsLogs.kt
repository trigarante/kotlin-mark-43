package com.ahorraseguros.mx.logskotlinmark43.models

import java.sql.Blob
import javax.persistence.*
import java.sql.Date
import java.sql.Timestamp
import java.util.Objects

data class EmpleadoModelsLogs (
    var id: Long = 0,
    var idBanco: Int? = null,
    var idCandidato: Long = 0,
    var idPuesto: Int = 0,
    var idTipoPuesto: Int = 0,
    var idUsuario: Long? = 0,
    var puestoDetalle: String? = null,
    var fechaAltaImss: Timestamp? = null,
    var documentosPersonales: String? = null,
    var documentosAdministrativos: String? = null,
    var fechaIngreso: Timestamp? = null,
    var sueldoDiario: Double? = null,
    var sueldoMensual: Double? = null,
    var kpi: Boolean? = null,
    var kpiMensual: Float? = null,
    var kpiTrimestral: Float? = null,
    var kpiSemestral: Float? = null,
    var fechaCambioSueldo: Date? = null,
    var ctaClabe: String? = null,
    var fechaAsignacion: Timestamp? = null,
    var comentarios: String? = null,
    var imss: String? = null,
    var imagenEmpleado: Blob? = null,
    var razonSocial: String? = null,
    var rfc: String? = null,
    var fechaFiniquito: Timestamp? = null,
    var idEstadoFiquito: Int? = 0,
    var montoFiniquito: Float? = null,
    var idTurnoEmpleado: Int? = 0,
    var tarjetaSecundaria: String? = null,
    var idLog: Long = 0
)

