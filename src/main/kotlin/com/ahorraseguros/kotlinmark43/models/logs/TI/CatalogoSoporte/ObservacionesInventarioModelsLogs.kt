package com.ahorraseguros.kotlinmark43.models.logs.TI.CatalogoSoporte



data class ObservacionesInventarioModelsLogs (

    var id: Long = 0,
    var observaciones: String = "",
    var activo: Int = 0,
    var idLogs: Long = 0
)