package com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte

import java.sql.Timestamp


data class MouseModelsLogs (
    var id: Long = 0,
    var idEmpleado: Long? = 0,
    var idMarca: Int = 0,
    var idEstadoInventario: Int = 0,
    var fechaRecepcion: Timestamp = Timestamp(System.currentTimeMillis()),
    var fechaSalida: Timestamp? = null,
    var idObservacionesInventario: Int = 0,
    var comentarios: String? = "",
    var activo: Int = 0,
    var idSede: Int = 0,
    var origenRecurso: String? = null,
    var idLogs: Long = 0
)