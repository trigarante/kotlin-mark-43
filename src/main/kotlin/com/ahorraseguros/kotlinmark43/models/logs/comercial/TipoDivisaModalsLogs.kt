package com.ahorraseguros.kotlinmark43.models.logs.comercial



data class TipoDivisaModalsLogs (

    var id: Long = 0,
    var divisa: String? = null,
    var descripcion: String? = null,
    var activo: Byte = 0,
    var idLog: Long = 0
)