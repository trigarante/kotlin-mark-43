package com.ahorraseguros.kotlinmark43.models.logs.comercial

data class ProductosSocioModelsLogs (

    var id: Long = 0,
    var idTipoProducto: Int = 0,
    var idSubRamo: Int = 0,
    var prioridad: Int = 0,
    var nombre: String? = null,
    var activo: Int = 0,
    var idLog: Long = 0
)