package com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte

import java.sql.Timestamp


data class ApsModelsLogs (

    var id: Int = 0,
    var idEmpleado: Long? = null,
    var idObservacionesInventario: Int = 0,
    var idEstadoInventario: Int = 0,
    var Folio: String? = null,
    var idMarca: Int = 0,
    var idSede: Int = 0,
    var origenRecurso: String? = null,
    var activo: Byte = 0,
    var fechaRecepcion: Timestamp = Timestamp(System.currentTimeMillis()),
    var fechaSalida: Timestamp? = null,
    var idLogs: Long = 0
)