package com.ahorraseguros.kotlinmark43.models.logs.comercial



data class SubRamoModelsLogs (

    var id: Long = 0,
    var prioridad: Int = 0,
    var idRamo: Int = 0,
    var idTipoSubRamo: Int = 0,
    var descripcion: String? = null,
    var activo: Int = 0,
    var idLog: Long = 0
)