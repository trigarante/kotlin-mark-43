package com.ahorraseguros.kotlinmark43.models.logs.catalogosComercial



data class TipoCompetenciaModelsLogs (

    var id: Long = 0,
    var tipo: String? = null,
    var activo: Byte = 0,
    var idLog: Long = 0
)