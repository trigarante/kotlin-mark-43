package com.ahorraseguros.mx.logskotlinmark43.models

import java.sql.Timestamp

data class SeguimientoModelsLogs (
    var id: Int = 0,
    var idSubarea: Long? = null,
    var idCapacitacion: Int? = 0,
    var idCoach: Long = 0,
    var fechaRegistro: Timestamp? = null,
    var fechaIngreso: Timestamp? = null,
    var comentarios: String? = null,
    var asistencia: String? = null,
    var registro: String? = null,
    var comentariosFaseDos: String? = null,
    var calificacionRollPlay: Boolean? = false,
    var idLog: Long = 0
)
