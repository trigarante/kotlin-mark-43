package com.ahorraseguros.kotlinmark43.models.logs.TI.CatalogoSoporte



data class MarcaSoporteModelsLogs (

    var id: Int = 0,
    var nombreMarca: String? = null,
    var activo: Byte = 0,
    var idLogs: Long = 0
)