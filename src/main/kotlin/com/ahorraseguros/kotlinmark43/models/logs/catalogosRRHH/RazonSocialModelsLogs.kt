package com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH

data class RazonSocialModelsLogs (
        var id: Int = 0,
        var nombreComercial: String? = null,
        var alias: String? = null,
        var activo: Byte? = 0,
        var idLog: Long = 0
)