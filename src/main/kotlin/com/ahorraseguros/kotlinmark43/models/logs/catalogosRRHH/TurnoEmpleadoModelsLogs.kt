package com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH

data class TurnoEmpleadoModelsLogs (

    var id: Int = 0,
    var turno: String? = null,
    var horario: String? = null,
    var activo: Byte = 0,
    var idLog: Long = 0
)