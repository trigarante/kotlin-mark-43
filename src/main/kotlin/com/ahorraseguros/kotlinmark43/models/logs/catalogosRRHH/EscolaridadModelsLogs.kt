package com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH

data class EscolaridadModelsLogs (
        var id: Int = 0,
        var nivel: String? = null,
        var activo: Int? = 0,
        var idLog: Long = 0
)