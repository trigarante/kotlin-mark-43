package com.ahorraseguros.kotlinmark43.models.logs.catalogosComercial

data class TipoCoberturaModelsLogs (

    var id: Long = 0,
    var cobertura: String? = null,
    var activo: Int = 0,
    var idLog: Long = 0
)