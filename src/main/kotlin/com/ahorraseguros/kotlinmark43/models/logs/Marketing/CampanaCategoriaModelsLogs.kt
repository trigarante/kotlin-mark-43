package com.ahorraseguros.kotlinmark43.models.logs.Marketing

data class CampanaCategoriaModelsLogs (

    var id: Int = 0,
    var idCampana: Int = 0,
    var idCategoria: Int = 0,
    var activo: Byte = 0,
    var comentarios: String? = null,
    var idLog: Long = 0
)