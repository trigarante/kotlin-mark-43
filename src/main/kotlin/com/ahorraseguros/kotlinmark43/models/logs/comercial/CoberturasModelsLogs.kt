package com.ahorraseguros.kotlinmark43.models.logs.comercial



data class CoberturasModelsLogs (

    var id: Long = 0,
    var idProducto: Int = 0,
    var idTipoCobertura: Int = 0,
    var detalle: String? = null,
    var activo: Int = 0,
    var idLog: Long = 0
)