package com.ahorraseguros.kotlinmark43.models.logs.TI

import java.sql.Timestamp

data class UsuarioModelsLogs (
    var id: Long = 0,
    var idCorreo: String? = null,
    var idGrupo: Int = 0,
    var idTipo: Int = 0,
    var idSubarea: Int = 0,
    var usuario: String? = null,
    var password: String? = null,
    var fechaCreacion: Timestamp? = null,
    var datosUsuario: String? = null,
    var estado: Byte = 0,
    var nombre: String? = null,
    var idLog: Long? = null
)