package com.ahorraseguros.kotlinmark43.models.logs.Marketing

data class CampanaModelsLogs(

        var id: Long = 0,
        var idSubRamo: Int = 0,
        var nombre: String? = null,
        var descripcion: String? = null,
        var activo: Int = 0,
        var idEstadoCampana: Int =0,
        var idLog: Long = 0
)