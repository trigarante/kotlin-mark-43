package com.ahorraseguros.kotlinmark43.models.logs.comercial



data class EjecutivoCuentaModelsLogs (

    var id: Long = 0,
    var idSocio: Int = 0,
    var nombre: String? = null,
    var apellidoPaterno: String? = null,
    var apellidoMaterno: String? = null,
    var telefono: String = "",
    var ext: String? = null,
    var celular: String? = null,
    var activo: Int? = null,
    var favorito: Int? = null,
    var email: String? = null,
    var puesto: String? = null,
    var direccion: String? = null,
    var idLog: Long = 0
)