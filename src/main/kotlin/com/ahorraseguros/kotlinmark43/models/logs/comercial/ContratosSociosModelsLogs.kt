package com.ahorraseguros.kotlinmark43.models.logs.comercial

import java.sql.Timestamp


data class ContratosSociosModelsLogs (

    var id: Long = 0,
    var idSocio: Int = 0,
    var fechaInicio: Timestamp? = null,
    var fechaFin: Timestamp? = null,
    var referencia: String? = null,
    var activo: Int = 0,
    var idLog: Long = 0
)