package com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH

data class AreaModelsLogs (
        var id: Int = 0,
        var idSede: Int = 0,
        var nombre: String? = null,
        var descripcion: String? = null,
        var activo: Int = 0,
        var codigo: String? = null,
        var idLog: Long = 0
)