package com.ahorraseguros.kotlinmark43.models.logs.comercial



data class UrlSociosModelsLogs (

    var id: Long = 0,
    var idSocio: Int = 0,
    var url: String? = null,
    var descripcion: String? = null,
    var activo: Int = 0,
    var idLogs: Long = 0
)