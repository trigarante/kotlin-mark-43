package com.ahorraseguros.kotlinmark43.models.logs.Marketing

data class MedioDifusionModelsLogs (

    var id: Int = 0,
    var idSubarea: Int = 0,
    var idProveedor: Int = 0,
    var idExterno: Int = 0,
    var idPagina: Int = 0,
    var idPresupuesto: Int = 0,
    var descripcion: String? = null,
    var configuracion: String? = null,
    var activo: Int = 0,
    var idLog: Long = 0
)