package com.ahorraseguros.kotlinmark43.models.logs.comercial

data class AnalisisCompetenciaModelsLogs (

    var id: Long = 0,
    var idCompetencia: Long = 0,
    var precios: Double = 0.toDouble(),
    var participacion: Double = 0.toDouble(),
    var activo: Int = 0,
    var idLog: Long = 0
)