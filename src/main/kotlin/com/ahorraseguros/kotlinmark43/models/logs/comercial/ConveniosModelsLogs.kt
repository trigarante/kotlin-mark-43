package com.ahorraseguros.kotlinmark43.models.logs.comercial

data class ConveniosModelsLogs (

    var id: Long = 0,
    var idSubRamo: Int = 0,
    var idSubCategoria: Int = 0,
    var idTipoDivisa: Int = 0,
    var cantidad: Double = 0.toDouble(),
    var activo: Int = 0,
    var idLog: Long = 0
)