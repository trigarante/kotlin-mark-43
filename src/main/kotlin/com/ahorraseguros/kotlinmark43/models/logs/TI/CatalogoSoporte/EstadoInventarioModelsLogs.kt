package com.ahorraseguros.kotlinmark43.models.logs.TI.CatalogoSoporte



data class EstadoInventarioModelsLogs (

    var id: Int = 0,
    var estado: String? = null,
    var idEstado: Int = 0,
    var activo: Int = 0,
    var idLogs: Long = 0
)