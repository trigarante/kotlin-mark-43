package com.ahorraseguros.kotlinmark43.models.logs.comercial

data class PresupuestoSocioModelsLogs (

    var id: Long = 0,
    var idSocio: Int = 0,
    var presupuesto: String = "",
    var activo: Int = 0,
    var anio: String? = null,
    var idLog: Long = 0
)