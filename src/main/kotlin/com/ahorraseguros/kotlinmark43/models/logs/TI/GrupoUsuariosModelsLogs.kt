package com.ahorraseguros.kotlinmark43.models.logs.TI

data class GrupoUsuariosModelsLogs (
    var id: Int = 0,
    var idSubarea: Int = 0,
    var idPuesto: Int = 0,
    var idPermisosVisualizacion: Int = 0,
    var nombre: String? = null,
    var descripcion: String? = null,
    var permisos: String? = null,
    var activo: Int = 0,
    var idLog: Long? = null
)