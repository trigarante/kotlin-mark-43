package com.ahorraseguros.kotlinmark43.models.logs.TI.CatalogoSoporte



data class ProcesadoresSoporteModelsLogs (

    var id: Int = 0,
    var nombreProcesador: String? = null,
    var activo: Byte = 0,
    var idLogs: Long = 0
)