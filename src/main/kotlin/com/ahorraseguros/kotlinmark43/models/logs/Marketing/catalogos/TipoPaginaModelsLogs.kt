package com.ahorraseguros.kotlinmark43.models.logs.Marketing.catalogos

data class TipoPaginaModelsLogs (

    var id: Int = 0,
    var tipo: String? = null,
    var objetivo: String? = null,
    var activo: Int = 0,
    var idLog: Long = 0
)