package com.ahorraseguros.kotlinmark43.models.logs.comercial

data class CompetenciaSocioModelsLogs (

    var id: Long = 0,
    var idProducto: Int = 0,
    var idTipoCompetencia: Int = 0,
    var competencia: String? = null,
    var activo: Int = 0,
    var idLog: Long = 0
)