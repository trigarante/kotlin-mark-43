package com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte

import java.sql.Timestamp


data class MonitorModelsLogs (

    var id: Long = 0,
    var idEmpleado: Long? = 0,
    var numSerie: String = "",
    var idMarca: Int = 0,
    var dimensiones: String = "",
    var idEstadoInventario: Int = 0,
    var fechaRecepcion: Timestamp = Timestamp(System.currentTimeMillis()),
    var fechaSalida: Timestamp? = null,
    var idObservacionesInventario: Int = 0,
    var comentarios: String? = null,
    var activo: Int = 0,
    var idSede: Int = 0,
    var origenRecurso: String? = null,
    var idLogs: Long = 0
)