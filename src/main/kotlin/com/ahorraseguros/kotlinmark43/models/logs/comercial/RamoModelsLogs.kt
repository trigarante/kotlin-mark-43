package com.ahorraseguros.kotlinmark43.models.logs.comercial



data class RamoModelsLogs (

    var id: Long = 0,
    var idSocio: Int = 0,
    var idTipoRamo: Int = 0,
    var descripcion: String? = null,
    var prioridad: Int = 0,
    var activo: Int = 0,
    var idLog: Long = 0
)