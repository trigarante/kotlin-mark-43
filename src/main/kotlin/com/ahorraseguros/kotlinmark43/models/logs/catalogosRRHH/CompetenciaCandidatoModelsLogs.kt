package com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH

data class CompetenciaCandidatoModelsLogs (
    var id: Int = 0,
    var descripcion: String? = null,
    var activo: Int = 0,
    var idLog: Long = 0
)