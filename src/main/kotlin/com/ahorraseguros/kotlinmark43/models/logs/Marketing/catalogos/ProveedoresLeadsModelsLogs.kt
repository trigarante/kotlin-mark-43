package com.ahorraseguros.kotlinmark43.models.logs.Marketing.catalogos



data class ProveedoresLeadsModelsLogs (

    var id: Int = 0,
    var nombre: String? = null,
    var activo: Int = 0,
    var idLog: Long = 0
)