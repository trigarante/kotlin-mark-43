package com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH

data class EstadoCivilModelsLogs (
        var id: Int = 0,
        var descripcion: String? = null,
        var activo: Int? = null,
        var idLog: Long = 0
)