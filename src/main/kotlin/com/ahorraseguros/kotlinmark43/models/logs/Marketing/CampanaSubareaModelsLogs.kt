package com.ahorraseguros.kotlinmark43.models.logs.Marketing

data class CampanaSubareaModelsLogs(
        var id: Int = 0,
        var idCampana: Long = 0,
        var idSubarea: Int? = null,
        var descripcionn: String? = null,
        var activo:Int = 0,
        var idLog: Long = 0
)