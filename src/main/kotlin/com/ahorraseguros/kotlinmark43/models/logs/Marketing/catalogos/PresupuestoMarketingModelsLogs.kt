package com.ahorraseguros.kotlinmark43.models.logs.Marketing.catalogos

import java.sql.Timestamp

data class PresupuestoMarketingModelsLogs (
    var id: Int = 0,
    var candidad: Double = 0.toDouble(),
    var fechaPresupuesto: Timestamp? = null,
    var activo: Int = 0,
    var idLog: Long = 0
)