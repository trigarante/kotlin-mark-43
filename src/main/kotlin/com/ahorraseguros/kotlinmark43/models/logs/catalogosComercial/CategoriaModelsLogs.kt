package com.ahorraseguros.kotlinmark43.models.logs.catalogosComercial

data class CategoriaModelsLogs (

    var id: Long = 0,
    var tipo: String? = null,
    var regla: String? = null,
    var activo: Int = 0,
    var idLog: Long = 0
)