package com.ahorraseguros.kotlinmark43.models.logs.catalogosComercial

data class EstadoSocioModelsLogs(

        var id: Int = 0,
        var idEstado: Int = 0,
        var estado: String? = null,
        var activo: Int? = 0,
        var idLog: Long = 0
)