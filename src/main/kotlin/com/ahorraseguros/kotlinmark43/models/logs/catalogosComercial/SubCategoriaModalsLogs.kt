package com.ahorraseguros.kotlinmark43.models.logs.catalogosComercial

data class SubCategoriaModalsLogs (

    var id: Int = 0,
    var detalle: String? = null,
    var regla: String? = null,
    var activo: Int = 0,
    var idCategoria: Int = 0,
    var idDivisas: Int = 0,
    var idLog: Long = 0
)