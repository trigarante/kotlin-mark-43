package com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH

data class PaisesModelsLogs (
        var id: Int = 0,
        var nombre: String? = null,
        var activo: Int = 0,
        var idLog: Long = 0
)