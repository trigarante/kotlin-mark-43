package com.ahorraseguros.kotlinmark43.models.logs.TI

data class ExtensionesModelsLogs (
    var id: Int = 0,
    var idSubarea: Int = 0,
    var idEstado: Int = 0,
    var descrip: String? = null,
    var idLog: Long = 0
)