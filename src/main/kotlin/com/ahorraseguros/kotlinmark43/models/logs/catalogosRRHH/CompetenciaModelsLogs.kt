package com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH

data class CompetenciaModelsLogs (
        var id: Int = 0,
        var escala: Int = 0,
        var estado: String? = null,
        var descripcion: String? = null,
        var activo: Int? = null,
        var idLogs: Long = 0
)
