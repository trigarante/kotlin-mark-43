package com.ahorraseguros.kotlinmark43.models.logs.Marketing.catalogos

data class EstadoCampanaModelsLogs (
        var id: Int = 0,
        var estado: String? = null,
        var activo: Int = 0,
        var idLog: Long = 0
)