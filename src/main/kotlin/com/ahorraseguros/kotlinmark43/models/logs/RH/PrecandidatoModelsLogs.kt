package com.ahorraseguros.kotlinmark43.models.logs.RH

import java.sql.Timestamp

data class PrecandidatoModelsLogs (
        var id: Long = 0,
        var idEtapa: Int = 0,
        var idEscolaridad: Int = 0,
        var idEstacion: Int? = 0,
        var idEstadoRH: Int = 0,
        var idEstadoCivil: Int = 0,
        var idEstadoEscolaridad: Int = 0,
        var idMedioTraslado: Int = 0,
        var idPais: Int = 0,
        var idSolicitudRRHH: Long? = 0,
        var nombre: String? = null,
        var apellidoPaterno: String? = null,
        var apellidoMaterno: String? = null,
        var fechaNacimiento: Timestamp? = null,
        var email: String? = null,
        var curp: String? = null,
        var genero: String? = null,
        var cp: Int = 0,
        var colonia: Int? = null,
        var calle: String? = null,
        var numeroExterior: String? = null,
        var numeroInterior: String? = null,
        var telefonoFijo: Long? = null,
        var telefonoMovil: Long = 0,
        var fechaCreacion: Timestamp? = null,
        var fechaBaja: Timestamp? = null,
        var tiempoTraslado: Int = 0,
        var recontratable: Boolean? = false,
        var detalleBaja: String? = null,
        var fechaRegistroBaja: Timestamp? = null,
        var idLog: Long? = null
)


