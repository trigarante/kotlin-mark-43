package com.ahorraseguros.kotlinmark43.models.logs.Marketing

import java.sql.Timestamp

data class PaginaModelsLogs(

        var id: Int = 0,
        var idCampana: Int = 0,
        var idTipoPagina: Int = 0,
        var url: String? = null,
        var descripcion: String? = null,
        var fechaRegistro: Timestamp? = null,
        var fechaActualizacion: Timestamp? = null,
        var configuracion: String? = null,
        var idLog: Long = 0
)