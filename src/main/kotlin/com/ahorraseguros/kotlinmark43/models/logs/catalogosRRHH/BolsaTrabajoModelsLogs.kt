package com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH

data class BolsaTrabajoModelsLogs (
   var id: Int = 0,
   var nombre: String? = null,
   var tipo: String? = null,
   var activo: Int = 0,
   var idLog: Long? = null
)