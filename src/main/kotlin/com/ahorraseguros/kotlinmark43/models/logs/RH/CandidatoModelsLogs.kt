package com.ahorraseguros.kotlinmark43.models.logs

import java.sql.Timestamp

data class CandidatoModelsLogs (
    var id: Long = 0,
    var idPrecandidato: Long = 0,
    var calificacionPsicometrico: Boolean = false,
    var calificacionExamen: Double = 0.toDouble(),
    var comentarios: String? = null,
    var fechaIngreso: Timestamp? = null,
    var idLog: Long = 0
)

