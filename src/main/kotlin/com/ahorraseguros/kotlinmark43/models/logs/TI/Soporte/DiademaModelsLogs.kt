package com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte

import java.sql.Timestamp

data class DiademaModelsLogs (

    var id: Long = 0,
    var idEmpleado: Long? = null,
    var numFolio: String = "",
    var idMarca: Int = 0,
    var idEstadoInventario: Int = 0,
    var fechaRecepcion: Timestamp = Timestamp(System.currentTimeMillis()),
    var fechaSalida: Timestamp? = null,
    var idObservacionesInventario: Int = 0,
    var comentarios: String? = null,
    var activo: Int? = 0,
    var idSede: Int = 0,
    var origenRecurso: String? = null,
    var idLogs: Long = 0
)