package com.ahorraseguros.mx.logskotlinmark43.models

import java.sql.Timestamp

data class SolicitudesRrhhModelsLogs (
        var id: Long = 0,
        var idBolsaTrabajo: Int = 0,
        var idReclutador: Long = 0,
        var idVacante: Int = 0,
        var nombre: String? = null,
        var apellidoPaterno: String? = null,
        var apellidoMaterno: String? = null,
        var telefono: String? = null,
        var correo: String? = null,
        var cp: String? = null,
        var edad: Int = 0,
        var fecha: Timestamp? = Timestamp(System.currentTimeMillis()),
        var estado: Int = 0,
        var fechaCita: Timestamp? = null,
        var fechaDescartado: Timestamp? = Timestamp(System.currentTimeMillis()),
        var documentos: String? = null,
        var comentarios: String? = null,
        var idLog: Long = 0
)
