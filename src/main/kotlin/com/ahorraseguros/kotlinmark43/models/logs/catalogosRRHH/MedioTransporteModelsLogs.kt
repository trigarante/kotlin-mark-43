package com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH

data class MedioTransporteModelsLogs (
        var id: Int = 0,
        var medio: String? = null,
        var activo: Int = 0,
        var idLog: Long = 0
)