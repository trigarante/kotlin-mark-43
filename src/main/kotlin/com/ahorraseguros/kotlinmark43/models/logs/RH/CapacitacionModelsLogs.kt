package com.ahorraseguros.mx.logskotlinmark43.models

import java.sql.Timestamp

data class CapacitacionModelsLogs (
        var id: Int = 0,
        var idCandidato: Long = 0,
        var calificacion: Double = 0.toDouble(),
        var idCompetenciaCandidato: Int = 0,
        var idCalificacionCompetencia: Int = 0,
        var comentarios: String? = null,
        var idCapacitador: Long = 0,
        var asistencia: String? = null,
        var fechaIngreso: Timestamp? = null,
        var fechaRegistro: Timestamp? = null,
        var idLog: Long = 0
)
