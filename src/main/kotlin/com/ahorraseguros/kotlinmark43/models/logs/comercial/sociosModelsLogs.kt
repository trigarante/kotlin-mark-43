package com.ahorraseguros.kotlinmark43.models.logs.comercial



data class sociosModelsLogs (

    var id: Int = 0,
    var prioridad: Int = 0,
    var nombreComercial: String? = null,
    var rfc: String? = null,
    var razonSocial: String? = null,
    var alias: String? = null,
    var idEstadoSocio: Int = 0,
    var activo: Int = 0,
    var idLog: Long = 0
)