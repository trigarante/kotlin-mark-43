package com.ahorraseguros.mx.logskotlinmark43.models

data class VacantesModelsLogs (
    var id: Int = 0,
    var nombre: String? = null,
    var idPuesto: Int = 0,
    var idSubarea: Int = 0,
    var idTipoPuesto: Int = 0,
    var activo: Int = 0,
    var idLog: Long = 0
)
