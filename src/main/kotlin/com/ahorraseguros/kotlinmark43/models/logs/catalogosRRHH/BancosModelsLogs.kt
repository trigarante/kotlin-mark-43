package com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH

data class BancosModelsLogs (
        var id: Int = 0,
        var nombre: String? = null,
        var activo: Byte = 0,
        var idLogs: Long = 0
)
