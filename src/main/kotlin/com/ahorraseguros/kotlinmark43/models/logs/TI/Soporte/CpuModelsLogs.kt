package com.ahorraseguros.kotlinmark43.models.logs.TI.Soporte

import java.sql.Timestamp

data class CpuModelsLogs (
    var id: Int = 0,
    var idEmpleado: Long? = 0,
    var idObservacionesInventario: Int = 0,
    var numeroSerie: String? = null,
    var idProcesador: Int = 0,
    var hdd: String? = null,
    var ram: String? = null,
    var idMarca: Int = 0,
    var modelo: String? = null,
    var fechaRecepcion: Timestamp = Timestamp(System.currentTimeMillis()),
    var fechaSalida: Timestamp? = null,
    var comentarios: String? = null,
    var activo: Int = 0,
    var idSede: Int = 0,
    var origenRecurso : String? = null,
    var idLogs: Long = 0
)