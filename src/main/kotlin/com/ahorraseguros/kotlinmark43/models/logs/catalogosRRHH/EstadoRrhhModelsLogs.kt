package com.ahorraseguros.kotlinmark43.models.logs.catalogosRRHH

data class EstadoRrhhModelsLogs (
        var id: Long = 0,
        var idEstadoRh: Byte = 0,
        var idEtapa: Int = 0,
        var estado: String? = null,
        var activo: Int = 0,
        var idLog: Long = 0
)
