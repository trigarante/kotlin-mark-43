package com.ahorraseguros.kotlinmark43.models.catalogosRRHH

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "turnoEmpleado")
class TurnoEmpleadoModel {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "turno")
    var turno: String? = null
    @get:Basic
    @get:Column(name = "horario")
    var horario: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
}
