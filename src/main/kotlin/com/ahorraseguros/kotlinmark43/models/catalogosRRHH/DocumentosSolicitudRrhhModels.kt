package com.ahorraseguros.kotlinmark43.models.catalogosRRHH


data class DocumentosSolicitudRrhhModels(
        var imss: Boolean? = false,
        var rfc: Boolean? = false,
        var identificacionOficial: Boolean? = false,
        var actaNacimiento: Boolean? = false,
        var comprobanteDomicilio: Boolean? = false,
        var comprobanteEstudios: Boolean? = false,
        var curp: Boolean? = false
)

