package com.ahorraseguros.kotlinmark43.models.catalogosRRHH

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "subarea")
class SubareaModels (
    @Id
    @Column(name = "id")
    var id: Long = 0,
    @Basic
    @Column(name = "idArea")
    var idArea: Int = 0,
    @Basic
    @Column(name = "subarea")
    var subarea: String? = null,
    @Basic
    @Column(name = "descripcion")
    var descripcion: String? = null,
    @Basic
    @Column(name = "activo")
    var activo: Byte = 0,

    @get:Column(name = "codigo")
    var codigo: String? = null

)