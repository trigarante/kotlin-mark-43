package com.ahorraseguros.kotlinmark43.models.catalogosRRHH

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "etapasModulos")
class EtapasModulosModels {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "idArea")
    var idArea: Int = 0
    @get:Basic
    @get:Column(name = "etapa")
    var etapa: String? = null
    @get:Basic
    @get:Column(name = "nivel")
    var nivel: Int = 0
    @get:Basic
    @get:Column(name = "visible")
    var visible: Int = 0
    @get:Basic
    @get:Column(name = "escritura")
    var escritura: Int = 0
}
