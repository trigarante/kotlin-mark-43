package com.ahorraseguros.kotlinmark43.models.catalogosRRHH

import javax.persistence.*

@Entity
@Table(name = "bolsaTrabajo")
class BolsaTrabajoModels (
    @Id
    @Column(name = "id")
    var id: Long = 0,
    @Basic
    @Column(name = "nombre")
    var nombre: String? = null,
    @Basic
    @Column(name = "tipo")
    var tipo: String? = null,
    @Basic
    @Column(name = "activo")
    var activo: Int = 0

)