package com.ahorraseguros.kotlinmark43.models.catalogosRRHH

import javax.persistence.*
import java.sql.Timestamp
import java.util.Objects

@Entity
@Table(name = "documentosRRHH")
class DocumentosRrhhModel (
    @Id
    @Column(name = "id")
    var id: Int = 0,
    @Basic
    @Column(name = "documento")
    var documento: String? = null,
    @Basic
    @Column(name = "idDocumento")
    var idDocumento: Int = 0,
    @Basic
    @Column(name = "fechaCreacion")
    var fechaCreacion: Timestamp? = null,
    @Basic
    @Column(name = "idEmpleado")
    var idEmpleado: Int = 0

    )