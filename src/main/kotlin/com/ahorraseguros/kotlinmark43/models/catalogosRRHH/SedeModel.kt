package com.ahorraseguros.kotlinmark43.models.catalogosRRHH

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "sede")
class SedeModel {
    @get:Id
    @get:Column(name = "id", nullable = false)
    var id: Int = 0
    @get:Basic
    @get:Column(name = "idEmpresa")
    var idEmpresa: Int = 0
    @get:Basic
    @get:Column(name = "idPais")
    var idPais: Int = 0
    @get:Basic
    @get:Column(name = "nombre", nullable = false, length = 255)
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "cp", nullable = false, length = 5)
    var cp: String? = null
    @get:Basic
    @get:Column(name = "colonia", nullable = false, length = 255)
    var colonia: String? = null
    @get:Basic
    @get:Column(name = "calle", nullable = false, length = 255)
    var calle: String? = null
    @get:Basic
    @get:Column(name = "numero", nullable = false, length = 255)
    var numero: String? = null
    @get:Column(name = "codigo", nullable = false, length = 255)
    var codigo: String? = null

}
