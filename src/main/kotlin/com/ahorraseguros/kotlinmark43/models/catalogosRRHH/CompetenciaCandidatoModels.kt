package com.ahorraseguros.kotlinmark43.models.catalogosRRHH

import javax.persistence.*

@Entity
@Table(name = "competenciaCandidato")
class CompetenciaCandidatoModels {
    @Id
    @Column(name = "id")
    var id: Long = 0
    @Basic
    @Column(name = "descripcion")
    var descripcion: String? = null
    @Basic
    @Column(name = "activo")
    var activo: Int = 0

}