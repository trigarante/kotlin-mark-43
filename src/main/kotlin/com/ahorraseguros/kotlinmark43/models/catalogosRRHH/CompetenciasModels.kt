package com.ahorraseguros.kotlinmark43.models.catalogosRRHH

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "competencias")
class CompetenciasModels (
    @Id
    @Column(name = "id")
    var id: Long = 0,
    @Basic
    @Column(name = "escala")
    var escala: Int = 0,
    @Basic
    @Column(name = "estado")
    var estado: String? = null,
    @Basic
    @Column(name = "descripcion")
    var descripcion: String? = null,
    @Basic
    @Column(name = "activo")
    var activo: Int = 0

)