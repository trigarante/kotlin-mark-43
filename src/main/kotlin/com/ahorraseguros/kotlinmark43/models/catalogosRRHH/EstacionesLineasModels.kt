package com.ahorraseguros.kotlinmark43.models.catalogosRRHH

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "estacionesLineas")
class EstacionesLineasModels(
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0,
    @get:Basic
    @get:Column(name = "idTransporte")
    var idTransporte: Int? = null,
    @get:Basic
    @get:Column(name = "estacion")
    var estacion: String? = null,
    @get:Basic
    @get:Column(name = "lineas")
    var lineas: String? = null
)