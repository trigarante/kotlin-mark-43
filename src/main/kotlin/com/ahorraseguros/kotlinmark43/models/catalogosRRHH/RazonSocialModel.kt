package com.ahorraseguros.kotlinmark43.models.catalogosRRHH

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "razonSocial", schema = "mark44", catalog = "")
class RazonSocialModel {
    @get:Id
    @get:Column(name = "id")
    var id: Int? = 0
    @get:Basic
    @get:Column(name = "nombreComercial")
    var nombreComercial: String? = null
    @get:Basic
    @get:Column(name = "alias")
    var alias: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte? = 0
}
