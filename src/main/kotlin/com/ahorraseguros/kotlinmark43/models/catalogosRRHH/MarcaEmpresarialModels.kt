package com.ahorraseguros.kotlinmark43.models.catalogosRRHH

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "marcaEmpresarial", schema = "mark44", catalog = "")
class MarcaEmpresarialModels {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idSede")
    var idSede: Int = 0
    @get:Basic
    @get:Column(name = "marca")
    var marca: String? = null
    @get:Basic
    @get:Column(name = "detalle")
    var detalle: String? = null
    @get:Basic
    @get:Column(name = "codigo")
    var codigo: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0

}
