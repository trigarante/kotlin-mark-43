package com.ahorraseguros.kotlinmark43.models.catalogosRRHH

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "area")
data class AreaModels (
    @Id
    @get:Column(name = "id")
    var id: Long = 0,

    @get:Column(name = "nombre")
    var nombre: String? = null,

    @get:Column(name = "descripcion")
    var descripcion: String? = null,

    @get:Column(name = "idSede")
    var idSede: Int = 0,

    @get:Column(name = "activo")
    var activo: Int = 0,

    @get:Column(name = "codigo")
    var codigo: String? = null

)