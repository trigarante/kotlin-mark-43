package com.ahorraseguros.kotlinmark43.models.catalogosRRHH

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "medioTransporte")
class MedioTransporteModels {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "medio")
    var medio: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
}
