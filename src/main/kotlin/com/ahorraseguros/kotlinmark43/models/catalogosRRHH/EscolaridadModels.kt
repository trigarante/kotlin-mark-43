package com.ahorraseguros.kotlinmark43.models.catalogosRRHH

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "escolaridad")
class EscolaridadModels {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "nivel")
    var nivel: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int? = null
}
