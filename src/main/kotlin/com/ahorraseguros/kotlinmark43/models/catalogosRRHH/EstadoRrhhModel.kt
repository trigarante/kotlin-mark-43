package com.ahorraseguros.kotlinmark43.models.catalogosRRHH

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "estadoRRHH")
data class EstadoRrhhModel (
    @Id
    @Column(name = "id")
    var id: Long = 0,
    @Basic
    @Column(name = "estado")
    var estado: String? = null,
    @Basic
    @Column(name = "idEstadoRH")
    var idEstadoRh: Byte = 0,
    @Basic
    @Column(name = "activo")
    var activo: Int = 0,
    @Basic
    @Column(name = "idEtapa")
    var idEtapa: Int = 0
)