package com.ahorraseguros.kotlinmark43.models.TI

data class UserDataModel(
        val id: String,
        val emails: Any,
        val name: String,
        val lastLoginTime: String,
        val orgUnitPath: String
)