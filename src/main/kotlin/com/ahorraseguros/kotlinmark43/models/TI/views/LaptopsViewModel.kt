package com.ahorraseguros.kotlinmark43.models.TI.views

import java.sql.Timestamp
import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "laptopsView") // , schema = "mark44", catalog = "")
class LaptopsViewModel {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "idEmpleado")
    var idEmpleado: Long? = null
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "observacionesInventario")
    var observacionesInventario: String? = null
    @get:Basic
    @get:Column(name = "estadoInventario")
    var estadoInventario: String? = null
    @get:Basic
    @get:Column(name = "idEstadoInventario")
    var idEstadoInventario: Long? = null
    @get:Basic
    @get:Column(name = "idObservacionesInventario")
    var idObservacionesInventario: Long? = null
    @get:Basic
    @get:Column(name = "numeroSerie")
    var numeroSerie: String? = null
    @get:Basic
    @get:Column(name = "idProcesador")
    var idProcesador: Int? = 0
    @get:Basic
    @get:Column(name = "hdd")
    var hdd: String? = null
    @get:Basic
    @get:Column(name = "ram")
    var ram: String? = null
    @get:Basic
    @get:Column(name = "idMarca")
    var idMarca: Int? = 0
    @get:Basic
    @get:Column(name = "modelo")
    var modelo: String? = null
    @get:Basic
    @get:Column(name = "fechaRecepcion")
    var fechaRecepcion: Timestamp? = null
    @get:Basic
    @get:Column(name = "fechaSalida")
    var fechaSalida: Timestamp? = null
    @get:Basic
    @get:Column(name = "comentarios")
    var comentarios: String? = null
    @get:Basic
    @get:Column(name = "medidaPantalla")
    var medidaPantalla: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Column(name = "usuario")
    var usuario: String? = null
    @get:Column(name = "nombreMarca")
    var nombreMarca: String? = null
    @get:Column(name = "nombreProcesador")
    var nombreProcesador: String? = null
    @get:Column(name = "idTipoSede")
    var idTipoSede: Int = 0
    @get:Column(name = "origenRecurso")
    var origenRecurso: String? = null
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
}
