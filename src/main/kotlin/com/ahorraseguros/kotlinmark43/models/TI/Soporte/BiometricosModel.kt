package com.ahorraseguros.kotlinmark43.models.TI.Soporte

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "biometricos")
class BiometricosModel {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "ip")
    var ip: String? = null
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
}
