package com.ahorraseguros.kotlinmark43.models.TI

data class GoogleUser(
        val id: String?,
        val givenName: String?,
        val familyName: String?,
        val email: String?,
        val orgUnitPath: String?
)