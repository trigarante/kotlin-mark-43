package com.ahorraseguros.kotlinmark43.models.TI.views

import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "grupoUsuarioView")
data class GrupoUsuarioViewModel (
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0,
    @get:Basic
    @get:Column(name = "idSubarea")
    var idSubarea: Int = 0,
    @get:Basic
    @get:Column(name = "idPuesto")
    var idPuesto: Int = 0,
    @get:Basic
    @get:Column(name = "idPermisosVisualizacion")
    var idPermisosVisualizacion: Int = 0,
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null,
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null,
    @get:Basic
    @get:Column(name = "permisos")
    var permisos: String? = null,
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0,
    @get:Basic
    @get:Column(name = "subarea")
    var subarea: String? = null,
    @get:Basic
    @get:Column(name = "puestoNombre")
    var puestoNombre: String? = null
)
