package com.ahorraseguros.kotlinmark43.models.TI

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "grupoUsuarios")
data class GrupoUsuariosModels(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long = 0,
        @get:Basic
        @get:Column(name = "idSubarea")
        var idSubarea: Int = 0,
        @get:Basic
        @get:Column(name = "idPuesto")
        var idPuesto: Int = 0,
        @get:Basic
        @get:Column(name = "idPermisosVisualizacion")
        var idPermisosVisualizacion: Int = 0,
        @get:Basic
        @get:Column(name = "nombre")
        var nombre: String? = null,
        @get:Basic
        @get:Column(name = "descripcion")
        var descripcion: String? = null,
        @get:Basic
        @get:Column(name = "permisos")
        var permisos: String? = null,
        @get:Basic
        @get:Column(name = "activo")
        var activo: Int = 0
)
