package com.ahorraseguros.kotlinmark43.models.TI

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "procesadoresSoporte", schema = "mark44", catalog = "")
class ProcesadoresSoporteModel {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "nombreProcesador")
    var nombreProcesador: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
}
