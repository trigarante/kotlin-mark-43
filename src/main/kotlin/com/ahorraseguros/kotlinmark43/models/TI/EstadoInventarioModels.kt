package com.ahorraseguros.kotlinmark43.models.TI

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "estadoInventario")
class EstadoInventarioModels {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "estado")
    var estado: String? = null
    @get:Basic
    @get:Column(name = "idEstado")
    var idEstado: Int = 0
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
}
