package com.ahorraseguros.kotlinmark43.models.TI

import java.math.BigInteger
import java.sql.Timestamp
import javax.persistence.*

@Entity
@Table(name = "tablets")
class TabletModels {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idEmpleado")
    var idEmpleado: Long? = 0
    @get:Basic
    @get:Column(name = "numFolio")
    var numFolio: String = ""
    @get:Basic
    @get:Column(name = "idProcesador")
    var idProcesador: Int = 0
    @get:Basic
    @get:Column(name = "capacidad")
    var capacidad: String = ""
    @get:Basic
    @get:Column(name = "ram")
    var ram: String = ""
    @get:Basic
    @get:Column(name = "idMarca")
    var idMarca: Int = 0
    @get:Basic
    @get:Column(name = "idEstadoInventario")
    var idEstadoInventario: Int = 0
    @get:Basic
    @get:Column(name = "telefono")
    var telefono: Int = 0
    @get:Basic
    @get:Column(name = "fechaRecepcion")
    var fechaRecepcion: Timestamp = Timestamp(System.currentTimeMillis())
    @get:Basic
    @get:Column(name = "fechaSalida")
    var fechaSalida: Timestamp? = null
    @get:Basic
    @get:Column(name = "idObservacionesInventario")
    var idObservacionesInventario: Int = 0
    @get:Basic
    @get:Column(name = "comentarios")
    var comentarios: String = ""
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Column(name = "idTipoSede")
    var idTipoSede: Int = 0
    @get:Column(name = "origenRecurso")
    var origenRecurso: String? = null
}