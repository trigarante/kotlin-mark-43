package com.ahorraseguros.kotlinmark43.models.TI.views

import java.sql.Timestamp
import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "telefonoIpView")
class TelefonolpViewModel {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "idEmpleado")
    var idEmpleado: Long? = null
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "estadoInventario")
    var estadoInventario: String? = null
    @get:Basic
    @get:Column(name = "idObservacionesInventario")
    var idObservacionesInventario: Int = 0
    @get:Basic
    @get:Column(name = "idEstadoInventario")
    var idEstadoInventario: Int = 0
    @get:Basic
    @get:Column(name = "folio")
    var folio: String? = null
    @get:Basic
    @get:Column(name = "idMarca")
    var idMarca: Int = 0
    @get:Basic
    @get:Column(name = "idTipoSede")
    var idTipoSede: Int = 0
    @get:Basic
    @get:Column(name = "origenRecurso")
    var origenRecurso: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
    @get:Basic
    @get:Column(name = "fechaRecepcion")
    var fechaRecepcion: Timestamp? = null
    @get:Basic
    @get:Column(name = "fechaSalida")
    var fechaSalida: Timestamp? = null
    @get:Basic
    @get:Column(name = "usuario")
    var usuario: String? = null
    @get:Basic
    @get:Column(name = "observaciones")
    var observaciones: String? = null
    @get:Basic
    @get:Column(name = "nombreMarca")
    var nombreMarca: String? = null
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
}
