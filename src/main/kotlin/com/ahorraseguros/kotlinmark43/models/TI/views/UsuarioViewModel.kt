package com.ahorraseguros.kotlinmark43.models.TI.views

import java.sql.Timestamp
import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "usuarioView")
class UsuarioViewModel {
    @get:Id
    @get:Column(name = "id", nullable = false)
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idCorreo", nullable = false, length = 50)
    var idCorreo: String? = null
    @get:Basic
    @get:Column(name = "idGrupo", nullable = false)
    var idGrupo: Int = 0
    @get:Basic
    @get:Column(name = "idTipo", nullable = false)
    var idTipo: Int = 0
    @get:Basic
    @get:Column(name = "idSubarea", nullable = false)
    var idSubarea: Int = 0
    @get:Basic
    @get:Column(name = "idPermisosVisualizacion", nullable = false)
    var idPermisosVisualizacion: Int = 0
    @get:Basic
    @get:Column(name = "usuario", nullable = false, length = 255)
    var usuario: String? = null
    @get:Basic
    @get:Column(name = "password", nullable = false, length = -1)
    var password: String? = null
    @get:Basic
    @get:Column(name = "fechaCreacion", nullable = false)
    var fechaCreacion: Timestamp? = null
    @get:Basic
    @get:Column(name = "datosUsuario", nullable = true, length = -1)
    var datosUsuario: String? = null
    @get:Basic
    @get:Column(name = "estado", nullable = false)
    var estado: Int = 0
    @get:Basic
    @get:Column(name = "idEmpleado", nullable = true)
    var idEmpleado: Long? = null
    @get:Basic
    @get:Column(name = "grupoUsuarios", nullable = false, length = 255)
    var grupoUsuarios: String? = null
    @get:Basic
    @get:Column(name = "permisos", nullable = false, length = -1)
    var permisos: String? = null
    @get:Basic
    @get:Column(name = "descripcionGrupo", nullable = false, length = 255)
    var descripcionGrupo: String? = null
    @get:Basic
    @get:Column(name = "tipoUsuario", nullable = false, length = 255)
    var tipoUsuario: String? = null
    @get:Basic
    @get:Column(name = "asignado", nullable = false)
    var asignado: Int = 0
    @get:Basic
    @get:Column(name = "nombre", nullable = false, length = 255)
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "imagenUrl", nullable = false, length = 255)
    var imagenUrl: String? = null
}
