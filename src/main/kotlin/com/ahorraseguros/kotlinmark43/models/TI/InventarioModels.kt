package com.ahorraseguros.kotlinmark43.models.TI

import java.sql.Timestamp
import javax.persistence.*

@Entity
@Table(name = "inventario")
class InventarioModels {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idEmpleado")
    var idEmpleado: Long = 0
    @get:Basic
    @get:Column(name = "idEquipo")
    var idEquipo: Long = 0
    @get:Basic
    @get:Column(name = "idTabla")
    var idTabla: Long = 0
    @get:Basic
    @get:Column(name = "fechaAsignacion")
    var fechaAsignacion: Timestamp = Timestamp(System.currentTimeMillis())
    @get:Basic
    @get:Column(name = "fechaLiberacion")
    var fechaLiberacion: Timestamp? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
}