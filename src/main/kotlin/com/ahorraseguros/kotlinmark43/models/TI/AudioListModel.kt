package com.ahorraseguros.kotlinmark43.models.TI

import java.util.*

data class AudioListModel (
        val tipo: String,
        val fecha: Date,
        val numero: String,
        val downloadLink: String
)