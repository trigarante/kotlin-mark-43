package com.ahorraseguros.kotlinmark43.models.TI

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "extenciones")
@NamedStoredProcedureQueries(
        NamedStoredProcedureQuery(name = "extencionesAuto",
                procedureName = "extencionesAuto",
                parameters = [StoredProcedureParameter(mode = ParameterMode.IN, name = "min", type = Int::class),
                    StoredProcedureParameter(mode = ParameterMode.IN, name = "max", type = Int::class),
                    StoredProcedureParameter(mode = ParameterMode.IN, name = "Subarea", type = Int::class),
                    StoredProcedureParameter(mode = ParameterMode.IN, name = "Estado", type = Int::class),
                    StoredProcedureParameter(mode = ParameterMode.IN, name = "Description", type = String::class)]))
data class ExtensionesModels(
        @get:Id
        @get:Column(name = "id")
        var id: Int = 0,
        @get:Basic
        @get:Column(name = "idSubarea")
        var idSubarea: Int = 0,
        @get:Basic
        @get:Column(name = "descrip")
        var descrip: String = "",
        @get:Basic
        @get:Column(name = "idEstado")
        var idEstado: Int = 0
)