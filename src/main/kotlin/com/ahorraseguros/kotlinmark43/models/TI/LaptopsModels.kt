package com.ahorraseguros.kotlinmark43.models.TI

import javax.persistence.*
import java.sql.Timestamp
import java.util.Objects

@Entity
@Table(name = "laptops")
class LaptopsModels {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idEmpleado")
    var idEmpleado: Long? = null
    @get: Basic
    @get:Column(name = "idEstadoInventario")
    var idEstadoInventario: Int = 0
    @get: Basic
    @get:Column(name = "idObservacionesInventario")
    var idObservacionesInventario: Int = 0
    @get:Basic
    @get:Column(name = "numeroSerie")
    var numeroSerie: String? = null
    @get:Basic
    @get:Column(name = "idProcesador")
    var idProcesador: Int? = 0
    @get:Basic
    @get:Column(name = "hdd")
    var hdd: String? = null
    @get:Basic
    @get:Column(name = "ram")
    var ram: String? = null
    @get:Basic
    @get:Column(name = "idMarca")
    var idMarca: Int? = 0
    @get:Basic
    @get:Column(name = "modelo")
    var modelo: String? = null
    @get:Basic
    @get:Column(name = "fechaRecepcion")
    var fechaRecepcion: Timestamp? = Timestamp(System.currentTimeMillis())
    @get:Basic
    @get:Column(name = "fechaSalida")
    var fechaSalida: Timestamp? = null
    @get:Basic
    @get:Column(name = "comentarios")
    var comentarios: String? = null
    @get:Basic
    @get:Column(name = "medidaPantalla")
    var medidaPantalla: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
    @get:Column(name = "idTipoSede")
    var idTipoSede: Int = 0
    @get:Column(name = "origenRecurso")
    var origenRecurso: String? = null

}
