package com.ahorraseguros.kotlinmark43.models.TI.views

import java.sql.Date
import java.sql.Timestamp
import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "empleadoEquipoView")
class EmpleadoEquipoViewModels {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idBanco")
    var idBanco: Int? = null
    @get:Basic
    @get:Column(name = "idCandidato")
    var idCandidato: Long = 0
    @get:Basic
    @get:Column(name = "idPuesto")
    var idPuesto: Int = 0
    @get:Basic
    @get:Column(name = "idPuestoTipo")
    var idPuestoTipo: Int = 0
    @get:Basic
    @get:Column(name = "idSubarea")
    var idSubarea: Int = 0
    @get:Basic
    @get:Column(name = "idUsuario")
    var idUsuario: Long? = null
    @get:Basic
    @get:Column(name = "idTipoUsuario")
    var idTipoUsuario: Int = 0
    @get:Basic
    @get:Column(name = "idArea")
    var idArea: Int = 0
    @get:Basic
    @get:Column(name = "subarea")
    var subarea: String? = null
    @get:Basic
    @get:Column(name = "idSede")
    var idSede: Int = 0
    @get:Basic
    @get:Column(name = "area")
    var area: String? = null
    @get:Basic
    @get:Column(name = "idEmpresa")
    var idEmpresa: Int = 0
    @get:Basic
    @get:Column(name = "sede")
    var sede: String? = null
    @get:Basic
    @get:Column(name = "idGrupo")
    var idGrupo: Int = 0
    @get:Basic
    @get:Column(name = "empresa")
    var empresa: String? = null
    @get:Basic
    @get:Column(name = "grupo")
    var grupo: String? = null
    @get:Basic
    @get:Column(name = "puesto")
    var puesto: String? = null
    @get:Basic
    @get:Column(name = "puestoTipo")
    var puestoTipo: String? = null
    @get:Basic
    @get:Column(name = "idPrecandidato")
    var idPrecandidato: Long = 0
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "apellidoPaterno")
    var apellidoPaterno: String? = null
    @get:Basic
    @get:Column(name = "apellidoMaterno")
    var apellidoMaterno: String? = null
    @get:Basic
    @get:Column(name = "fechaNacimiento")
    var fechaNacimiento: Date? = null
    @get:Basic
    @get:Column(name = "email")
    var email: String? = null
    @get:Basic
    @get:Column(name = "genero")
    var genero: String? = null
    @get:Basic
    @get:Column(name = "idEstadoRH")
    var idEstadoRh: Int = 0
    @get:Basic
    @get:Column(name = "fechaCreacion")
    var fechaCreacion: Timestamp? = null
    @get:Basic
    @get:Column(name = "fechaBaja")
    var fechaBaja: Timestamp? = null
    @get:Basic
    @get:Column(name = "idEtapa")
    var idEtapa: Int = 0
    @get:Basic
    @get:Column(name = "fechaRegistroBaja")
    var fechaRegistroBaja: Timestamp? = null
    @get:Basic
    @get:Column(name = "nombreEtapa")
    var nombreEtapa: String? = null
    @get:Basic
    @get:Column(name = "cpu")
    var cpu: Int? = null
    @get:Basic
    @get:Column(name = "diadema")
    var diadema: Int? = null
    @get:Basic
    @get:Column(name = "laptops")
    var laptops: Int? = null
    @get:Basic
    @get:Column(name = "monitor")
    var monitor: Int? = null
    @get:Basic
    @get:Column(name = "mouse")
    var mouse: Int? = null
    @get:Basic
    @get:Column(name = "tablets")
    var tablets: Int? = null
    @get:Basic
    @get:Column(name = "teclado")
    var teclado: Int? = null
    @get:Basic
    @get:Column(name = "asignado")
    var asignado: Long? = null
}
