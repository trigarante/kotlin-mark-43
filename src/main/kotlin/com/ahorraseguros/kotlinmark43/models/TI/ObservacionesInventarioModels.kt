package com.ahorraseguros.kotlinmark43.models.TI

import javax.persistence.*

@Entity
@Table(name = "observacionesInventario")
class ObservacionesInventarioModels {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "observaciones")
    var observaciones: String = ""
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
}