package com.ahorraseguros.kotlinmark43.models.TI.views

import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "extensionesView")
class ExtensionesViewModel {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idEstado")
    var idEstado: Int = 0
    @get:Basic
    @get:Column(name = "idSubarea")
    var idSubarea: Int = 0
    @get:Basic
    @get:Column(name = "area")
    var area: String? = null
    @get:Basic
    @get:Column(name = "subarea")
    var subarea: String? = null
    @get:Basic
    @get:Column(name = "descrip")
    var descrip: String? = null
    @get:Basic
    @get:Column(name = "estado")
    var estado: String? = null
    @get:Basic
    @get:Column(name = "idArea")
    var idArea: Int = 0
}
