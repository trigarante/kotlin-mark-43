package com.ahorraseguros.kotlinmark43.models.TI

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "licenciasDisponibles")
class LicenciasDisponiblesModel {
    @get:Id
    @get:Column(name = "id", nullable = false)
    var id: Int = 0
    @get:Basic
    @get:Column(name = "nombre", nullable = false)
    var nombre: String = ""
    @get:Basic
    @get:Column(name = "cantidad", nullable = false)
    var cantidad: Int = 0
}
