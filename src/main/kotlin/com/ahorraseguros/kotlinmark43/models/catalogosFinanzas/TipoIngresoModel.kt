package com.ahorraseguros.kotlinmark43.models.catalogosFinanzas

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "tipoIngreso", schema = "trigarante2020", catalog = "")
class TipoIngresoModel {
    @Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "ingresos")
    var ingresos: Int = 0
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
}
