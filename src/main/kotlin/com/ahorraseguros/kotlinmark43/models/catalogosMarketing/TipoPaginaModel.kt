package com.ahorraseguros.kotlinmark43.models.catalogosMarketing

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "tipoPagina")
class TipoPaginaModel {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "tipo")
    var tipo: String? = null
    @get:Basic
    @get:Column(name = "objetivo")
    var objetivo: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
}
