package com.ahorraseguros.kotlinmark43.models.catalogosMarketing

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "proveedoresLeads")
class ProveedoresLeadsModel {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
}
