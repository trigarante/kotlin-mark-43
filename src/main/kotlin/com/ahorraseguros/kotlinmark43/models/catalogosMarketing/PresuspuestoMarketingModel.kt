package com.ahorraseguros.kotlinmark43.models.catalogosMarketing

import javax.persistence.*
import java.sql.Timestamp
import java.util.Objects

@Entity
@Table(name = "presuspuestoMarketing")
class PresuspuestoMarketingModel {
    @Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "candidad")
    var candidad: Double = 0.toDouble()
    @get:Basic
    @get:Column(name = "fechaPresupuesto")
    var fechaPresupuesto: Timestamp? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
}
