package com.ahorraseguros.kotlinmark43.models.catalogosClientes

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "directorioPNN")
@NamedStoredProcedureQueries(
        NamedStoredProcedureQuery(name = "validacionPNN",
                procedureName = "validacionPNN",
                parameters = [StoredProcedureParameter(mode = ParameterMode.IN, name = "numero", type = String::class),
                    StoredProcedureParameter(mode = ParameterMode.OUT, name = "red", type = String::class)]))
class DirectorioPnnModels (
    @Id
    @Column(name = "id")
    var id: Int = 0,
    @Basic
    @Column(name = "clave")
    var clave: String? = null,
    @Basic
    @Column(name = "poblacion")
    var poblacion: String? = null,
    @Basic
    @Column(name = "municipio")
    var municipio: String? = null,
    @Basic
    @Column(name = "estado")
    var estado: String? = null,
    @Basic
    @Column(name = "presuscripcion")
    var presuscripcion: String? = null,
    @Basic
    @Column(name = "region")
    var region: Short = 0,
    @Basic
    @Column(name = "asl")
    var asl: Short = 0,
    @Basic
    @Column(name = "nir")
    var nir: Short = 0,
    @Basic
    @Column(name = "serie")
    var serie: Short = 0,
    @Basic
    @Column(name = "numeracionI")
    var numeracionI: Short = 0,
    @Basic
    @Column(name = "numeracionF")
    var numeracionF: Short = 0,
    @Basic
    @Column(name = "ocupacion")
    var ocupacion: Short = 0,
    @Basic
    @Column(name = "tipoRed")
    var tipoRed: String? = null,
    @Basic
    @Column(name = "modalidad")
    var modalidad: String? = null,
    @Basic
    @Column(name = "razonSocial")
    var razonSocial: String? = null,
    @Basic
    @Column(name = "fechaAsignacion")
    var fechaAsignacion: String? = null,
    @Basic
    @Column(name = "fechaConsilidacion")
    var fechaConsilidacion: String? = null,
    @Basic
    @Column(name = "fechaMigracion")
    var fechaMigracion: String? = null,
    @Basic
    @Column(name = "nirAnterior")
    var nirAnterior: Short = 0

)