package com.ahorraseguros.kotlinmark43.models.catalogosClientes

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "tipoPago")
class TipoPagoModels {
    @get:Id
    @get:Column(name = "id")
    var id: Byte = 0
    @get:Basic
    @get:Column(name = "cantidadPagos")
    var cantidadPagos: Byte = 0
    @get:Basic
    @get:Column(name = "tipoPago")
    var tipoPago: String? = null
}
