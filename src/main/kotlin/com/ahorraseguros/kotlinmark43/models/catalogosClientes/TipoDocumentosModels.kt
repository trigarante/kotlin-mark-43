package com.ahorraseguros.kotlinmark43.models.catalogosClientes

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "tipoDocumentos")
class TipoDocumentosModels {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "idPais")
    var idPais: Int = 0
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
}
