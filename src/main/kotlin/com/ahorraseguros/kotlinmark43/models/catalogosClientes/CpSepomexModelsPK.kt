package com.ahorraseguros.kotlinmark43.models.catalogosClientes

import javax.persistence.Column
import javax.persistence.Id
import java.io.Serializable
import java.util.Objects

class CpSepomexModelsPK : Serializable {
    @get:Column(name = "idCp")
    @get:Id
    var idCp: Int = 0
    @get:Column(name = "cp")
    @get:Id
    var cp: Int = 0
}
