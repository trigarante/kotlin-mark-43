package com.ahorraseguros.kotlinmark43.models.catalogosClientes

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "cpSepomex")
@IdClass(CpSepomexModelsPK::class)
class CpSepomexModels {
    @get:Id
    @get:Column(name = "idCp")
    var idCp: Int = 0
    @get:Id
    @get:Column(name = "cp")
    var cp: Int = 0
    @get:Basic
    @get:Column(name = "asenta")
    var asenta: String? = null
    @get:Basic
    @get:Column(name = "tipoAsenta")
    var tipoAsenta: String? = null
    @get:Basic
    @get:Column(name = "delMun")
    var delMun: String? = null
    @get:Basic
    @get:Column(name = "estado")
    var estado: String? = null
    @get:Basic
    @get:Column(name = "ciudad")
    var ciudad: String? = null
    private var dCp: Int? = null
    private var cEdo: Int = 0
    private var cOficina: Int? = null
    private var cCp: Int? = null
    private var cTipoAsenta: Int? = null
    private var cMnpio: Int? = null
    @get:Basic
    @get:Column(name = "idAsentaCpcons")
    var idAsentaCpcons: Int? = null
    private var dZona: String? = null
    private var cCveCiudad: Int? = null

    @Basic
    @Column(name = "dCp")
    fun getdCp(): Int? {
        return dCp
    }

    fun setdCp(dCp: Int?) {
        this.dCp = dCp
    }

    @Basic
    @Column(name = "cEdo")
    fun getcEdo(): Int {
        return cEdo
    }

    fun setcEdo(cEdo: Int) {
        this.cEdo = cEdo
    }

    @Basic
    @Column(name = "cOficina")
    fun getcOficina(): Int? {
        return cOficina
    }

    fun setcOficina(cOficina: Int?) {
        this.cOficina = cOficina
    }

    @Basic
    @Column(name = "cCP")
    fun getcCp(): Int? {
        return cCp
    }

    fun setcCp(cCp: Int?) {
        this.cCp = cCp
    }

    @Basic
    @Column(name = "cTipoAsenta")
    fun getcTipoAsenta(): Int? {
        return cTipoAsenta
    }

    fun setcTipoAsenta(cTipoAsenta: Int?) {
        this.cTipoAsenta = cTipoAsenta
    }

    @Basic
    @Column(name = "cMnpio")
    fun getcMnpio(): Int? {
        return cMnpio
    }

    fun setcMnpio(cMnpio: Int?) {
        this.cMnpio = cMnpio
    }

    @Basic
    @Column(name = "dZona")
    fun getdZona(): String? {
        return dZona
    }

    fun setdZona(dZona: String) {
        this.dZona = dZona
    }

    @Basic
    @Column(name = "cCveCiudad")
    fun getcCveCiudad(): Int? {
        return cCveCiudad
    }

    fun setcCveCiudad(cCveCiudad: Int?) {
        this.cCveCiudad = cCveCiudad
    }
}
