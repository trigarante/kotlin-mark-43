package com.ahorraseguros.kotlinmark43.models.atencionaClientes.views

import java.sql.Timestamp
import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "endososCancelacionesView")
class EndososCancelacionesViewModel {
    @Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idSolicitudEndoso")
    var idSolicitudEndoso: Long = 0
    @get:Basic
    @get:Column(name = "idTipoEndoso")
    var idTipoEndoso: Int = 0
    @get:Basic
    @get:Column(name = "idEstadoEndoso")
    var idEstadoEndoso: Int = 0
    @get:Basic
    @get:Column(name = "fechaEmision")
    var fechaEmision: Timestamp? = null
    @get:Basic
    @get:Column(name = "fechaAplicacion")
    var fechaAplicacion: Timestamp? = null
    @get:Basic
    @get:Column(name = "prima")
    var prima: Double = 0.toDouble()
    @get:Basic
    @get:Column(name = "comentarios")
    var comentarios: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
    @get:Basic
    @get:Column(name = "idRegistro")
    var idRegistro: Long = 0
    @get:Basic
    @get:Column(name = "idEstadoSolicitudEndosos")
    var idEstadoSolicitudEndosos: Int = 0
    @get:Basic
    @get:Column(name = "solicitudEndosoComentarios")
    var solicitudEndosoComentarios: String? = null
    @get:Basic
    @get:Column(name = "tipoEndososDescripcion")
    var tipoEndososDescripcion: String? = null
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
}
