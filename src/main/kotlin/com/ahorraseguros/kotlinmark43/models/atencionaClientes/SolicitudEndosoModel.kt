package com.ahorraseguros.kotlinmark43.models.atencionaClientes

import javax.persistence.*
import java.sql.Timestamp
import java.util.Objects

@Entity
@Table(name = "solicitudEndoso")
class SolicitudEndosoModel {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idRegistro")
    var idRegistro: Long = 0
    @get:Basic
    @get:Column(name = "idEstadoSolicitudEndosos")
    var idEstadoSolicitudEndosos: Int = 0
    @get:Basic
    @get:Column(name = "idEmpleado")
    var idEmpleado: Long? = null
    @get:Basic
    @get:Column(name = "idTipoSolicitudEndoso")
    var idTipoSolicitudEndoso: Int? = null
    @get:Basic
    @get:Column(name = "fechaSolicitud")
    var fechaSolicitud: Timestamp? = null
    @get:Basic
    @get:Column(name = "fechaResolucion")
    var fechaResolucion: Timestamp? = Timestamp(System.currentTimeMillis())
    @get:Basic
    @get:Column(name = "comentarios")
    var comentarios: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int? = null
}
