package com.ahorraseguros.kotlinmark43.models.atencionaClientes.views

import java.sql.Timestamp
import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "solicitudEndosoView")
class SolicitudEndosoViewModel {
    @Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idRegistro")
    var idRegistro: Long = 0
    @get:Basic
    @get:Column(name = "idEstadoSolicitudEndosos")
    var idEstadoSolicitudEndosos: Int = 0
    @get:Basic
    @get:Column(name = "fechaSolicitud")
    var fechaSolicitud: Timestamp? = null
    @get:Basic
    @get:Column(name = "fechaResolucion")
    var fechaResolucion: Timestamp? = null
    @get:Basic
    @get:Column(name = "comentarios")
    var comentarios: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte? = null
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "idEmpleado")
    var idEmpleado: Long = 0


}
