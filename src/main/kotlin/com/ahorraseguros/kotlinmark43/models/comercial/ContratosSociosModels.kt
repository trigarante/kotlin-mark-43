package com.ahorraseguros.kotlinmark43.models.comercial

import javax.persistence.*
import java.sql.Timestamp
import java.util.Objects

@Entity
@Table(name = "contratosSocios")
class ContratosSociosModels {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get: Basic
    @get: Column(name = "idSocio")
    var idSocio: Int = 0
    @get:Basic
    @get:Column(name = "fechaInicio")
    var fechaInicio: Timestamp? = null
    @get:Basic
    @get:Column(name = "fechaFin")
    var fechaFin: Timestamp? = null
    @get:Basic
    @get:Column(name = "referencia")
    var referencia: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0

}
