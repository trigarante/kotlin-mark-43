package com.ahorraseguros.kotlinmark43.models.comercial

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "presupuestoSocio")
class PresupuestoSocioModels {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idSocio")
    var idSocio: Int = 0
    @get:Basic
    @get:Column(name = "presupuesto")
    var presupuesto: String = ""
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "anio")
    var anio: String? = null
}
