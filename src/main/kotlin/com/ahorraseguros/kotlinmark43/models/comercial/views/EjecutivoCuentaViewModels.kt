package com.ahorraseguros.kotlinmark43.models.comercial.views

import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "ejecutivoCuentaView")
class EjecutivoCuentaViewModels {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idSocio")
    var idSocio: Int = 0
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "apellidoPaterno")
    var apellidoPaterno: String? = null
    @get:Basic
    @get:Column(name = "apellidoMaterno")
    var apellidoMaterno: String? = null
    @get:Basic
    @get:Column(name = "telefono")
    var telefono: String? = null
    @get:Basic
    @get:Column(name = "ext")
    var ext: String? = null
    @get:Basic
    @get:Column(name = "celular")
    var celular: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
    @get:Basic
    @get:Column(name = "idEstadoSocio")
    var idEstadoSocio: Int = 0
    @get:Basic
    @get:Column(name = "nombreComercial")
    var nombreComercial: String? = null
    @get:Basic
    @get:Column(name = "alias")
    var alias: String? = null
    @get:Basic
    @get:Column(name = "favorito")
    var favorito: Int? = null
    @get:Basic
    @get:Column(name = "email")
    var email: String? = null
    @get:Basic
    @get:Column(name = "puesto")
    var puesto: String? = null
    @get:Basic
    @get:Column(name = "direccion")
    var direccion: String? = null

}
