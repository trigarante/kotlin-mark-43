package com.ahorraseguros.kotlinmark43.models.comercial.views

import java.sql.Timestamp
import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "contratosSociosView")
class ContratosSociosViewModals {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idSocio")
    var idSocio: Int = 0
    @get:Basic
    @get:Column(name = "fechaInicio")
    var fechaInicio: Timestamp? = null
    @get:Basic
    @get:Column(name = "fechaFin")
    var fechaFin: Timestamp? = null
    @get:Basic
    @get:Column(name = "referencia")
    var referencia: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "alias")
    var alias: String? = null
    @get:Basic
    @get:Column(name = "idEstadoSocio")
    var idEstadoSocio: Int = 0
    @get:Basic
    @get:Column(name = "nombreComercial")
    var nombreComercial: String? = null
    @get:Basic
    @get:Column(name = "estado")
    var estado: String? = null
    @get:Basic
    @get:Column(name = "idEstado")
    var idEstado: Int = 0
    @get:Basic
    @get:Column(name = "diasRestantes")
    var diasRestantes: Int = 0

}
