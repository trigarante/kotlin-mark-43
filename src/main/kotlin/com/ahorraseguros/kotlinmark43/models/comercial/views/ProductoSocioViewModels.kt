package com.ahorraseguros.kotlinmark43.models.comercial.views

import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "productoSocio")
class ProductoSocioViewModels {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idSubRamo")
    var idSubRamo: Long = 0
    @get:Basic
    @get:Column(name = "prioridad")
    var prioridad: Int = 0
    @get:Basic
    @get:Column(name = "prioridades")
    var prioridades: String? = null
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "idRamo")
    var idRamo: Int = 0
    @get:Basic
    @get:Column(name = "idTipoSubRamo")
    var idTipoSubRamo: Int = 0
    @get:Basic
    @get:Column(name = "tipoRamo")
    var tipoRamo: String? = null
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "nombreComercial")
    var nombreComercial: String? = null
    @get:Basic
    @get:Column(name = "alias")
    var alias: String? = null
    @get:Basic
    @get:Column(name = "idEstadoSocio")
    var idEstadoSocio: Int = 0
    @get:Basic
    @get:Column(name = "estado")
    var estado: String? = null
    @get:Basic
    @get:Column(name = "idEstado")
    var idEstado: Byte = 0
    @get:Basic
    @get:Column(name = "tipoSubRamo")
    var tipoSubRamo: String? = null
    @get:Basic
    @get:Column(name = "idTipoProducto")
    var idTipoProducto: Int = 0
}
