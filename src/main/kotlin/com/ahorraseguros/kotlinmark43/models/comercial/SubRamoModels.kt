package com.ahorraseguros.kotlinmark43.models.comercial

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "subRamo")
class SubRamoModels {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "prioridad")
    var prioridad: Int = 0
    @get:Basic
    @get:Column(name = "idRamo")
    var idRamo: Int = 0
    @get:Basic
    @get:Column(name = "idTipoSubRamo")
    var idTipoSubRamo: Int = 0
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0

}
