package com.ahorraseguros.kotlinmark43.models.comercial

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "socios")
class SociosModels {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "prioridad")
    var prioridad: Int = 0
    @get:Basic
    @get:Column(name = "nombreComercial")
    var nombreComercial: String? = null
    @get:Basic
    @get:Column(name = "rfc")
    var rfc: String? = null
    @get:Basic
    @get:Column(name = "razonSocial")
    var razonSocial: String? = null
    @get:Basic
    @get:Column(name = "alias")
    var alias: String? = null
    @get:Basic
    @get:Column(name = "idEstadoSocio")
    var idEstadoSocio: Int = 0
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
}
