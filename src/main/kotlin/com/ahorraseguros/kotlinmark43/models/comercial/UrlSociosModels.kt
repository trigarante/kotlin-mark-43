package com.ahorraseguros.kotlinmark43.models.comercial

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "urlSocios")
class UrlSociosModels {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idSocio")
    var idSocio: Int = 0
    @get:Basic
    @get:Column(name = "url")
    var url: String? = null
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0

}
