package com.ahorraseguros.kotlinmark43.models.comercial

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "competenciaSocio")
class CompetenciaSocioModels {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idProducto")
    var idProducto: Int = 0
    @get:Basic
    @get:Column(name = "idTipoCompetencia")
    var idTipoCompetencia: Int = 0
    @get:Basic
    @get:Column(name = "competencia")
    var competencia: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0

}
