package com.ahorraseguros.kotlinmark43.models.comercial.views

import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "conveniosView")
class ConveniosViewModels {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idSubCategoria")
    var idSubCategoria: Int = 0
    @get:Basic
    @get:Column(name = "idSubRamo")
    var idSubRamo: Int = 0
    @get:Basic
    @get:Column(name = "cantidad")
    var cantidad: Double = 0.toDouble()
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
//    @get:Basic
//    @get:Column(name = "idEstado")
//    var idEstado: Byte = 0
    @get:Basic
    @get:Column(name = "idEstadoSocio")
    var idEstadoSocio: Int = 0
    @get:Basic
    @get:Column(name = "estado")
    var estado: String? = null
    @get:Basic
    @get:Column(name = "idRamo")
    var idRamo: Int = 0
    @get:Basic
    @get:Column(name = "idTipoSubRamo")
    var idTipoSubRamo: Int = 0
    @get:Basic
    @get:Column(name = "tipoSubRamo")
    var tipoSubRamo: String? = null
    @get:Basic
    @get:Column(name = "tipoRamo")
    var tipoRamo: String? = null
    @get:Basic
    @get:Column(name = "nombreComercial")
    var nombreComercial: String? = null
    @get:Basic
    @get:Column(name = "alias")
    var alias: String? = null
    @get:Basic
    @get:Column(name = "idTipoDivisa")
    var idTipoDivisa: Int = 0
    @get:Basic
    @get:Column(name = "detalle")
    var detalle: String? = null
    @get:Basic
    @get:Column(name = "idCategoria")
    var idCategoria: Int = 0
    @get:Basic
    @get:Column(name = "tipo")
    var tipo: String? = null
    @get:Basic
    @get:Column(name = "divisa")
    var divisa: String? = null
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
}
