package com.ahorraseguros.kotlinmark43.models.comercial.views

import javax.persistence.*

@Entity
@Table(name = "ramoView")
class RamoViewModels {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idSocio")
    var idSocio: Long = 0
    @get:Basic
    @get:Column(name = "idTipoRamo")
    var idTipoRamo: Int = 0
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "prioridad")
    var prioridad: Int = 0
    @get:Basic
    @get:Column(name = "prioridades")
    var prioridades: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "nombreComercial")
    var nombreComercial: String? = null
    @get:Basic
    @get:Column(name = "alias")
    var alias: String? = null
    @get:Basic
    @get:Column(name = "idEstadoSocio")
    var idEstadoSocio: Int = 0
    @get:Basic
    @get:Column(name = "estado")
    var estado: String? = null
    @get:Basic
    @get:Column(name = "idEstado")
    var idEstado: Int = 0
    @get:Basic
    @get:Column(name = "tipoRamo")
    var tipoRamo: String? = null

}
