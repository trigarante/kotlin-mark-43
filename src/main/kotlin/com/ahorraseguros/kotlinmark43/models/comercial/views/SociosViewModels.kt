package com.ahorraseguros.kotlinmark43.models.comercial.views

import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "sociosView")
class SociosViewModels {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "prioridad")
    var prioridad: Int = 0
    @get:Basic
    @get:Column(name = "prioridades")
    var prioridades: String? = null
    @get:Basic
    @get:Column(name = "nombreComercial")
    var nombreComercial: String? = null
    @get:Basic
    @get:Column(name = "rfc")
    var rfc: String? = null
    @get:Basic
    @get:Column(name = "razonSocial")
    var razonSocial: String? = null
    @get:Basic
    @get:Column(name = "alias")
    var alias: String? = null
    @get:Basic
    @get:Column(name = "idEstadoSocio")
    var idEstadoSocio: Int = 0
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "estado")
    var estado: String? = null
//    @get:Basic
//    @get:Column(name = "idEstado")
//    var idEstado: Int = 0
}
