package com.ahorraseguros.kotlinmark43.models.updates

import java.sql.Date
import java.sql.Timestamp
import javax.print.DocFlavor


data class UpdateEmpleado(


        var idEmpleado: Long = 0,

        var idBanco: Int? = null,

        var idCandidato: Long = 0,

        var idEmpresa: Int = 0,

        var kpiMensual: Float? = null,

        var idEstadoEmpleado: Int = 0,

        var idPuesto: Int = 0,

        var fechaRegistro: Timestamp? = null,

        var idTipoPuesto: Int = 0,

        var idSubarea: Int = 0,

        var idUsuario: Long = 0,

        var idHuella: Int = 0,

        var puestoDetalle: String? = null,

        var fechaAltaImss: Timestamp? = null,

        var documentosPersonales: String? = null,

        var documentosAdministrativos: String? = null,

        var fechaIngreso: Timestamp? = Timestamp(System.currentTimeMillis()),

        var sueldoDiario: Double? = null,

        var sueldoMensual: Double? = null,

        var kpi: Boolean? = null,

        var fechaCambioSueldo: Date? = null,

        var ctaClabe: String? = null,

        var fechaBajaEmpleado: Date? = null,


        var fechaAsigancion: Timestamp? = null,

        var tipoUsuario: Int = 0,


        var idPrecandidato: Long = 0,

        var idEtapa: Int = 0,

        var idEscolaridad: Int = 0,

        var idEstacion: Int? = null,

        var idEstadoPrecandidato: Int = 0,

        var idEstadoCivil: Int = 0,

        var idEstadoEscolaridad: Int = 0,

        var idMedioTraslado: Int = 0,

        var idPais: Int = 0,

        var idSolicitudRRHH: Long = 0,

        var nombre: String? = null,

        var apellidoPaterno: String? = null,

        var apellidoMaterno: String? = null,

        var fechaNacimiento: Date? = null,

        var email: String? = null,

        var genero: String? = null,

        var cp: Int = 0,

        var colonia: String? = null,

        var calle: String? = null,

        var numeroExterior: String? = null,

        var numeroInterior: String? = null,

        var telefonoFijo: Long? = null,

        var telefonoMovil: Long = 0,

        var fechaCreacion: Timestamp? = Timestamp(System.currentTimeMillis()),

        var fechaBajaPrecandidato: Timestamp? = null,

        var tiempoTraslado: Int = 0,

        var curp: String? = null,

        var kpiTrimestral: Float? = null,

        var kpiSemestral: Float? = null,

        var fechaRegistroBaja: Timestamp? = null,

        var recontratable: Boolean? = false,

        var detalleBaja: String? = null,

        var comentarios: String? = null,

        var idSede: Int = 0
        )