package com.ahorraseguros.kotlinmark43.models.catalogosComercial

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "tipoCobertura")
class TipoCoberturaModels {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "cobertura")
    var cobertura: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0

}
