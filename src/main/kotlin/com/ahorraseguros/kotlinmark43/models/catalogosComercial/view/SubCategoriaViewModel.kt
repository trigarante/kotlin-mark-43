package com.ahorraseguros.kotlinmark43.models.catalogosComercial.view

import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "subCategoriaView")
class SubCategoriaViewModel {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "idCategoria")
    var idCategoria: Int = 0
    @get:Basic
    @get:Column(name = "tipo")
    var tipo: String? = null
    @get:Basic
    @get:Column(name = "detalle")
    var detalle: String? = null
    @get:Basic
    @get:Column(name = "regla")
    var regla: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
    @get:Basic
    @get:Column(name = "idDivisas")
    var idDivisas: Int = 0
    @get:Basic
    @get:Column(name = "divisa")
    var divisa: String? = null
}
