package com.ahorraseguros.kotlinmark43.models.catalogosComercial.view

import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "estadoSociosView")
class EstadoSociosViewModel {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "idEstado")
    var idEstado: Int = 0
    @get:Basic
    @get:Column(name = "estado")
    var estado: String? = null

}
