package com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.view

import javax.persistence.*

@Entity
@Table(name = "productoClienteView")
class ProductoClienteViewModel {
    @Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Column(name = "idCliente")
    @get:Basic
    var idCliente: Long = 0
    @get:Column(name = "idSolictud")
    @get:Basic
    var idSolictud: Long = 0
    @get:Column(name = "datos")
    @get:Basic
    var datos: String? = null
    @get:Column(name = "nombre")
    @get:Basic
    var nombre: String? = null
    @get:Column(name = "paterno")
    @get:Basic
    var paterno: String? = null
    @get:Column(name = "materno")
    @get:Basic
    var materno: String? = null
    @get:Column(name = "descripcion")
    @get:Basic
    var descripcion: String? = null
    @get:Column(name = "idSubRamo")
    @get:Basic
    var idSubRamo = 0
}