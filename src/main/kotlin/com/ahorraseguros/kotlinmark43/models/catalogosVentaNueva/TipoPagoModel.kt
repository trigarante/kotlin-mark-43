package com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "tipoPago")
class TipoPagoModel {
    @Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "cantidadPagos")
    var cantidadPagos: Int = 0
    @get:Basic
    @get:Column(name = "tipoPago")
    var tipoPago: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
}
