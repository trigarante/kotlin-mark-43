package com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "tarjetasBanco")
class TarjetasBancoModel {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "idCarrier")
    var idCarrier: Int = 0
    @get:Basic
    @get:Column(name = "idBanco")
    var idBanco: Int = 0
    @get:Basic
    @get:Column(name = "tarjeta")
    var tarjeta: String? = null
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
}
