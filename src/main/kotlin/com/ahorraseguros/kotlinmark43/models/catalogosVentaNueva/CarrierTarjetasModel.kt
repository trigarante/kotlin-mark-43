package com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "carrierTarjetas")
class CarrierTarjetasModel {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "carrier")
    var carrier: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
}
