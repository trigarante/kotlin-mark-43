package com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "estadoPago")
class EstadoPagoModel {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "description")
    var description: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
}
