package com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "formaPago", schema = "trigarante2020-P", catalog = "")
class FormaPagoModel {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "description")
    var description: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
}
