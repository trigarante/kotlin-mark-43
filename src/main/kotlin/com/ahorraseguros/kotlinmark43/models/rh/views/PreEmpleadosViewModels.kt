package com.ahorraseguros.kotlinmark43.models.rh.views

import java.sql.Date
import java.sql.Timestamp
import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "preEmpleadosView")
class PreEmpleadosViewModels {
    @get:Id
    @get:Column(name = "idCandidato")
    var idCandidato: Long = 0
    @get:Basic
    @get:Column(name = "idPrecandidato")
    var idPrecandidato: Long = 0
    @get:Basic
    @get:Column(name = "calificacionPsicometrico")
    var calificacionPsicometrico: Int = 0
    @get:Basic
    @get:Column(name = "calificacionExamen")
    var calificacionExamen: Double? = null
    @get:Basic
    @get:Column(name = "comentarios")
    var comentarios: String? = null
    @get:Basic
    @get:Column(name = "fechaIngreso")
    var fechaIngreso: Timestamp? = null
    @get:Basic
    @get:Column(name = "preEmpleado")
    var preEmpleado: String? = null
    @get:Basic
    @get:Column(name = "fechaNacimiento")
    var fechaNacimiento: Date? = null
    @get:Basic
    @get:Column(name = "email")
    var email: String? = null
    @get:Basic
    @get:Column(name = "genero")
    var genero: String? = null
    @get:Basic
    @get:Column(name = "idEstadoCivil")
    var idEstadoCivil: Int = 0
    @get:Basic
    @get:Column(name = "idEscolaridad")
    var idEscolaridad: Int = 0
    @get:Basic
    @get:Column(name = "idEstadoEscolaridad")
    var idEstadoEscolaridad: Int = 0
    @get:Basic
    @get:Column(name = "cp")
    var cp: Int = 0
    @get:Basic
    @get:Column(name = "colonia")
    var colonia: String? = ""
    @get:Basic
    @get:Column(name = "calle")
    var calle: String? = null
    @get:Basic
    @get:Column(name = "numeroExterior")
    var numeroExterior: String? = null
    @get:Basic
    @get:Column(name = "numeroInterior")
    var numeroInterior: String? = null
    @get:Basic
    @get:Column(name = "telefonoFijo")
    var telefonoFijo: Long? = null
    @get:Basic
    @get:Column(name = "telefonoMovil")
    var telefonoMovil: Long = 0
    @get:Basic
    @get:Column(name = "idEstadoRH")
    var idEstadoRh: Int = 0
    @get:Basic
    @get:Column(name = "fechaCreacion")
    var fechaCreacion: Timestamp? = null
    @get:Basic
    @get:Column(name = "fechaBaja")
    var fechaBaja: Timestamp? = null
    @get:Basic
    @get:Column(name = "tiempoTraslado")
    var tiempoTraslado: Int = 0
    @get:Basic
    @get:Column(name = "idMedioTraslado")
    var idMedioTraslado: Int = 0
    @get:Basic
    @get:Column(name = "idEstacion")
    var idEstacion: Int? = null
    @get:Basic
    @get:Column(name = "estadoCivil")
    var estadoCivil: String? = null
    @get:Basic
    @get:Column(name = "escolaridad")
    var escolaridad: String? = null
    @get:Basic
    @get:Column(name = "estadoEscolar")
    var estadoEscolar: String? = null
    @get:Basic
    @get:Column(name = "estadoRH")
    var estadoRh: String? = null
    @get:Basic
    @get:Column(name = "medio")
    var medio: String? = null
    @get:Basic
    @get:Column(name = "estacion")
    var estacion: String? = null
    @get:Basic
    @get:Column(name = "lineas")
    var lineas: String? = null
    @get:Basic
    @get:Column(name = "idEtapa")
    var idEtapa: Int = 0
    @get:Basic
    @get:Column(name = "recontratable")
    var recontratable: Int = 0
    @get:Basic
    @get:Column(name = "curp")
    var curp: String? = null
    @get:Basic
    @get:Column(name = "detalleBaja")
    var detalleBaja: String? = null
    @get:Basic
    @get:Column(name = "fechaRegistroBaja")
    var fechaRegistroBaja: Timestamp? = null
    @get:Basic
    @get:Column(name = "nombreEtapa")
    var nombreEtapa: String? = null
    @get:Basic
    @get:Column(name = "idVacante")
    var idVacante: Int = 0
    @get:Basic
    @get:Column(name = "vacante")
    var vacante: String? = null
    @get:Basic
    @get:Column(name = "idSubarea")
    var idSubarea: Int = 0
    @get:Basic
    @get:Column(name = "idPuesto")
    var idPuesto: Int = 0
    @get:Basic
    @get:Column(name = "idTipoPuesto")
    var idTipoPuesto: Int = 0
    @get:Basic
    @get:Column(name = "idArea")
    var idArea: Int = 0
    @get:Basic
    @get:Column(name = "puesto")
    var puesto: String? = null
    @get:Basic
    @get:Column(name = "puestoTipo")
    var puestoTipo: String? = null
    @get:Basic
    @get:Column(name = "idEmpresa")
    var idEmpresa: Int = 0
    @get:Basic
    @get:Column(name = "idSede")
    var idSede: Int = 0
    @get:Basic
    @get:Column(name = "empresa")
    var empresa: String? = null
    @get:Basic
    @get:Column(name = "subarea")
    var subarea: String? = null
    @get:Basic
    @get:Column(name = "sede")
    var sede: String? = null
    @get:Basic
    @get:Column(name = "idGrupo")
    var idGrupo: Int = 0
    @get:Basic
    @get:Column(name = "grupo")
    var grupo: String? = null

}
