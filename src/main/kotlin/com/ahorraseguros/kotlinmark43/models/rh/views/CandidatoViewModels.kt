package com.ahorraseguros.kotlinmark43.models.rh.views

import java.sql.Timestamp
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "candidatoView")
data class CandidatoViewModels(
        @Id
        val id: Long = 0,
        val calificacionExamen: Double = 0.0,
        val comentarios: String = "",
        val fechaIngreso: Timestamp? = Timestamp(System.currentTimeMillis()),
        val nombre: String = "",
        val apellidoPaterno: String = "",
        val apellidoMaterno: String = "",
        val email: String = "",
        val calificacionPsicometrico: Boolean = false,
        val idPrecandidato: Long = 0,
        val telefonoMovil: String = "",
        val idEstadoRH: Long = 0,
        val fechaCreacion: Timestamp? = Timestamp(System.currentTimeMillis()),
        val fechaBaja: Timestamp? = Timestamp(System.currentTimeMillis()),
        val estadoRH: String = "",
        val puesto: String ="",
        val idEtapa: Long = 0,
        val idPuesto: Int? = 0,
        val nombreEtapa: String = "",
        val idVacante: Long? = null,
//        val idTipoUsuario: Long? = null,
        val vacante: String = "",
        val idSede: Int? = null,
        val sede: String? = null,
        val idSubarea: Int? = null,
        val subarea: String? = null

)