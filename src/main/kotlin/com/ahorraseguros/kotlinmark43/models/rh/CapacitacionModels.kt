package com.ahorraseguros.kotlinmark43.models.rh

import java.sql.Date
import java.sql.Timestamp
import javax.persistence.*


@Entity
@Table(name = "capacitacion")
data class CapacitacionModels(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long = 0,
        @get:Basic
        @get:Column(name = "idCandidato")
        var idCandidato: Long = 0,
        @get:Basic
        @get:Column(name = "calificacion")
        var calificacion: Double = 0.0,
        @get:Basic
        @get:Column(name = "idCompetenciaCandidato")
        var idCompetenciaCandidato: Int = 0,
        @get:Basic
        @get:Column(name = "idCalificacionCompetencia")
        var idCalificacionCompetencia: Int = 0,
        @get:Basic
        @get:Column(name = "comentarios")
        var comentarios: String? = null,
        @get:Basic
        @get:Column(name = "idCapacitador")
        var idCapacitador: Long = 0,
        @get:Basic
        @get:Column(name = "asistencia")
        var asistencia: String? = null,
        @get:Basic
        @get:Column(name = "fechaIngreso")
        var fechaIngreso: Timestamp? = null,
        @get:Basic
        @get:Column(name = "fechaRegistro")
        var fechaRegistro: Timestamp? = Timestamp(System.currentTimeMillis())


)

