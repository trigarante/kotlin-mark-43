package com.ahorraseguros.kotlinmark43.models.rh

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "test")
class TestModel {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
}
