package com.ahorraseguros.kotlinmark43.models.rh

import javax.persistence.*
import java.sql.Date
import java.sql.Timestamp
import java.util.Objects

@Entity
@Table(name = "precandidato")
data class PrecandidatoModels (
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0,
    @get:Basic
    @get:Column(name = "idEtapa")
    var idEtapa: Int = 0,
    @get:Basic
    @get:Column(name = "idEscolaridad")
    var idEscolaridad: Int = 0,
    @get:Basic
    @get:Column(name = "idEstacion")
    var idEstacion: Int? = null,
    @get:Basic
    @get:Column(name = "idEstadoRH")
    var idEstadoRH: Int = 0,
    @get:Basic
    @get:Column(name = "idEstadoCivil")
    var idEstadoCivil: Int = 0,
    @get:Basic
    @get:Column(name = "idEstadoEscolaridad")
    var idEstadoEscolaridad: Int = 0,
    @get:Basic
    @get:Column(name = "idMedioTraslado")
    var idMedioTraslado: Int = 0,
    @get:Basic
    @get:Column(name = "idSolicitudRRHH")
    var idSolicitudRRHH: Long? = null,
    @get:Basic
    @get:Column(name = "idPais")
    var idPais: Int = 0,
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null,
    @get:Basic
    @get:Column(name = "apellidoPaterno")
    var apellidoPaterno: String? = null,
    @get:Basic
    @get:Column(name = "apellidoMaterno")
    var apellidoMaterno: String? = null,
    @get:Basic
    @get:Column(name = "fechaNacimiento")
    var fechaNacimiento: Timestamp? = null,
    @get:Basic
    @get:Column(name = "email")
    var email: String? = null,
    @get:Basic
    @get:Column(name = "genero")
    var genero: String = "",
    @get:Basic
    @get:Column(name = "cp")
    var cp: Int = 0,
    @get:Basic
    @get:Column(name = "idColonia")
    var idColonia: Int? = null,
    @get:Basic
    @get:Column(name = "calle")
    var calle: String? = null,
    @get:Basic
    @get:Column(name = "numeroExterior")
    var numeroExterior: String? = null,
    @get:Basic
    @get:Column(name = "numeroInterior")
    var numeroInterior: String? = null,
    @get:Basic
    @get:Column(name = "telefonoFijo")
    var telefonoFijo: Long? = null,
    @get:Basic
    @get:Column(name = "telefonoMovil")
    var telefonoMovil: Long = 0,
    @get:Basic
    @get:Column(name = "fechaCreacion")
    var fechaCreacion: Timestamp? = Timestamp(System.currentTimeMillis()),
    @get:Basic
    @get:Column(name = "fechaBaja")
    var fechaBaja: Timestamp? = null,
    @get:Basic
    @get:Column(name = "fechaRegistroBaja")
    var fechaRegistroBaja: Timestamp? = null,
    @get:Basic
    @get:Column(name = "tiempoTraslado")
    var tiempoTraslado: Int = 0,
    @get:Basic
    @get:Column(name = "curp")
    var curp: String? = null,
    @get:Basic
    @get:Column(name = "recontratable")
    var recontratable: Boolean? = false,
    @get:Basic
    @get:Column(name = "detalleBaja")
    var detalleBaja: String? = null
)
