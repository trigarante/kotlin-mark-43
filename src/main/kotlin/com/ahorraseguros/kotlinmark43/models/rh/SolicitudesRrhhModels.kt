package com.ahorraseguros.kotlinmark43.models.rh

import javax.persistence.*
import java.sql.Timestamp

@Entity
@Table(name = "solicitudesRRHH")
data class SolicitudesRrhhModels(
        @get:Id
        @get:Column(name = "id")
        var id: Long = 0,
        @get:Basic
        @get:Column(name = "nombre")
        var nombre: String? = null,
        @get:Basic
        @get:Column(name = "apellidoPaterno")
        var apellidoPaterno: String? = null,
        @get:Basic
        @get:Column(name = "apellidoMaterno")
        var apellidoMaterno: String? = null,
        @get:Basic
        @get:Column(name = "telefono")
        var telefono: String? = null,
        @get:Basic
        @get:Column(name = "correo")
        var correo: String? = null,
        @get:Basic
        @get:Column(name = "cp")
        var cp: String? = null,
        @get:Basic
        @get:Column(name = "edad")
        var edad: Int = 0,
        @get:Basic
        @get:Column(name = "fecha")
        var fecha: Timestamp? = Timestamp(System.currentTimeMillis()),
        @get:Basic
        @get:Column(name = "estado")
        var estado: Int = 0,
        @get:Basic
        @get:Column(name = "fechaCita")
        var fechaCita: Timestamp? = null,
        @get:Basic
        @get:Column(name = "fechaDescartado")
        var fechaDescartado: Timestamp? = Timestamp(System.currentTimeMillis()),
        @get:Basic
        @get:Column(name = "documentos")
        var documentos: String? = null,
        @get:Basic
        @get:Column(name = "comentarios")
        var comentarios: String? = null,
        @get:Basic
        @get:Column(name = "idBolsaTrabajo")
        var idBolsaTrabajo: Int = 0,
        @get:Basic
        @get:Column(name = "idReclutador")
        var idReclutador: Long = 0,
        @get:Basic
        @get:Column(name = "idVacante")
        var idVacante: Int = 0

)

