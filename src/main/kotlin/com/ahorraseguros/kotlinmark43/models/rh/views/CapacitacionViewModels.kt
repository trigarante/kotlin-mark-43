package com.ahorraseguros.kotlinmark43.models.rh.views

import java.sql.Date
import java.sql.Timestamp
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "capacitacionView")
data class CapacitacionViewModels(
        @Id
        val id: Long = 0,
        val idCandidato: Long = 0,
        val comentarios: String = "",
        val idCapacitador: Long = 0,
        val asistencias: String = "",
        val calificacionExamen: Double = 0.0,
        val comentariosReclutador: String = "",
        val nombre: String = "",
        val apellidoPaterno: String = "",
        val apellidoMaterno: String = "",
        val email: String = "",
        val empleado: String = "",
        val calificacion: Long = 0,
        val idCompetenciaCandidato: Long = 0,
        val idCalificacionCompetencia: Long = 0,
        val competenciaCandidato: String = "",
        val competencias: String = "",
        val escala: Long = 0,
        val idPrecandidato: Long = 0,
        val idEtapa: Long = 0,
        val idEstado: Long = 0,
        val fechaIngreso: Date? = null,
        val idSede: Int? = null,
        val sede: String? = null,
        val idSubarea: Int? = null,
        val subarea: String? = null,
        val fechaRegistro: Timestamp? = Timestamp(System.currentTimeMillis())
        )