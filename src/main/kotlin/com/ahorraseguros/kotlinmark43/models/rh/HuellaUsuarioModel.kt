package com.ahorraseguros.kotlinmark43.models.rh

import javax.persistence.*
import java.util.Arrays
import java.util.Objects

@Entity
@Table(name = "huellaUsuario")
data class HuellaUsuarioModel(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int = 0,


        var huella: ByteArray? = null
)

