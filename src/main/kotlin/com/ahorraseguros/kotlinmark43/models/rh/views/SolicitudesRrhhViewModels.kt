package com.ahorraseguros.kotlinmark43.models.rh.views

import java.sql.Timestamp
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name = "solicitudesRRHHView")
data class SolicitudesRrhhViewModels(
        @Id
        val id: Long = 0,
        val nombre: String = "",
        val apellidoPaterno: String = "",
        val apellidoMaterno: String = "",
        val telefono: String = "",
        val correo: String = "",
        val fecha: Timestamp?= Timestamp(System.currentTimeMillis()),
        val idReclutador: Long = 0,
        val estado: String = "",
        val fechaCita: Timestamp?=Timestamp(System.currentTimeMillis()),
        val idBolsaTrabajo: Int = 0,
        val idVacante: Int = 0,
        val empleado: String = "",
        val bolsaTrabajo: String = "",
        val tipoBolsa: String = "",
        val vacante: String = "",
        val idSubarea: Int = 0,
        val idPuesto: Int = 0,
        val idTipoPuesto: Int = 0,
        val idArea: Int = 0,
//        val idTipoUsuario: Int = 0,
        val subarea: String = "",
        val puesto: String = "",
        val puestoTipo: String = "",
//        val tipoUsuario: String = "",
        val area: String = "",
        val idEmpresa: Int = 0,
        val empresa: String ="",
        val cp: String = "",
        val edad: Int = 0,
        val documentos: String = "",
        val idSede: Int = 0,
        val sede: String = "",
        val idEstadoRH: Int = 0,
        val motivo: String = "",
        val idEtapa: Long = 0
)