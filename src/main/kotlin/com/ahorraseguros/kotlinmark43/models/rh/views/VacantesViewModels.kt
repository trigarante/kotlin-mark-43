package com.ahorraseguros.kotlinmark43.models.rh.views

import java.sql.Date
import java.sql.Timestamp
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "vacantesView")
data class VacantesViewModels(
    @Id
    val id: Long = 0,
    val nombre: String = "",
    val idSubarea: Int = 0,
    val idPuesto: Int = 0,
    val idTipoPuesto: Int = 0,
    val idArea: Int = 0,
    val subarea: String = "",
    val puesto: String = "",
    val puestoTipo: String = "",
//    val tipoUsuario: String = "",
    val area: String = "",
//    val idTipoUsuario: Int = 0,
    val sede: String = "",
    val idSede: Int = 0,
    val empresa: String = "",
    val idEmpresa: Int = 0,
    val activo: Int = 0,
    val idGrupo: Int = 0,
    val grupo: String = ""
)