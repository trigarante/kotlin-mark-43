package com.ahorraseguros.kotlinmark43.models.rh

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "biometricos")
class BiometricosModels {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "ip")
    var ip: String? = null
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
}
