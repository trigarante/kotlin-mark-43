package com.ahorraseguros.kotlinmark43.models.rh

import javax.persistence.*
import java.sql.Timestamp

@Entity
@Table(name = "candidato")
data class CandidatoModels(
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0,
    @get:Basic
    @get:Column(name = "calificacionPsicometrico")
    var calificacionPsicometrico:Boolean = false,
    @get:Basic
    @get:Column(name = "calificacionExamen")
    var calificacionExamen: Double = 0.toDouble(),
    @get:Basic
    @get:Column(name = "comentarios")
    var comentarios: String? = null,
    @get:Basic
    @get:Column(name = "fechaIngreso")
    var fechaIngreso: Timestamp? = Timestamp(System.currentTimeMillis()),
    @get:Basic
    @get:Column(name = "idPrecandidato")
    var idPrecandidato: Long = 0
//    @get:OneToMany(mappedBy = "candidatoByIdCandidato")
//    var capacitacionsById: Collection<CapacitacionModels>? = null
//    @get:OneToMany(mappedBy = "empleadoByIdCandidato")
//    var empleadosById: Collection<EmpleadoModels>? = null

)