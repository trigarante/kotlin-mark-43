package com.ahorraseguros.kotlinmark43.models.rh

import com.ahorraseguros.kotlinmark43.models.TI.UsuariosModels
import javax.persistence.*
import java.sql.Timestamp

@Entity
@Table(name = "seguimiento")
data class SeguimientoModels(
        @get:Id
        @get:Column(name = "id")
        var id: Int = 0,
        @get:Basic
        @get:Column(name = "idCapacitacion" )
        var idCapacitacion: Int? = 0,
        @get:Basic
        @get:Column(name = "fechaRegistro")
        var fechaRegistro: Timestamp? = Timestamp(System.currentTimeMillis()),
        @get:Basic
        @get:Column(name = "fechaIngreso")
        var fechaIngreso: Timestamp? = null,
        @get:Basic
        @get:Column(name = "comentarios")
        var comentarios: String? = null,
        @get:Basic
        @get:Column(name = "asistencia")
        var asistencia: String? = null,
        @get:Basic
        @get:Column(name = "registro")
        var registro: String? = null,
        @get:Basic
        @get:Column(name = "comentariosFaseDos")
        var comentariosFaseDos: String? = null,
        @get:Basic
        @get:Column(name = "calificacionRollPlay")
        var calificacionRollPlay: Boolean? = false,
        @get:Basic
        @get:Column(name = "idSubarea")
        var idSubarea: Long? = 0,
        @get:Basic
        @get:Column(name = "idCoach")
        var idCoach: Long = 0
)
