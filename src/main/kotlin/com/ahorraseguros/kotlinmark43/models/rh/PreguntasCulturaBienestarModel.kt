package com.ahorraseguros.kotlinmark43.models.rh

import javax.persistence.*
import java.sql.Timestamp
import java.util.Objects

@Entity
@Table(name = "preguntasCulturaBienestar")
class PreguntasCulturaBienestarModel {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "pregunta")
    var pregunta: String? = null
    @get:Basic
    @get:Column(name = "fechaInicioVigencia")
    var fechaInicioVigencia: Timestamp? = null
    @get:Basic
    @get:Column(name = "fechaFinVigencia")
    var fechaFinVigencia: Timestamp? = null
    @get:Basic
    @get:Column(name = "idArea")
    var idArea: Int = 0
}
