package com.ahorraseguros.kotlinmark43.models.rh

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "vacantes")
data class VacantesModels (
    @Id
    @Column(name = "id")
    var id: Long = 0,
    @Basic
    @Column(name = "idPuesto")
    var idPuesto: Int = 0,
    @Basic
    @Column(name = "idTipoPuesto")
    var idTipoPuesto: Int = 0,
    @Basic
    @Column(name = "idSubarea")
    var idSubarea: Int = 0,
    @Basic
    @Column(name = "nombre")
    var nombre: String = "",
    @Basic
    @Column(name = "activo")
    var activo: Int  = 0
)