package com.ahorraseguros.kotlinmark43.models.rh.views

import java.sql.Date
import java.sql.Timestamp
import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "precandidatoView")
class PrecandidatoViewModels {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idSolicitudRRHH")
    var idSolicitudRrhh: Long? = null
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "apellidoPaterno")
    var apellidoPaterno: String? = null
    @get:Basic
    @get:Column(name = "apellidoMaterno")
    var apellidoMaterno: String? = null
    @get:Basic
    @get:Column(name = "fechaNacimiento")
    var fechaNacimiento: Date? = null
    @get:Basic
    @get:Column(name = "email")
    var email: String? = null
    @get:Basic
    @get:Column(name = "genero")
    var genero: String? = null
    @get:Basic
    @get:Column(name = "idEstadoCivil")
    var idEstadoCivil: Int = 0
    @get:Basic
    @get:Column(name = "idEscolaridad")
    var idEscolaridad: Int = 0
    @get:Basic
    @get:Column(name = "idEstadoEscolaridad")
    var idEstadoEscolaridad: Int = 0
    @get:Basic
    @get:Column(name = "idPais")
    var idPais: Int = 0
    @get:Basic
    @get:Column(name = "cp")
    var cp: Int? = 0
    @get:Basic
    @get:Column(name = "idColonia")
    var idColonia: Int? = 0
    @get:Basic
    @get:Column(name = "calle")
    var calle: String? = null
    @get:Basic
    @get:Column(name = "numeroExterior")
    var numeroExterior: String? = null
    @get:Basic
    @get:Column(name = "numeroInterior")
    var numeroInterior: String? = null
    @get:Basic
    @get:Column(name = "telefonoFijo")
    var telefonoFijo: Long? = null
    @get:Basic
    @get:Column(name = "telefonoMovil")
    var telefonoMovil: Long? = 0
    @get:Basic
    @get:Column(name = "idEstadoRH")
    var idEstadoRH: Int = 0
    @get:Basic
    @get:Column(name = "fechaCreacion")
    var fechaCreacion: Timestamp? = null
    @get:Basic
    @get:Column(name = "fechaBaja")
    var fechaBaja: Timestamp? = null
    @get:Basic
    @get:Column(name = "tiempoTraslado")
    var tiempoTraslado: Int = 0
    @get:Basic
    @get:Column(name = "idMedioTraslado")
    var idMedioTraslado: Int = 0
    @get:Basic
    @get:Column(name = "idEstacion")
    var idEstacion: Int? = null
    @get:Basic
    @get:Column(name = "estadoCivil")
    var estadoCivil: String? = null
    @get:Basic
    @get:Column(name = "escolaridad")
    var escolaridad: String? = null
    @get:Basic
    @get:Column(name = "estadoEscolar")
    var estadoEscolar: String? = null
    @get:Basic
    @get:Column(name = "estadoRH")
    var estadoRh: String? = null
    @get:Basic
    @get:Column(name = "medio")
    var medio: String? = null
    @get:Basic
    @get:Column(name = "estacion")
    var estacion: String? = null
    @get:Basic
    @get:Column(name = "lineas")
    var lineas: String? = null
    @get:Basic
    @get:Column(name = "idEtapa")
    var idEtapa: Int = 0
    @get:Basic
    @get:Column(name = "curp")
    var curp: String? = null
    @get:Basic
    @get:Column(name = "recontratable")
    var recontratable: Byte = 0
    @get:Basic
    @get:Column(name = "detalleBaja")
    var detalleBaja: String? = null
    @get:Basic
    @get:Column(name = "fechaRegistroBaja")
    var fechaRegistroBaja: Timestamp? = null
    @get:Basic
    @get:Column(name = "nombreEtapa")
    var nombreEtapa: String? = null
    @get:Basic
    @get:Column(name = "idEstadoRRHH")
    var idEstadoRRHH: Int? = null
    @get:Basic
    @get:Column(name = "subarea")
    var subarea: String? = null
    @get:Basic
    @get:Column(name = "sede")
    var sede: String? = null
    @get:Basic
    @get:Column(name = "idSubarea")
    var idSubarea: Int? = null
    @get:Basic
    @get:Column(name = "idSede")
    var idSede: Int? = null
    @get:Basic
    @get:Column(name = "idGrupo")
    var idGrupo: Int? = null
    @get:Basic
    @get:Column(name = "grupo")
    var grupo: String? = null
    @get:Basic
    @get:Column(name = "recontratables")
    var recontratables: String? = null
    @get:Basic
    @get:Column(name = "colonia")
    var colonia: String? = null
}
