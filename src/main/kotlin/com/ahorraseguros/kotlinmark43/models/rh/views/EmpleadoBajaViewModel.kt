package com.ahorraseguros.kotlinmark43.models.rh.views

import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "empleadoBajaView", schema = "trigarante2020-P", catalog = "")
class EmpleadoBajaViewModel {
    @get:Id
    @get:Column(name = "empleadoID")
    var empleadoId: Long = 0
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "apellidoPaterno")
    var apellidoPaterno: String? = null
    @get:Basic
    @get:Column(name = "apellidoMaterno")
    var apellidoMaterno: String? = null
    @get:Basic
    @get:Column(name = "baja")
    var baja: String? = null
    @get:Basic
    @get:Column(name = "registroBaja")
    var registroBaja: String? = null
    @get:Basic
    @get:Column(name = "tiempoEmpresa")
    var tiempoEmpresa: Int? = null
    @get:Basic
    @get:Column(name = "areaID")
    var areaId: Int? = null
    @get:Basic
    @get:Column(name = "area")
    var area: String? = null
    @get:Basic
    @get:Column(name = "subareaID")
    var subareaId: Int? = null
    @get:Basic
    @get:Column(name = "subarea")
    var subarea: String? = null
    @get:Basic
    @get:Column(name = "detalleBaja")
    var detalleBaja: String? = null
}
