package com.ahorraseguros.kotlinmark43.models.rh

import javax.persistence.*
import java.sql.Timestamp
import java.util.Objects

@Entity
@Table(name = "pagina")
class PaginaModels {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "url")
    var url: String? = null
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "status")
    var status: Int = 0
    @get:Basic
    @get:Column(name = "fechaegistro")
    var fechaegistro: Timestamp? = null
    @get:Basic
    @get:Column(name = "fecha_actualizacion")
    var fechaActualizacion: Timestamp? = null
    @get:Basic
    @get:Column(name = "idCampana")
    var idCampana: Int = 0
    @get:Basic
    @get:Column(name = "idTipo")
    var idTipo: Int = 0
    @get:Basic
    @get:Column(name = "configuracion")
    var configuracion: String? = null


}
