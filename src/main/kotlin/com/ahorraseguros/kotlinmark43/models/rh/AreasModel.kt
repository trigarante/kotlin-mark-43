package com.ahorraseguros.kotlinmark43.models.rh

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "areas")
class AreasModel {
    @Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "idSede")
    var idSede: Int = 0
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "codigo")
    var codigo: String? = null
    @get:Basic
    @get:Column(name = "idMarcaEmpresarial")
    var idMarcaEmpresarial: Int = 0
}
