package com.ahorraseguros.kotlinmark43.models.rh

import java.sql.Timestamp
import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "contadorCulturaBienestar")
data class ContadorCulturaBienestarModel(
        @get:Basic
        @get:Column(name = "idRespuesta")
        var idRespuesta: Int? = null,
        @get:Basic
        @get:Column(name = "fecha")
        var fecha: Timestamp? = null,
        @get:Id
        @get:Column(name = "id")
        var id: Int = 0
)
