package com.ahorraseguros.kotlinmark43.models.rh.views

import java.sql.Timestamp
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "seguimientoView")

data class SeguimientoViewModels (
        @Id
        val id: Long=0,
        val calificacion: Double=0.0,
        val comentariosCapacitacion: String="",
        val asistenciaCapacitacion: String="",
        val calificacionExamen: Double=0.0,
        val comentariosReclutador: String="",
        val nombre: String="",
        val apellidoPaterno: String="",
        val apellidoMaterno: String="",
        val email: String="",
        val empleado: String="",
        val fechaIngreso: Timestamp= Timestamp(System.currentTimeMillis()),
        val calificacionRollplay: Double=0.0,
        val idCapacitacion: Long=0,
        val comentarios: String="",
        val idCoach: Long=0,
        val asistencia: String="",
        val registro: String="",
        val comentariosFaseDos: String="",
        val idCandidato: Long=0,
        val idPrecandidato: Long=0,
        val idEtapa: Long=0,
        val nombreSubarea: String="",
        val idEstadoRRHH: Int=0,
        val idSede: Int? = null,
        val sede: String? = null,
        val idSubarea: Int? = null,
        val subarea: String? = null
)