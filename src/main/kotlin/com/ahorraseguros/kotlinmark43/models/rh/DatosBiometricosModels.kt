package com.ahorraseguros.kotlinmark43.models.rh

import javax.persistence.*
import java.sql.Date
import java.sql.Time

@Entity
@Table(name = "datosBiometricos")
data class DatosBiometricosModels (
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0,
    @get:Basic
    @get:Column(name = "idEmpleado")
    var idEmpleado: Long = 0,
    @get:Basic
    @get:Column(name = "entradaSalida")
    var entradaSalida: String? = null,
    @get:Basic
    @get:Column(name = "fecha")
    var fecha: Date? = null,
    @get:Basic
    @get:Column(name = "hora")
    var hora: Time? = null,
    @get:ManyToOne
    @get:JoinColumn(name = "idBiometrico", referencedColumnName = "id", nullable = false)
    var biometricosByIdBiometrico: BiometricosModels? = null


)
