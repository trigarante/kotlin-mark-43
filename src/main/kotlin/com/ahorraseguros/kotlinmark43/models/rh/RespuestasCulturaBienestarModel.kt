package com.ahorraseguros.kotlinmark43.models.rh

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "respuestasCulturaBienestar", schema = "mark44", catalog = "")
class RespuestasCulturaBienestarModel {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "respuestas")
    var respuestas: String? = null
    @get:Basic
    @get:Column(name = "icono")
    var icono: String? = null
    @get:Basic
    @get:Column(name = "idPregunta")
    var idPregunta: Int = 0
    @get:Basic
    @get:Column(name = "color")
    var color: String? = null
    @get:Basic
    @get:Column(name = "fechaCreacion")
    var fechaCreacion: String? = null
}
