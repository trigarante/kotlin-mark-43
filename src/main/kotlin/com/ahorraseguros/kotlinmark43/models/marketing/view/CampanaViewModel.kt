package com.ahorraseguros.kotlinmark43.models.marketing.view

import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "campanaView")
class CampanaViewModel {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
    @get:Basic
    @get:Column(name = "idEstadoCampana")
    var idEstadoCampana: Int = 0
    @get:Basic
    @get:Column(name = "estado")
    var estado: String? = null
    @get:Basic
    @get:Column(name = "idSubRamo")
    var idSubRamo: Int = 0
    @get:Basic
    @get:Column(name = "subRamoDescripcion")
    var subRamoDescripcion: String? = null
}
