package com.ahorraseguros.kotlinmark43.models.marketing

import javax.persistence.*
import java.sql.Timestamp
import java.util.Objects

@Entity
@Table(name = "pagina")
class PaginaModel {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idCampana")
    var idCampana: Int = 0
    @get:Basic
    @get:Column(name = "idTipoPagina")
    var idTipoPagina: Int = 0
    @get:Basic
    @get:Column(name = "url")
    var url: String? = null
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "fechaRegistro")
    var fechaRegistro: Timestamp? = null
    @get:Basic
    @get:Column(name = "fechaActualizacion")
    var fechaActualizacion: Timestamp? = null
    @get:Basic
    @get:Column(name = "configuracion")
    var configuracion: String? = null
    @get:Column(name = "activo")
    var activo: Int = 0
}
