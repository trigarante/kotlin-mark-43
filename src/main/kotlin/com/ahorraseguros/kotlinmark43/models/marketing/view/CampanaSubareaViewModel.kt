package com.ahorraseguros.kotlinmark43.models.marketing.view

import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "campanaSubareaView")
class CampanaSubareaViewModel {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idCampana")
    var idCampana: Long = 0
    @get:Basic
    @get:Column(name = "idSubarea")
    var idSubarea: Int = 0
    @get:Basic
    @get:Column(name = "descripcionn")
    var descripcionn: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "subarea")
    var subarea: String? = null

}
