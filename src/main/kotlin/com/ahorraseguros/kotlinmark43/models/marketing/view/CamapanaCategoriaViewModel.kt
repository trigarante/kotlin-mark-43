package com.ahorraseguros.kotlinmark43.models.marketing.view

import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "camapanaCategoriaView")
class CamapanaCategoriaViewModel {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idCampana")
    var idCampana: Long = 0
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
    @get:Basic
    @get:Column(name = "comentarios")
    var comentarios: String? = null
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "idSubRamo")
    var idSubRamo: Int = 0
    @get:Basic
    @get:Column(name = "prioridad")
    var prioridad: Int = 0
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
}
