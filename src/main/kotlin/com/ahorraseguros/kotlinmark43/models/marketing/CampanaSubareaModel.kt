package com.ahorraseguros.kotlinmark43.models.marketing

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "campanaSubarea")
class CampanaSubareaModel {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idCampana")
    var idCampana: Long = 0
    @get:Basic
    @get:Column(name = "idSubarea")
    var idSubarea: Int = 0
    @get:Basic
    @get:Column(name = "descripcionn")
    var descripcionn: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
}
