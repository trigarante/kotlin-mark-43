package com.ahorraseguros.kotlinmark43.models.marketing.view

import java.sql.Timestamp
import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "paginaView")
class PaginaViewModel {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idTipoPagina")
    var idTipoPagina: Int = 0
    @get:Basic
    @get:Column(name = "idCampana")
    var idCampana: Long = 0
    @get:Basic
    @get:Column(name = "url")
    var url: String? = null
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "fechaRegistro")
    var fechaRegistro: Timestamp? = null
    @get:Basic
    @get:Column(name = "fechaActualizacion")
    var fechaActualizacion: Timestamp? = null
    @get:Basic
    @get:Column(name = "configuracion")
    var configuracion: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
    @get:Basic
    @get:Column(name = "tipo")
    var tipo: String? = null
    @get:Basic
    @get:Column(name = "objetivo")
    var objetivo: String? = null
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
}
