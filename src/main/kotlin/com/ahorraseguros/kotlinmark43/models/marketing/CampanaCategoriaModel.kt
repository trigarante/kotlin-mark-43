package com.ahorraseguros.kotlinmark43.models.marketing

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "campanaCategoria")
class CampanaCategoriaModel {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idCampana")
    var idCampana: Int = 0
    @get:Basic
    @get:Column(name = "idSubRamo")
    var idSubRamo: Int = 0
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
    @get:Basic
    @get:Column(name = "comentarios")
    var comentarios: String? = null

}
