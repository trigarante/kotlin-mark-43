package com.ahorraseguros.kotlinmark43.models.marketing.view

import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "mediosDifusionViews")
class MediosDifusionViewsModel {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "idProveedor")
    var idProveedor: Int = 0
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "idSubarea")
    var idSubarea: Int = 0
    @get:Basic
    @get:Column(name = "subarea")
    var subarea: String? = null
    @get:Basic
    @get:Column(name = "idPagina")
    var idPagina: Int = 0
    @get:Basic
    @get:Column(name = "descripcionPagina")
    var descripcionPagina: String? = null
    @get:Basic
    @get:Column(name = "idPresupuesto")
    var idPresupuesto: Int = 0
    @get:Basic
    @get:Column(name = "configuracion")
    var configuracion: String? = null
    @get:Basic
    @get:Column(name = "candidad")
    var candidad: Double = 0.toDouble()

}
