package com.ahorraseguros.kotlinmark43.models.finanzas.views

import java.sql.Date
import java.sql.Timestamp
import javax.persistence.*

@Entity
@Table(name = "aplicacionesView")
class AplicacionesViewModel {
    @Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Column(name = "idEmpleado")
    @get:Basic
    var idEmpleado: Long = 0
    @get:Column(name = "idProducto")
    @get:Basic
    var idProducto: Long = 0
    @get:Column(name = "idTipoPago")
    @get:Basic
    var idTipoPago = 0
    @get:Column(name = "idProductoSocio")
    @get:Basic
    var idProductoSocio: Long = 0
    @get:Column(name = "idFlujoPoliza")
    @get:Basic
    var idFlujoPoliza = 0
    @get:Column(name = "poliza")
    @get:Basic
    var poliza: String? = null
    @get:Column(name = "fechaInicio")
    @get:Basic
    var fechaInicio: Date? = null
    @get:Column(name = "primaNeta")
    @get:Basic
    var primaNeta = 0.0
    @get:Column(name = "fechaRegistro")
    @get:Basic
    var fechaRegistro: Timestamp? = null
    @get:Column(name = "archivo")
    @get:Basic
    var archivo: String? = null
    @get:Column(name = "cantidadPagos")
    @get:Basic
    var cantidadPagos: Int = 0
    @get:Column(name = "tipoPago")
    @get:Basic
    var tipoPago: String? = null
    @get:Column(name = "pruductoSocio")
    @get:Basic
    var pruductoSocio: String? = null
    @get:Column(name = "datos")
    @get:Basic
    var datos: String? = null
    @get:Column(name = "idEstadoPoliza")
    @get:Basic
    var idEstadoPoliza = 0
    @get:Column(name = "estado")
    @get:Basic
    var estado: String? = null
    @get:Column(name = "idArea")
    @get:Basic
    var idArea = 0
    @get:Column(name = "nombre")
    @get:Basic
    var nombre: String? = null
    @get:Column(name = "apellidoPaterno")
    @get:Basic
    var apellidoPaterno: String? = null
    @get:Column(name = "apellidoMaterno")
    @get:Basic
    var apellidoMaterno: String? = null
    @get:Column(name = "idSede")
    @get:Basic
    var idSede = 0
    @get:Column(name = "idGrupo")
    @get:Basic
    var idGrupo = 0
    @get:Column(name = "idPuesto")
    @get:Basic
    var idPuesto = 0
    @get:Column(name = "idEmpresa")
    @get:Basic
    var idEmpresa = 0
    @get:Column(name = "idSubarea")
    @get:Basic
    var idSubarea = 0
    @get:Column(name = "idCliente")
    @get:Basic
    var idCliente: Long = 0
    @get:Column(name = "idSolictud")
    @get:Basic
    var idSolictud: Long = 0
    @get:Column(name = "descripcion")
    @get:Basic
    var descripcion: String? = null
    @get:Column(name = "idSubRamo")
    @get:Basic
    var idSubRamo = 0
    @get:Column(name = "nombreCliente")
    @get:Basic
    var nombreCliente: String? = null
    @get:Column(name = "apellidoPaternoCliente")
    @get:Basic
    var apellidoPaternoCliente: String? = null
    @get:Column(name = "apellidoMaternoCliente")
    @get:Basic
    var apellidoMaternoCliente: String? = null
    @get:Column(name = "idRecibo")
    @get:Basic
    var idRecibo: Long = 0
    @get:Column(name = "estadoRecibo")
    @get:Basic
    var estadoRecibo: Long = 0
}