package com.ahorraseguros.kotlinmark43.models.finanzas.views

import java.sql.Timestamp
import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "gestionIngresosView")
class GestionIngresosViewModel {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idSubarea")
    var idSubarea: Int = 0
    @get:Basic
    @get:Column(name = "idTipoIngreso")
    var idTipoIngreso: Int = 0
    @get:Basic
    @get:Column(name = "cantidad")
    var cantidad: Double = 0.toDouble()
    @get:Basic
    @get:Column(name = "fecha")
    var fecha: Timestamp? = null
    @get:Basic
    @get:Column(name = "fechaRegistro")
    var fechaRegistro: Timestamp? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "subarea")
    var subarea: String? = null
    @get:Basic
    @get:Column(name = "ingresos")
    var ingresos: Int = 0

}
