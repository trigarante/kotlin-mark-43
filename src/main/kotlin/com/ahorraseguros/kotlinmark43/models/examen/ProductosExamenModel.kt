package com.ahorraseguros.kotlinmark43.models.examen

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "productosExamen")
class ProductosExamenModel {
    @get:Column(name = "id")
    @get:Id
    var id: Long = 0
    @get:Column(name = "nombre")
    @get:Basic
    var nombre: String? = null
    @get:Column(name = "precio")
    @get:Basic
    var precio: Double? = null
    @get:Column(name = "cantidadDisponible")
    @get:Basic
    var cantidadDisponible: Int? = null
}