package com.ahorraseguros.kotlinmark43.models.examen

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "examen", schema = "trigarante2020-P", catalog = "")
class ExamenModel {
    @Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "edad")
    var edad: Int? = null
    @get:Basic
    @get:Column(name = "genero")
    var genero: String? = null
    @get:Basic
    @get:Column(name = "correo")
    var correo: String? = null
}
