package com.ahorraseguros.kotlinmark43.models.catalogosTI

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "tipoLog")
class TipoLogModels {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "tipo")
    var tipo: String? = null
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
}
