package com.ahorraseguros.kotlinmark43.models.catalogosTI

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "equiposAS")
class EquiposAsModel {
    @get:Id
    @get:Column(name = "id", nullable = false)
    var id: Int = 0
    @get:Basic
    @get:Column(name = "equipo", nullable = false, length = 255)
    var equipo: String? = null
    @get:Basic
    @get:Column(name = "activo", nullable = false)
    var activo: Int = 0
}
