package com.ahorraseguros.kotlinmark43.models.catalogosSolicitudes

import javax.persistence.*

@Entity
@Table(name = "campana")
class CampanaModels (
    @Id
    @Column(name = "id")
    var id: Int = 0,
    @Basic
    @Column(name = "nombre")
    var nombre: String? = null,
    @Basic
    @Column(name = "activo")
    var activo: Int = 0

)
