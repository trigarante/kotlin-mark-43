package com.ahorraseguros.kotlinmark43.models.catalogosSolicitudes

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "estadoWS")
class EstadoWsModels {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "estado")
    var estado: String? = null
}
