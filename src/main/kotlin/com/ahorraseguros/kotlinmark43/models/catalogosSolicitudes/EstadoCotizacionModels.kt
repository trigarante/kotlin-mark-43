package com.ahorraseguros.kotlinmark43.models.catalogosSolicitudes

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "estadoCotizacion")
class EstadoCotizacionModels {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "estado")
    var estado: String? = null
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
}
