package com.ahorraseguros.kotlinmark43.models.catalogosSolicitudes

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "tipoContacto")
class TipoContactoModels {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
}
