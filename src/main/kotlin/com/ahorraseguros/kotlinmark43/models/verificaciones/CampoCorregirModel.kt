package com.ahorraseguros.kotlinmark43.models.verificaciones

import javax.persistence.*

@Entity
@Table(name = "campoCorregir")
data class CampoCorregirModel (
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0,
    @get:Basic
    @get:Column(name = "idTabla")
    var idTabla: Int = 0,
    @get:Basic
    @get:Column(name = "campo")
    var campo: String = "",
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null,
    @get:Basic
    @get:Column(name = "activo")
    var activo: Boolean = false
)
