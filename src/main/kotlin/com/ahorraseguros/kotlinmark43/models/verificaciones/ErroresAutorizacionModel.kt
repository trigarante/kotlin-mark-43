package com.ahorraseguros.kotlinmark43.models.verificaciones

import javax.persistence.*

@Entity
@Table(name = "erroresAutorizacion")
data class ErroresAutorizacionModel (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @get:Column(name = "id")
    var id: Long = 0,
    @get:Basic
    @get:Column(name = "idAutorizacionRegistro")
    var idAutorizacionRegistro: Long = 0,
    @get:Basic
    @get:Column(name = "idEmpleado")
    var idEmpleado: Long = 0,
    @get:Basic
    @get:Column(name = "idEstadoCorrecion")
    var idEstadoCorrecion: Int = 0,
    @get:Basic
    @get:Column(name = "idCampoCorregir")
    var idCampoCorregir: Int = 0,
    @get:Basic
    @get:Column(name = "idEmpleadoCorreccion")
    var idEmpleadoCorreccion: Long = 0,
    @get:Basic
    @get:Column(name = "comentario")
    var comentario: String? = null
)
