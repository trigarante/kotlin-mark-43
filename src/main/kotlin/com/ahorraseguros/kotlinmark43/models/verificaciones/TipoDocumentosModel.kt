package com.ahorraseguros.kotlinmark43.models.verificaciones

import javax.persistence.*

@Entity
@Table(name = "tipoDocumentos")
data class TipoDocumentosModel (
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0,
    @get:Basic
    @get:Column(name = "idTabla")
    var idTabla: Int = 0,
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null,
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null,
    @get:Basic
    @get:Column(name = "activo")
    var activo: Boolean = false
)
