package com.ahorraseguros.kotlinmark43.models.verificaciones.view

import javax.persistence.*

@Entity
@Table(name = "autorizacionRegistroView")
class AutorizacionRegistroViewModel {
    @Id
    @get:Column(name = "id")
    @get:Basic
    var id: Long = 0
    @get:Column(name = "idRegistro")
    @get:Basic
    var idRegistro: Long = 0
    @get:Column(name = "idEstadoVerificacion")
    @get:Basic
    var idEstadoVerificacion = 0
    @get:Column(name = "numero")
    @get:Basic
    var numero = 0
    @get:Column(name = "verificadoCliente")
    @get:Basic
    var verificadoCliente: Byte = 0
    @get:Column(name = "verificadoProducto")
    @get:Basic
    var verificadoProducto: Byte = 0
    @get:Column(name = "verificadoRegistro")
    @get:Basic
    var verificadoRegistro: Byte = 0
    @get:Column(name = "verificadoPago")
    @get:Basic
    var verificadoPago: Byte = 0
    @get:Column(name = "verificadoAnexos")
    @get:Basic
    var verificadoAnexos: Byte = 0
    @get:Column(name = "verificadoInspeccion")
    @get:Basic
    var verificadoInspeccion: Byte = 0
    @get:Column(name = "idEmpleado")
    @get:Basic
    var idEmpleado: Long = 0
    @get:Column(name = "poliza")
    @get:Basic
    var poliza: String? = null
    @get:Column(name = "idCliente")
    @get:Basic
    var idCliente: Long = 0
    @get:Column(name = "nombreCliente")
    @get:Basic
    var nombreCliente: String? = null
    @get:Column(name = "apellidoPaternoCliente")
    @get:Basic
    var apellidoPaternoCliente: String? = null
    @get:Column(name = "apellidoMaternoCliente")
    @get:Basic
    var apellidoMaternoCliente: String? = null
    @get:Column(name = "carpetaRegistro")
    @get:Basic
    var carpetaRegistro: String? = null
    @get:Column(name = "archivoPago")
    @get:Basic
    var archivoPago: String? = null
    @get:Column(name = "estadoVerificacion")
    @get:Basic
    var estadoVerificacion: String? = null
}