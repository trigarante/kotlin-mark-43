package com.ahorraseguros.kotlinmark43.models.verificaciones

import javax.persistence.*

@Entity
@Table(name = "autorizacionRegistro")
data class AutorizacionRegistroModel (
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0,
    @get:Basic
    @get:Column(name = "idRegistro")
    var idRegistro: Long = 0,
    @get:Basic
    @get:Column(name = "idEstadoVerificacion")
    var idEstadoVerificacion: Int = 1,
    @get:Basic
    @get:Column(name = "numero")
    var numero: Int = 0,
    @get:Basic
    @get:Column(name = "verificadoCliente")
    var verificadoCliente: Boolean = false,
    @get:Basic
    @get:Column(name = "verificadoProducto")
    var verificadoProducto: Boolean = false,
    @get:Basic
    @get:Column(name = "verificadoRegistro")
    var verificadoRegistro: Boolean = false,
    @get:Basic
    @get:Column(name = "verificadoPago")
    var verificadoPago: Boolean = false,
    @get:Basic
    @get:Column(name = "verificadoAnexos")
    var verificadoAnexos: Boolean = false,
    @get:Basic
    @get:Column(name = "verificadoInspeccion")
    var verificadoInspeccion: Boolean = false
)
