package com.ahorraseguros.kotlinmark43.models.verificaciones

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "erroresAnexosA")
data class ErroresAnexosAModel (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @get:Column(name = "id")
    var id: Long = 0,
    @get:Basic
    @get:Column(name = "idAutorizacionRegistro")
    var idAutorizacionRegistro: Long = 0,
    @get:Basic
    @get:Column(name = "idEmpleado")
    var idEmpleado: Long = 0,
    @get:Basic
    @get:Column(name = "idEstadoCorreccion")
    var idEstadoCorreccion: Int = 0,
    @get:Basic
    @get:Column(name = "idTipoDocumento")
    var idTipoDocumento: Int = 0,
    @get:Basic
    @get:Column(name = "idEmpleadoCorreccion")
    var idEmpleadoCorreccion: Long = 0,
    @get:Basic
    @get:Column(name = "comentario")
    var comentario: String? = null
)
