package com.ahorraseguros.kotlinmark43.models.ventaNueva.view

import java.sql.Timestamp
import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "productoSolicitudView")
class ProductoSolicitudViewModel {
    @get:Basic
    @get:Column(name = "id")
    @Id
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idProspecto")
    var idProspecto: Long = 0
    @get:Basic
    @get:Column(name = "idTipoSubRamo")
    var idTipoSubRamo: Int = 0
    @get:Basic
    @get:Column(name = "datos")
    var datos: String? = null
    @get:Basic
    @get:Column(name = "correo")
    var correo: String? = null
    @get:Basic
    @get:Column(name = "numero")
    var numero: Long = 0
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "fechaCreacion")
    var fechaCreacion: Timestamp? = null
    @get:Basic
    @get:Column(name = "tipo")
    var tipo: String? = null
}
