package com.ahorraseguros.kotlinmark43.models.ventaNueva

import javax.persistence.*
import java.math.BigDecimal
import java.sql.Date
import java.sql.Timestamp
import java.util.Objects

@Entity
@Table(name = "registro")
class RegistroModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idEmpleado")
    var idEmpleado: Long = 0
    @get:Basic
    @get:Column(name = "idProducto")
    var idProducto: Int = 0
    @get:Basic
    @get:Column(name = "idTipoPago")
    var idTipoPago: Byte = 0
    @get:Basic
    @get:Column(name = "idEstadoPoliza")
    var idEstadoPoliza: Int = 0
    @get:Basic
    @get:Column(name = "idProductoSocio")
    var idProductoSocio: Int = 0
    @get:Basic
    @get:Column(name = "idFlujoPoliza")
    var idFlujoPoliza: Int = 0
    @get:Basic
    @get:Column(name = "poliza")
    var poliza: String? = null
    @get:Basic
    @get:Column(name = "fechaInicio")
    var fechaInicio: Date? = null
    @get:Basic
    @get:Column(name = "primaNeta")
    var primaNeta: BigDecimal? = null
    @get:Basic
    @get:Column(name = "archivo")
    var archivo: String = ""
}
