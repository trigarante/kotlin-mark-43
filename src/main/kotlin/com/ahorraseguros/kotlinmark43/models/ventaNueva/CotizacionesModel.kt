package com.ahorraseguros.kotlinmark43.models.ventaNueva

import javax.persistence.*
import java.sql.Timestamp
import java.util.Objects

@Entity
@Table(name = "cotizaciones")
class CotizacionesModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idProducto")
    var idProducto: Long = 0
    @get:Basic
    @get:Column(name = "idPagina")
    var idPagina: Int? = null
    @get:Basic
    @get:Column(name = "idMedioDifusion")
    var idMedioDifusion: Int? = null
    @get:Basic
    @get:Column(name = "idTipoContacto")
    var idTipoContacto: Int? = null
    @get:Basic
    @get:Column(name = "idEstadoCotizacion")
    var idEstadoCotizacion: Int? = null


//    @get:Basic
//    @get:Column(name = "fechaCreacion")
//    var fechaCreacion: Timestamp? = null

}
