package com.ahorraseguros.kotlinmark43.models.ventaNueva.view

import java.sql.Date
import java.sql.Timestamp
import javax.persistence.*

@Entity
@Table(name = "clienteView")
data class ClienteViewModel (
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0,
    @get:Basic
    @get:Column(name = "idPais")
    var idPais: Int = 0,
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null,
    @get:Basic
    @get:Column(name = "paterno")
    var paterno: String? = null,
    @get:Basic
    @get:Column(name = "materno")
    var materno: String? = null,
    @get:Basic
    @get:Column(name = "cp")
    var cp: Int = 0,
    @get:Basic
    @get:Column(name = "calle")
    var calle: String? = null,
    @get:Basic
    @get:Column(name = "numInt")
    var numInt: String? = null,
    @get:Basic
    @get:Column(name = "numExt")
    var numExt: String? = null,
    @get:Basic
    @get:Column(name = "idColonia")
    var idColonia: Int = 0,
    @get:Basic
    @get:Column(name= "colonia")
    var colonia: String = "",
    @get:Basic
    @get:Column(name = "genero")
    var genero: String? = null,
    @get:Basic
    @get:Column(name = "telefonoFijo")
    var telefonoFijo: String? = null,
    @get:Basic
    @get:Column(name = "telefonoMovil")
    var telefonoMovil: String? = null,
    @get:Basic
    @get:Column(name = "correo")
    var correo: String? = null,
    @get:Basic
    @get:Column(name = "fechaNacimiento")
    var fechaNacimiento: Date? = null,
    @get:Basic
    @get:Column(name = "fechaRegistro")
    var fechaRegistro: Timestamp? = null,
    @get:Basic
    @get:Column(name = "archivo")
    var archivo: String? = null,
    @get:Basic
    @get:Column(name = "curp")
    var curp: String? = null,
    @get:Basic
    @get:Column(name = "rfc")
    var rfc: String? = null,
    @get:Basic
    @get:Column(name = "nombrePaises")
    var nombrePaises: String? = null,
    @get:Basic
    @get:Column(name = "razonSocial")
    var razonSocial: Byte = 0
)
