package com.ahorraseguros.kotlinmark43.models.ventaNueva

import javax.persistence.*
import java.sql.Timestamp
import java.util.Objects

@Entity
@Table(name = "conductorHabitual", schema = "trigarante2020-P", catalog = "")
class ConductorHabitualModel {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idPais")
    var idPais: Int = 0
    @get:Basic
    @get:Column(name = "idRegistro")
    var idRegistro: Long = 0
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "paterno")
    var paterno: String? = null
    @get:Basic
    @get:Column(name = "materno")
    var materno: String? = null
    @get:Basic
    @get:Column(name = "correo")
    var correo: String? = null
    @get:Basic
    @get:Column(name = "cp")
    var cp: String? = null
    @get:Basic
    @get:Column(name = "idColonia")
    var idColonia: Int = 0
    @get:Basic
    @get:Column(name = "calle")
    var calle: String? = null
    @get:Basic
    @get:Column(name = "numInt")
    var numInt: String? = null
    @get:Basic
    @get:Column(name = "numExt")
    var numExt: String? = null
    @get:Basic
    @get:Column(name = "genero")
    var genero: String? = null
    @get:Basic
    @get:Column(name = "telefonoFijo")
    var telefonoFijo: String? = null
    @get:Basic
    @get:Column(name = "telefonoMovil")
    var telefonoMovil: String? = null
    @get:Basic
    @get:Column(name = "fechaNacimiento")
    var fechaNacimiento: Timestamp? = null
    @get:Basic
    @get:Column(name = "fechaRegistro")
    var fechaRegistro: Timestamp? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
    
}
