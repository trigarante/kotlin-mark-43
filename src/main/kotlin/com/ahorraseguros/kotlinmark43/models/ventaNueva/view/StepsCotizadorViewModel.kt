package com.ahorraseguros.kotlinmark43.models.ventaNueva.view

import javax.persistence.*

@Entity
@Table(name = "stepsCotizadorView")
data class StepsCotizadorViewModel (
    @Id
    @get:Column(name = "idSolictud")
    var idSolictud: Long = 0,
    @get:Basic
    @get:Column(name = "idProductoCliente")
    var idProductoCliente: Long? = null,
    @get:Basic
    @get:Column(name = "idCliente")
    var idCliente: Long? = null,
    @get:Basic
    @get:Column(name = "idRegistro")
    var idRegistro: Long? = null
)
