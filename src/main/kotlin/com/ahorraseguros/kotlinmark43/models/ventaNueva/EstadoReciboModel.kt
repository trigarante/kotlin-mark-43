package com.ahorraseguros.kotlinmark43.models.ventaNueva

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "estadoRecibo", schema = "trigarante2020-P", catalog = "")
class EstadoReciboModel {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "estdo")
    var estdo: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
}
