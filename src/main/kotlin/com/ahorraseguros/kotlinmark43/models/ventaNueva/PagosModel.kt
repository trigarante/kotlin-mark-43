package com.ahorraseguros.kotlinmark43.models.ventaNueva

import javax.persistence.*
import java.sql.Date
import java.sql.Timestamp
import java.util.Objects

@Entity
@Table(name = "pagos")
class PagosModel {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "idRecibo")
    var idRecibo: Int? = 0
    @get:Basic
    @get:Column(name = "idFormaPago")
    var idFormaPago: Int? = null
    @get:Basic
    @get:Column(name = "idEstadoPago")
    var idEstadoPago: Int? = null
    @get:Basic
    @get:Column(name = "fechaPago")
    var fechaPago: Date? = null
    @get:Basic
    @get:Column(name = "fechaRegistro")
    var fechaRegistro: Timestamp? = Timestamp(System.currentTimeMillis())
    @get:Basic
    @get:Column(name = "cantidad")
    var cantidad: Double? = null
    @get:Basic
    @get:Column(name = "archivo")
    var archivo: String? = null
    @get:Basic
    @get:Column(name = "datos")
    var datos: String? = null
}
