package com.ahorraseguros.kotlinmark43.models.ventaNueva.view

import java.sql.Date
import java.sql.Timestamp
import javax.persistence.*

@Entity
@Table(name = "recibosView")
class RecibosViewModel {
    @Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idRegistro")
    var idRegistro: Long = 0
    @get:Basic
    @get:Column(name = "idEmpleado")
    var idEmpleado: Long = 0
    @get:Basic
    @get:Column(name = "idEstadoRecibos")
    var idEstadoRecibos = 0
    @get:Basic
    @get:Column(name = "numero")
    var numero: Byte = 0
    @get:Basic
    @get:Column(name = "cantidad")
    var cantidad = 0.0
    @get:Basic
    @get:Column(name = "fechaVigencia")
    var fechaVigencia: Date? = null
    @get:Basic
    @get:Column(name = "fechaLiquidacion")
    var fechaLiquidacion: Date? = null
    @get:Basic
    @get:Column(name = "estdo")
    var estdo: String? = null
    @get:Basic
    @get:Column(name = "idEstadoPoliza")
    var idEstadoPoliza = 0
    @get:Basic
    @get:Column(name = "primaNeta")
    var primaNeta = 0.0
    @get:Basic
    @get:Column(name = "poliza")
    var poliza: String? = null
    @get:Basic
    @get:Column(name = "estado")
    var estado: String? = null
    @get:Basic
    @get:Column(name = "idArea")
    var idArea = 0
    @get:Basic
    @get:Column(name = "idPuesto")
    var idPuesto = 0
    @get:Basic
    @get:Column(name = "idEmpresa")
    var idEmpresa = 0
    @get:Basic
    @get:Column(name = "idGrupo")
    var idGrupo = 0
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "apellidoPaterno")
    var apellidoPaterno: String? = null
    @get:Basic
    @get:Column(name = "apellidoMaterno")
    var apellidoMaterno: String? = null
    @get:Basic
    @get:Column(name = "idSubarea")
    var idSubarea = 0
    @get:Basic
    @get:Column(name = "idSede")
    var idSede = 0
    @get:Basic
    @get:Column(name = "idProductoSocio")
    var idProductoSocio = 0
    @get:Basic
    @get:Column(name = "nombreComercial")
    var nombreComercial: String? = null
    @get:Basic
    @get:Column(name = "area")
    var area: String? = null
    @get:Basic
    @get:Column(name = "fechaRegistro")
    var fechaRegistro:  Timestamp? = null
    @get:Basic
    @get:Column(name = "idFlujoPoliza")
    var idFlujoPoliza = 0
}