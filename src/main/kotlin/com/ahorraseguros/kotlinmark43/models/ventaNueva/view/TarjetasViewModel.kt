package com.ahorraseguros.kotlinmark43.models.ventaNueva.view

import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "tarjetasView")
class TarjetasViewModel {
    @get:Id
    @get:Column(name = "idTarjetas")
    var idTarjetas: Int = 0
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "idBanco")
    var idBanco: Int = 0
    @get:Basic
    @get:Column(name = "nombreBanco")
    var nombreBanco: String? = null
    @get:Basic
    @get:Column(name = "idCarrier")
    var idCarrier: Int = 0
    @get:Basic
    @get:Column(name = "carrier")
    var carrier: String? = null
    @get:Basic
    @get:Column(name = "tarjeta")
    var tarjeta: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
}
