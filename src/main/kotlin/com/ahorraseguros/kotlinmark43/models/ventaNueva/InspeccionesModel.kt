package com.ahorraseguros.kotlinmark43.models.ventaNueva

import javax.persistence.*
import java.sql.Timestamp
import java.util.Objects

@Entity
@Table(name = "inspecciones")
class InspeccionesModel {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idRegistro")
    var idRegistro: Long = 0
    @get:Basic
    @get:Column(name = "idEstadoInspeccion")
    var idEstadoInspeccion: Int = 0
    @get:Basic
    @get:Column(name = "idColonia")
    var idColonia: Int = 0
    @get:Basic
    @get:Column(name = "cp")
    var cp: String? = null
    @get:Basic
    @get:Column(name = "calle")
    var calle: String = ""
    @get:Basic
    @get:Column(name = "numInt")
    var numInt: String = ""
    @get:Basic
    @get:Column(name = "numExt")
    var numExt: String? = null
    @get:Basic
    @get:Column(name = "fechaInspeccion")
    var fechaInspeccion: Timestamp? = null
    @get:Basic
    @get:Column(name = "kilometraje")
    var kilometraje: Int = 0
    @get:Basic
    @get:Column(name = "placas")
    var placas: String? = null
    @get:Basic
    @get:Column(name = "urlPreventa")
    var urlPreventa: Byte? = null
    @get:Basic
    @get:Column(name = "urlPostventa")
    var urlPostventa: String? = null
    @get:Basic
    @get:Column(name = "archivo")
    var archivo: String? = null
    @get:Basic
    @get:Column(name = "comentarios")
    var comentarios: String? = null
}
