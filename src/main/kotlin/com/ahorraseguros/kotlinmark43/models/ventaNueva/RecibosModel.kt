package com.ahorraseguros.kotlinmark43.models.ventaNueva

import javax.persistence.*
import java.sql.Date
import java.util.Objects

@Entity
@Table(name = "recibos", schema = "trigarante2020-P", catalog = "")
class RecibosModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idRegistro")
    var idRegistro: Long = 0
    @get:Basic
    @get:Column(name = "idEmpleado")
    var idEmpleado: Long = 0
    @get:Basic
    @get:Column(name = "idEstadoRecibos")
    var idEstadoRecibos: Int = 0
    @get:Basic
    @get:Column(name = "numero")
    var numero: Byte = 0
    @get:Basic
    @get:Column(name = "cantidad")
    var cantidad: Double = 0.toDouble()
    @get:Basic
    @get:Column(name = "fechaVigencia")
    var fechaVigencia: Date? = null
    @get:Basic
    @get:Column(name = "fechaLiquidacion")
    var fechaLiquidacion: Date? = null
    @get:Basic
    @get:Column(name = "fechaPromesaPago")
    var fechaPromesaPago: Date? = null
}
