package com.ahorraseguros.kotlinmark43.models.ventaNueva

import javax.persistence.*
import java.sql.Timestamp
import java.util.Objects

@Entity
@Table(name = "prospecto")
class ProspectoModel {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "numero")
    var numero: Long = 0
    @get:Basic
    @get:Column(name = "correo")
    var correo: String? = null
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "sexo")
    var sexo: String? = null
    @get:Basic
    @get:Column(name = "edad")
    var edad: Int? = null
}
