package com.ahorraseguros.kotlinmark43.models.ventaNueva.view

import java.sql.Time
import java.sql.Timestamp
import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "productividadView")
class ProductividadViewModel {
    @Id
    @get:Column(name = "id")
    var id: Long? = 0
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "apellidoPaterno")
    var apellidoPaterno: String? = null
    @get:Basic
    @get:Column(name = "apellidoMaterno")
    var apellidoMaterno: String? = null
    @get:Basic
    @get:Column(name = "campana")
    var campana: String? = null
    @get:Basic
    @get:Column(name = "idSubarea")
    var idSubarea: Int? = null
    @get:Basic
    @get:Column(name = "subarea")
    var subarea: String? = null
    @get:Basic
    @get:Column(name = "tiempoEmpresa")
    var tiempoEmpresa: Time? = null
    @get:Basic
    @get:Column(name = "fechaRegistro")
    var fechaRegistro: Timestamp? = null
    @get:Basic
    @get:Column(name = "poliza")
    var poliza: String? = null
    @get:Basic
    @get:Column(name = "tipoPago")
    var tipoPago: String? = null
    @get:Basic
    @get:Column(name = "primaNeta")
    var primaNeta: Double? = null
    @get:Basic
    @get:Column(name = "recibioCantidad")
    var recibioCantidad: Double? = null
    @get:Basic
    @get:Column(name = "pagoCantidad")
    var pagoCantidad: Double? = null
    @get:Basic
    @get:Column(name = "description")
    var description: String? = null
}
