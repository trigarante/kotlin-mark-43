package com.ahorraseguros.kotlinmark43.models.ventaNueva

import javax.persistence.*
import java.sql.Timestamp
import java.util.Objects

@Entity
@Table(name = "solicitudes")
class SolicitudesModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idCotizacionAli")
    var idCotizacionAli: Long = 0
    @get:Basic
    @get:Column(name = "idEmpleado")
    var idEmpleado: Long = 0
    @get:Basic
    @get:Column(name = "idEstadoSolicitud")
    var idEstadoSolicitud: Int = 0
    @get:Basic
    @get:Column(name = "idEtiquetaSolicitud")
    var idEtiquetaSolicitud: Int = 0
    @get:Basic
    @get:Column(name = "idFlujoSolicitud")
    var idFlujoSolicitud: Int = 0
    @get:Basic
    @get:Column(name = "comentarios")
    var comentarios: String? = null
}
