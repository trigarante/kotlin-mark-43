package com.ahorraseguros.kotlinmark43.models.ventaNueva.view

import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "datosProductoClienteView", schema = "trigarante2020-P", catalog = "")
class DatosProductoClienteViewModel {
    @Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "cp")
    var cp: String? = null
    @get:Basic
    @get:Column(name = "edad")
    var edad: String? = null
    @get:Basic
    @get:Column(name = "clave")
    var clave: String? = null
    @get:Basic
    @get:Column(name = "marca")
    var marca: String? = null
    @get:Basic
    @get:Column(name = "genero")
    var genero: String? = null
    @get:Basic
    @get:Column(name = "modelo")
    var modelo: String? = null
    @get:Basic
    @get:Column(name = "paquete")
    var paquete: String? = null
    @get:Basic
    @get:Column(name = "servicio")
    var servicio: String? = null
    @get:Basic
    @get:Column(name = "descuento")
    var descuento: String? = null
    @get:Basic
    @get:Column(name = "movimiento")
    var movimiento: String? = null
    @get:Basic
    @get:Column(name = "aseguradora")
    var aseguradora: String? = null
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "fechaNacimiento")
    var fechaNacimiento: String? = null
    @get:Basic
    @get:Column(name = "numeroMotor")
    var numeroMotor: String? = null
    @get:Basic
    @get:Column(name = "numeroPlacas")
    var numeroPlacas: String? = null
}
