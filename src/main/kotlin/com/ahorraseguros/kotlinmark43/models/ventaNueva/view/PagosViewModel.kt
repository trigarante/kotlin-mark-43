package com.ahorraseguros.kotlinmark43.models.ventaNueva.view

import java.sql.Date
import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "pagosView")
data class PagosViewModel (
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0,
    @get:Basic
    @get:Column(name = "idRecibo")
    var idRecibo: Long? = null,
    @get:Basic
    @get:Column(name = "idFormaPago")
    var idFormaPago: Int? = null,
    @get:Basic
    @get:Column(name = "idEstadoPago")
    var idEstadoPago: Int? = null,
    @get:Basic
    @get:Column(name = "fechaPago")
    var fechaPago: Date? = null,
    @get:Basic
    @get:Column(name = "cantidad")
    var cantidad: Double? = null,
    @get:Basic
    @get:Column(name = "archivo")
    var archivo: String? = null,
    @get:Basic
    @get:Column(name = "datos")
    var datos: String? = null,
    @get:Basic
    @get:Column(name = "numero")
    var numero: Byte = 0,
    @get:Basic
    @get:Column(name = "recibosCantidad")
    var recibosCantidad: Double = 0.toDouble(),
    @get:Basic
    @get:Column(name = "formapagoDescripcio")
    var formapagoDescripcio: String? = null,
    @get:Basic
    @get:Column(name = "estadoPagoDescripcion")
    var estadoPagoDescripcion: String? = null
)
