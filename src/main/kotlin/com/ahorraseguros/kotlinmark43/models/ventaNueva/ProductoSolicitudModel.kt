package com.ahorraseguros.kotlinmark43.models.ventaNueva

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "productoSolicitud")
class ProductoSolicitudModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idProspecto")
    var idProspecto: Long = 0
    @get:Basic
    @get:Column(name = "idTipoSubRamo")
    var idTipoSubRamo: Int = 0
    @get:Basic
    @get:Column(name = "datos")
    var datos: String? = null
}
