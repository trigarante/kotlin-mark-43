package com.ahorraseguros.kotlinmark43.models.ventaNueva.view

import java.sql.Timestamp
import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "inspeccionesView", schema = "trigarante2020-P", catalog = "")
class InspeccionesViewModel {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idRegistro")
    var idRegistro: Long = 0
    @get:Basic
    @get:Column(name = "idEstadoInspeccion")
    var idEstadoInspeccion: Int = 0
    @get:Basic
    @get:Column(name = "idColonia")
    var idColonia: Int = 0
    @get:Basic
    @get:Column(name = "cp")
    var cp: String? = null
    @get:Basic
    @get:Column(name = "fechaInspeccion")
    var fechaInspeccion: Timestamp? = null
    @get:Basic
    @get:Column(name = "kilometraje")
    var kilometraje: Int = 0
    @get:Basic
    @get:Column(name = "placas")
    var placas: String? = null
    @get:Basic
    @get:Column(name = "urlPreventa")
    var urlPreventa: Byte? = null
    @get:Basic
    @get:Column(name = "urlPostventa")
    var urlPostventa: String? = null
    @get:Basic
    @get:Column(name = "estado")
    var estado: String? = null
    @get:Basic
    @get:Column(name = "asenta")
    var asenta: String? = null
    @get:Basic
    @get:Column(name = "poliza")
    var poliza: String? = null
    @get:Basic
    @get:Column(name = "primaNeta")
    var primaNeta: String? = null
}
