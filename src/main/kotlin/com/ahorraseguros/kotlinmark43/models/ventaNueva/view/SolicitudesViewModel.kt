package com.ahorraseguros.kotlinmark43.models.ventaNueva.view

import java.sql.Timestamp
import javax.persistence.*

@Entity
@Table(name = "solicitudesView")
data class SolicitudesViewModel (
    @get:Basic
    @get:Column(name = "id")
    @Id
    var id: Long = 0,
    @get:Basic
    @get:Column(name = "idCotizacionAli")
    var idCotizacionAli: Long = 0,
    @get:Basic
    @get:Column(name = "idEmpleado")
    var idEmpleado: Long = 0,
    @get:Basic
    @get:Column(name = "idEstadoSolicitud")
    var idEstadoSolicitud: Int = 0,
    @get:Basic
    @get:Column(name = "fechaSolicitud")
    var fechaSolicitud: Timestamp? = null,
    @get:Basic
    @get:Column(name = "comentarios")
    var comentarios: String? = null,
    @get:Basic
    @get:Column(name = "idCotizacion")
    var idCotizacion: Long = 0,
    @get:Basic
    @get:Column(name = "peticion")
    var peticion: String? = null,
    @get:Basic
    @get:Column(name = "respuesta")
    var respuesta: String? = null,
    @get:Basic
    @get:Column(name = "fechaCotizacion")
    var fechaCotizacion: Timestamp? = null,
    @get:Basic
    @get:Column(name = "fechaActualizacion")
    var fechaActualizacion: Timestamp? = null,
    @get:Basic
    @get:Column(name = "estado")
    var estado: String? = null,
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0,
    @get:Basic
    @get:Column(name = "idUsuario")
    var idUsuario: Long? = null,
    @get:Basic
    @get:Column(name = "idBanco")
    var idBanco: Int? = null,
    @get:Basic
    @get:Column(name = "idCandidato")
    var idCandidato: Long = 0,
    @get:Basic
    @get:Column(name = "idPuesto")
    var idPuesto: Int = 0,
    @get:Basic
    @get:Column(name = "idSubarea")
    var idSubArea: Int = 0,
    @get:Basic
    @get:Column(name = "idTipoContacto")
    var idTipoContacto: Int = 0,
    @get:Basic
    @get:Column(name = "puestoDetalle")
    var puestoDetalle: String? = null,
    @get:Basic
    @get:Column(name = "idPrecandidato")
    var idPrecandidato: Int = 0,
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null,
    @get:Basic
    @get:Column(name = "apellidoPaterno")
    var apellidoPaterno: String? = null,
    @get:Basic
    @get:Column(name = "apellidoMaterno")
    var apellidoMaterno: String? = null,
    @get:Basic
    @get:Column(name = "idProducto")
    var idProducto: Int = 0,
    @get:Basic
    @get:Column(name = "idProspecto")
    var idProspecto: Int = 0,
    @get:Basic
    @get:Column(name = "idSubRamo")
    var idSubRamo: Int = 0,
    @get:Basic
    @get:Column(name = "nombreProspecto")
    var nombreProspecto: String? = null,
    @get:Basic
    @get:Column(name = "correoProspecto")
    var correoProspecto: String? = null,
    @get:Basic
    @get:Column(name = "numeroProspecto")
    var numeroProspecto: String? = null,
    @get:Basic
    @get:Column(name = "idFlujoSolicitud")
    var idFlujoSolicitud: Int = 0,
    @get:Basic
    @get:Column(name = "idEtiquetaSolicitud")
    var idEtiquetaSolicitud: Int = 0,
    @get:Basic
    @get:Column(name = "etiquetaSolicitud")
    var etiquetaSolicitud: String? = null
)
