package com.ahorraseguros.kotlinmark43.models.ventaNueva.view

import java.sql.Date
import java.sql.Timestamp
import javax.persistence.*

@Entity
@Table(name = "registroView")
class RegistroViewModel {
    @Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Column(name = "idEmpleado")
    @get:Basic
    var idEmpleado: Long = 0
    @get:Column(name = "idProducto")
    @get:Basic
    var idProducto: Long = 0
    @get:Column(name = "idTipoPago")
    @get:Basic
    var idTipoPago = 0
    @get:Column(name = "idProductoSocio")
    @get:Basic
    var idProductoSocio: Long = 0
    @get:Column(name = "idFlujoPoliza")
    @get:Basic
    var idFlujoPoliza = 0
    @get:Column(name = "poliza")
    @get:Basic
    var poliza: String? = null
    @get:Column(name = "fechaInicio")
    @get:Basic
    var fechaInicio: Date? = null
    @get:Column(name = "primaNeta")
    @get:Basic
    var primaNeta = 0.0
    @get:Column(name = "fechaRegistro")
    @get:Basic
    var fechaRegistro: Timestamp? = null
    @get:Column(name = "archivo")
    @get:Basic
    var archivo: String? = null
    @get:Column(name = "cantidadPagos")
    @get:Basic
    var cantidadPagos: Byte = 0
    @get:Column(name = "tipoPago")
    @get:Basic
    var tipoPago: String? = null
    @get:Column(name = "pruductoSocio")
    @get:Basic
    var pruductoSocio: String? = null
    @get:Column(name = "idSocio")
    @get:Basic
    var idSocio: Long = 0
    @get:Column(name = "idRamo")
    @get:Basic
    var idRamo: Long = 0
    @get:Column(name = "idSubramo")
    @get:Basic
    var idSubramo: Long = 0
    @get:Column(name = "datos")
    @get:Basic
    var datos: String? = null
    @get:Column(name = "idEstadoPoliza")
    @get:Basic
    var idEstadoPoliza = 0
    @get:Column(name = "estado")
    @get:Basic
    var estado: String? = null
    @get:Column(name = "idArea")
    @get:Basic
    var idArea = 0
    @get:Column(name = "nombre")
    @get:Basic
    var nombre: String? = null
    @get:Column(name = "apellidoPaterno")
    @get:Basic
    var apellidoPaterno: String? = null
    @get:Column(name = "apellidoMaterno")
    @get:Basic
    var apellidoMaterno: String? = null
    @get:Column(name = "idSede")
    @get:Basic
    var idSede = 0
    @get:Column(name = "idGrupo")
    @get:Basic
    var idGrupo = 0
    @get:Column(name = "idPuesto")
    @get:Basic
    var idPuesto = 0
    @get:Column(name = "idEmpresa")
    @get:Basic
    var idEmpresa = 0
    @get:Column(name = "idSubarea")
    @get:Basic
    var idSubarea = 0
}