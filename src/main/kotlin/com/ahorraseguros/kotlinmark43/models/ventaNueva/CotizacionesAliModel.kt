package com.ahorraseguros.kotlinmark43.models.ventaNueva

import javax.persistence.*
import java.sql.Timestamp
import java.util.Objects

@Entity
@Table(name = "cotizacionesAli")
class CotizacionesAliModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idCotizacion")
    var idCotizacion: Long = 0
    @get:Basic
    @get:Column(name = "idSubRamo")
    var idSubRamo: Int = 0
    @get:Basic
    @get:Column(name = "peticion")
    var peticion: String? = null
    @get:Basic
    @get:Column(name = "respuesta")
    var respuesta: String? = null
    @get:Basic
    @get:Column(name = "fechaCotizacion")
    var fechaCotizacion: Timestamp? = Timestamp(System.currentTimeMillis())
    @get:Basic
    @get:Column(name = "fechaActualizacion")
    var fechaActualizacion: Timestamp? = null
}
