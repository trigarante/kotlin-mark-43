package com.ahorraseguros.kotlinmark43.models.ventaNueva.view

import java.sql.Date
import java.util.Objects
import javax.persistence.*

@Entity
@Table(name = "panelVentaView")
class PanelVentaViewModel {
    @Id
    @get:Column(name = "id")
    var id: Long? = null
    @get:Basic
    @get:Column(name = "PNE")
    var pne: Double? = null
    @get:Basic
    @get:Column(name = "numeroRecibo")
    var numeroRecibo: Byte? = null
    @get:Basic
    @get:Column(name = "PNA")
    var pna: Double? = null
    @get:Basic
    @get:Column(name = "fechaPago")
    var fechaPago: Date? = null
    @get:Basic
    @get:Column(name = "PNC")
    var pnc: Double? = null
    @get:Basic
    @get:Column(name = "description")
    var description: String? = null
    @get:Basic
    @get:Column(name = "idSubarea")
    var idSubarea: Int? = null
    @get:Basic
    @get:Column(name = "subarea")
    var subarea: String? = null
}
