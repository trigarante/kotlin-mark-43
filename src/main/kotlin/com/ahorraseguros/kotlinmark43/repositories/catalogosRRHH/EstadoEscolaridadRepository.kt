package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.EstadoEscolaridadModels
import org.springframework.data.jpa.repository.JpaRepository

interface EstadoEscolaridadRepository : JpaRepository<EstadoEscolaridadModels,Long> {
}