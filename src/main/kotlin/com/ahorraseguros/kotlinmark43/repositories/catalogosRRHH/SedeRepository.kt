package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.SedeModel
import org.springframework.data.jpa.repository.JpaRepository

interface SedeRepository : JpaRepository<SedeModel, Int> {
    fun findByNombre(nombre:String):SedeModel
}