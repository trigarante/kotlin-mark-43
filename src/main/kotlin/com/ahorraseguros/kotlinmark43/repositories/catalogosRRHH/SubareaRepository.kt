package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.SubareaModels
import org.springframework.data.jpa.repository.JpaRepository

interface SubareaRepository : JpaRepository<SubareaModels, Long> {

    fun findAllByIdArea(id: Int): MutableList<SubareaModels>
}