package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.EmpresaModels
import org.springframework.data.jpa.repository.JpaRepository

interface EmpresaRepository : JpaRepository<EmpresaModels, Long>