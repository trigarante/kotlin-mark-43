package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.DocumentosRrhhModel
import org.springframework.data.jpa.repository.JpaRepository

interface DocumentosRrhhRepository : JpaRepository<DocumentosRrhhModel, Long>