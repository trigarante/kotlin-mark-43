package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.MedioTransporteModels
import org.springframework.data.jpa.repository.JpaRepository

interface MedioTransporteRepository : JpaRepository<MedioTransporteModels,Long>