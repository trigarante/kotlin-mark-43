package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.EscolaridadModels
import org.springframework.data.jpa.repository.JpaRepository

interface EscolaridadRepository: JpaRepository<EscolaridadModels,Long>