package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.PuestoTipoModels
import org.springframework.data.jpa.repository.JpaRepository

interface PuestoTipoRepository: JpaRepository<PuestoTipoModels,Long>