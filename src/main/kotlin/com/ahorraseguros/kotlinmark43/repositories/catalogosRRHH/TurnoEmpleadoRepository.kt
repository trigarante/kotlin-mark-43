package com.ahorraseguros.kotlinmark43.repositories.catalogosRRHH

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.TurnoEmpleadoModel
import org.springframework.data.jpa.repository.JpaRepository

interface TurnoEmpleadoRepository: JpaRepository<TurnoEmpleadoModel, Long> {
}