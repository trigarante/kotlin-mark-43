package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.CompetenciasModels
import org.springframework.data.jpa.repository.JpaRepository

interface CompetenciaRepository: JpaRepository<CompetenciasModels,Long>