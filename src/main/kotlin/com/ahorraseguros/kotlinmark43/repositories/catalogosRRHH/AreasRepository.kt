package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.AreaModels
import org.springframework.data.jpa.repository.JpaRepository

interface AreasRepository : JpaRepository<AreaModels,Long>{

   fun findAllByIdSedeAndActivo(idSede:Int,activo:Int):MutableList<AreaModels>
}


