package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.CompetenciaCandidatoModels
import org.springframework.data.jpa.repository.JpaRepository

interface CompetenciaCandidatoRepository: JpaRepository<CompetenciaCandidatoModels, Long>