package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.MarcaEmpresarialModels
import org.springframework.data.jpa.repository.JpaRepository

interface MarcaEmpresarialRepository : JpaRepository<MarcaEmpresarialModels,Long>{

    fun findAllByIdSedeAndActivo(id:Long,activo:Int):MutableList<MarcaEmpresarialModels>
}


