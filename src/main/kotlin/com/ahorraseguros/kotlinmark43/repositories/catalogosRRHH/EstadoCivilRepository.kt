package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.EstadoCivilModels
import org.springframework.data.jpa.repository.JpaRepository

interface EstadoCivilRepository :JpaRepository<EstadoCivilModels,Long>