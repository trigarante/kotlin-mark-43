package com.ahorraseguros.kotlinmark43.repositories.catalogosRRHH

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.EtapasModulosModels
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface EtapasModulosRepository : JpaRepository<EtapasModulosModels,Int>