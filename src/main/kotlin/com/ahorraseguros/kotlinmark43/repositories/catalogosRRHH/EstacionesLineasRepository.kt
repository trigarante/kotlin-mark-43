package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.EstacionesLineasModels
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

interface EstacionesLineasRepository : JpaRepository<EstacionesLineasModels, Long> {

    @Query("FROM EstacionesLineasModels WHERE idTransporte=:idTransporte group by lineas order by lineas asc")
    fun findByIdTransporteOrderByLineas(@Param("idTransporte") idTransporte: Int): MutableList<EstacionesLineasModels>

    fun findAllByIdTransporteAndLineasOrderByEstacionAsc(@Param("idTransporte") idTransporte: Int,
                                       @Param("idLinea") idLinea: String): MutableList<EstacionesLineasModels>

}