package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.PaisesModels
import org.springframework.data.jpa.repository.JpaRepository

interface PaisesRepository : JpaRepository<PaisesModels, Long>