package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.PuestoModels
import org.springframework.data.jpa.repository.JpaRepository

interface PuestoRepository:JpaRepository<PuestoModels,Long> {
}