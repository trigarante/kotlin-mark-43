package com.ahorraseguros.kotlinmark43.repositories.catalogoAtencionClientes

import com.ahorraseguros.kotlinmark43.models.catalogosAtencionClientes.EstadoEndosoModel
import org.springframework.data.jpa.repository.JpaRepository

interface EstadoEndosoRepository: JpaRepository<EstadoEndosoModel, Int> {
}