package com.ahorraseguros.kotlinmark43.repositories.catalogoAtencionClientes

import com.ahorraseguros.kotlinmark43.models.catalogosAtencionClientes.TipoEndosoModel
import org.springframework.data.jpa.repository.JpaRepository

interface TipoEndosoRepository: JpaRepository<TipoEndosoModel, Int> {
}