package com.ahorraseguros.kotlinmark43.repositories.catalogosComercial

import com.ahorraseguros.kotlinmark43.models.catalogosComercial.TipoSubRamoModels
import org.springframework.data.jpa.repository.JpaRepository

interface TipoSubramoRepository: JpaRepository<TipoSubRamoModels, Long> {
}