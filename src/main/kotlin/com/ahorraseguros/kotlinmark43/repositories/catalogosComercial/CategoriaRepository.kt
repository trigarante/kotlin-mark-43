package com.ahorraseguros.kotlinmark43.repositories.catalogosComercial

import com.ahorraseguros.kotlinmark43.models.catalogosComercial.CategoriaModels
import org.springframework.data.jpa.repository.JpaRepository

interface CategoriaRepository: JpaRepository<CategoriaModels, Long> {
}