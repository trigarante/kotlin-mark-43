package com.ahorraseguros.kotlinmark43.repositories.catalogosComercial.view

import com.ahorraseguros.kotlinmark43.models.catalogosComercial.view.SubCategoriaViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface SubCategoriaViewRepository: JpaRepository<SubCategoriaViewModel, Int> {
}