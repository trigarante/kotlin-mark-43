package com.ahorraseguros.kotlinmark43.repositories.catalogosComercial

import com.ahorraseguros.kotlinmark43.models.catalogosComercial.EstadoSocioModel
import org.springframework.data.jpa.repository.JpaRepository

interface EstadoSocioRepository : JpaRepository<EstadoSocioModel, Long>