package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.catalogosSolicitudes.CampanaModels
import org.springframework.data.jpa.repository.JpaRepository

interface CampanasRepository : JpaRepository<CampanaModels, Long>