package com.ahorraseguros.kotlinmark43.repositories.catalogosComercial

import com.ahorraseguros.kotlinmark43.models.catalogosMarketing.EstadoCampanaModel
import org.springframework.data.jpa.repository.JpaRepository

interface EstadoCampanaRespository: JpaRepository<EstadoCampanaModel, Long> {
}