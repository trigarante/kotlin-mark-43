package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.rh.PaginaModels
import org.springframework.data.jpa.repository.JpaRepository

interface PaginaRepository : JpaRepository<PaginaModels, Long>