package com.ahorraseguros.kotlinmark43.repositories.catalogosComercial

import com.ahorraseguros.kotlinmark43.models.catalogosComercial.TipoProductoModels
import org.springframework.data.jpa.repository.JpaRepository

interface TipoProductoRepository: JpaRepository<TipoProductoModels, Long> {
}