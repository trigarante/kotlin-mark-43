package com.ahorraseguros.kotlinmark43.repositories.catalogosComercial

import com.ahorraseguros.kotlinmark43.models.catalogosComercial.TipoCompetenciaModels
import org.springframework.data.jpa.repository.JpaRepository

interface TipoCompetenciaRepository: JpaRepository<TipoCompetenciaModels, Long> {
}