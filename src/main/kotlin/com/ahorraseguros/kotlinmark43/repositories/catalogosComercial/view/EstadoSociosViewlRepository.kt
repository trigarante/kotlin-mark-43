package com.ahorraseguros.kotlinmark43.repositories.catalogosComercial.view

import com.ahorraseguros.kotlinmark43.models.catalogosComercial.view.EstadoSociosViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface EstadoSociosViewlRepository: JpaRepository<EstadoSociosViewModel, Long>
