package com.ahorraseguros.kotlinmark43.repositories.catalogosComercial

import com.ahorraseguros.kotlinmark43.models.catalogosComercial.SubCategoriaModels
import org.springframework.data.jpa.repository.JpaRepository

interface SubCategoriaRepository: JpaRepository<SubCategoriaModels, Long> {
}