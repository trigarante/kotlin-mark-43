package com.ahorraseguros.kotlinmark43.repositories.catalogosComercial

import com.ahorraseguros.kotlinmark43.models.catalogosComercial.TipoRamoModels
import org.springframework.data.jpa.repository.JpaRepository

interface TipoRamoRepository:JpaRepository<TipoRamoModels,Long> {
}