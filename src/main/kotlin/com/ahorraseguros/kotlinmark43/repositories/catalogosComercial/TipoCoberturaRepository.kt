package com.ahorraseguros.kotlinmark43.repositories.catalogosComercial

import com.ahorraseguros.kotlinmark43.models.catalogosComercial.TipoCoberturaModels
import org.springframework.data.jpa.repository.JpaRepository

interface TipoCoberturaRepository: JpaRepository<TipoCoberturaModels, Long> {
}