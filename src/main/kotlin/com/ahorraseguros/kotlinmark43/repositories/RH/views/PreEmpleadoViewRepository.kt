package com.ahorraseguros.kotlinmark43.repositories.views

import com.ahorraseguros.kotlinmark43.models.rh.views.PreEmpleadosViewModels
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface PreEmpleadoViewRepository : JpaRepository<PreEmpleadosViewModels, Long>{
    fun findByCurp(curp:String):Optional<PreEmpleadosViewModels>
    fun findByIdEtapaOrderByFechaCreacionDesc(idEtapa: Int):MutableList<PreEmpleadosViewModels>
}

