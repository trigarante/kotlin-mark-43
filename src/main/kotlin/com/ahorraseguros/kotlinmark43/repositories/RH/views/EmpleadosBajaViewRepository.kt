package com.ahorraseguros.kotlinmark43.repositories.RH.views

import com.ahorraseguros.kotlinmark43.models.rh.views.EmpleadoBajaViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface EmpleadosBajaViewRepository : JpaRepository<EmpleadoBajaViewModel, Long> {
}