package com.ahorraseguros.kotlinmark43.repositories.views

import com.ahorraseguros.kotlinmark43.models.rh.views.EmpleadoViewModels
import org.springframework.data.jpa.repository.JpaRepository

interface EmpleadoViewRepository : JpaRepository<EmpleadoViewModels, Long> {

    fun findAllByIdSubarea(id: Long): MutableList<EmpleadoViewModels>

    fun findAllByIdSubareaAndIdUsuario(idSubarea:Int,idUsuario:Long):EmpleadoViewModels
}