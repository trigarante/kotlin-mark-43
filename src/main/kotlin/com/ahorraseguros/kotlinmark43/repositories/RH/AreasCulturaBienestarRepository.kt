package com.ahorraseguros.kotlinmark43.repositories.RH

import com.ahorraseguros.kotlinmark43.models.rh.AreasModel
import org.springframework.data.jpa.repository.JpaRepository

interface AreasCulturaBienestarRepository: JpaRepository<AreasModel,Int> {
}