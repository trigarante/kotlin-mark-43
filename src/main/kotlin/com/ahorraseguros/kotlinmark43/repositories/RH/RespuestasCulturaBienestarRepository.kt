package com.ahorraseguros.kotlinmark43.repositories.RH

import com.ahorraseguros.kotlinmark43.models.rh.RespuestasCulturaBienestarModel
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface RespuestasCulturaBienestarRepository:JpaRepository<RespuestasCulturaBienestarModel,Int> {

    fun findByIdPregunta(idPregunta:Int): Optional<RespuestasCulturaBienestarModel>

    fun findAllByIdPregunta(idPregunta:Int): MutableList<RespuestasCulturaBienestarModel>


}