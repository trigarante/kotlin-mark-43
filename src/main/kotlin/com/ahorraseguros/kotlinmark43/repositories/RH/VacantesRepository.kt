package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.rh.VacantesModels
import org.springframework.data.jpa.repository.JpaRepository


interface VacantesRepository : JpaRepository<VacantesModels, Long>

