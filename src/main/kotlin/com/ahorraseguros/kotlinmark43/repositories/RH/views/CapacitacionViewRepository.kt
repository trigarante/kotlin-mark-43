package com.ahorraseguros.kotlinmark43.repositories.views

import com.ahorraseguros.kotlinmark43.models.rh.views.CapacitacionViewModels
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

interface CapacitacionViewRepository:JpaRepository<CapacitacionViewModels,Long> {
    fun findAllByIdCapacitador(idEmpleado: Long?): MutableList<CapacitacionViewModels>
}