package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.rh.CandidatoModels
import org.springframework.data.jpa.repository.JpaRepository

interface CandidatoRepository : JpaRepository<CandidatoModels, Long>