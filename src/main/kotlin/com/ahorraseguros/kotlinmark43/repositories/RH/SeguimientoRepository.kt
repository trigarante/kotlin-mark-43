package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.rh.SeguimientoModels
import org.springframework.data.jpa.repository.JpaRepository

interface SeguimientoRepository : JpaRepository<SeguimientoModels, Int>