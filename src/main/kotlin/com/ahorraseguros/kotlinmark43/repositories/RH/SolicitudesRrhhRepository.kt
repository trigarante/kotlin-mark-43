package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.rh.SolicitudesRrhhModels
import org.springframework.data.jpa.repository.JpaRepository


interface SolicitudesRrhhRepository : JpaRepository<SolicitudesRrhhModels, Long> {
//    @Query(name = "SolicitudesRrhhModel.find")
//    fun findAllCustom(): MutableList<SolicitudCustom>


}
