package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.rh.CapacitacionModels
import org.springframework.data.jpa.repository.JpaRepository

interface CapacitacionRepository : JpaRepository<CapacitacionModels, Long>