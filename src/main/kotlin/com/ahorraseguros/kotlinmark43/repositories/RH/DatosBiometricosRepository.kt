package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.rh.DatosBiometricosModels
import org.springframework.data.jpa.repository.JpaRepository

interface DatosBiometricosRepository:JpaRepository<DatosBiometricosModels,Long> {
}