package com.ahorraseguros.kotlinmark43.repositories.RH

import com.ahorraseguros.kotlinmark43.models.rh.PreguntasCulturaBienestarModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.sql.Timestamp


interface PreguntasCulturaBienestar : JpaRepository<PreguntasCulturaBienestarModel, Int> {
    fun findAllByIdArea(idArea: Int): MutableList<PreguntasCulturaBienestarModel>
    @Query(value = "select * from preguntasCulturaBienestar where idArea= :idArea and fechaInicioVigencia<= :fi and fechaFinVigencia>= :ff", nativeQuery = true)
    fun aiiuda(@Param("idArea") idArea: Int,@Param("fi")  fechaInicio: String,@Param("ff") fechaFin: String):
            MutableList<PreguntasCulturaBienestarModel>
}
