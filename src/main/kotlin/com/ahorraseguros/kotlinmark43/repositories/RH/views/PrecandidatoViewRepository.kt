package com.ahorraseguros.kotlinmark43.repositories.views

import com.ahorraseguros.kotlinmark43.models.rh.views.PrecandidatoViewModels
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface PrecandidatoViewRepository : JpaRepository<PrecandidatoViewModels, Long>{
    fun findByCurp(curp:String):Optional<PrecandidatoViewModels>
    fun findByIdEtapaOrderByFechaCreacionDesc(idEtapa: Int):MutableList<PrecandidatoViewModels>
    @Query(value = "Select PV.* from precandidatoView PV, solicitudesRRHH SRRHH where SRRHH.idReclutador = :idEmpleado " +
            "AND PV.idSolicitudRRHH = SRRHH.id",
            nativeQuery = true)
    fun findByIdEmpleado(@Param("idEmpleado") idEmpleado: Long?): MutableList<PrecandidatoViewModels>
}

