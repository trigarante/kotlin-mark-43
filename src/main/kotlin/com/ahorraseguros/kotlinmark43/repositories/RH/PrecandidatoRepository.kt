package com.ahorraseguros.kotlinmark43.repositories.RH

import com.ahorraseguros.kotlinmark43.models.rh.PrecandidatoModels
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface PrecandidatoRepository : JpaRepository<PrecandidatoModels, Long>{
    fun findByCurp(curp:String):Optional<PrecandidatoModels>
    fun findByIdEtapaOrderByFechaCreacionDesc(idEtapa: Int):MutableList<PrecandidatoModels>
}

