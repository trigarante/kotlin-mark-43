package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.rh.views.SolicitudesRrhhViewModels
import org.springframework.data.jpa.repository.JpaRepository


interface SolicitudesRrhhViewRepository : JpaRepository<SolicitudesRrhhViewModels, Long> {
//    @Query(name = "SolicitudesRrhhModel.find")
//    fun findAllCustom(): MutableList<SolicitudCustom>
    fun findAllByIdReclutador(reclutador: Long?): MutableList<SolicitudesRrhhViewModels>

}
