package com.ahorraseguros.kotlinmark43.repositories.views

import com.ahorraseguros.kotlinmark43.models.rh.views.CandidatoViewModels
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

interface CandidatoViewRepository: JpaRepository<CandidatoViewModels,Long> {
    @Query(value = "Select CV.* from candidatoView CV, precandidato P, solicitudesRRHH SRRHH where " +
            "CV.idPrecandidato = P.id AND P.idsolicitudRRHH = SRRHH.id AND SRRHH.idReclutador = :idEmpleado ",
            nativeQuery = true)
    fun findByIdEmpleado(@Param("idEmpleado") idEmpleado: Long?): MutableList<CandidatoViewModels>
}