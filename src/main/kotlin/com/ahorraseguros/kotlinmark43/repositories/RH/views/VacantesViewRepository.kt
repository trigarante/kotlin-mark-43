package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.rh.views.VacantesViewModels
import org.springframework.data.jpa.repository.JpaRepository


interface VacantesViewRepository : JpaRepository<VacantesViewModels, Long>

