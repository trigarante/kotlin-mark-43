package com.ahorraseguros.kotlinmark43.repositories.RH

import com.ahorraseguros.kotlinmark43.models.rh.TestModel
import org.springframework.data.jpa.repository.JpaRepository

interface TestRepository :JpaRepository<TestModel, Int> {

}