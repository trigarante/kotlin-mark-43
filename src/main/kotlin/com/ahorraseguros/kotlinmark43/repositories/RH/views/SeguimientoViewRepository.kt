package com.ahorraseguros.kotlinmark43.repositories.views

import com.ahorraseguros.kotlinmark43.models.rh.views.SeguimientoViewModels
import org.springframework.data.jpa.repository.JpaRepository

interface SeguimientoViewRepository:JpaRepository<SeguimientoViewModels,Long> {
    fun findAllByIdCoach(idCoach: Long?): MutableList<SeguimientoViewModels>
}