package com.ahorraseguros.kotlinmark43.repositories.RH

import com.ahorraseguros.kotlinmark43.models.catalogosRRHH.RazonSocialModel
import org.springframework.data.jpa.repository.JpaRepository

interface RazonSocialRepository : JpaRepository<RazonSocialModel, Int>