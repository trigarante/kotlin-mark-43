package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.rh.EmpleadoModels
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.jpa.repository.query.Procedure

interface EmpleadoRepository : JpaRepository<EmpleadoModels, Long>{
    @Procedure(name = "updateEmpleados")
    fun updateEmpleado(empleadoAnt: Long, empleadoNew: Long): Long?
}