package com.ahorraseguros.kotlinmark43.repositories.RH

import com.ahorraseguros.kotlinmark43.models.rh.ContadorCulturaBienestarModel
import org.springframework.data.jpa.repository.JpaRepository

interface ContadorCulturaBienestarRepository:JpaRepository<ContadorCulturaBienestarModel,Int> {
fun countAllByIdRespuesta(idRespuesta:Int):Int
}