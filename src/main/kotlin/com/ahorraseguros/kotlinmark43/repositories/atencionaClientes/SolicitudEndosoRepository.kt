package com.ahorraseguros.kotlinmark43.repositories.atencionaClientes

import com.ahorraseguros.kotlinmark43.models.atencionaClientes.SolicitudEndosoModel
import org.springframework.data.jpa.repository.JpaRepository

interface SolicitudEndosoRepository: JpaRepository<SolicitudEndosoModel, Long> {
}