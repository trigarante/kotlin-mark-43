package com.ahorraseguros.kotlinmark43.repositories.atencionaClientes.views

import com.ahorraseguros.kotlinmark43.models.atencionaClientes.views.EndososCancelacionesViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface EndososCancelacionesViewRepository: JpaRepository<EndososCancelacionesViewModel, Long> {
}