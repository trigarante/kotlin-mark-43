package com.ahorraseguros.kotlinmark43.repositories.atencionaClientes.views

import com.ahorraseguros.kotlinmark43.models.atencionaClientes.views.SolicitudEndosoViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface SolicitudEndosoViewRepository: JpaRepository<SolicitudEndosoViewModel, Long> {
}