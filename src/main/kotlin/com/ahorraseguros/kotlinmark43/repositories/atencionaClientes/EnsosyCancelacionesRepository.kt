package com.ahorraseguros.kotlinmark43.repositories.atencionaClientes

import com.ahorraseguros.kotlinmark43.models.atencionaClientes.EndososCancelacionesModel
import org.springframework.data.jpa.repository.JpaRepository

interface EnsosyCancelacionesRepository: JpaRepository<EndososCancelacionesModel, Long> {
}