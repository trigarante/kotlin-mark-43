package com.ahorraseguros.kotlinmark43.repositories.verificaciones

import com.ahorraseguros.kotlinmark43.models.verificaciones.ErroresAutorizacionModel
import org.springframework.data.jpa.repository.JpaRepository

interface ErroresAutorizacionRepository: JpaRepository<ErroresAutorizacionModel, Long> {
}