package com.ahorraseguros.kotlinmark43.repositories.verificaciones

import com.ahorraseguros.kotlinmark43.models.verificaciones.TipoDocumentosModel
import org.springframework.data.jpa.repository.JpaRepository

interface TipoDocumentosRepository: JpaRepository<TipoDocumentosModel,Int> {
    fun findAllByIdTablaAndActivo(idTabla: Int, activo: Boolean = true): MutableList<TipoDocumentosModel>
}