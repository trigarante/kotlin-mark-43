package com.ahorraseguros.kotlinmark43.repositories.verificaciones.views

import com.ahorraseguros.kotlinmark43.models.verificaciones.view.AutorizacionRegistroViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface AutorizacionRegistroViewRepository: JpaRepository<AutorizacionRegistroViewModel,Long> {
}