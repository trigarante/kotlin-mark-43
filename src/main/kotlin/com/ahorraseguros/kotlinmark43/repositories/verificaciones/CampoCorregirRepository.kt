package com.ahorraseguros.kotlinmark43.repositories.verificaciones

import com.ahorraseguros.kotlinmark43.models.verificaciones.CampoCorregirModel
import org.springframework.data.jpa.repository.JpaRepository

interface CampoCorregirRepository: JpaRepository<CampoCorregirModel,Int> {
    fun findAllByIdTablaAndActivo(idTabla: Int, activo: Boolean = true): MutableList<CampoCorregirModel>
}