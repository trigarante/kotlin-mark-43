package com.ahorraseguros.kotlinmark43.repositories.verificaciones

import com.ahorraseguros.kotlinmark43.models.verificaciones.ErroresAnexosAModel
import org.springframework.data.jpa.repository.JpaRepository

interface ErroresAnexosARepository: JpaRepository<ErroresAnexosAModel,Long>

