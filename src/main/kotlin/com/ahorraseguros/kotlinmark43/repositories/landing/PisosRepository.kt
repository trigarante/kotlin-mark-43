package com.ahorraseguros.kotlinmark43.repositories.landing

import com.ahorraseguros.kotlinmark43.models.landing.PisosModels
import org.springframework.data.jpa.repository.JpaRepository

interface PisosRepository: JpaRepository<PisosModels, Int>