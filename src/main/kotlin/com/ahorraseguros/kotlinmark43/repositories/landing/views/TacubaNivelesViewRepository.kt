package com.ahorraseguros.kotlinmark43.repositories.landing.views

import com.ahorraseguros.kotlinmark43.models.landing.views.TacubaNivelesViewModels
import org.springframework.data.jpa.repository.JpaRepository

interface TacubaNivelesViewRepository: JpaRepository<TacubaNivelesViewModels, Int>