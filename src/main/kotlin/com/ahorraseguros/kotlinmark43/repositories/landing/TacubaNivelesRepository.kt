package com.ahorraseguros.kotlinmark43.repositories.landing

import com.ahorraseguros.kotlinmark43.models.landing.TacubaNivelesModels
import org.springframework.data.jpa.repository.JpaRepository

interface TacubaNivelesRepository : JpaRepository<TacubaNivelesModels, Int>