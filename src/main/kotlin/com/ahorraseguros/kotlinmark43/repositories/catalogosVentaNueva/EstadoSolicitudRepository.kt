package com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.EstadoSolicitudModel
import org.springframework.data.jpa.repository.JpaRepository

interface EstadoSolicitudRepository: JpaRepository <EstadoSolicitudModel, Int>{
}