package com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.TipoPagoModel
import org.springframework.data.jpa.repository.JpaRepository

interface TipoPagoRepository:JpaRepository<TipoPagoModel, Int> {
}