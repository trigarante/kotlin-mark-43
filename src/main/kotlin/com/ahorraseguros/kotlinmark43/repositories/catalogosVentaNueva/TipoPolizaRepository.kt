package com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.TipoPolizaModel
import org.springframework.data.jpa.repository.JpaRepository

interface TipoPolizaRepository: JpaRepository<TipoPolizaModel, Long> {
}