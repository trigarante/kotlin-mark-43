package com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.EstadoPagoModel
import org.springframework.data.jpa.repository.JpaRepository

interface EstadoPagoRepository: JpaRepository<EstadoPagoModel, Int>
