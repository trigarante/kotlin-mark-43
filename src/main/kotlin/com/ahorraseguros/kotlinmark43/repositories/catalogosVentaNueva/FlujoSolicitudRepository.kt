package com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.FlujoSolicitudModel
import org.springframework.data.jpa.repository.JpaRepository

interface FlujoSolicitudRepository: JpaRepository<FlujoSolicitudModel, Long> {
}