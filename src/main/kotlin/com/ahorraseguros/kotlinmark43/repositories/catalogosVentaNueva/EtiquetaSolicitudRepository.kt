package com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.EtiquetaSolicitudModel
import org.springframework.data.jpa.repository.JpaRepository

interface EtiquetaSolicitudRepository: JpaRepository<EtiquetaSolicitudModel, Long> {
}