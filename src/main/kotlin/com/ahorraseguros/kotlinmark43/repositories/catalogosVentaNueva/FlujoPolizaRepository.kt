package com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.FlujoPolizaModel
import org.springframework.data.jpa.repository.JpaRepository

interface FlujoPolizaRepository: JpaRepository<FlujoPolizaModel, Int> {
}