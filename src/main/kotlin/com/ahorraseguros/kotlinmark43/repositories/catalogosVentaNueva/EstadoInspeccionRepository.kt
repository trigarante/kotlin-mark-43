package com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.EstadoInspeccionModel
import org.springframework.data.jpa.repository.JpaRepository

interface EstadoInspeccionRepository: JpaRepository<EstadoInspeccionModel, Int> {
}