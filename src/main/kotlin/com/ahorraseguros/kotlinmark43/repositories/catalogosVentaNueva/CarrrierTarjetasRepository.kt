package com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.CarrierTarjetasModel
import org.springframework.data.jpa.repository.JpaRepository

interface CarrrierTarjetasRepository: JpaRepository<CarrierTarjetasModel, Int> {
}