package com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.EstadoPolizaModel
import org.springframework.data.jpa.repository.JpaRepository

interface EstadoPolizaRepository: JpaRepository<EstadoPolizaModel, Int> {
}