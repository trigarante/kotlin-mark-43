package com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva.view

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.view.ProductoClienteViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface ProductoClienteViewRepository: JpaRepository<ProductoClienteViewModel, Long> {
}