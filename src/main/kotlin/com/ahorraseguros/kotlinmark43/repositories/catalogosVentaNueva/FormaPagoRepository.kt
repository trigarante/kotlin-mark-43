package com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.FormaPagoModel
import org.springframework.data.jpa.repository.JpaRepository

interface FormaPagoRepository: JpaRepository<FormaPagoModel, Long>
