package com.ahorraseguros.kotlinmark43.repositories.catalogosVentaNueva

import com.ahorraseguros.kotlinmark43.models.catalogosVentaNueva.TarjetasBancoModel
import org.springframework.data.jpa.repository.JpaRepository

interface TarjestasBancoRepository: JpaRepository<TarjetasBancoModel, Int> {
    fun findAllByIdBancoAndIdCarrier(idBanco: Int, idCarrier: Int): MutableList<TarjetasBancoModel>
}