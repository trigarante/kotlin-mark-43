package com.ahorraseguros.kotlinmark43.repositories.ti.views

import com.ahorraseguros.kotlinmark43.models.TI.views.BiometricosSoporteViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface BiometricosViewRepository: JpaRepository<BiometricosSoporteViewModel, Int> {
}