package com.ahorraseguros.kotlinmark43.repositories.ti

import com.ahorraseguros.kotlinmark43.models.TI.LicenciasDisponiblesModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface LicenciasDisponiblesRepository : JpaRepository<LicenciasDisponiblesModel,Int>