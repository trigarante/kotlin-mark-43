package com.ahorraseguros.kotlinmark43.repositories.ti.views

import com.ahorraseguros.kotlinmark43.models.TI.views.TabletsViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface TabletsViewRepository : JpaRepository<TabletsViewModel, Int>