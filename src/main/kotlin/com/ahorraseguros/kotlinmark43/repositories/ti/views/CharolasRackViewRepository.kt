package com.ahorraseguros.kotlinmark43.repositories.ti.views

import com.ahorraseguros.kotlinmark43.models.TI.views.CharolasRackViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface CharolasRackViewRepository: JpaRepository<CharolasRackViewModel, Int> {
}