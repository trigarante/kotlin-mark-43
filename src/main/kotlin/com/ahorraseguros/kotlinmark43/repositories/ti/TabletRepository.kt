package com.ahorraseguros.kotlinmark43.repositories.ti

import com.ahorraseguros.kotlinmark43.models.TI.TabletModels
import org.springframework.data.jpa.repository.JpaRepository

interface TabletRepository: JpaRepository<TabletModels, Long>