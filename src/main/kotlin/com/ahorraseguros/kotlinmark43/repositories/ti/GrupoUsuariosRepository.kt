package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.TI.GrupoUsuariosModels
import org.springframework.data.jpa.repository.JpaRepository

interface GrupoUsuariosRepository : JpaRepository<GrupoUsuariosModels,Long>{
    fun findByNombre(nombre: String): GrupoUsuariosModels
}