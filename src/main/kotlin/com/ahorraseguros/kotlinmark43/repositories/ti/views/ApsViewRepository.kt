package com.ahorraseguros.kotlinmark43.repositories.ti.views

import com.ahorraseguros.kotlinmark43.models.TI.views.ApsViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface ApsViewRepository: JpaRepository<ApsViewModel, Int> {
}