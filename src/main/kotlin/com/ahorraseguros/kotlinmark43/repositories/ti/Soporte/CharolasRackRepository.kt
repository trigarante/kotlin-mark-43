package com.ahorraseguros.kotlinmark43.repositories.ti.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.CharolasRackModel
import org.springframework.data.jpa.repository.JpaRepository

interface CharolasRackRepository: JpaRepository<CharolasRackModel, Long> {
}