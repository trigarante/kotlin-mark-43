package com.ahorraseguros.kotlinmark43.repositories.ti;

import com.ahorraseguros.kotlinmark43.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsuario(String usuario);

}
