package com.ahorraseguros.kotlinmark43.repositories.ti

import com.ahorraseguros.kotlinmark43.models.TI.LaptopsModels
import org.springframework.data.jpa.repository.JpaRepository

interface LaptopRepository: JpaRepository<LaptopsModels, Long>