package com.ahorraseguros.kotlinmark43.repositories.ti.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.DvrModel
import org.springframework.data.jpa.repository.JpaRepository

interface DvrRepository: JpaRepository<DvrModel, Long> {
}