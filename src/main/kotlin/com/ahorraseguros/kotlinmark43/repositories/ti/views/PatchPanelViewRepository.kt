package com.ahorraseguros.kotlinmark43.repositories.ti.views

import com.ahorraseguros.kotlinmark43.models.TI.views.PatchPanelViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface PatchPanelViewRepository: JpaRepository<PatchPanelViewModel, Int> {
}