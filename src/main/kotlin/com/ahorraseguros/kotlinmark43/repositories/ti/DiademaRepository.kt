package com.ahorraseguros.kotlinmark43.repositories.ti

import com.ahorraseguros.kotlinmark43.models.TI.DiademaModels
import org.springframework.data.jpa.repository.JpaRepository

interface DiademaRepository : JpaRepository<DiademaModels, Long>