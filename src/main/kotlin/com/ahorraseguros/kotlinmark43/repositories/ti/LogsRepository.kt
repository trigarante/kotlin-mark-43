package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.logs.LogsModels
import org.springframework.data.jpa.repository.JpaRepository

interface LogsRepository: JpaRepository<LogsModels,Long>