package com.ahorraseguros.kotlinmark43.repositories.ti.views

import com.ahorraseguros.kotlinmark43.models.TI.views.LaptopsViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface LaptopsViewRepository : JpaRepository<LaptopsViewModel, Int>