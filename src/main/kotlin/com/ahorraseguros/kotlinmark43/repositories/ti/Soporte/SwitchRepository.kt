package com.ahorraseguros.kotlinmark43.repositories.ti.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.SwitchsModel
import org.springframework.data.jpa.repository.JpaRepository

interface SwitchRepository: JpaRepository<SwitchsModel, Long> {
}