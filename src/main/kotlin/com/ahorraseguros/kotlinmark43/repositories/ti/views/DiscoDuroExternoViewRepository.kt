package com.ahorraseguros.kotlinmark43.repositories.ti.views

import com.ahorraseguros.kotlinmark43.models.TI.views.DiscoDuroExternoViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface DiscoDuroExternoViewRepository: JpaRepository<DiscoDuroExternoViewModel, Int> {
}