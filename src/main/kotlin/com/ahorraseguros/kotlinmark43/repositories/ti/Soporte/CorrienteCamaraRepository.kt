package com.ahorraseguros.kotlinmark43.repositories.ti.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.CorrienteCamaraModel
import org.springframework.data.jpa.repository.JpaRepository

interface CorrienteCamaraRepository: JpaRepository<CorrienteCamaraModel, Long> {
}