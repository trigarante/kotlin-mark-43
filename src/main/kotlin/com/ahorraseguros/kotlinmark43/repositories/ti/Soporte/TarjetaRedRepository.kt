package com.ahorraseguros.kotlinmark43.repositories.ti.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.TarjetaRedModel
import org.springframework.data.jpa.repository.JpaRepository

interface TarjetaRedRepository: JpaRepository<TarjetaRedModel, Long> {
}