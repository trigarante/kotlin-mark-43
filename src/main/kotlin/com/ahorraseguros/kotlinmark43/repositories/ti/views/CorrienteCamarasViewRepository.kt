package com.ahorraseguros.kotlinmark43.repositories.ti.views

import com.ahorraseguros.kotlinmark43.models.TI.views.CorrienteCamaraViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface CorrienteCamarasViewRepository: JpaRepository<CorrienteCamaraViewModel, Int> {
}