package com.ahorraseguros.kotlinmark43.repositories.ti.views

import com.ahorraseguros.kotlinmark43.models.TI.views.ChromeBitViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface ChromeBitViewRepository: JpaRepository<ChromeBitViewModel, Int> {
}