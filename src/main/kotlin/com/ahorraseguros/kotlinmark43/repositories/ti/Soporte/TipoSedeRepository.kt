package com.ahorraseguros.kotlinmark43.repositories.ti.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.TipoSedeModel
import org.springframework.data.jpa.repository.JpaRepository

interface TipoSedeRepository: JpaRepository<TipoSedeModel, Int> {
}