package com.ahorraseguros.kotlinmark43.repositories.ti

import com.ahorraseguros.kotlinmark43.models.TI.MonitorModels
import org.springframework.data.jpa.repository.JpaRepository

interface MonitorRepository : JpaRepository<MonitorModels, Long>