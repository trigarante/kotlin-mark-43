package com.ahorraseguros.kotlinmark43.repositories.ti.views

import com.ahorraseguros.kotlinmark43.models.TI.views.ServidoresViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface ServidoresViewRepository: JpaRepository<ServidoresViewModel, Int> {
}