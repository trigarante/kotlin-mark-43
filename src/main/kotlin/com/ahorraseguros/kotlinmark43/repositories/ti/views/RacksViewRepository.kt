package com.ahorraseguros.kotlinmark43.repositories.ti.views

import com.ahorraseguros.kotlinmark43.models.TI.views.RacksViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface RacksViewRepository: JpaRepository<RacksViewModel, Int> {
}