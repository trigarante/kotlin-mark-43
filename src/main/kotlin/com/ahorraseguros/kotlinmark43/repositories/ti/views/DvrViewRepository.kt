package com.ahorraseguros.kotlinmark43.repositories.ti.views

import com.ahorraseguros.kotlinmark43.models.TI.views.DvrViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface DvrViewRepository: JpaRepository<DvrViewModel, Int> {
}