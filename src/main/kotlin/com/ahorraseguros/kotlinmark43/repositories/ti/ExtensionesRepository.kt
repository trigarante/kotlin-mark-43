package com.ahorraseguros.kotlinmark43.repositories.ti

import com.ahorraseguros.kotlinmark43.models.TI.ExtensionesModels
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.query.Procedure

interface ExtensionesRepository : JpaRepository<ExtensionesModels, Int> {

    fun findAllByIdSubareaAndIdEstado(idSubarea: Int, idEstado: Int): MutableList<ExtensionesModels>
    @Procedure(name = "extencionesAuto")
    fun extencionesAuto(min: Int, max: Int, Subarea: Int, Estado: Int, Description: String)
}