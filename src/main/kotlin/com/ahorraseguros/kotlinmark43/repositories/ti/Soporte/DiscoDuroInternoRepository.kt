package com.ahorraseguros.kotlinmark43.repositories.ti.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.DiscoDuroInternoModel
import org.springframework.data.jpa.repository.JpaRepository

interface DiscoDuroInternoRepository: JpaRepository<DiscoDuroInternoModel, Long> {
}