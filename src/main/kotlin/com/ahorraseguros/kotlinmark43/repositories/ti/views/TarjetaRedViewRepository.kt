package com.ahorraseguros.kotlinmark43.repositories.ti.views

import com.ahorraseguros.kotlinmark43.models.TI.views.TarjetaRedViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface TarjetaRedViewRepository: JpaRepository<TarjetaRedViewModel, Int> {
}