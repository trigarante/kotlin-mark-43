package com.ahorraseguros.kotlinmark43.repositories.ti

import com.ahorraseguros.kotlinmark43.models.TI.TecladoModels
import org.springframework.data.jpa.repository.JpaRepository

interface TecladoRepository: JpaRepository<TecladoModels, Long>