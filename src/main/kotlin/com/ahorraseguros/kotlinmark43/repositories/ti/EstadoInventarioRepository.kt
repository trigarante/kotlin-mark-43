package com.ahorraseguros.kotlinmark43.repositories.ti

import com.ahorraseguros.kotlinmark43.models.TI.EstadoInventarioModels
import org.springframework.data.jpa.repository.JpaRepository

interface EstadoInventarioRepository: JpaRepository<EstadoInventarioModels,Long>