package com.ahorraseguros.kotlinmark43.repositories.ti.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.ServidoresModel
import org.springframework.data.jpa.repository.JpaRepository

interface ServidoresRpository: JpaRepository <ServidoresModel, Long> {
}