package com.ahorraseguros.kotlinmark43.repositories.ti.views

import com.ahorraseguros.kotlinmark43.models.TI.views.TelefonolpViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface TelefonoIpViewRepository : JpaRepository<TelefonolpViewModel, Int>{
}