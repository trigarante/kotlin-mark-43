package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.TI.UsuariosModels
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.transaction.annotation.Transactional

interface UsuarioRepository :JpaRepository<UsuariosModels,Long>{
    fun findByIdGrupo(idGrupo: Int): MutableList<UsuariosModels>
    @Modifying
    @Query("update usuarios set usuarios.idGrupo = ? where usuarios.idGrupo = ?", nativeQuery = true)
    @Transactional
    fun updateGrupo(newIdGrupo: Int, oldIdGrupo: Int): Int
}