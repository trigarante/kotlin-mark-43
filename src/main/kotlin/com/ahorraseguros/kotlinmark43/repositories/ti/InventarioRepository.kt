package com.ahorraseguros.kotlinmark43.repositories.ti

import com.ahorraseguros.kotlinmark43.models.TI.InventarioModels
import org.springframework.data.jpa.repository.JpaRepository

interface InventarioRepository :JpaRepository<InventarioModels, Long>