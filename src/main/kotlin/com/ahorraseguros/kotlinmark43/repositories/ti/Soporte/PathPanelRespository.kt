package com.ahorraseguros.kotlinmark43.repositories.ti.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.PatchPanelModel
import org.springframework.data.jpa.repository.JpaRepository

interface PathPanelRespository: JpaRepository<PatchPanelModel, Long> {
}