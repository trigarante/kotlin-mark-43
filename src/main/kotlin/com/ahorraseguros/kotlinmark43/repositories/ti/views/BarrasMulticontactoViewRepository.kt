package com.ahorraseguros.kotlinmark43.repositories.ti.views

import com.ahorraseguros.kotlinmark43.models.TI.views.BarrasMulticontactoViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface BarrasMulticontactoViewRepository: JpaRepository<BarrasMulticontactoViewModel, Int> {
}