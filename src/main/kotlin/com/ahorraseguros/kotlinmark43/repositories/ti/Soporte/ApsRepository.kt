package com.ahorraseguros.kotlinmark43.repositories.ti.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.ApsModel
import org.springframework.data.jpa.repository.JpaRepository

interface ApsRepository: JpaRepository<ApsModel, Long> {
}