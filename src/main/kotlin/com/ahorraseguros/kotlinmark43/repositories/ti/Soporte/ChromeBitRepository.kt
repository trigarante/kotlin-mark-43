package com.ahorraseguros.kotlinmark43.repositories.ti.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.ChromeBitModel
import org.springframework.data.jpa.repository.JpaRepository

interface ChromeBitRepository: JpaRepository<ChromeBitModel, Long> {
}