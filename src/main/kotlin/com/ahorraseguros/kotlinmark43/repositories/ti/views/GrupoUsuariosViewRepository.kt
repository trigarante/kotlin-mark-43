package com.ahorraseguros.kotlinmark43.repositories.ti.views

import com.ahorraseguros.kotlinmark43.models.TI.views.GrupoUsuarioViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface GrupoUsuariosViewRepository: JpaRepository<GrupoUsuarioViewModel, Int> {
}