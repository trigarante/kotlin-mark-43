package com.ahorraseguros.kotlinmark43.repositories.ti.views


import com.ahorraseguros.kotlinmark43.models.TI.views.MonitorViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface MonitorViewRepository : JpaRepository<MonitorViewModel, Int>