package com.ahorraseguros.kotlinmark43.repositories.ti.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.CamarasModel
import org.springframework.data.jpa.repository.JpaRepository

interface CamarasRespository : JpaRepository<CamarasModel, Long>{
}