package com.ahorraseguros.kotlinmark43.repositories.ti.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.RacksModel
import org.springframework.data.jpa.repository.JpaRepository

interface RacksRepository: JpaRepository<RacksModel,Long> {
}