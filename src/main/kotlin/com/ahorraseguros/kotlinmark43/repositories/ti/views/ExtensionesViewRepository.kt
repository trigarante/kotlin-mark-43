package com.ahorraseguros.kotlinmark43.repositories.ti.views

import com.ahorraseguros.kotlinmark43.models.TI.views.ExtensionesViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface ExtensionesViewRepository : JpaRepository<ExtensionesViewModel, Long>{
    fun findByIdSubareaAndIdEstado(idSubarea: Int, idEstado: Int): MutableList<ExtensionesViewModel>
}