package com.ahorraseguros.kotlinmark43.repositories.ti.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.BarrasMulticontactoModel
import org.springframework.data.jpa.repository.JpaRepository

interface BarrasMulticontactoRepository: JpaRepository<BarrasMulticontactoModel, Long> {
}