package com.ahorraseguros.kotlinmark43.repositories.ti.views


import com.ahorraseguros.kotlinmark43.models.TI.views.TecladoViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface TecladoViewRepository : JpaRepository<TecladoViewModel, Int>