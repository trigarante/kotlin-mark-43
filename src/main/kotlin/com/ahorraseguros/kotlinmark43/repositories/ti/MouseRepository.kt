package com.ahorraseguros.kotlinmark43.repositories.ti

import com.ahorraseguros.kotlinmark43.models.TI.MouseModels
import org.springframework.data.jpa.repository.JpaRepository

interface MouseRepository : JpaRepository<MouseModels, Long>