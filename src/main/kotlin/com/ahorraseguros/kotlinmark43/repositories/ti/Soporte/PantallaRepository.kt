package com.ahorraseguros.kotlinmark43.repositories.ti.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.PantallasModel
import org.springframework.data.jpa.repository.JpaRepository

interface PantallaRepository: JpaRepository<PantallasModel, Int> {
}