package com.ahorraseguros.kotlinmark43.repositories.ti.views

// com.ahorraseguros.kotlinmark43.models.ti.UsuariosModels
import com.ahorraseguros.kotlinmark43.models.TI.views.UsuarioViewModel
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface UsuarioViewRepository : JpaRepository<UsuarioViewModel, Long> {

    fun findAllByIdSubareaAndAndAsignado(idSubarea: Int, asignado: Int): MutableList<UsuarioViewModel>

    fun findAllByEstadoAndIdSubareaBetween(idestado: Int, menor: Int, mayor: Int): MutableList<UsuarioViewModel>

    fun findAllByAsignado(asignado: Int): MutableList<UsuarioViewModel>

    fun findByUsuario(s: String): Optional<UsuarioViewModel>

//    fun findById2(id: Long): UsuarioViewModel
}