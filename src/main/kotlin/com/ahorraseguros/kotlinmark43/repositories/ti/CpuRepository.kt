package com.ahorraseguros.kotlinmark43.repositories.ti

import com.ahorraseguros.kotlinmark43.models.TI.CpuModel
import org.springframework.data.jpa.repository.JpaRepository

interface CpuRepository: JpaRepository<CpuModel, Long>