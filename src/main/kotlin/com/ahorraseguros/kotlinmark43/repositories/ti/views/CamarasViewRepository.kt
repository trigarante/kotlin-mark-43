package com.ahorraseguros.kotlinmark43.repositories.ti.views

import com.ahorraseguros.kotlinmark43.models.TI.views.CamarasViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface CamarasViewRepository: JpaRepository<CamarasViewModel, Int> {
}