package com.ahorraseguros.kotlinmark43.repositories.ti.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.TelefonoIpModel
import org.springframework.data.jpa.repository.JpaRepository

interface TelefonoIpRepository: JpaRepository<TelefonoIpModel, Long> {
}