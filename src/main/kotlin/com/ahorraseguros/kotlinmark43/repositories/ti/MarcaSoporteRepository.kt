package com.ahorraseguros.kotlinmark43.repositories.ti

import com.ahorraseguros.kotlinmark43.models.TI.MarcaSoporteModel
import org.springframework.data.jpa.repository.JpaRepository


interface MarcaSoporteRepository: JpaRepository <MarcaSoporteModel, Long>
