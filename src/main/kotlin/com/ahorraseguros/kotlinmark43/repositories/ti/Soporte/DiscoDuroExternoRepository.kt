package com.ahorraseguros.kotlinmark43.repositories.ti.Soporte

import com.ahorraseguros.kotlinmark43.models.TI.Soporte.DiscoDuroExternoModel
import org.springframework.data.jpa.repository.JpaRepository

interface DiscoDuroExternoRepository: JpaRepository<DiscoDuroExternoModel, Long> {
}