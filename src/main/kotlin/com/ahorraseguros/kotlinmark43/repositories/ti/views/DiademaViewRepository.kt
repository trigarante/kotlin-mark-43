package com.ahorraseguros.kotlinmark43.repositories.ti.views


import com.ahorraseguros.kotlinmark43.models.TI.views.DiademaViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface DiademaViewRepository : JpaRepository<DiademaViewModel, Int>