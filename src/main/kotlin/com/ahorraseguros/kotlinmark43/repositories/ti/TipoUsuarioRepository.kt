package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.catalogosTI.TipoUsuarioModels
import org.springframework.data.jpa.repository.JpaRepository

interface TipoUsuarioRepository : JpaRepository<TipoUsuarioModels, Long> {
}