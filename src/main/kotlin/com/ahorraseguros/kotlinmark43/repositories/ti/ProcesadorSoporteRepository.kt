package com.ahorraseguros.kotlinmark43.repositories.ti

import com.ahorraseguros.kotlinmark43.models.TI.ProcesadoresSoporteModel
import org.springframework.data.jpa.repository.JpaRepository

interface ProcesadorSoporteRepository: JpaRepository<ProcesadoresSoporteModel, Long>
