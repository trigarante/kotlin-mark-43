package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.rh.HuellaUsuarioModel
import org.springframework.data.jpa.repository.JpaRepository

interface HuellaUsuarioRepository : JpaRepository<HuellaUsuarioModel, Long>