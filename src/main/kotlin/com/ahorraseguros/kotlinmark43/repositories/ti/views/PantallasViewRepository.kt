package com.ahorraseguros.kotlinmark43.repositories.ti.views

import com.ahorraseguros.kotlinmark43.models.TI.views.PantallasViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface PantallasViewRepository: JpaRepository<PantallasViewModel, Int> {
}