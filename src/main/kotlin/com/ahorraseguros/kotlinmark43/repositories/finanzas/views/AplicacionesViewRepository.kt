package com.ahorraseguros.kotlinmark43.repositories.finanzas.views

import com.ahorraseguros.kotlinmark43.models.finanzas.views.AplicacionesViewModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

interface AplicacionesViewRepository: JpaRepository<AplicacionesViewModel, Long> {
    @Query("SELECT * FROM aplicacionesView RV LEFT JOIN autorizacionRegistro AR ON  RV.id = AR.idRegistro where AR.id " +
            "IS NULL AND RV.idEstadoPoliza = 1 ORDER BY RV.id DESC", nativeQuery = true)
    fun registrosSinAutorizacionSinFiltroEmpleado (): MutableList<AplicacionesViewModel?>

    @Query("SELECT * FROM aplicacionesView RV LEFT JOIN `autorizacionRegistro` AR ON  RV.id = AR.idRegistro " +
            "where (AR.id IS NULL AND RV.idEstadoPoliza = 1  AND RV.idEmpleado = :idEmpleado) ORDER BY RV.id DESC;", nativeQuery = true)
    fun registrosSinAutorizacionConFiltroEmpleado (@Param("idEmpleado") idEmpleado: Long): MutableList<AplicacionesViewModel?>
}