package com.ahorraseguros.kotlinmark43.repositories.finanzas

import com.ahorraseguros.kotlinmark43.models.finanzas.GestionIngresosModel
import org.springframework.data.jpa.repository.JpaRepository

interface GestionIngresosRepository: JpaRepository<GestionIngresosModel, Long> {
}