package com.ahorraseguros.kotlinmark43.repositories.finanzas.views

import com.ahorraseguros.kotlinmark43.models.finanzas.views.GestionIngresosViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface GestionIngresosViewRepository: JpaRepository<GestionIngresosViewModel, Long> {
}