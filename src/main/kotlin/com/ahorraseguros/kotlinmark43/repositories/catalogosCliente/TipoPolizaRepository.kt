package com.ahorraseguros.mx.catalogoservice.repositories

import com.ahorraseguros.kotlinmark43.models.catalogosClientes.TipoPolizaModels
import org.springframework.data.jpa.repository.JpaRepository

interface TipoPolizaRepository : JpaRepository<TipoPolizaModels, Long>