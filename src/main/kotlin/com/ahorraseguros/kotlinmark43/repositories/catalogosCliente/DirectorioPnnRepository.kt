package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.catalogosClientes.DirectorioPnnModels
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.query.Procedure

interface DirectorioPnnRepository :JpaRepository<DirectorioPnnModels,Long>{
    @Procedure(name = "validacionPNN")
    fun validacionPNN(numero: String): String?
}