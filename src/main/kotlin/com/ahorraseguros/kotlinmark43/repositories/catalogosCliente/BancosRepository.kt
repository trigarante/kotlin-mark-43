package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.catalogosClientes.BancosModels
import org.springframework.data.jpa.repository.JpaRepository

interface BancosRepository :JpaRepository<BancosModels,Long>
