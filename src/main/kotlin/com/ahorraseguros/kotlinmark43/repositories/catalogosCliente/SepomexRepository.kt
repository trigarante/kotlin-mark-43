package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.catalogosClientes.CpSepomexModels
import org.springframework.data.jpa.repository.JpaRepository

interface SepomexRepository :JpaRepository<CpSepomexModels,Long>{

    fun findAllByCp(cp:Int):MutableList<CpSepomexModels>

}

