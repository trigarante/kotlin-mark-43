package com.ahorraseguros.kotlinmark43.repositories.catalogosMarketing

import com.ahorraseguros.kotlinmark43.models.catalogosMarketing.TipoPaginaModel
import org.springframework.data.jpa.repository.JpaRepository

interface TipoPaginaRepository : JpaRepository<TipoPaginaModel, Long>