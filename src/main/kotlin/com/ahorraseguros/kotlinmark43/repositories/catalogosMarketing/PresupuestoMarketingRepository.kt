package com.ahorraseguros.kotlinmark43.repositories.catalogosMarketing

import com.ahorraseguros.kotlinmark43.models.catalogosMarketing.PresuspuestoMarketingModel
import org.springframework.data.jpa.repository.JpaRepository

interface PresupuestoMarketingRepository: JpaRepository<PresuspuestoMarketingModel, Long> {
}