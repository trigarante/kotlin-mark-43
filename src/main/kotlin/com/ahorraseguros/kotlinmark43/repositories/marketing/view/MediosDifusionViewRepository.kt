package com.ahorraseguros.kotlinmark43.repositories.marketing.view

import com.ahorraseguros.kotlinmark43.models.marketing.view.MediosDifusionViewsModel
import org.springframework.data.jpa.repository.JpaRepository

interface MediosDifusionViewRepository: JpaRepository<MediosDifusionViewsModel, Long> {
}