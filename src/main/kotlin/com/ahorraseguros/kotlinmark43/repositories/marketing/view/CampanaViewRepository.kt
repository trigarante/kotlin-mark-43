package com.ahorraseguros.kotlinmark43.repositories.marketing.view

import com.ahorraseguros.kotlinmark43.models.marketing.view.CampanaViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface CampanaViewRepository: JpaRepository<CampanaViewModel, Long> {
}