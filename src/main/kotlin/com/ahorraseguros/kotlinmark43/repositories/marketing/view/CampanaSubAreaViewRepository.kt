package com.ahorraseguros.kotlinmark43.repositories.marketing.view

import com.ahorraseguros.kotlinmark43.models.marketing.view.CampanaSubareaViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface CampanaSubAreaViewRepository: JpaRepository<CampanaSubareaViewModel, Long> {
}