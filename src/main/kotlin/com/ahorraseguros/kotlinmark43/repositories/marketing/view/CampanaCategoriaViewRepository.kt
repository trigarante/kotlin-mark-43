package com.ahorraseguros.kotlinmark43.repositories.marketing.view

import com.ahorraseguros.kotlinmark43.models.marketing.view.CamapanaCategoriaViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface CampanaCategoriaViewRepository: JpaRepository<CamapanaCategoriaViewModel, Long> {
}