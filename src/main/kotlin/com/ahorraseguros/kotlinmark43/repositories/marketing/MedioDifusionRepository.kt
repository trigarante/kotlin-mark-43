package com.ahorraseguros.kotlinmark43.repositories.marketing

import com.ahorraseguros.kotlinmark43.models.marketing.MedioDifusionModel
import org.springframework.data.jpa.repository.JpaRepository

interface MedioDifusionRepository : JpaRepository<MedioDifusionModel, Long>