package com.ahorraseguros.kotlinmark43.repositories.marketing

import com.ahorraseguros.kotlinmark43.models.marketing.CampanaModel
import org.springframework.data.jpa.repository.JpaRepository

interface CampanaRepository : JpaRepository<CampanaModel, Long>