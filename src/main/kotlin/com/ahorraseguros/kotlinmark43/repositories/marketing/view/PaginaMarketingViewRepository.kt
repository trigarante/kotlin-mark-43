package com.ahorraseguros.kotlinmark43.repositories.marketing.view


import com.ahorraseguros.kotlinmark43.models.marketing.view.PaginaViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface PaginaMarketingViewRepository : JpaRepository<PaginaViewModel, Long>