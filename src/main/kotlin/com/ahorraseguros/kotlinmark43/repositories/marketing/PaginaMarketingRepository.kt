package com.ahorraseguros.kotlinmark43.repositories.marketing

import com.ahorraseguros.kotlinmark43.models.marketing.PaginaModel
import org.springframework.data.jpa.repository.JpaRepository

interface PaginaMarketingRepository : JpaRepository<PaginaModel,Long>