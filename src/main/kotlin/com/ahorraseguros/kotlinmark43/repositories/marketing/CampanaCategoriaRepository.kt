package com.ahorraseguros.kotlinmark43.repositories.marketing

import com.ahorraseguros.kotlinmark43.models.marketing.CampanaCategoriaModel
import org.springframework.data.jpa.repository.JpaRepository

interface CampanaCategoriaRepository : JpaRepository<CampanaCategoriaModel, Long>