package com.ahorraseguros.kotlinmark43.repositories.marketing

import com.ahorraseguros.kotlinmark43.models.marketing.CampanaSubareaModel
import org.springframework.data.jpa.repository.JpaRepository

interface CampanaSubareaRepository: JpaRepository<CampanaSubareaModel, Long> {
}