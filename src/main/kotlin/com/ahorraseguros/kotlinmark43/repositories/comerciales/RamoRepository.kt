package com.ahorraseguros.kotlinmark43.repositories.comerciales

import com.ahorraseguros.kotlinmark43.models.comercial.RamoModels
import org.springframework.data.jpa.repository.JpaRepository

interface RamoRepository :JpaRepository<RamoModels,Long> {
}