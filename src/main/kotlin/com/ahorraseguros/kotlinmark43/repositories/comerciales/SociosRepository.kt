package com.ahorraseguros.kotlinmark43.repositories.comerciales

import com.ahorraseguros.kotlinmark43.models.comercial.SociosModels
import org.springframework.data.jpa.repository.JpaRepository

interface SociosRepository: JpaRepository<SociosModels,Long>
