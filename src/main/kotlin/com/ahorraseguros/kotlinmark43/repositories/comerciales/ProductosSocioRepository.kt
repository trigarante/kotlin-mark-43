package com.ahorraseguros.kotlinmark43.repositories.comerciales

import com.ahorraseguros.kotlinmark43.models.comercial.ProductosSocioModels
import org.springframework.data.jpa.repository.JpaRepository

interface ProductosSocioRepository :JpaRepository<ProductosSocioModels,Long> {
}