package com.ahorraseguros.kotlinmark43.repositories.comerciales.views

import com.ahorraseguros.kotlinmark43.models.comercial.views.ConveniosViewModels
import org.springframework.data.jpa.repository.JpaRepository

interface ConvenioViewRepository: JpaRepository<ConveniosViewModels,Long> {
}