package com.ahorraseguros.kotlinmark43.repositories.comerciales

import com.ahorraseguros.kotlinmark43.models.comercial.SubRamoModels
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface SubRamoRepository: JpaRepository<SubRamoModels, Long> {
}