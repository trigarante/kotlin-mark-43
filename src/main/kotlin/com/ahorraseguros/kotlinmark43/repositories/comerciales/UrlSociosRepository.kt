package com.ahorraseguros.kotlinmark43.repositories.comerciales

import com.ahorraseguros.kotlinmark43.models.comercial.UrlSociosModels
import org.springframework.data.jpa.repository.JpaRepository

interface UrlSociosRepository : JpaRepository<UrlSociosModels,Long> {
}