package com.ahorraseguros.kotlinmark43.repositories.comerciales.views

import com.ahorraseguros.kotlinmark43.models.comercial.views.RamoViewModels
import org.springframework.data.jpa.repository.JpaRepository

interface RamoViewRepository :JpaRepository<RamoViewModels,Long> {
    fun findAllByIdSocioAndActivo(idSocio: Long, activo: Int = 1): MutableList<RamoViewModels>
}