package com.ahorraseguros.kotlinmark43.repositories.comerciales.views

import com.ahorraseguros.kotlinmark43.models.comercial.views.PresupuestoSocioViewModels
import org.springframework.data.jpa.repository.JpaRepository

interface PresupuestoSocioViewRepository :JpaRepository<PresupuestoSocioViewModels,Long> {
}