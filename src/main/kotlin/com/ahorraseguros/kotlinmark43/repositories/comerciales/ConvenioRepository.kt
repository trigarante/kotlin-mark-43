package com.ahorraseguros.kotlinmark43.repositories.comerciales

import com.ahorraseguros.kotlinmark43.models.comercial.ConveniosModels
import org.springframework.data.jpa.repository.JpaRepository

interface ConvenioRepository: JpaRepository<ConveniosModels,Long> {
}