package com.ahorraseguros.kotlinmark43.repositories.comerciales.views

import com.ahorraseguros.kotlinmark43.models.comercial.views.SociosViewModels
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface SociosViewRepository :JpaRepository<SociosViewModels,Long> {
    fun findByAlias(alias:String): Optional<SociosViewModels>

}