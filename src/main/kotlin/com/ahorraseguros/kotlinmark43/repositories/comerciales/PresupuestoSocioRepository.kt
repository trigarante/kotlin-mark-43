package com.ahorraseguros.kotlinmark43.repositories.comerciales

import com.ahorraseguros.kotlinmark43.models.comercial.PresupuestoSocioModels
import org.springframework.data.jpa.repository.JpaRepository

interface PresupuestoSocioRepository :JpaRepository<PresupuestoSocioModels,Long> {
}