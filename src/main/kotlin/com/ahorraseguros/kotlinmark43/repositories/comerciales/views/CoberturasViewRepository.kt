package com.ahorraseguros.kotlinmark43.repositories.comerciales.views

import com.ahorraseguros.kotlinmark43.models.comercial.views.CoberturasViewModels
import org.springframework.data.jpa.repository.JpaRepository

interface CoberturasViewRepository: JpaRepository<CoberturasViewModels,Long>