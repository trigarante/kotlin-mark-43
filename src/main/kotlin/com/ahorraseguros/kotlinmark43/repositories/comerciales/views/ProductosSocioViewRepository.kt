package com.ahorraseguros.kotlinmark43.repositories.comerciales.views

import com.ahorraseguros.kotlinmark43.models.comercial.views.ProductoSocioViewModels
import org.springframework.data.jpa.repository.JpaRepository

interface ProductosSocioViewRepository :JpaRepository<ProductoSocioViewModels,Long> {
    fun findAllByIdSubRamoAndActivo(idSubramo: Long, activo: Int = 1): MutableList<ProductoSocioViewModels>
}