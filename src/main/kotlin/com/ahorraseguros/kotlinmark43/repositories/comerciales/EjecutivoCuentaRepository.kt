package com.ahorraseguros.kotlinmark43.repositories.comerciales

import com.ahorraseguros.kotlinmark43.models.comercial.EjecutivoCuentaModels
import org.springframework.data.jpa.repository.JpaRepository

interface EjecutivoCuentaRepository :JpaRepository<EjecutivoCuentaModels,Long> {
}