package com.ahorraseguros.kotlinmark43.repositories.comerciales.views

import com.ahorraseguros.kotlinmark43.models.comercial.views.EjecutivoCuentaViewModels
import org.springframework.data.jpa.repository.JpaRepository

interface EjecutivoCuentaViewRepository :JpaRepository<EjecutivoCuentaViewModels,Long> {
}