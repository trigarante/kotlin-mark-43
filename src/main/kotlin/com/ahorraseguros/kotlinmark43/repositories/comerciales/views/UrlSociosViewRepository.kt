package com.ahorraseguros.kotlinmark43.repositories.comerciales.views

import com.ahorraseguros.kotlinmark43.models.comercial.views.UrlSociosViewModels
import org.springframework.data.jpa.repository.JpaRepository

interface UrlSociosViewRepository : JpaRepository<UrlSociosViewModels,Long> {
}