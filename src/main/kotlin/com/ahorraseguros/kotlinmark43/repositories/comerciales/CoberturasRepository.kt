package com.ahorraseguros.kotlinmark43.repositories.comerciales

import com.ahorraseguros.kotlinmark43.models.comercial.CoberturasModels
import org.springframework.data.jpa.repository.JpaRepository

interface CoberturasRepository: JpaRepository<CoberturasModels,Long>