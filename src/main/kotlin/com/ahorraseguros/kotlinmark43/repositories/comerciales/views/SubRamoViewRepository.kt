package com.ahorraseguros.kotlinmark43.repositories.comerciales.views

import com.ahorraseguros.kotlinmark43.models.catalogosComercial.TipoSubRamoModels
import com.ahorraseguros.kotlinmark43.models.comercial.views.SubRamoViewModels
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.http.ResponseEntity
import java.util.*

interface SubRamoViewRepository: JpaRepository<SubRamoViewModels, Long> {
    fun findByIdTipoSubRamoAndIdEstadoSocio(idTipoSubRamo: Int, idEstadoSocio: Int): MutableList<SubRamoViewModels>

    fun findByIdTipoSubRamoAndAlias(idTipoSubRamo: Int, alias: String): Optional<SubRamoViewModels>
    fun findAllByIdRamoAndActivo(idRamo: Long, activo: Int = 1): MutableList<SubRamoViewModels>
}