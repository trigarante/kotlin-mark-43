package com.ahorraseguros.kotlinmark43.repositories.ventaNueva

import com.ahorraseguros.kotlinmark43.models.ventaNueva.RecibosModel
import org.springframework.data.jpa.repository.JpaRepository

interface RecibosRepository: JpaRepository<RecibosModel, Long> {
}