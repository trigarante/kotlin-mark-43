package com.ahorraseguros.kotlinmark43.repositories.ventaNueva

import com.ahorraseguros.kotlinmark43.models.ventaNueva.CotizacionesAliModel
import org.springframework.data.jpa.repository.JpaRepository

interface CotizacionesAliRepository: JpaRepository <CotizacionesAliModel, Long> {
}