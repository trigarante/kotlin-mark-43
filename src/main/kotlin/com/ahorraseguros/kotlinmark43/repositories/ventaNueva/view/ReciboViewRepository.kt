package com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view

import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.RecibosViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface ReciboViewRepository: JpaRepository<RecibosViewModel, Long> {
    fun findAllByIdRegistro (idRegistro: Long): MutableList<RecibosViewModel>
}