package com.ahorraseguros.kotlinmark43.repositories.ventaNueva

import com.ahorraseguros.kotlinmark43.models.ventaNueva.SolicitudesModel
import org.springframework.data.jpa.repository.JpaRepository

interface SolicitudesRepository: JpaRepository <SolicitudesModel, Long> {
}