package com.ahorraseguros.kotlinmark43.repositories.ventaNueva

import com.ahorraseguros.kotlinmark43.models.ventaNueva.RegistroModel
import org.springframework.data.jpa.repository.JpaRepository

interface RegistroRepository:JpaRepository<RegistroModel, Long> {
}