package com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view

import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.ConductorHabitualViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface ConductorHabitualViewRepository: JpaRepository<ConductorHabitualViewModel, Long> {
}