package com.ahorraseguros.kotlinmark43.repositories.ventaNueva

import com.ahorraseguros.kotlinmark43.models.ventaNueva.ConductorHabitualModel
import org.springframework.data.jpa.repository.JpaRepository
import javax.swing.JPanel

interface ConductorHabitualRespository: JpaRepository<ConductorHabitualModel, Long> {
}