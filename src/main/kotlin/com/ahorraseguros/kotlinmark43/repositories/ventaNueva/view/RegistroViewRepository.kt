package com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view

import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.RegistroViewModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

interface RegistroViewRepository: JpaRepository<RegistroViewModel, Long> {
    @Query("SELECT AR.id AS id AR.idRegistro AS idRegistro FROM autorizacionRegistro AR, erroresAutorizacion EA " +
            "where EA.idEstadoCorrecion != 1 AND EA.idAutorizacionRegistro = AR.id group by id;", nativeQuery = true)
    fun registrosConErroresAutorizacion  ()

    @Query("select estado from registroView where datos -> '$.numeroSerie' = :noSerie", nativeQuery = true)
    fun findNoSerie(noSerie: String): String?
}