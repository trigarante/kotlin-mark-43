package com.ahorraseguros.kotlinmark43.repositories.ventaNueva

import com.ahorraseguros.kotlinmark43.models.ventaNueva.ProductoClienteModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface ProductoClienteRepository: JpaRepository<ProductoClienteModel, Long>
