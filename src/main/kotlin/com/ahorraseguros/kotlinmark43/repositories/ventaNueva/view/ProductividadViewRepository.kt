package com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view


import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.ProductividadViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface ProductividadViewRepository : JpaRepository<ProductividadViewModel, Long> {
}