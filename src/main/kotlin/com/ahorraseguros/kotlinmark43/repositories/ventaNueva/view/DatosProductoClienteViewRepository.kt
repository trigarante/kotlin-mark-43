package com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view

import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.DatosProductoClienteViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface DatosProductoClienteViewRepository: JpaRepository<DatosProductoClienteViewModel, Long> {
}