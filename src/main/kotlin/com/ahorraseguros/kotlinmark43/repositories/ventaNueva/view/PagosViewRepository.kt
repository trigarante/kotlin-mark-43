package com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view

import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.PagosViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface PagosViewRepository: JpaRepository<PagosViewModel, Long>