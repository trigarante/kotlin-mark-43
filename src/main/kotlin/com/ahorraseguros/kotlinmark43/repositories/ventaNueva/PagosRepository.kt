package com.ahorraseguros.kotlinmark43.repositories.ventaNueva

import com.ahorraseguros.kotlinmark43.models.ventaNueva.PagosModel
import org.springframework.data.jpa.repository.JpaRepository

interface PagosRepository: JpaRepository<PagosModel, Int>
