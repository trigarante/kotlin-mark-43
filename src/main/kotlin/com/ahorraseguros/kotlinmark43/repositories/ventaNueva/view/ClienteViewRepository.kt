package com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view

import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.ClienteViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface ClienteViewRepository: JpaRepository<ClienteViewModel, Long> {
}