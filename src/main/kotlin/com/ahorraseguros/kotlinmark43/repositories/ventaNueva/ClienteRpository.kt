package com.ahorraseguros.kotlinmark43.repositories.ventaNueva

import com.ahorraseguros.kotlinmark43.models.ventaNueva.ClienteModel
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface ClienteRpository: JpaRepository<ClienteModel, Long> {
//    fun findByCurp(curp:String): Optional<ClienteModel>
fun findByCurp(curp:String): ClienteModel?
fun findByrfc(rfc:String): ClienteModel?
}