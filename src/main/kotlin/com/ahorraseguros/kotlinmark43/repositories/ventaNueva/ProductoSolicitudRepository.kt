package com.ahorraseguros.kotlinmark43.repositories.ventaNueva

import com.ahorraseguros.kotlinmark43.models.ventaNueva.ProductoSolicitudModel
import org.springframework.data.jpa.repository.JpaRepository

interface ProductoSolicitudRepository: JpaRepository<ProductoSolicitudModel, Long> {
}