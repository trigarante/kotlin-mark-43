package com.ahorraseguros.kotlinmark43.repositories.ventaNueva

import com.ahorraseguros.kotlinmark43.models.ventaNueva.CotizacionesModel
import org.springframework.data.jpa.repository.JpaRepository

interface CotizacionesRepository: JpaRepository <CotizacionesModel, Long> {
}