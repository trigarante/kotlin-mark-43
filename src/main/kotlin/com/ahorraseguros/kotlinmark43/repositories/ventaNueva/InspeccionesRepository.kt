package com.ahorraseguros.kotlinmark43.repositories.ventaNueva

import com.ahorraseguros.kotlinmark43.models.ventaNueva.InspeccionesModel
import org.springframework.data.jpa.repository.JpaRepository

interface InspeccionesRepository: JpaRepository<InspeccionesModel, Long> {
}