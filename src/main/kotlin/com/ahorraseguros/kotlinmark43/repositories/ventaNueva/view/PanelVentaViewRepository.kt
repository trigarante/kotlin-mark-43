package com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view


import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.PanelVentaViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface PanelVentaViewRepository: JpaRepository<PanelVentaViewModel, Long> {
}