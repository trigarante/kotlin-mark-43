package com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view

import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.DatosProductoSolicitudViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface DatosProductoSolicitudViewRepository: JpaRepository<DatosProductoSolicitudViewModel, Long> {
}