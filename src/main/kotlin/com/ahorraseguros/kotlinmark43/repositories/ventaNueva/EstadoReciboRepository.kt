package com.ahorraseguros.kotlinmark43.repositories.ventaNueva

import com.ahorraseguros.kotlinmark43.models.ventaNueva.EstadoReciboModel
import org.springframework.data.jpa.repository.JpaRepository

interface EstadoReciboRepository: JpaRepository<EstadoReciboModel, Int> {
}