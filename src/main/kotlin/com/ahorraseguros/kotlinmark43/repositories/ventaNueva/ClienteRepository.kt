package com.ahorraseguros.kotlinmark43.repositories.ventaNueva

import com.ahorraseguros.kotlinmark43.models.ventaNueva.ClienteModel
import org.springframework.data.jpa.repository.JpaRepository

interface ClienteRepository: JpaRepository<ClienteModel, Long> {
}