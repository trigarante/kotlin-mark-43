package com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view

import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.StepsCotizadorViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface StepsCotizadorViewRepository: JpaRepository<StepsCotizadorViewModel, Long>