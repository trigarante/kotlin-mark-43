package com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view

import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.SolicitudesViewModelSinJSON
import org.springframework.data.jpa.repository.JpaRepository

interface SolicitudesViewRepositorySinJSON: JpaRepository<SolicitudesViewModelSinJSON, Long> {
    fun findAllByIdEstadoSolicitudAndIdTipoContactoAndIdSubArea(idEstado: Int, idTipoContacto: Int, idSubArea: Int): MutableList<SolicitudesViewModelSinJSON>
    fun findAllByIdTipoContactoAndIdEstadoSolicitudNotAndIdSubArea(idTipoContacto: Int, idSubArea: Int, idEstadoSolicitud: Int = 4): MutableList<SolicitudesViewModelSinJSON>
    fun findAllByIdEstadoSolicitudAndIdTipoContactoAndIdEmpleado(idEstado: Int, idTipoContacto: Int, idEmpleado: Long): MutableList<SolicitudesViewModelSinJSON>
    fun findAllByIdTipoContactoAndIdEstadoSolicitudNotAndIdEmpleado(idTipoContacto: Int, idEmpleado: Long, idEstadoSolicitud: Int = 4): MutableList<SolicitudesViewModelSinJSON>
    fun findAllByIdTipoContactoAndIdEstadoSolicitud(idTipoContacto: Int, idEstadoSolicitud: Int = 4): MutableList<SolicitudesViewModelSinJSON>
}