package com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view

import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.SolicitudesViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface SolicitudesViewRepository: JpaRepository<SolicitudesViewModel, Long> {
    fun findAllByIdEmpleado (idEmpleado: Long?): MutableList<SolicitudesViewModel>
}