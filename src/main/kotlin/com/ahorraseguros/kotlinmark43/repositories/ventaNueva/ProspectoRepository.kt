package com.ahorraseguros.kotlinmark43.repositories.ventaNueva

import com.ahorraseguros.kotlinmark43.models.ventaNueva.ProspectoModel
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface ProspectoRepository: JpaRepository<ProspectoModel, Long> {
    fun findByCorreo(correo:String): Optional<ProspectoModel>
}