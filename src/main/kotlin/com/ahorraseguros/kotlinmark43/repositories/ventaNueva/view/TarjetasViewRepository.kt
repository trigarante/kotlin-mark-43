package com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view

import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.TarjetasViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface TarjetasViewRepository: JpaRepository<TarjetasViewModel, Long>
