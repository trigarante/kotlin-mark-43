package com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view

import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.InspeccionesViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface InspeccionesViewRespository: JpaRepository<InspeccionesViewModel, Long> {
}