package com.ahorraseguros.kotlinmark43.repositories.ventaNueva.view

import com.ahorraseguros.kotlinmark43.models.ventaNueva.view.ProductoSolicitudViewModel
import org.springframework.data.jpa.repository.JpaRepository

interface ProductoSolicitudViewRepository: JpaRepository<ProductoSolicitudViewModel, Long> {
}