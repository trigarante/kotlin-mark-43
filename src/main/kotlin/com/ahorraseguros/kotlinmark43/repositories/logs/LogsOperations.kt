package com.ahorraseguros.kotlinmark43.repositories.logs

import com.ahorraseguros.kotlinmark43.models.logs.LogsModels

interface LogsOperations {
    fun createLogs(idEmpleado:Long,idTipoLog:Int,idTabla:Long): LogsModels
    fun callServerLogs(a:Any):Any
}