package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.rh.EstadoExtencionModels
import org.springframework.data.jpa.repository.JpaRepository

interface EstadoExtensionRepository :JpaRepository<EstadoExtencionModels,Long>