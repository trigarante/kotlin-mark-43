package com.ahorraseguros.kotlinmark43.repositories.catalogosTI

import com.ahorraseguros.kotlinmark43.models.catalogosTI.EquiposAsModel
import org.springframework.data.jpa.repository.JpaRepository

interface EquipoAsRepository : JpaRepository<EquiposAsModel, Long>