package com.ahorraseguros.kotlinmark43.repositories

import com.ahorraseguros.kotlinmark43.models.catalogosSolicitudes.EstadoWsModels
import org.springframework.data.jpa.repository.JpaRepository

interface EstadoWsRepository: JpaRepository<EstadoWsModels,Long>