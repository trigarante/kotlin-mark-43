package com.ahorraseguros.kotlinmark43.repositories.catalogoFinanzas

import com.ahorraseguros.kotlinmark43.models.catalogosFinanzas.TipoIngresoModel
import org.springframework.data.jpa.repository.JpaRepository

interface TipoIngresoRepository: JpaRepository<TipoIngresoModel, Int> {
}