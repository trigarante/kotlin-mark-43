package com.ahorraseguros.kotlinmark43.repositories.examen

import com.ahorraseguros.kotlinmark43.models.examen.ProductosExamenModel
import org.springframework.data.jpa.repository.JpaRepository

interface ProductosExamenRepository: JpaRepository<ProductosExamenModel, Long> {
}