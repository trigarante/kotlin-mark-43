package com.ahorraseguros.kotlinmark43.repositories.examen

import com.ahorraseguros.kotlinmark43.models.examen.ExamenModel
import org.springframework.data.jpa.repository.JpaRepository

interface ExamenRepository: JpaRepository<ExamenModel, Int> {
}