package com.ahorraseguros.kotlinmark43.proxies;

import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "kotlin-logs-mark-43",url = "")
public interface LogsServiceProxy {
}
