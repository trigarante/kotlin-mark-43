package com.ahorraseguros.kotlinmark43

import com.ahorraseguros.kotlinmark43.config.AppProperties
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.cloud.openfeign.EnableFeignClients

@SpringBootApplication
//@EnableFeignClients("com.ahorraseguros.kotlinmark43")
@EnableConfigurationProperties(AppProperties::class)
class KotlinMark43Application

fun main(args: Array<String>) {
    runApplication<KotlinMark43Application>(*args)
}
